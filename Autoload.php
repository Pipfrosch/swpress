<?php
declare(strict_types=1);

/**
 * Autoload class for SW Press.
 *
 * @package SWPress
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace SWP;

/**
 * Provides autoload functions for PSR-4, PSR-0, and PEAR classes
 */
class Autoload
{
    /**
     * @var array Array of allowed class file suffixes
     */
    const SUFFIXARRAY = array('.php', '.class.php', '.inc.php');

    /**
     * Looks for a class and loads it if found.
     *
     * For classes with just a vendor namespace and no product namespace,
     * require they be installed as Vendor/ClassName/ClassName.suffix.
     *
     * @param string $class The class to be loaded.
     *
     * @return void
     */
    public static function loadClass(string $class)
    {
        $arr = explode("\\", $class);
        $j = count($arr);
        if ($j < 3) {
            if ($j === 2) {
                $arr[] = $arr[1];
            } else {
                return;
            }
        }
        $pathNoSuffix = SWP_PSR4 . '/' . implode('/', $arr);
        foreach (self::SUFFIXARRAY as $suffix) {
            $path = $pathNoSuffix . $suffix;
            if (file_exists($path)) {
                require_once($path);
                return;
            }
        }
    }//end loadClass

    /**
     * Looks for a PSR-0 class and loads it if found
     *
     * @param string $class The class to be loaded.
     *
     * @return void
     */
    public static function loadPSR0Class(string $class)
    {
        if (strpos($class, '.') === false) {
            $file = str_replace('_', '/', $class).'.php';
            $path = SWP_PSR0 . '/' . $file;
            if (file_exists($path)) {
                require_once($path);
            }
        }
    }//end loadPSR0Class()

    /**
     * Attempts to load a PEAR module.
     * Not yet tested
     *
     * @param string $class The class to be loaded.
     *
     * @return void
     */
    public static function loadPear(string $class)
    {
        if (strpos($class, '.') === false) {
            $file = str_replace('_', '/', $class).'.php';
            if ($path = stream_resolve_include_path($file)) {
                require_once($path);
                return;
            }
            if (! defined('SWP_PEAR_PATH')) {
                return;
            }
            $path = SWP_PEAR_PATH . '/' . $file;
            if (file_exists($path)) {
                require_once($path);
            }
        }
    }//end loadPear()
}//end Autoload

?>
