<?php

/**
 * WordPress functions as static methods
 *
 * TODO (besides adding function)
 *  - Rewrite date_i18n() function
 *
 * @package SWPress
 * @author  WordPress Developers <wp-hackers@lists.automattic.com>
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/GPL-2.0 GPLv2
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

// note to self -
// https://stackoverflow.com/questions/13649197/how-can-i-use-phpdoc-to-type-hint-the-parameters-of-a-callable

/**
 * This class contains static functions from the wp-includes/functions.php and wp-includes/load.php scrips.
 *  Most of them have minor tweaks, but some have been rewritten.
 */
class SWP
{
    // define these constants here
    
    /**
     * Hour in seconds.
     *
     * @var int
     */
    protected static $hourInSeconds = 3600;
    
    /**
     * Array of HTTP status codes.
     *
     * @var array
     */
    protected static $headerCodesToDescription = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        103 => 'Early Hints',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        226 => 'IM Used',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Reserved',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        421 => 'Misdirected Request',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        451 => 'Unavailable For Legal Reasons',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        510 => 'Not Extended',
        511 => 'Network Authentication Required'
    );

    /**
     * Default allowed Protocol List - from wp-includes/functions.inc
     *
     * @var array
     */
    protected static $allowedProtocolList = array(
        'http',
        'https',
        'ftp',
        'ftps',
        'mailto',
        'news',
        'irc',
        'gopher',
        'nntp',
        'feed',
        'telnet',
        'mms',
        'rtsp',
        'svn',
        'tel',
        'fax',
        'xmpp',
        'webcal',
        'urn',
    );
    
    /**
     * Wrapper for translation function
     *
     * __NO UNIT TEST FOR THIS METHOD__
     *
     * @author Alice Wonder <paypal@domblogger.net>
     *
     * @param string $string The string to translate.
     *
     * @return string The translated string if translation exists, otherwise the input string.
     */
    public static function translateString($string): string
    {
        if (function_exists('__')) {
            return __($string);
        }
        return $string;
    }//end translateString()

    /** Try to keep methods in alphabetical order after this point */

    /**
     * Convert a value to non-negative integer.
     *
     * @since 2.5.0
     *
     * @param mixed $maybeint Data you wish to have converted to a non-negative integer.
     *
     * @return int A non-negative integer.
     */
    public static function absint($maybeint): int
    {
        return abs(intval($maybeint, 10));
    }//end absint()

    /**
     * Retrieves a modified URL query string.
     *
     * Complete rewrite of add_query_arg() in WP wp-includes/function.php to use the
     * \Pipfrosch\NetCom set of classes. A side effect, the result always has URI escaping.
     *
     * @author Alice Wonder <paypal@domblogger.net>
     *
     * @param array     $arr  An array of KEY=>VALUE parameters.
     * @param string    $url  An HTTP or HTTPS URL to act upon.
     * @param bool|null $idna Optional. If true, internationalized domain names are supported. If false,
     *                        international domain names are converted to xn-- ASCII. If null and the
     *                        boolean constant SWP_IDNA_HOSTANAMES is defined, that is used. Otherwise
     *                        it will be set to false. Default value is null.
     *
     * @return string New URL query string (URI escaped with + for spaces in parameters).
     */
    public static function addQueryArg(array $arr, string $url, $idna = null): string
    {
        if (! is_bool($idna)) {
            if (defined('SWP_IDNA_HOSTANAMES') && is_bool(SWP_IDNA_HOSTANAMES)) {
                $idna = SWP_IDNA_HOSTANAMES;
            } else {
                $idna = false;
            }
        }
        $URI = new \Pipfrosch\NetCom\URI();
        $URI->setUnicode($idna);
        $QB = new \Pipfrosch\NetCom\GetQueryBuilder(true);
        $QB->setMode('ARRAY');
        $URI->parseURI($url);
        $scheme = $URI->getScheme();
        if (! in_array($scheme, array('http', 'https'))) {
            // FIXME throw InvalidArgumentError
            return '';
        }
        $IQ = $URI->getQuery();
        $tmp = explode('&', $IQ);
        foreach ($tmp as $set) {
            $tb = explode('=', $set);
            $QB->addKeyVal($tb[0], $tb[1]);
        }
        foreach ($arr as $key=>$value) {
            if ($value === false) {
                $QB->removeKey($key);
            } else {
                $QB->addKeyVal($key, $value);
            }
        }
        $URI->setGetQuery($QB);
        return $URI->__toString();
    }//end addQueryArg()

    
  
    /**
     * Retrieve the current time based on specified type.
     *
     * This is the `current_time()` function in wp-includes/functions.php
     *
     * The 'mysql' type will return the time in the format for MySQL DATETIME field.
     * The 'timestamp' type will return the current timestamp.
     * Other strings will be interpreted as PHP date formats (e.g. 'Y-m-d').
     *
     * If $gmt is set to either '1' or 'true', then both types will use GMT time.
     * if $gmt is false, the output is adjusted with the GMT offset in the WordPress option.
     *
     * @since 1.0.0
     * @since 5.1.1swp - $gmt boolean only, added $offset parameter
     *
     * @param string   $type   Type of time to retrieve. Accepts 'mysql', 'timestamp', or PHP date
     *                         format string (e.g. 'Y-m-d').
     * @param bool     $gmt    Optional. Whether to use GMT timezone. Default false.
     * @param int      $offset Optional. Seconds to use for offset.
     *
     * @return int|string Integer if $type is 'timestamp', string otherwise.
     */
    public static function currentTime(string $type, bool $gmt = false, $offset = 0)
    {
        if (($offset === 0) && ($gmt === false)) {
            $offset = intval((\SWP_Option::get_option('gmt_offset') * self::$hourInSeconds ), 10);
        }
        switch ($type) {
            case 'mysql':
                return gmdate('Y-m-d H:i:s', ( time() + $offset ));
            case 'timestamp':
                return time();
                // I believe below is a WordPress bug - timestamp is seconds from epoch and
                //  epoch is defined as 1970-01-01 00:00:00 GMT
                //  regardless of the timezone offset
                //return ( $gmt ) ? time() : time() + ( get_option('gmt_offset') * self::$hourInSeconds );
            default:
                return gmdate($type, time() + $offset);
        }
    }//end currentTime()

    /**
     * Return a comma-separated string of functions that have been called to get to the current point in code.
     *
     * __NO UNIT TEST FOR THIS METHOD__
     *
     * This is wp_debug_backtrace_summary in WordPress wp-includes/functions.php
     *
     * @see https://core.trac.wordpress.org/ticket/19589
     *
     * @param null|string $ignoreClass Optional. A class to ignore all function calls within - useful
     *                                 when you want to just give info about the callee. Default null.
     * @param int    $skipFrames       Optional. A number of stack frames to skip - useful for unwinding
     *                                 back to the source of the issue. Default 0.
     * @param bool   $pretty           Optional. Whether or not you want a comma separated string or raw
     *                                 array returned. Default true.
     *
     * @return string|array Either a string containing a reversed comma separated trace or an array
     *                      of individual calls.
     */
    public static function debugBacktraceSummary($ignoreClass = null, int $skipFrames = 0, bool $pretty = true)
    {
        $trace = debug_backtrace(false);
        $caller = array();
        // FIXME FUCKING FIXME - write easier to understand code for this line
        $checkClass = ! is_null($ignoreClass);
        
        $truncatePaths = array();
        if (defined('WP_CONTENT_DIR') && defined('ABSPATH')) {
            $truncatePaths[] = self::normalizePath(WP_CONTENT_DIR);
            $truncatePaths[] = self::normalizePath(ABSPATH);
        }
        foreach ($trace as $call) {
            if ($skipFrames > 0) {
                $skipFrames--;
            } elseif (isset($call['class'])) {
                if ($checkClass && $ignoreClass === $call['class']) {
                    continue; // Filter out calls
                }
                $caller[] = "{$call['class']}{$call['type']}{$call['function']}";
            } else {
                // FIXME FUCKME FIXME This may need fixing for function names
                if (in_array(
                    $call['function'],
                    array(
                        'do_action',
                        'apply_filters',
                        'do_action_ref_array',
                        'apply_filters_ref_array'
                    )
                )) {
                    $caller[] = "{$call['function']}('{$call['args'][0]}')";
                } elseif (in_array(
                    $call['function'],
                    array(
                        'include',
                        'include_once',
                        'require',
                        'require_once'
                    )
                )) {
                    $filename = isset($call['args'][0]) ? $call['args'][0] : '';
                    $caller[] = $call['function'] . "('" . str_replace($truncatePaths, '', self::normalizePath($filename)) . "')";
                } else {
                    $caller[] = $call['function'];
                }
            }
        }
        if ($pretty) {
            return join(', ', array_reverse($caller));
        }
        return $caller;
    }//end debugBacktraceSummary()

    /**
     * Mark a function as deprecated.
     *
     * __NO UNIT TEST FOR THIS METHOD__
     *
     * This is _deprecated_function in WordPress wp-includes/functions.php
     *
     * @psalm-suppress UndefinedFunction
     *
     * @param string      $function    The function that was called.
     * @param string      $version     The version of WordPress that deprecated the function.
     * @param null|string $replacement Optional. The function that should have been called. Default null.
     *
     * @return void
     */
    public static function deprecatedFunction(string $function, string $version, $replacement = null): void
    {
        do_action('deprecated_function_run', $function, $replacement, $version);
        $debug = false;
        if (defined('WP_DEBUG') && WP_DEBUG) {
            $debug = true;
        }
        if ($debug && apply_filters('deprecated_function_trigger_error', true)) {
            if (! is_null($replacement)) {
                $errorMessage = self::translateString('%1$s is <strong>deprecated</strong> since version %2$s! Use %3$s instead.');
                trigger_error(sprintf($errorMessage, $function, $version, $replacement));
            } else {
                $errorMessage = self::translateString('%1$s is <strong>deprecated</strong> since version %2$s with no alternative available.');
                trigger_error(sprintf($errorMessage, $function, $version));
            }
        }
    }//end deprecatedFunction()


    /**
     * Mark something as being incorrectly called.
     *
     * __NO UNIT TEST FOR THIS METHOD__
     *
     * There is a hook {@see 'doing_it_wrong_run'} that will be called that can be used
     * to get the backtrace up to what file and function called the deprecated
     * function.
     *
     * The current behavior is to trigger a user error if `WP_DEBUG` is true.
     *
     * This is the _doing_it_wrong function.
     *
     * @since  3.1.0
     * @access private
     *
     * @psalm-suppress UndefinedFunction
     *
     * @param string $function The function that was called.
     * @param string $message  A message explaining what has been done incorrectly.
     * @param string $version  The version of WordPress where the message was added.
     *
     * @return void
     */
    public static function doing_it_wrong($function, $message, $version)
    {
        do_action('doing_it_wrong_run', $function, $message, $version);
      
        /**
         * Filters whether to trigger an error for _doing_it_wrong() calls.
         *
         * @since 3.1.0
         * @since 5.1.0 Added the $function, $message and $version parameters.
         *
         * @psalm-suppress UndefinedFunction
         *
         * @param bool   $trigger  Whether to trigger the error for _doing_it_wrong() calls. Default true.
         * @param string $function The function that was called.
         * @param string $message  A message explaining what has been done incorrectly.
         * @param string $version  The version of WordPress where the message was added.
         */
        if (WP_DEBUG && apply_filters('doing_it_wrong_trigger_error', true, $function, $message, $version)) {
            if (function_exists('__')) {
                if (! empty($version)) {
                    /* translators: %s: version number */
                    $version = sprintf(__('(This message was added in version %s.)'), $version);
                }
                /* translators: %s: Codex URL */
                $message .= ' ' . sprintf(
                    __('Please see <a href="%s">Debugging in WordPress</a> for more information.'),
                    __('https://codex.wordpress.org/Debugging_in_WordPress')
                );
                /**
                 * Translators: Developer debugging message. 1: PHP function name,
                 *                                           2: Explanatory message,
                 *                                           3: Version information message
                 */
                trigger_error(
                    sprintf(
                        __('%1$s was called <strong>incorrectly</strong>. %2$s %3$s'),
                        $function,
                        $message,
                        $version
                    )
                );
            } else {
                if (! empty($version)) {
                    $version = sprintf('(This message was added in version %s.)', $version);
                }
                $message .= sprintf(
                    ' Please see <a href="%s">Debugging in WordPress</a> for more information.',
                    'https://codex.wordpress.org/Debugging_in_WordPress'
                );
                trigger_error(
                    sprintf(
                        '%1$s was called <strong>incorrectly</strong>. %2$s %3$s',
                        $function,
                        $message,
                        $version
                    )
                );
            }
        }
    }//end _doing_it_wrong()

    /**
     * Retrieve the HTTP header code array. Not from WordPress but used to set a global
     *  in WordPress. May not be needed.
     *
     * @author Alice Wonder <paypal@domblogger.net>
     *
     * @return array
     */
    public static function get_http_header_to_desc(): array
    {
        return self::$headerCodesToDescription;
    }//end get_http_header_to_desc()

    /**
     * Retrieve list of allowed protocols
     *
     * This is `wp_allowed_protocols()` in WP wp-includes/functions.php
     *
     * Rewritten
     *
     * @author Alice Wonder <paypal@domblogger.net>
     *
     * @return array
     */
    public static function getAllowedProtocols(): array
    {
        global $SWPcache;
        if (! isset($SWPcache)) {
            $SWPcache = new \SWPress\Core\CacheWrapper();
        }
        $key = 'kses-protocols';
        $return = $SWPcache->get($key);
        if (is_array($return)) {
            return $return;
        }
        $protocols = self::$allowedProtocolList;
        if(function_exists('apply_filters')) {
            $protocols = array_unique((array) apply_filters('kses_allowed_protocols', $protocols));
        }
        $SWPcache->set($key, $protocols, 14400); //cache for four hours
        return $protocols;
    }//end getAllowedProtocols()


    /**
     * Retrieve the description for the HTTP status.
     *
     * This is `get_status_header_desc()` from WP wp-includes/functions.php
     *
     * @since 2.3.0
     * @since 3.9.0 Added status codes 418, 428, 429, 431, and 511.
     * @since 4.5.0 Added status codes 308, 421, and 451.
     * @since 5.1.0 Added status code 103.
     * @since 5.1.1swp Removed global, done static now.
     *
     * @param int|null $code HTTP status code.
     *
     * @return string Empty string if not found, or description if found.
     */
    public static function getStatusHeaderDesc($code): string
    {
        $code = self::absint($code);
        if ((! empty($code)) && isset(self::$headerCodesToDescription[$code])) {
            return self::$headerCodesToDescription[$code];
        }
        return '';
    }//end getStatusHeaderDesc()


    /**
     * From php.net (modified by Mark Jaquith to behave like the native PHP5 function).
     *
     * @since 3.2.0
     * formerly [@]access private
     *
     * __NO UNIT TEST FOR THIS METHOD__ but plan on nuking
     *
     * This is _http_build_query from WP 5.1.1 wp-includes/functions.inc
     *
     * @see https://secure.php.net/manual/en/function.http-build-query.php
     *
     * @param array|object  $data       An array or object of data. Converted to array.
     * @param string|null   $prefix     Optional. Numeric index. If set, start parameter numbering with it.
     *                                  Default null.
     * @param string|null   $sep        Optional. Argument separator; defaults to 'arg_separator.output'.
     *                                  Default null.
     * @param string        $key        Optional. Used to prefix key name. Default empty.
     * @param bool          $urlencode  Optional. Whether to use urlencode() in the result. Default true.
     *
     * @return string The query string.
     */
    public static function http_build_query($data, $prefix = null, $sep = null, string $key = '', $urlencode = true)
    {
        $ret = array();

        foreach ((array) $data as $k => $v) {
            if ($urlencode) {
                    $k = urlencode($k);
            }
            if (is_int($k) && $prefix != null) {
                    $k = $prefix . $k;
            }
            if (! empty($key)) {
                    $k = $key . '%5B' . $k . '%5D';
            }
            if ($v === null) {
                    continue;
            } elseif ($v === false) {
                    $v = '0';
            }

            if (is_array($v) || is_object($v)) {
                    array_push($ret, self::http_build_query($v, '', $sep, $k, $urlencode));
            } elseif ($urlencode) {
                    array_push($ret, $k . '=' . urlencode($v));
            } else {
                    array_push($ret, $k . '=' . $v);
            }
        }

        if (null === $sep) {
                $sep = ini_get('arg_separator.output');
        }

        return implode($sep, $ret);
    }//end http_build_query()

    /**
     * A replacement for WordPress `_http_build_query()` function. There are some differences. Keys that require
     * uri encoding are not considered valid, if your code uses such keys you are doing it wrong. For the value,
     * the urlencode option is ignored, it is __always__ done. This function exists for building a URI query where
     * URI encoding is mandatory. However it is done intelligently so that double encoding does not happen.
     *
     * This function does not deal with multi-level iterables. Sorry but if you need to transmit multi-level, use
     * JSON with post, it does not belong in a URI query string. The way the WordPress function attempts to
     * serialize multi-level arrays is hackish and does not conform to any standard, there isn't a standard for
     * how to serialize multi-level arrays with GET. There is however JSON and XML, both of which work really well
     * over POST.
     *
     * Almost nothing in WordPress core uses this function.
     *
     * @param iterable    $data      The key => value pairs.
     * @param string|null $prefix    Optional. The prefix to use when the index is numeric. Defaults to null.
     * @param string|null $sep       Optional. The separator to use between query pairs if for some insane reason
     *                                 you want to break things by using something other than am ampersand.
     * @param string      $key       Optional. Used to prefix a key name. Defaults to empty string.
     * @param bool        $urlencode Optional. Whether or not to apply URI encoding. Defaults to true.
     * @return string
     */
    public static function httpBuildQuery(iterable $data, $prefix = null, $sep = null, string $key = '', $urlencode = true)
    {
        $QB = new \Pipfrosch\NetCom\GetQueryBuilder(true);
        $QB->setMode('ARRAY');
        if (! is_null($sep) && $sep !== '&') {
            $QB->setSeparator($sep);
        }
        foreach ((array) $data as $k => $v) {
            if (is_int($k) && is_string($prefix)) {
                $k = $prefix . (string) $k;
            }
            if (! empty($key)) {
                $k = $key . '[' . $k . ']';
            }
            // boolean false casts to empty string, not '0' - but does cast to integer 0.
            if (is_bool($v)) {
                $v = (int) $v;
            }
            if (is_int($v)) {
                $v = (string) $v;
            }
            if (is_string($v)) {
                $QB->addKeyVal($k, $v);
            }
        }
        
        return $QB->__toString();
    }

    /**
     * Are we multisite?
     *
     * __NO UNIT TEST FOR THIS METHOD__
     *
     * The is is_multisite in wp-includes/load.php
     *
     * @return bool True if multisite, otherwise false.
     */
    public static function isMultisite(): bool
    {
        if (defined('MULTISITE') && is_bool(MULTISITE)) {
            return MULTISITE;
        }
        if (defined('SUBDOMAIN_INSTALL')
        || defined('VHOST')
        || defined('SUNRISE')) {
            return true;
        }
        return false;
    }//end isMultisite()


    /**
     * Test if path is a stream URL
     *
     * This is wp_is_stream in WordPress wp-includes/functions.php
     *
     * @param string $path The resource path URL.
     *
     * @return bool True if stream URL, otherwise false.
     */
    public static function isStream($path)
    {
        $pos = strpos($path, '://');
        if (is_int($pos)) {
            $stream = substr($path, 0, $pos);
            $wrappers = stream_get_wrappers();
            if (in_array($stream, $wrappers)) {
                return true;
            }
        }
        return false;
    }//end isStream()

    /**
     * Set the mbstring internal encoding to a binary safe encoding when func_overload
     * is enabled.
     *
     * __NO UNIT TEST FOR THIS METHOD__
     *
     * When mbstring.func_overload is in use for multi-byte encodings, the results from
     * strlen() and similar functions respect the utf8 characters, causing binary data
     * to return incorrect lengths.
     *
     * This function overrides the mbstring encoding to a binary-safe encoding, and
     * resets it to the users expected encoding afterwards through the
     * `reset_mbstring_encoding` function.
     *
     * It is safe to recursively call this function, however each
     * `mbstring_binary_safe_encoding()` call must be followed up with an equal number
     * of `reset_mbstring_encoding()` calls.
     *
     * @since 3.7.0
     *
     * @see reset_mbstring_encoding()
     *
     * @staticvar array $encodings
     * @staticvar bool  $overloaded
     *
     * @param bool $reset Optional. Whether to reset the encoding back to a previously-set encoding.
     *                    Default false.
     *
     * @return void
     */
    public static function mbstring_binary_safe_encoding(bool $reset = false) {
        // FIXME FUCKME FIXME - overloading is deprecated in PHP 7.2 and shouldn't be used.
        $encodings  = array();
        $overloaded = false;
        if (function_exists( 'mb_internal_encoding' )) {
            $string = ini_get('mbstring.func_overload');
            if (is_numeric($string)) {
               $n = intval($string, 10);
               if ($n === 2) {
                   $overloaded = true;
               }
            }
        }

        if ($overloaded === false) {
            return;
        }

        if (! $reset) {
            $encoding = mb_internal_encoding();
            array_push($encodings, $encoding);
            mb_internal_encoding('ISO-8859-1');
        }

        if ($reset && $encodings) {
            $encoding = array_pop($encodings);
            if(is_string($encoding)) {
                mb_internal_encoding($encoding);
            }
        }
    }

    /**
     * Convert given date string into a different format.
     *
     * $format should be either a PHP date format string, e.g. 'U' for a Unix
     * timestamp, or 'G' for a Unix timestamp assuming that $date is GMT.
     *
     * If $translate is true then the given date and format string will
     * be passed to date_i18n() for translation.
     *
     * @since 0.71
     *
     * @param string $format    Format of the date to return.
     * @param string $date      Date string to convert.
     * @param bool   $translate Whether the return date should be translated. Default true.
     *
     * @return string|int|bool Formatted date string or Unix timestamp. False if $date is empty.
     */
    public static function mysql2date(string $format, string $date, bool $translate = true)
    {
        if (empty($date)) {
            return false;
        }
        if ('G' == $format) {
                return strtotime($date . ' +0000');
        }
        $i = strtotime($date);
        if ('U' == $format) {
                return $i;
        }
        if ($translate && function_exists('date_i18n')) {
                return date_i18n($format, $i);
        } else {
              return date($format, $i);
        }
    }//end mysql2date()
    
    /**
     * Parses and formats a MySQL datetime (Y-m-d H:i:s) for ISO8601 (Y-m-d\TH:i:s).
     *
     * __NO UNIT TEST FOR THIS METHOD__
     *
     * Explicitly strips timezones, as datetimes are not saved with any timezone
     * information. Including any information on the offset could be misleading.
     *
     * Despite historical function name, the output does not conform to RFC3339 format,
     * which must contain timezone.
     *
     * @since 4.4.0
     *
     * @param string $date_string Date string to parse and format.
     *
     * @return string|false Date formatted for ISO8601 without time zone.
     */
    public static function mysql_to_rfc3339(string $date_string)
    {
        $return = self::mysql2date('Y-m-d\TH:i:s', $date_string, false);
        if (is_string($return)) {
            return $return;
        }
        return false;
    }//end mysql_to_rfc3339()

    /**
     * Normalize a filesystem path.
     *
     * On Windows, replaces backslashes with forward slashes and forced upper-case drive letters.
     * Allows for two leading slashes for Windows network shares.
     *
     * This is wp_normalize_path in WordPress wp-includes/functions.php
     *
     * @param string $path The path to normalize.
     *
     * @return string The normalized path.
     */
    public static function normalizePath(string $path): string
    {
        $wrapper = '';
        if (self::isStream($path)) {
            list ($wrapper, $path) = explode('://', $path, 2);
        }
        // Standardise all paths to use /
        $path = str_replace('\\', '/', $path);
        // Replace multiple slashes down to a singular, allowing for network shares having two slashes.
        $path = preg_replace('|(?<=.)/+|', '/', $path);
        if (substr($path, 1, 1) === ':') {
            $path = ucfirst($path);
        }
        return $wrapper . $path;
    }//end normalizePath()

    /**
     * Removes one or more query args from a URL
     *
     * Complete rewrite of remove_query_arg() in WP wp-includes/function.php
     *
     * @param string|array $arg  The query arg, or an array of query args to remove.
     * @param string       $url  The URL to act upon.
     * @param bool         $idna Optional. If true, internationalized domain names are supported. If false,
     *                           international domain names are converted to xn-- ASCII. If null and the
     *                           boolean constant SWP_IDNA_HOSTANAMES is defined, that is used. Otherwise
     *                           it will be set to false. Default value is null.
     *
     * @return string New URL query string (URI escaped with + for spaces in parameters).
     */
    public static function removeQueryArg($arg, string $url, $idna = null): string
    {
        $removeArr = array();
        if (! is_array($arg)) {
            $removeArr[$arg] = false;
        } else {
            foreach($arg as $key) {
                $removeArr[$key] = false;
            }
        }
        return self::addQueryArg($removeArr, $url, $idna);
    }//end removeQueryArg()

    /**
     * Use RegEx to extract URLs from arbitrary content.
     *
     * __NO UNIT TEST FOR THIS METHOD__
     *
     * @since 3.7.0
     *
     * @param string $content Content to extract URLs from.
     *
     * @return array URLs found in passed string.
     */
    public static function swp_extract_urls(string $content): array
    {
        preg_match_all(
            "#([\"']?)("
                        . '(?:([\w-]+:)?//?)'
                        . '[^\s()<>]+'
                        . '[.]'
                        . '(?:'
                                . '\([\w\d]+\)|'
                                . '(?:'
                                        . "[^`!()\[\]{};:'\".,<>«»“”‘’\s]|"
                                        . '(?:[:]\d+)?/?'
                                . ')+'
                        . ')'
                . ")\\1#",
            $content,
            $post_links
        );

        $post_links = array_unique(array_map('html_entity_decode', $post_links[2]));

        return array_values($post_links);
    }//end swp_extract_urls()



    /**
     * Merge user defined arguments into defaults array.
     *
     * __NO UNIT TEST FOR THIS METHOD__
     *
     * from historic wp-includes/functions.inc as wp_parse_args
     *
     * This function is used throughout WordPress to allow for both string or array
     * to be merged into another array.
     *
     * @since 2.2.0
     * @since 2.3.0 `$args` can now also be an object.
     *
     * @param string|array|object $args     Value to merge with $defaults.
     * @param array               $defaults Optional. Array that serves as the defaults. Default empty.
     *
     * @return array Merged user defined values with defaults.
     */
    public static function swp_parse_args($args, $defaults = array())
    {
        if (is_object($args)) {
            $r = get_object_vars($args);
        } elseif (is_array($args)) {
                $r =& $args;
        } else {
                $r = array();
                \SWP_Formatting::swp_parse_str($args, $r);
        }
        if (count($defaults) > 0) {
                return array_merge($defaults, $r);
        }
        return $r;
    }//end parse_args()
}//end class
