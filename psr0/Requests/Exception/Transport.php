<?php
declare(strict_types=1);

/**
 * Exception class for Transport
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Extends Requests_Exception
 */
class Requests_Exception_Transport extends Requests_Exception
{

}//end class
