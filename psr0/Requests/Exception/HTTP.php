<?php
declare(strict_types=1);

/**
 * Exception based on HTTP response
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception based on HTTP response
 *
 * @package Requests
 */
class Requests_Exception_HTTP extends Requests_Exception
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 0;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Unknown';

    /**
     * Create a new exception
     *
     * There is no mechanism to pass in the status code, as this is set by the
     * subclass used. Reason phrases can vary, however.
     *
     * @param string|null $reason Reason phrase.
     * @param mixed       $data   Associated data.
     */
    public function __construct($reason = null, $data = null)
    {
        if ($reason !== null) {
            $this->reason = $reason;
        }

        $message = sprintf('%d %s', $this->code, $this->reason);
        parent::__construct($message, 'httpresponse', $data, $this->code);
    }//end __construct()


    /**
     * Get the status message
     *
     * @return string The Reason.
     */
    public function getReason()
    {
        return $this->reason;
    }//end getReason()


    /**
     * Get the correct exception class for a given error code
     *
     * @param int $code HTTP status code, or 0 if unavailable.
     *
     * @return string Exception class name to use.
     */
    public static function get_class($code)
    {
        if ($code === 0) {
            return 'Requests_Exception_HTTP_Unknown';
        }

        $class = sprintf('Requests_Exception_HTTP_%d', $code);
        if (class_exists($class)) {
            return $class;
        }

        return 'Requests_Exception_HTTP_Unknown';
    }//end get_class()
}//end class
