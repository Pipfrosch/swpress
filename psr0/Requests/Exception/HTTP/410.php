<?php
declare(strict_types=1);

/**
 * Exception for 410 Gone responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 410 Gone responses
 */
class Requests_Exception_HTTP_410 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 410;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Gone';
}//end class
