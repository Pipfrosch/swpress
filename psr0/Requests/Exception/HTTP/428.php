<?php
declare(strict_types=1);

/**
 * Exception for 428 Precondition Required responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 * @see        https://tools.ietf.org/html/rfc6585
 */

/**
 * Exception for 428 Precondition Required responses
 */
class Requests_Exception_HTTP_428 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 428;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Precondition Required';
}//end class
