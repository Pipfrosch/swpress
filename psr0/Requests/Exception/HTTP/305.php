<?php
declare(strict_types=1);

/**
 * Exception for 305 Use Proxy responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 305 Use Proxy responses
 */
class Requests_Exception_HTTP_305 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 305;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Use Proxy';
}//end class
