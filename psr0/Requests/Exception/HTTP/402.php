<?php
declare(strict_types=1);

/**
 * Exception for 402 Payment Required responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 402 Payment Required responses
 */
class Requests_Exception_HTTP_402 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 402;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Payment Required';
}//end class
