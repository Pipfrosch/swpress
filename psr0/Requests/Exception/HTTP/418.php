<?php
declare(strict_types=1);

/**
 * Exception for 418 I'm A Teapot responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 * @see        https://tools.ietf.org/html/rfc2324
 */

/**
 * Exception for 418 I'm A Teapot responses
 */
class Requests_Exception_HTTP_418 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 418;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = "I'm A Teapot";
}//end class
