<?php
declare(strict_types=1);

/**
 * Exception for 414 Request-URI Too Large responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 414 Request-URI Too Large responses
 */
class Requests_Exception_HTTP_414 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 414;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Request-URI Too Large';
}//end class
