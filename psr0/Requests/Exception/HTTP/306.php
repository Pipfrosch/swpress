<?php
declare(strict_types=1);

/**
 * Exception for 306 Switch Proxy responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 306 Switch Proxy responses
 */
class Requests_Exception_HTTP_306 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 306;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Switch Proxy';
}//end class
