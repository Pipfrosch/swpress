<?php
declare(strict_types=1);

/**
 * Exception for 400 Bad Request responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 400 Bad Request responses
 */
class Requests_Exception_HTTP_400 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 400;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Bad Request';
}//end class
