<?php
declare(strict_types=1);

/**
 * Exception for 501 Not Implemented responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 501 Not Implemented responses
 */
class Requests_Exception_HTTP_501 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 501;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Not Implemented';
}//end class
