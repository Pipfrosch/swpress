<?php
declare(strict_types=1);

/**
 * Exception for 416 Requested Range Not Satisfiable responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 416 Requested Range Not Satisfiable responses
 */
class Requests_Exception_HTTP_416 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 416;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Requested Range Not Satisfiable';
}//end class
