<?php
declare(strict_types=1);

/**
 * Exception for 411 Length Required responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 411 Length Required responses
 */
class Requests_Exception_HTTP_411 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 411;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Length Required';
}//end class
