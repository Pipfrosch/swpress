<?php
declare(strict_types=1);

/**
 * Exception for 405 Method Not Allowed responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 405 Method Not Allowed responses
 */
class Requests_Exception_HTTP_405 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 405;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Method Not Allowed';
}//end class
