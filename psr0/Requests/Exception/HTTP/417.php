<?php
declare(strict_types=1);

/**
 * Exception for 417 Expectation Failed responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 417 Expectation Failed responses
 */
class Requests_Exception_HTTP_417 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 417;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Expectation Failed';
}//end class
