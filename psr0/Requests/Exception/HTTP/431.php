<?php
declare(strict_types=1);

/**
 * Exception for 431 Request Header Fields Too Large responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 * @see        https://tools.ietf.org/html/rfc6585
 */

/**
 * Exception for 431 Request Header Fields Too Large responses
 */
class Requests_Exception_HTTP_431 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 431;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Request Header Fields Too Large';
}//end class
