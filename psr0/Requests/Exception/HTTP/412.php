<?php
declare(strict_types=1);

/**
 * Exception for 412 Precondition Failed responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 412 Precondition Failed responses
 */
class Requests_Exception_HTTP_412 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 412;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Precondition Failed';
}//end class
