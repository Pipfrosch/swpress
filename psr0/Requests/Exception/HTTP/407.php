<?php
declare(strict_types=1);

/**
 * Exception for 407 Proxy Authentication Required responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 407 Proxy Authentication Required responses
 */
class Requests_Exception_HTTP_407 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 407;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Proxy Authentication Required';
}//end class
