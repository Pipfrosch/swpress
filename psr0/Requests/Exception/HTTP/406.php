<?php
declare(strict_types=1);

/**
 * Exception for 406 Not Acceptable responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 406 Not Acceptable responses
 */
class Requests_Exception_HTTP_406 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 406;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Not Acceptable';
}//end class
