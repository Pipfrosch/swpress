<?php
declare(strict_types=1);

/**
 * Exception for 502 Bad Gateway responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 502 Bad Gateway responses
 */
class Requests_Exception_HTTP_502 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 502;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Bad Gateway';
}//end class
