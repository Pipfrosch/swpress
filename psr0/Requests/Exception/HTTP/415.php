<?php
declare(strict_types=1);

/**
 * Exception for 415 Unsupported Media Type responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 415 Unsupported Media Type responses
 */
class Requests_Exception_HTTP_415 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 415;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Unsupported Media Type';
}//end class
