<?php
declare(strict_types=1);

/**
 * Exception for 408 Request Timeout responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 408 Request Timeout responses
 */
class Requests_Exception_HTTP_408 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 408;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Request Timeout';
}//end class
