<?php
declare(strict_types=1);

/**
 * Exception for 404 Not Found responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 404 Not Found responses
 */
class Requests_Exception_HTTP_404 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 404;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Not Found';
}//end class
