<?php
declare(strict_types=1);

/**
 * Exception for 500 Internal Server Error responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 500 Internal Server Error responses
 */
class Requests_Exception_HTTP_500 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 500;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Internal Server Error';
}//end class
