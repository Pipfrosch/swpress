<?php
declare(strict_types=1);

/**
 * Exception for 409 Conflict responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 409 Conflict responses
 */
class Requests_Exception_HTTP_409 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 409;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Conflict';
}//end class
