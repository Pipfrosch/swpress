<?php
declare(strict_types=1);

/**
 * Exception for 304 Not Modified responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 304 Not Modified responses
 */
class Requests_Exception_HTTP_304 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 304;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Not Modified';
}//end class
