<?php
declare(strict_types=1);

/**
 * Exception for 401 Unauthorized responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 401 Unauthorized responses
 */
class Requests_Exception_HTTP_401 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 401;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Unauthorized';
}//end class
