<?php
declare(strict_types=1);

/**
 * Exception for 413 Request Entity Too Large responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 413 Request Entity Too Large responses
 */
class Requests_Exception_HTTP_413 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 413;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Request Entity Too Large';
}//end class
