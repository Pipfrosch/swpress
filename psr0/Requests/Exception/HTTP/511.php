<?php
declare(strict_types=1);

/**
 * Exception for 511 Network Authentication Required responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 * @see        https://tools.ietf.org/html/rfc6585
 */

/**
 * Exception for 511 Network Authentication Required responses
 */
class Requests_Exception_HTTP_511 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 511;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Network Authentication Required';
}//end class
