<?php
declare(strict_types=1);

/**
 * Exception for 504 Gateway Timeout responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 504 Gateway Timeout responses
 */
class Requests_Exception_HTTP_504 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 504;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Gateway Timeout';
}//end class
