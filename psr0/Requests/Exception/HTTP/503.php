<?php
declare(strict_types=1);

/**
 * Exception for 503 Service Unavailable responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 503 Service Unavailable responses
 */
class Requests_Exception_HTTP_503 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 503;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Service Unavailable';
}//end class
