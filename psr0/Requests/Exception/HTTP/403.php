<?php
declare(strict_types=1);

/**
 * Exception for 403 Forbidden responses
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Exception for 403 Forbidden responses
 */
class Requests_Exception_HTTP_403 extends Requests_Exception_HTTP
{
    /**
     * HTTP status code
     *
     * @var integer
     */
    protected $code = 403;

    /**
     * Reason phrase
     *
     * @var string
     */
    protected $reason = 'Forbidden';
}//end class
