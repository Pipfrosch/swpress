<?php
declare(strict_types=1);

/**
 * Exception class for cURL
 *
 * @package    Requests
 * @subpackage Exception
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 * @since      1.6
 */

/**
 * Exception class for cURL
 */
class Requests_Exception_Transport_cURL extends Requests_Exception_Transport
{

    const EASY = 'cURLEasy';
    const MULTI = 'cURLMulti';
    const SHARE = 'cURLShare';

    /**
     * The cURL error code
     *
     * @var integer
     */
    protected $code = -1;

    /**
     * Which type of cURL error
     *
     * EASY|MULTI|SHARE
     *
     * @var string
     */
    protected $type = 'Unknown';

    /**
     * Clear text error message
     *
     * @var string
     */
    protected $reason = 'Unknown';

    /**
     * The constructor
     *
     * @psalm-suppress RedundantConditionGivenDocblockType
     *
     * @param string $message   The error message.
     * @param string|null $type The error type.
     * @param string|null $data The data.
     * @param int $code         The error code.
     */
    public function __construct($message, $type, $data = null, $code = 0)
    {
        if ($type !== null) {
            $this->type = $type;
        }

        if ($code !== null) {
            $this->code = $code;
        }

        if ($message !== null) {
            $this->reason = $message;
        }

        $message = sprintf('%d %s', $this->code, $this->reason);
        parent::__construct($message, $this->type, $data, $this->code);
    }//end __construct()


    /**
     * Get the error message
     *
     * @return string The reason.
     */
    public function getReason(): string
    {
        return $this->reason;
    }//end getReason()
}//end class
