<?php
declare(strict_types=1);

/**
 * Session handler for persistent requests and default parameters
 *
 * @package    Requests
 * @subpackage Session Handler
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Session handler for persistent requests and default parameters
 *
 * Allows various options to be set as default values, and merges both the
 * options and URL properties together. A base URL can be set for all requests,
 * with all subrequests resolved from this. Base options can be set (including
 * a shared cookie jar), then overridden for individual requests.
 *
 * @package    Requests
 * @subpackage Session Handler
 */
class Requests_Session
{
    /**
     * Base URL for requests
     *
     * URLs will be made absolute using this as the base
     *
     * @var string|null
     */
    public $url = null;

    /**
     * Base headers for requests
     *
     * @var array
     */
    public $headers = array();

    /**
     * Base data for requests
     *
     * If both the base data and the per-request data are arrays, the data will
     * be merged before sending the request.
     *
     * @var array
     */
    public $data = array();

    /**
     * Base options for requests
     *
     * The base options are merged with the per-request data for each request.
     * The only default option is a shared cookie jar between requests.
     *
     * Values here can also be set directly via properties on the Session
     * object, e.g. `$session->useragent = 'X';`
     *
     * @var array
     */
    public $options = array();

    /**
     * Create a new session
     *
     * @param string|null $url Base URL for requests.
     * @param array $headers Default headers for requests.
     * @param array $data Default data for requests.
     * @param array $options Default options for requests.
     */
    public function __construct($url = null, $headers = array(), $data = array(), $options = array())
    {
        $this->url = $url;
        $this->headers = $headers;
        $this->data = $data;
        $this->options = $options;

        if (empty($this->options['cookies'])) {
            $this->options['cookies'] = new Requests_Cookie_Jar();
        }
    }//end __construct()


    /**
     * Get a property's value
     *
     * @param string $key Property key.
     *
     * @return mixed|null Property value, null if none found.
     */
    public function __get($key)
    {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }

        return null;
    }//end __get()


    /**
     * Set a property's value
     *
     * @param string $key Property key.
     * @param mixed $value Property value.
     *
     * @return void
     */
    public function __set($key, $value)
    {
        $this->options[$key] = $value;
    }//end __set()


    /**
     * Is a property value set?
     *
     * @param string $key Property key.
     *
     * @return bool Whether or not the property is set.
     */
    public function __isset($key)
    {
        return isset($this->options[$key]);
    }//end __isset()


    /**
     * Remove a property's value
     *
     * @param string $key Property key.
     *
     * @return void
     */
    public function __unset($key)
    {
        if (isset($this->options[$key])) {
            unset($this->options[$key]);
        }
    }//end __unset()


    /**
     * Send a GET request
     *
     * @param string     $url     URL to request.
     * @param array      $headers Extra headers to send with the request.
     * @param array      $options Options for the request (see {@see Requests::request}).
     *
     * @return Requests_Response A Requests_Response object.
     */
    public function get($url, $headers = array(), $options = array())
    {
        return $this->request($url, $headers, null, Requests::GET, $options);
    }//end get()


    /**
     * Send a HEAD request
     *
     * @param string     $url     URL to request.
     * @param array      $headers Extra headers to send with the request.
     * @param array      $options Options for the request (see {@see Requests::request}).
     *
     * @return Requests_Response A Requests_Response object.
     */
    public function head($url, $headers = array(), $options = array())
    {
        return $this->request($url, $headers, null, Requests::HEAD, $options);
    }//end head()


    /**
     * Send a DELETE request
     *
     * @param string     $url     URL to request.
     * @param array      $headers Extra headers to send with the request.
     * @param array      $options Options for the request (see {@see Requests::request}).
     *
     * @return Requests_Response A Requests_Response object.
     */
    public function delete($url, $headers = array(), $options = array())
    {
        return $this->request($url, $headers, null, Requests::DELETE, $options);
    }//end delete()

    /**#@-*/

    /**
     * Send a POST request
     *
     * @param string     $url     URL to request.
     * @param array      $headers Extra headers to send with the request.
     * @param array|null $data    Data to send either as a query string for GET/HEAD requests, or in the body for
     *                            POST requests.
     * @param array      $options Options for the request (see {@see Requests::request}).
     *
     * @return Requests_Response A Requests_Response object.
     */
    public function post($url, $headers = array(), $data = array(), $options = array())
    {
        return $this->request($url, $headers, $data, Requests::POST, $options);
    }//end post()


    /**
     * Send a PUT request
     *
     * @param string     $url     URL to request.
     * @param array      $headers Extra headers to send with the request.
     * @param array|null $data    Data to send either as a query string for GET/HEAD requests, or in the body for
     *                            POST requests.
     * @param array      $options Options for the request (see {@see Requests::request}).
     *
     * @return Requests_Response A Requests_Response object.
     */
    public function put($url, $headers = array(), $data = array(), $options = array())
    {
        return $this->request($url, $headers, $data, Requests::PUT, $options);
    }//end put()


    /**
     * Send a PATCH request
     *
     * Note: Unlike {@see post} and {@see put}, `$headers` is required, as the
     * specification recommends that should send an ETag
     *
     * @link https://tools.ietf.org/html/rfc5789
     *
     * @param string     $url     URL to request.
     * @param array      $headers Extra headers to send with the request.
     * @param array|null $data    Data to send either as a query string for GET/HEAD requests, or in the body for
     *                            POST requests.
     * @param array      $options Options for the request (see {@see Requests::request}).
     *
     * @return Requests_Response A Requests_Response object.
     */
    public function patch($url, $headers, $data = array(), $options = array())
    {
        return $this->request($url, $headers, $data, Requests::PATCH, $options);
    }//end patch()

    /**#@-*/

    /**
     * Main interface for HTTP requests
     *
     * This method initiates a request and sends it via a transport before
     * parsing.
     *
     * @see Requests::request()
     *
     * @throws Requests_Exception On invalid URLs (`nonhttp`).
     *
     * @param string     $url      URL to request.
     * @param array      $headers  Extra headers to send with the request.
     * @param array|null $data     Data to send either as a query string for GET/HEAD requests, or in the body for
     *                             POST requests.
     * @param string     $type     HTTP request type (use Requests constants).
     * @param array      $options  Options for the request (see {@see Requests::request}).
     *
     * @return Requests_Response A Requests_Response object.
     */
    public function request($url, $headers = array(), $data = array(), $type = Requests::GET, $options = array())
    {
        $request = $this->merge_request(compact('url', 'headers', 'data', 'options'));

        return Requests::request($request['url'], $request['headers'], $request['data'], $type, $request['options']);
    }//end request()


    /**
     * Send multiple HTTP requests simultaneously
     *
     * @see Requests::request_multiple()
     *
     * @param array $requests Requests data (see {@see Requests::request_multiple}).
     * @param array $options  Global and default options (see {@see Requests::request}).
     *
     * @return array Responses (either Requests_Response or a Requests_Exception object).
     */
    public function request_multiple($requests, $options = array())
    {
        foreach ($requests as $key => $request) {
            $requests[$key] = $this->merge_request($request, false);
        }

        $options = array_merge($this->options, $options);

        // Disallow forcing the type, as that's a per request setting
        unset($options['type']);

        return Requests::request_multiple($requests, $options);
    }//end request_multiple()


    /**
     * Merge a request's data with the default data
     *
     * @param array $request Request data (same form as {@see request_multiple}).
     * @param bool  $merge_options Should we merge options as well?.
     *
     * @return array Request data.
     */
    protected function merge_request($request, $merge_options = true)
    {
        if ($this->url !== null) {
            $request['url'] = Requests_IRI::absolutize($this->url, $request['url']);
            if (is_object($request['url'])) {
                $request['url'] = $request['url']->uri;
            }
        }

        if (empty($request['headers'])) {
            $request['headers'] = array();
        }
        $request['headers'] = array_merge($this->headers, $request['headers']);

        if (empty($request['data'])) {
            if (is_array($this->data)) {
                $request['data'] = $this->data;
            }
        } elseif (is_array($request['data']) && is_array($this->data)) {
            $request['data'] = array_merge($this->data, $request['data']);
        }

        if ($merge_options !== false) {
            $request['options'] = array_merge($this->options, $request['options']);

            // Disallow forcing the type, as that's a per request setting
            unset($request['options']['type']);
        }

        return $request;
    }//end merge_request()
}//end class
