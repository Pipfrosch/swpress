<?php
declare(strict_types=1);

/**
 * HTTP response class
 *
 * Contains a response from Requests::request()
 *
 * @package Requests
 * @author  Ryan McCue <me@ryanmccue.info>
 * @license https://opensource.org/licenses/ISC ISC
 * @link    http://requests.ryanmccue.info/
 */

/**
 * HTTP response class
 *
 * Contains a response from Requests::request()
 *
 * @package Requests
 */
class Requests_Response
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->headers = new Requests_Response_Headers();
        $this->cookies = new Requests_Cookie_Jar();
    }//end __construct()


    /**
     * Response body
     *
     * @var string
     */
    public $body = '';

    /**
     * Raw HTTP data from the transport
     *
     * @var string
     */
    public $raw = '';

    /**
     * Headers, as an associative array
     *
     * @var array|\Requests_Response_Headers Array-like object representing headers
     */
    public $headers = array();

    /**
     * Status code, false if non-blocking
     *
     * @var integer|boolean
     */
    public $status_code = false;

    /**
     * Protocol version, false if non-blocking
  *
     * @var float|boolean
     */
    public $protocol_version = false;

    /**
     * Whether the request succeeded or not
     *
     * @var boolean
     */
    public $success = false;

    /**
     * Number of redirects the request used
     *
     * @var integer
     */
    public $redirects = 0;

    /**
     * URL requested
     *
     * @var string
     */
    public $url = '';

    /**
     * Previous requests (from redirects)
     *
     * @var array Array of Requests_Response objects
     */
    public $history = array();

    /**
     * Cookies from the request
     *
     * @var array|\Requests_Cookie_Jar Array-like object representing a cookie jar
     */
    public $cookies = array();

    /**
     * Is the response a redirect?
     *
     * @return boolean True if redirect (3xx status), false if not.
     */
    public function is_redirect()
    {
        $code = $this->status_code;
        return in_array($code, array(300, 301, 302, 303, 307)) || $code > 307 && $code < 400;
    }//end is_redirect()


    /**
     * Throws an exception if the request was not successful
     *
     * @throws Requests_Exception      If `$allow_redirects` is false, and code is 3xx (`response.no_redirects`).
     * @throws Requests_Exception_HTTP On non-successful status code. Exception class corresponds to code (e.g.
     *                                 {@see Requests_Exception_HTTP_404}).
     *
     * @param bool $allow_redirects Set to false to throw on a 3xx as well.
     *
     * @return void
     */
    public function throw_for_status($allow_redirects = true)
    {
        if ($this->is_redirect()) {
            if (!$allow_redirects) {
                throw new \Requests_Exception('Redirection not allowed', 'response.no_redirects', $this);
            }
        } elseif (!$this->success) {
            $exceptStatusCode = $this->status_code;
            if (! is_int($exceptStatusCode)) {
                $exceptStatusCode = 0;
            }
            switch ($exceptStatusCode) {
                case 304:
                    throw new \Requests_Exception_HTTP_304(null, $this);
                  break;
                case 305:
                    throw new \Requests_Exception_HTTP_305(null, $this);
                  break;
                case 306:
                    throw new \Requests_Exception_HTTP_306(null, $this);
                  break;
                case 400:
                    throw new \Requests_Exception_HTTP_400(null, $this);
                  break;
                case 401:
                    throw new \Requests_Exception_HTTP_401(null, $this);
                  break;
                case 402:
                    throw new \Requests_Exception_HTTP_402(null, $this);
                  break;
                case 403:
                    throw new \Requests_Exception_HTTP_403(null, $this);
                  break;
                case 404:
                    throw new \Requests_Exception_HTTP_404(null, $this);
                  break;
                case 405:
                    throw new \Requests_Exception_HTTP_405(null, $this);
                  break;
                case 406:
                    throw new \Requests_Exception_HTTP_406(null, $this);
                  break;
                case 407:
                    throw new \Requests_Exception_HTTP_407(null, $this);
                  break;
                case 408:
                    throw new \Requests_Exception_HTTP_408(null, $this);
                  break;
                case 409:
                    throw new \Requests_Exception_HTTP_409(null, $this);
                  break;
                case 410:
                    throw new \Requests_Exception_HTTP_410(null, $this);
                  break;
                case 411:
                    throw new \Requests_Exception_HTTP_411(null, $this);
                  break;
                case 412:
                    throw new \Requests_Exception_HTTP_412(null, $this);
                  break;
                case 413:
                    throw new \Requests_Exception_HTTP_413(null, $this);
                  break;
                case 414:
                    throw new \Requests_Exception_HTTP_414(null, $this);
                  break;
                case 415:
                    throw new \Requests_Exception_HTTP_415(null, $this);
                  break;
                case 416:
                    throw new \Requests_Exception_HTTP_416(null, $this);
                  break;
                case 417:
                    throw new \Requests_Exception_HTTP_417(null, $this);
                  break;
                case 418:
                    throw new \Requests_Exception_HTTP_418(null, $this);
                  break;
                case 428:
                    throw new \Requests_Exception_HTTP_428(null, $this);
                  break;
                case 429:
                    throw new \Requests_Exception_HTTP_429(null, $this);
                  break;
                case 431:
                    throw new \Requests_Exception_HTTP_431(null, $this);
                  break;
                case 500:
                    throw new \Requests_Exception_HTTP_500(null, $this);
                  break;
                case 501:
                    throw new \Requests_Exception_HTTP_501(null, $this);
                  break;
                case 502:
                    throw new \Requests_Exception_HTTP_502(null, $this);
                  break;
                case 503:
                    throw new \Requests_Exception_HTTP_503(null, $this);
                  break;
                case 504:
                    throw new \Requests_Exception_HTTP_504(null, $this);
                  break;
                case 505:
                    throw new \Requests_Exception_HTTP_505(null, $this);
                  break;
                case 511:
                    throw new \Requests_Exception_HTTP_511(null, $this);
                  break;
                default:
                    throw new \Requests_Exception_HTTP_Unknown(null, $this);
                  break;
            }
//            $exception = \Requests_Exception_HTTP::get_class($exceptStatusCode);
//            throw new $exception(null, $this);
        }
    }//end throw_for_status()
}//end class
