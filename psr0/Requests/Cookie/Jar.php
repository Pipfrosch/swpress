<?php
declare(strict_types=1);

/**
 * Cookie holder object
 *
 * @package    Requests
 * @subpackage Cookies
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Cookie holder object
 */
class Requests_Cookie_Jar implements ArrayAccess, IteratorAggregate
{
    /**
     * Actual item data
     *
     * @var array
     */
    protected $cookies = array();

    /**
     * Create a new jar
     *
     * @param array $cookies Existing cookie values.
     */
    public function __construct($cookies = array())
    {
        $this->cookies = $cookies;
    }//end __construct()


    /**
     * Normalise cookie data into a Requests_Cookie
     *
     * @param string|\Requests_Cookie $cookie The cookie.
     * @param string|null            $key    The key.
     *
     * @return \Requests_Cookie
     */
    public function normalize_cookie($cookie, $key = null)
    {
        if ($cookie instanceof \Requests_Cookie) {
            return $cookie;
        }

        return \Requests_Cookie::parse($cookie, $key);
    }//end normalize_cookie()


    /**
     * Normalise cookie data into a \Requests_Cookie
     *
     * @codeCoverageIgnore
     * @deprecated         Use {@see \Requests_Cookie_Jar::normalize_cookie}
     *
     * @param string|\Requests_Cookie $cookie The cookie.
     * @param string|null            $key    The key.
     *
     * @return \Requests_Cookie
     */
    public function normalizeCookie($cookie, $key = null)
    {
        return $this->normalize_cookie($cookie, $key);
    }//end normalizeCookie()


    /**
     * Check if the given item exists
     *
     * @param string $key Item key.
     *
     * @return bool Does the item exist?
     */
    public function offsetExists($key)
    {
        return isset($this->cookies[$key]);
    }//end offsetExists()


    /**
     * Get the value for the item
     *
     * @param string $key Item key.
     *
     * @return string|null Item value.
     */
    public function offsetGet($key)
    {
        if (!isset($this->cookies[$key])) {
            return null;
        }

        return $this->cookies[$key];
    }//end offsetGet()


    /**
     * Set the given item
     *
     * @throws Requests_Exception On attempting to use dictionary as list (`invalidset`).
     *
     * @param string|null $key   Item name.
     * @param string      $value Item value.
     *
     * @return void
     */
    public function offsetSet($key, $value)
    {
        if ($key === null) {
            throw new Requests_Exception('Object is a dictionary, not a list', 'invalidset');
        }

        $this->cookies[$key] = $value;
    }//end offsetSet()


    /**
     * Unset the given header
     *
     * @param string $key The key.
     *
     * @return void
     */
    public function offsetUnset($key)
    {
        unset($this->cookies[$key]);
    }//end offsetUnset()


    /**
     * Get an iterator for the data
     *
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->cookies);
    }//end getIterator()


    /**
     * Register the cookie handler with the request's hooking system
     *
     * @param Requests_Hooker $hooks Hooking system.
     *
     * @return void
     */
    public function register(Requests_Hooker $hooks)
    {
        $hooks->register('requests.before_request', array($this, 'before_request'));
        $hooks->register('requests.before_redirect_check', array($this, 'before_redirect_check'));
    }//end register()


    /**
     * Add Cookie header to a request if we have any
     *
     * As per RFC 6265, cookies are separated by '; '
     *
     * @param string $url    The URL.
     * @param array $headers The headers array.
     * @param array $data    The data array.
     * @param string $type   The type.
     * @param array $options The options.
     *
     * @return void
     */
    public function before_request($url, &$headers, &$data, &$type, &$options)
    {
        if (!$url instanceof Requests_IRI) {
            $url = new Requests_IRI($url);
        }

        if (!empty($this->cookies)) {
            $cookies = array();
            foreach ($this->cookies as $key => $cookie) {
                $cookie = $this->normalize_cookie($cookie, $key);

                // Skip expired cookies
                if ($cookie->is_expired()) {
                    continue;
                }

                if ($cookie->domain_matches($url->host)) {
                    $cookies[] = $cookie->format_for_header();
                }
            }

            $headers['Cookie'] = implode('; ', $cookies);
        }
    }//end before_request()


    /**
     * Parse all cookies from a response and attach them to the response
     *
     * @param \Requests_Response $return A Request_Resonse object.
     *
     * @return void
     */
    public function before_redirect_check(Requests_Response &$return)
    {
        $url = $return->url;
        if (!$url instanceof \Requests_IRI) {
            $url = new \Requests_IRI($url);
        }

        $cookies = \Requests_Cookie::parse_from_headers($return->headers, $url);
        $this->cookies = array_merge($this->cookies, $cookies);
        $return->cookies = $this;
    }//end before_redirect_check()
}//end class
