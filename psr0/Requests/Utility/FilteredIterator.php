<?php
declare(strict_types=1);

/**
 * Iterator for arrays requiring filtered values
 *
 * @package    Requests
 * @subpackage Utilities
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Iterator for arrays requiring filtered values
 *
 * @package    Requests
 * @subpackage Utilities
 */
class Requests_Utility_FilteredIterator extends ArrayIterator
{
    /**
     * Callback to run as a filter
     *
     * @var callable
     */
    protected $callback;

    /**
     * Create a new iterator
     *
     * @param array    $data     The data.
     * @param callable $callback Callback to be called on each value.
     */
    public function __construct($data, $callback)
    {
        parent::__construct($data);

        $this->callback = $callback;
    }//end __construct()


    /**
     * Get the current item's value after filtering
     *
     * @return string
     */
    public function current(): string
    {
        $value = parent::current();
        $value = call_user_func($this->callback, $value);
        return $value;
    }//end current()
}//end class
