<?php
declare(strict_types=1);

/**
 * Case-insensitive dictionary, suitable for HTTP headers
 *
 * @package    Requests
 * @subpackage Utilities
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Case-insensitive dictionary, suitable for HTTP headers
 *
 * @package    Requests
 * @subpackage Utilities
 */
class Requests_Utility_CaseInsensitiveDictionary implements ArrayAccess, IteratorAggregate
{
    /**
     * Actual item data
     *
     * @var array
     */
    protected $data = array();

    /**
     * Creates a case insensitive dictionary.
     *
     * @param array $data Dictionary/map to convert to case-insensitive.
     */
    public function __construct(array $data = array())
    {
        foreach ($data as $key => $value) {
            $this->offsetSet($key, $value);
        }
    }//end __construct()


    /**
     * Check if the given item exists
     *
     * @param string $key Item key.
     *
     * @return bool Does the item exist?
     */
    public function offsetExists($key)
    {
        $key = strtolower($key);
        return isset($this->data[$key]);
    }//end offsetExists()


    /**
     * Get the value for the item
     *
     * @param string $key Item key.
     *
     * @return string|null Item value
     */
    public function offsetGet($key)
    {
        $key = strtolower($key);
        if (!isset($this->data[$key])) {
            return null;
        }

        return $this->data[$key];
    }//end offsetGet()


    /**
     * Set the given item
     *
     * @throws Requests_Exception On attempting to use dictionary as list (`invalidset`)
     *
     * @param string|null $key   Item name.
     * @param string      $value Item value.
     *
     * @return void
     */
    public function offsetSet($key, $value)
    {
        if ($key === null) {
            throw new Requests_Exception('Object is a dictionary, not a list', 'invalidset');
        }

        $key = strtolower($key);
        $this->data[$key] = $value;
    }//end offsetSet()


    /**
     * Unset the given header
     *
     * @param string $key A key.
     *
     * @return void
     */
    public function offsetUnset($key)
    {
        unset($this->data[strtolower($key)]);
    }//end offsetUnset()


    /**
     * Get an iterator for the data
     *
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->data);
    }//end getIterator()


    /**
     * Get the headers as an array
     *
     * @return array Header data
     */
    public function getAll(): array
    {
        return $this->data;
    }//end getAll()
}//end class
