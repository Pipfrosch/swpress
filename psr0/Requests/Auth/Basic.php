<?php
declare(strict_types=1);

/**
 * Basic Authentication provider
 *
 * @package    Requests
 * @subpackage Authentication
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Basic Authentication provider
 *
 * Provides a handler for Basic HTTP authentication via the Authorization
 * header.
 */
class Requests_Auth_Basic implements Requests_Auth
{
    /**
     * Username
     *
     * @var string
     */
    public $user;

    /**
     * Password
     *
     * @var string
     */
    public $pass;

    /**
     * Constructor
     *
     * @throws Requests_Exception On incorrect number of arguments (`authbasicbadargs`)
     *
     * @param array|null $args Array of user and password. Must have exactly two elements.
     */
    public function __construct($args = null)
    {
        if (is_array($args)) {
            if (count($args) !== 2) {
                throw new Requests_Exception('Invalid number of arguments', 'authbasicbadargs');
            }

            list($this->user, $this->pass) = $args;
        }
    }//end __construct()


    /**
     * Register the necessary callbacks
     *
     * @see curl_before_send
     * @see fsockopen_header
     *
     * @param Requests_Hooks $hooks Hook system.
     *
     * @return void
     */
    public function register(Requests_Hooks &$hooks)
    {
        $hooks->register('curl.before_send', array(&$this, 'curl_before_send'));
        $hooks->register('fsockopen.after_headers', array(&$this, 'fsockopen_header'));
    }//end register()


    /**
     * Set cURL parameters before the data is sent
     *
     * @param resource $handle cURL resource.
     *
     * @return void
     */
    public function curl_before_send(&$handle)
    {
        curl_setopt($handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($handle, CURLOPT_USERPWD, $this->getAuthString());
    }//end curl_before_send()


    /**
     * Add extra headers to the request before sending
     *
     * @param string $out HTTP header string.
     *
     * @return void
     */
    public function fsockopen_header(&$out)
    {
        $out .= sprintf("Authorization: Basic %s\r\n", base64_encode($this->getAuthString()));
    }//end fsockopen_header()


    /**
     * Get the authentication string (user:pass)
     *
     * @return string
     */
    public function getAuthString()
    {
        return $this->user . ':' . $this->pass;
    }//end getAuthString()
}//end class
