<?php
declare(strict_types=1);

/**
 * Event dispatcher
 *
 * @package    Requests
 * @subpackage Utilities
 * @author     Ryan McCue <me@ryanmccue.info>
 * @license    https://opensource.org/licenses/ISC ISC
 * @link       http://requests.ryanmccue.info/
 */

/**
 * Event dispatcher
 */
interface Requests_Hooker
{
    /**
     * Register a callback for a hook
     *
     * @param string   $hook Hook name.
     * @param callable $callback  Function/method to call on event.
     * @param int      $priority  Priority number. <0 is executed earlier, >0 is executed later.
     *
     * @return void
     */
    public function register($hook, $callback, $priority = 0);

    /**
     * Dispatch a message
     *
     * @param string $hook Hook name.
     * @param array $parameters Parameters to pass to callbacks.
     *
     * @return bool Successfulness.
     */
    public function dispatch($hook, $parameters = array());
}//end interface
