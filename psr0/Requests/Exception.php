<?php
declare(strict_types=1);

/**
 * Exception for HTTP requests
 *
 * @package Requests
 * @author  Ryan McCue <me@ryanmccue.info>
 * @license https://opensource.org/licenses/ISC ISC
 * @link    http://requests.ryanmccue.info/
 */

/**
 * Exception for HTTP requests
 *
 * @package Requests
 */
class Requests_Exception extends Exception
{
    /**
     * Type of exception
     *
     * @var string
     */
    protected $type;

    /**
     * Data associated with the exception
     *
     * @var mixed
     */
    protected $data;

    /**
     * Create a new exception
     *
     * @param string  $message Exception message.
     * @param string  $type    Exception type.
     * @param mixed   $data    Associated data.
     * @param int     $code    Exception numerical code, if applicable.
     */
    public function __construct($message, $type, $data = null, $code = 0)
    {
        parent::__construct($message, $code);

        $this->type = $type;
        $this->data = $data;
    }//end __construct()


    /**
     * Like {@see getCode()}, but a string code.
     *
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }//end getType()


    /**
     * Gives any relevant data
     *
     * @codeCoverageIgnore
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }//end getData()
}//end class
