<?php
declare(strict_types=1);

/**
 * WordPress formatting functions as static methods
 *
 * @package SWPress
 * @author  WordPress Developers <wp-hackers@lists.automattic.com>
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/GPL-2.0 GPLv2
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

/**
 * This class contains static functions from the wp-includes/formatting.php script. Most of them have minor tweaks, but
 * some have been rewritten.
 */
class SWP_Formatting
{

    /**
     * Set to false on production,
     * database functions won't work
     */
    protected static $debugme = true;

    /**
     * Perform a deep string replace operation to ensure the values in $search are no longer present
     *
     * Repeats the replacement operation until it no longer replaces anything so as to remove "nested" values
     * e.g. $subject = '%0%0%0DDD', $search ='%0D', $result ='' rather than the '%0%0DD' that
     * str_replace would return
     *
     * This is _deep_replace in WordPress wp-includes/formatting.php
     *
     * @since 2.8.1
     * Not anymore... [at]access private
     *
     * @param string|array $search  The value being searched for, otherwise known as the needle.
     *                              An array may be used to designate multiple needles.
     * @param string       $subject The string being searched and replaced on, otherwise known as the haystack.
     *
     * @return string The string with the replaced values.
     */
    protected static function deepReplace($search, string $subject): string
    {
        $subject = (string) $subject;
        $count = 1;
        while ($count) {
            $subject = str_replace($search, '', $subject, $count);
        }
        return $subject;
    }//end deepReplace()
    
    /**
     * Checks and cleans a URL.
     *
     * A number of characters are removed from the URL. If the URL is for displaying
     * (the default behaviour) ampersands are also replaced. The {@see 'clean_url'} filter
     * is applied to the returned cleaned URL.
     *
     * This is esc_url in WordPress wp-includes/formatting.php
     *
     * Rewritten for SWPress
     *
     * @author Alice Wonder
     *
     * @since 2.8.0
     *
     * @param string $url       The URL to be cleaned.
     * @param array  $protocols Optional. An array of acceptable protocols.
     *                          Defaults to return value of wp_allowed_protocols().
     * @param string $_context  Private. Use escapeURL_raw() for database usage.
     *
     * @return string The cleaned $url after the {@see 'clean_url'} filter is applied.
     */
    public static function escapeURL(string $url, array $protocols = null, string $_context = 'display'): string
    {
        $original_url = $url;
        $url = rawurldecode($url);
        if (empty($protocols)) {
            $protocols = \SWP::getAllowedProtocols();
        } else {
            $arr = array();
            foreach($arr as $str) {
                $str = (string) $str;
                $arr[] = strtolower(trim($str));
            }
            $protocols = $arr;
        }
        $idna = false;
        $URI = new \Pipfrosch\NetCom\URI();
        if ($_context === 'display') {
            if (defined('SWP_IDNA_HOSTANAMES') && is_bool(SWP_IDNA_HOSTANAMES)) {
                $idna = SWP_IDNA_HOSTANAMES;
            }
        }
        $URI->setUnicode($idna);
        try {
            $URI->parseURI($url);
        } catch (Exception $e) {
            error_log($e->getMessage());
            return '';
        }
        $scheme = $URI->getScheme();
        if (empty($scheme)) {
            $scheme = $URI->guessScheme();
            if (is_string($scheme)) {
                if (! $URI->setScheme($scheme)) {
                    return '';
                }
            } else {
                return '';
            }
        }
        if (! in_array($scheme, $protocols)) {
            return '';
        }
        $escapedURI = $URI->__toString();
        /**
         * Replace me with a class in NetCom FIXME FUCKING FIXME
         *
         * Replace ampersands and single quotes only when displaying.
         */
        if ($_context === 'display') {
            $escapedURI = \SWPress\Core\EntitySanitizer::stringSanitizer($escapedURI);
            $escapedURI = str_replace('&amp;', '&#38;', $escapedURI);
            $escapedURI = str_replace("'", '&#39;', $escapedURI);
        }
        /**
         * Filters a string cleaned and escaped for output as a URL.
         *
         * @since 2.3.0
         *
         * @param string $good_protocol_url The cleaned URL to be returned.
         * @param string $original_url      The URL prior to cleaning.
         * @param string $_context          If 'display', replace ampersands and single quotes only.
         */
        if (function_exists('apply_filters')) {
            return apply_filters('clean_url', $escapedURI, $original_url, $_context);
        }
        return $escapedURI;
    }//end escapeURL()
    
    /**
     * Performs esc_url() for database usage.
     *
     * @since 2.8.0
     *
     * @param string $url       The URL to be cleaned.
     * @param array  $protocols An array of acceptable protocols.
     *
     * @return string The cleaned URL.
     */
    public static function escapeURL_raw($url, $protocols = null)
    {
        return self::escapeURL($url, $protocols, 'db');
    }//end escapeURL_raw()

    /**
     * Verifies that an email is valid.
     *
     * __replaced__ the is_email function from WordPress wp-includes/formatting.php
     *  and works with internationalized domain names.
     *
     * @param string $email    Email address to validate.
     *
     * @return string|bool Email address if valid, otherwise false.
     */
    public static function isEmail(string $email)
    {
        $testEmail = \SWPress\Core\PunycodeStatic::punycodeEmail($email);
        if (filter_var($testEmail, FILTER_VALIDATE_EMAIL)) {
            return $email;
        }
        return false;
    }//end isEmail()


    /**
     * Maps a function to all non-iterable elements of an array or an object.
     *
     * This is similar to `array_walk_recursive()` but acts upon objects too.
     *
     * @since 4.4.0
     *
     * @param mixed    $value    The array, object, or scalar.
     * @param callable $callback The function to map onto $value.
     *
     * @return mixed The value with the callback applied to all non-arrays and non-objects inside it.
     */
    public static function map_deep($value, $callback)
    {
        if (is_array($value)) {
            foreach ($value as $index => $item) {
                    $value[ $index ] = self::map_deep($item, $callback);
            }
        } elseif (is_object($value)) {
                $object_vars = get_object_vars($value);
            foreach ($object_vars as $property_name => $property_value) {
                    $value->$property_name = self::map_deep($property_value, $callback);
            }
        } else {
                $value = call_user_func($callback, $value);
        }
        return $value;
    }//end map_deep()
    
    /**
     * Strips out all characters that are not allowable in an email.
     *
     * This is sanitize_email in WordPress wp-includes/formatting.php
     * modified to convert to punycode first and return if PHP built in filters
     * validate the e-mail address before continuing with what the original
     * function does.
     *
     * @since 1.5.0
     *
     * @psalm-suppress UndefinedFunction
     *
     * @param string $email Email address to filter.
     *
     * @return string Filtered email address.
     */
    public static function sanitizeEmail(string $email): string
    {
        $email = \SWPress\Core\PunycodeStatic::punycodeEmail($email);
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return apply_filters('sanitize_email', $sanitized_email, $email, null);
        }
        /**
         * At this point everything below really just tries to identify what went wrong.
         */
        if (strlen($email) < 6) {
            return apply_filters('sanitize_email', '', $email, 'email_too_short');
        }
        if (strpos($email, '@', 1) === false) {
            return apply_filters('sanitize_email', '', $email, 'email_no_at');
        }
        // Split out the local and domain parts
        list( $local, $domain ) = explode('@', $email, 2);
        // LOCAL PART
        // Test for invalid characters
        $local = preg_replace('/[^a-zA-Z0-9!#$%&\'*+\/=?^_`{|}~\.-]/', '', $local);
        if ('' === $local) {
            return apply_filters('sanitize_email', '', $email, 'local_invalid_chars');
        }
        // DOMAIN PART
        // Test for sequences of periods
        if ('' === $domain) {
            return apply_filters('sanitize_email', '', $email, 'domain_period_sequence');
        }
        // Test for leading and trailing periods and whitespace
        $domain = trim($domain, " \t\n\r\0\x0B.");
        if ('' === $domain) {
            return apply_filters('sanitize_email', '', $email, 'domain_period_limits');
        }
        // Split the domain into subs
        $subs = explode('.', $domain);
        // Assume the domain will have at least two subs
        if (2 > count($subs)) {
            return apply_filters('sanitize_email', '', $email, 'domain_no_periods');
        }
        // Create an array that will contain valid subs
        $new_subs = array();
        // Loop through each sub
        foreach ($subs as $sub) {
            // Test for leading and trailing hyphens
            $sub = trim($sub, " \t\n\r\0\x0B-");

            // Test for invalid characters
            $sub = preg_replace('/[^a-z0-9-]+/i', '', $sub);

            // If there's anything left, add it to the valid subs
            if ('' !== $sub) {
                $new_subs[] = $sub;
            }
        }
        // If there aren't 2 or more valid subs
        if (2 > count($new_subs)) {
            /** This filter is documented in wp-includes/formatting.php */
            return apply_filters('sanitize_email', '', $email, 'domain_no_valid_subs');
        }
        // Join valid subs into the new domain
        $domain = join('.', $new_subs);
        // Put the email back together
        $sanitized_email = $local . '@' . $domain;
        // if it passed these tests I don't see how it failed the initial PHP filter_var test but...
        return apply_filters('sanitize_email', $sanitized_email, $email, null);
    }//end sanitizeEmail()

    /**
     * Sanitises various option values based on the nature of the option.
     *
     * This is basically a switch statement which will pass $value through a number
     * of functions depending on the $option.
     *
     * This is the function sanitize_option in WordPress wp-includes/formatting.php
     *
     * @since 2.0.5
     *
     * @global wpdb $wpdb WordPress database abstraction object.
     *
     * @param string $option The name of the option.
     * @param string $value  The unsanitised value.
     *
     * @return string Sanitized value.
     */
    public static function sanitizeOption(string $option, string $value): string
    {
        global $wpdb;
        if(! isset($wpdb)) {
            $dbuser     = defined( 'DB_USER' ) ? DB_USER : '';
            $dbpassword = defined( 'DB_PASSWORD' ) ? DB_PASSWORD : '';
            $dbname     = defined( 'DB_NAME' ) ? DB_NAME : '';
            $dbhost     = defined( 'DB_HOST' ) ? DB_HOST : '';
            $wpdb = new \wpdb($dbuser, $dbpassword, $dbname, $dbhost);
            //$wpdb = \SWP_Load::wpdbConnect();
        }

        $original_value = $value;
        $error          = '';

        switch ($option) {
            case 'admin_email':
            case 'new_admin_email':
                $value = $wpdb->strip_invalid_text_for_column($wpdb->options, 'option_value', $value);
                if ($value instanceof \SWPress\Error\Error) {
                    $error = $value->get_error_message();
                } else {
                    $value = self::sanitizeEmail($value);
                    if (! self::isEmail($value)) {
                        $error = \SWP::translateString('The email address entered did not appear to be a valid email address. Please enter a valid email address.');
                    }
                }
                break;

            case 'thumbnail_size_w':
            case 'thumbnail_size_h':
            case 'medium_size_w':
            case 'medium_size_h':
            case 'medium_large_size_w':
            case 'medium_large_size_h':
            case 'large_size_w':
            case 'large_size_h':
            case 'mailserver_port':
            case 'comment_max_links':
            case 'page_on_front':
            case 'page_for_posts':
            case 'rss_excerpt_length':
            case 'default_category':
            case 'default_email_category':
            case 'default_link_category':
            case 'close_comments_days_old':
            case 'comments_per_page':
            case 'thread_comments_depth':
            case 'users_can_register':
            case 'start_of_week':
            case 'site_icon':
                $value = \SWP::absint($value);
                break;

            case 'posts_per_page':
            case 'posts_per_rss':
                if (empty($value)) {
                    $value = 1;
                } else {
                    $value = \SWP::absint($value);
                }
                break;

            case 'default_ping_status':
            case 'default_comment_status':
                // Options that if not there have 0 value but need to be something like "closed"
                if ($value == '0' || $value == '') {
                    $value = 'closed';
                }
                break;

            case 'blogdescription':
            case 'blogname':
                $value = $wpdb->strip_invalid_text_for_column($wpdb->options, 'option_value', $value);
                if ($value !== $original_value) {
                    /**
                     * @psalm-suppress UndefinedFunction
                     */
                    $value = $wpdb->strip_invalid_text_for_column($wpdb->options, 'option_value', wp_encode_emoji($original_value));
                }

                if ($value instanceof \SWPress\Error\Error) {
                    $error = $value->get_error_message();
                } else {
                    $value = esc_html($value);
                }
                break;

            case 'blog_charset':
                $value = preg_replace('/[^a-zA-Z0-9_-]/', '', $value); // strips slashes
                break;

            case 'blog_public':
                // This is the value if the settings checkbox is not checked on POST. Don't rely on this.
                if (empty($value)) {
                    $value = 1;
                } else {
                    $value = intval($value, 10);
                }
                break;

            case 'date_format':
            case 'time_format':
            case 'mailserver_url':
            case 'mailserver_login':
            case 'mailserver_pass':
            case 'upload_path':
                $value = $wpdb->strip_invalid_text_for_column($wpdb->options, 'option_value', $value);
                if ($value instanceof \SWPress\Error\Error) {
                    $error = $value->get_error_message();
                } else {
                    $value = strip_tags($value);
                    /**
                     * @psalm-suppress UndefinedFunction
                     */
                    $value = wp_kses_data($value);
                }
                break;
            case 'ping_sites':
                $value = explode("\n", $value);
                $value = array_filter(array_map('trim', $value));
                // FIXME FUCKME FIXME does this need to change?
                $value = array_filter(array_map('esc_url_raw', $value));
                $value = implode("\n", $value);
                break;

            case 'gmt_offset':
                $value = preg_replace('/[^0-9:.-]/', '', $value); // strips slashes
                break;

            case 'siteurl':
                $value = $wpdb->strip_invalid_text_for_column($wpdb->options, 'option_value', $value);
                if ($value instanceof \SWPress\Error\Error) {
                    $error = $value->get_error_message();
                } else {
                    if (preg_match('#http(s?)://(.+)#i', $value)) {
                        $value = self::escapeURL_raw($value);
                    } else {
                        $error = \SWP::translateString('The WordPress address you entered did not appear to be a valid URL. Please enter a valid URL.');
                    }
                }
                break;

            case 'home':
                $value = $wpdb->strip_invalid_text_for_column($wpdb->options, 'option_value', $value);
                if ($value instanceof \SWPress\Error\Error) {
                    $error = $value->get_error_message();
                } else {
                    if (preg_match('#http(s?)://(.+)#i', $value)) {
                        $value = self::escapeURL_raw($value);
                    } else {
                        $error = \SWP::translateString('The Site address you entered did not appear to be a valid URL. Please enter a valid URL.');
                    }
                }
                break;

            case 'WPLANG':
                /**
                 * @psalm-suppress UndefinedFunction
                 */
                $allowed = get_available_languages();
                if (! is_multisite() && defined('WPLANG') && '' !== WPLANG && 'en_US' !== WPLANG) {
                    $allowed[] = WPLANG;
                }
                if (! in_array($value, $allowed) && ! empty($value)) {
                    $value = \SWP_Option::get_option($option);
                }
                break;
            case 'illegal_names':
                $value = $wpdb->strip_invalid_text_for_column($wpdb->options, 'option_value', $value);
                if ($value instanceof \SWPress\Error\Error) {
                    $error = $value->get_error_message();
                } else {
                    if (! is_array($value)) {
                        $value = explode(' ', $value);
                    }

                    $value = array_values(array_filter(array_map('trim', $value)));

                    if (! $value) {
                            $value = '';
                    }
                }
                break;

            case 'limited_email_domains':
            case 'banned_email_domains':
                $value = $wpdb->strip_invalid_text_for_column($wpdb->options, 'option_value', $value);
                if ($value instanceof \SWPress\Error\Error) {
                    $error = $value->get_error_message();
                } else {
                    if (! is_array($value)) {
                        $value = explode("\n", $value);
                    }
                    $domains = array_values(array_filter(array_map('trim', $value)));
                    $value   = array();

                    foreach ($domains as $domain) {
                        if (! preg_match('/(--|\.\.)/', $domain) && preg_match('|^([a-zA-Z0-9-\.])+$|', $domain)) {
                            $value[] = $domain;
                        }
                    }
                    if (! $value) {
                        $value = '';
                    }
                }
                break;

            case 'timezone_string':
                $allowed_zones = timezone_identifiers_list();
                if (! in_array($value, $allowed_zones) && ! empty($value)) {
                    $error = \SWP::translateString('The timezone you have entered is not valid. Please select a valid timezone.');
                }
                break;

            case 'permalink_structure':
            case 'category_base':
            case 'tag_base':
                $value = $wpdb->strip_invalid_text_for_column($wpdb->options, 'option_value', $value);
                if ($value instanceof \SWPress\Error\Error) {
                    $error = $value->get_error_message();
                } else {
                    $value = self::escapeURL_raw($value);
                    $value = str_replace('http://', '', $value);
                }

                if (is_string($value)) {
                    if ('permalink_structure' === $option && '' !== $value && ! preg_match('/%[^\/%]+%/', $value)) {
                        /* translators: $codexurl: Codex URL */
                        $codexurl = 'https://codex.wordpress.org/Using_Permalinks#Choosing_your_permalink_structure';
                        $error = \SWP::translateString('A structure tag is required when using custom permalinks. <a href="');
                        $error .= \SWP::translateString($codexurl);
                        $error .= \SWP::translateString('">Learn more</a>');
                    }
                }
                break;

            case 'default_role':
                /**
                 * @psalm-suppress UndefinedFunction
                 */
                if (! get_role($value) && get_role('subscriber')) {
                    $value = 'subscriber';
                }
                break;

            case 'moderation_keys':
            case 'blacklist_keys':
                $value = $wpdb->strip_invalid_text_for_column($wpdb->options, 'option_value', $value);
                if ($value instanceof \SWPress\Error\Error) {
                    $error = $value->get_error_message();
                } else {
                    $value = explode("\n", $value);
                    $value = array_filter(array_map('trim', $value));
                    $value = array_unique($value);
                    $value = implode("\n", $value);
                }
                break;
        }

        if (! empty($error)) {
            $value = \SWP_Option::get_option($option);
            if (function_exists('add_settings_error')) {
                add_settings_error($option, "invalid_{$option}", $error);
            }
        }

         /**
         * Filters an option value following sanitization.
         *
         * @since 2.3.0
         * @since 4.3.0 Added the `$original_value` parameter.
         *
         * @param string $value          The sanitized option value.
         * @param string $option         The option name.
         * @param string $original_value The original value passed to the function.
         */
         
        /**
         * @psalm-suppress UndefinedFunction
         */
        return apply_filters("sanitize_option_{$option}", $value, $option, $original_value);
    }//end sanitize_option()

    /**
     * Parses a string into variables to be stored in an array.
     *
     * Uses {@link https://secure.php.net/parse_str parse_str()} and stripslashes if
     * {@link https://secure.php.net/magic_quotes magic_quotes_gpc} is on.
     *
     * @since 2.2.1
     *
     * @psalm-suppress UndefinedFunction
     *
     * @param string $string The string to be parsed.
     * @param array  $array  Variables will be stored in this array.
     *
     * @return void
     */
    public static function swp_parse_str(string $string, array &$array): void
    {
        parse_str($string, $array);
        /**
         * Filters the array of variables derived from a parsed string.
         *
         * @since 2.3.0
         *
         * @param array $array The array populated with variables.
         */
        $array = apply_filters('wp_parse_str', $array);
    }//end swp_parse_str()
}//end class
