<?php
declare(strict_types=1);

/**
 * WordPress Option API as static methods
 *
 * @package    SWPress
 * @subpackage SWPstatic
 * @author     WordPress Developers <wp-hackers@lists.automattic.com>
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/GPL-2.0 GPLv2
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

/**
 * This class contains static functions from the wp-includes/load.php script. Most of them have minor tweaks, but
 * some have been rewritten.
 */
class SWP_Load
{
    /**
     * Detect if server is using TLS
     *
     * Thus is `is_ssl()` in WP wp-includes/load.php
     *
     * @return bool True if server is using TLS
     */
    public static function checkForTLS(): bool
    {
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] === 'on' || intval($_SERVER['HTTPS'], 10) === 1) {
                return true;
            }
        }
        if (isset($_SERVER['SERVER_PORT']) && intval($_SERVER['SERVER_PORT'], 10) === 443) {
            return true;
        }
        return false;
    }//end checkForTLS()

    /**
     * Returns array of must-use plugin files.
     *
     * This is `wp_get_mu_plugins()` in WP wp-includes/load.php
     *
     * @return array Array of files to include.
     */
    public static function getMustUsePlugins(): array
    {
        if (! defined('WPMU_PLUGIN_DIR') || ! is_dir(WPMU_PLUGIN_DIR)) {
            return array();
        }
        $dh = opendir(WPMU_PLUGIN_DIR);
        if ($dh === false) {
            return array();
        }
        $return = array();
        while (($plugin = readdir($dh)) !== false) {
            if (substr($plugin, -4) === '.php') {
                $return[] = WPMU_PLUGIN_DIR . '/' . $plugin;
            }
        }
        closedir($dh);
        sort($return);
        return $return;
    }//end getMustUsePlugins()

    /**
     * Return the HTTP protocol sent by the server.
     *
     * This is `wp_get_server_protocol()` in WP wp-includes/load.php
     *
     * @return string The HTTP protocol. Default: HTTP/1.0.
     */
    public static function getServerProtocol(): string
    {
        $protocol = 'not detected';
        if (isset($_SERVER['SERVER_PROTOCOL'])) {
            $protocol = $_SERVER['SERVER_PROTOCOL'];
        }
        if (! in_array($protocol, array('HTTP/1.1', 'HTTP/2', 'HTTP/2.0'))) {
            return 'HTTP/1.0';
        }
        return $protocol;
    }//end getServerProtocol()

    /**
     * This is NOT from wp-includes/load.php but it should be.
     * Established database connection and returns object.
     *
     * In future will allow for more than just one database class.
     *
     * For whatever reason, this does not seem to satisfy psalm.
     *
     * @return \SWPress\Iface\Database
     */
    public static function wpdbConnect()
    {
        global $wpdb;

        $dbuser     = defined('DB_USER')     ? DB_USER     : '';
        $dbpassword = defined('DB_PASSWORD') ? DB_PASSWORD : '';
        $dbname     = defined('DB_NAME')     ? DB_NAME     : '';
        $dbhost     = defined('DB_HOST')     ? DB_HOST     : '';

        $engine = 'default';
        if (defined('SWP_DATABASE_CLASS') && is_string(SWP_DATABASE_CLASS)) {
            $engine = SWP_DATABASE_CLASS;
        }
        switch($engine) {
            default:
                $wpdb = new \wpdb($dbuser, $dbpassword, $dbname, $dbhost);
        }
        if (! $wpdb instanceof \SWPress\Iface\Database) {
            throw \SWP_TypeException::incorrectDatabaseClass();
        }
//        return $wpdb;
    }//end wpdbConnect()

    /**
     * Check whether a variable is a WordPress error.
     *
     * This is `is_wp_error()` in WP wp-includes/load.php
     *
     * @param mixed $var the variable to check.
     *
     * @return bool True if instance of \SWPress\Error\Error
     */
    public static function wpError($var): bool
    {
        return ($var instanceof \SWPress\Error\Error);
    }//end wpError()
}//end class