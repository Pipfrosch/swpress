<?php
declare(strict_types=1);

/**
 * Type exceptions for the WPS psr0 static classes.
 *
 * @package    SWPress
 * @subpackage SWPstatic
 * @author     WordPress Developers <wp-hackers@lists.automattic.com>
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

/**
 * This class contains TypeError exceptions.
 */
class SWP_TypeException extends \TypeError
{
    /**
     * Database handler does not implement \SWPress\Iface\Database
     *
     * @return \TypeError
     */
    public static function incorrectDatabaseClass()
    {
        return new self(sprintf(
            'The database class you are attempting to use does not implement <code>%s</code>',
            '\SWPress\Iface\Database'
        ));
    }//end incorrectDatabaseClass()
}//end class

?>