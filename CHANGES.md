CHANGES FROM WORDPRESS
======================

BASE DIRECTORY
--------------

These are changes within the base directory

### mksalts.php

New file, better generation of the salts that are used.

### wp-config-sample.php

put the salts defined there in a conditional. Eventually will just remove them
from that file but when I hack the installer.

### wp-load.php

Require the autoloader and the salts.

### wp-settings.php

Comment out of classes moved to autoloading, work in progress.

### swp-autoload.php

New file, provides a PSR-4/PSR-0/PEAR class autoloader.


WP-INCLUDES
-----------

### pluggable.php

Modified several functions to do things better. Work in progress.



PSR-0 Classes
-------------

# Requests Class

Defaults to `ini_get('openssl.cafile')` for the PEM bundle, alleviating the
need to call `Requests::set_certificate_path()` and alleviating the need to
bundle a root trust anchor store. If that directive is not set or if it does
not point to a file, it defaults to `/etc/pki/tls/certs/ca-bundle.crt` which
should work on RHEL/CentOS/Fedora systems.

The Requests class moved out of `wp-includes` into `psr0` directory for
autoloading.




PSR-4 Classes
-------------

### PasswordHash class

Put into \Obsolete\PasswordHash namespace and moved out of `wp-includes` into
`psr4` for autoloading. I am in the process of nuking all references to this
class *except* where needed to upgrade existing password to Argon2ID.

WordPress did not only use it for passwords, they also used it in the context
where a NONCE is often used, such as activation codes. In those instances, I
am using a salted cryptographic hash algorithm from libsodium instead. Most
instances are purged, but the class will need to remain for cases where an old
WordPress install is upgraded to SWPress, to smoothly transition the user
password hashes as users log in.

### WP_Error Class

Renamed to `SWPress\Error\Error` and moved out of `wp-includes` into `psr4`
directory for autoloading. Plugins and themes that use this class should add
the following:

    if (defined('SWP_PSR4')) {
        use \SWPress\Error\Error as WP_Error;
    }