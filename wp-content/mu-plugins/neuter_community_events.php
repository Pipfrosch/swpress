<?php
/**
 * Plugin Name: Neuter Community Events
 * Plugin URI:  https://gitlab.com/Pipfrosch/swpress/blob/master/wp-content/mu-plugins/neuter_community_events.php
 * Description: Strips information from the WordPress community events request
 * Version:     0.3
 * Author:      SWPress
 * Author URI:  https://gitlab.com/Pipfrosch/swpress/
 * License:     MIT
 */

if (! defined('WPINC')) {
    exit;
}

/**
 * Filters out content sent to events API server.
 *
 * @param array  $args Array of arguments set to the API server.
 * @param string $url  The url the args are sent to.
 *
 * @return array Either empty array or the input array, depending upon
 *               the $url parameter.
 */
function neuter_community_events_leak(array $args, string $url)
{
    /* specific to the community events plugin */
    if (substr_count($url, 'api.wordpress.org/events') !== 0) {
        // This neuters everything sent to the events API server, including the
        // blog domain and the location information.
        return array();
    }
    return $args;
}//end neuter_community_events_leak();

/* apply function to the http_request_args filter */
add_filter('http_request_args', 'neuter_community_events_leak', 10, 2);

// end of script
?>
