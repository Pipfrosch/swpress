<?php
declare(strict_types=1);

/**
 * Unit testing for \Pipfrosch\NetCom\URI
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;

/**
 * Test class for \Pipfrosch\NetCom\URI
 */
// @codingStandardsIgnoreLine
final class URITest extends TestCase
{
    /**
     * Test parsing of a valid URI
     *
     * @return void
     */
    public function testValidUriParse()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('https://www.example.org:443/some/path/index.php?foo=bar&fubar=funny stuff#fragmentTest');
        $actual = $foo->getPort();
        // port should be null
        $this->assertNull($actual);
        // scheme should be https
        $expected = 'https';
        $actual = $foo->getScheme();
        // user info should be empty
        $this->assertSame($expected, $actual);
        $expected = '';
        $actual = $foo->getUserInfo();
        $this->assertSame($expected, $actual);
        // hostname should be www.example.org
        $expected = 'www.example.org';
        $actual = $foo->getHost();
        $this->assertSame($expected, $actual);
        // authority should be www.example.org
        $expected = 'www.example.org';
        $actual = $foo->getAuthority();
        $this->assertSame($expected, $actual);
        // path should be '/some/path/index.php'
        $expected = '/some/path/index.php';
        $actual = $foo->getPath();
        $this->assertSame($expected, $actual);
        // query should be 'foo=bar&fubar=funny%20stuff
        $expected = 'foo=bar&fubar=funny%20stuff';
        $actual = $foo->getQuery();
        $this->assertSame($expected, $actual);
        // fragment should be 'fragmentTest'
        $expected = 'fragmentTest';
        $actual = $foo->getFragment();
        $this->assertSame($expected, $actual);
        // serialized URI should be url encoded for the space
        $expected = 'https://www.example.org/some/path/index.php?foo=bar&fubar=funny%20stuff#fragmentTest';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testValidUriParse()

    /**
     * Scheme operations
     *
     * @return void
     */
    public function testSchemeOperations()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://www.example.org:8080');
        $foo->setScheme('ftp');
        $expected = 'ftp://www.example.org:8080';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo->setPort(21);
        $foo->setScheme('https');
        $actual = $foo->getPort();
        $this->assertNull($actual);
        $bar = $foo->withScheme('http');
        $expected = 'http://www.example.org';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
    }//end testSchemeOperations()

    /**
     * Test port operations
     *
     * @return void
     */
    public function testPortOperations()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://www.example.org:8080');
        $expected = 8080;
        $actual = $foo->getPort();
        $this->assertSame($expected, $actual);
        $expected = 'www.example.org:8080';
        $actual = $foo->getAuthority();
        $this->assertSame($expected, $actual);
        $foo->setPort(443);
        $expected = 443;
        $actual = $foo->getPort();
        $this->assertSame($expected, $actual);
        $expected = 'www.example.org:443';
        $actual = $foo->getAuthority();
        $this->assertSame($expected, $actual);
        // should be null after switching scheme to https
        $foo->setScheme('https');
        $actual = $foo->getPort();
        $this->assertNull($actual);
        $expected = 'www.example.org';
        $actual = $foo->getAuthority();
        $this->assertSame($expected, $actual);
        $bar = $foo->withPort(80);
        $expected = 80;
        $actual = $bar->getPort();
        $this->assertSame($expected, $actual);
        $expected = 'www.example.org:80';
        $actual = $bar->getAuthority();
        $this->assertSame($expected, $actual);
        $fubar = $bar->withScheme('http');
        $actual = $fubar->getPort();
        $this->assertNull($actual);
        $expected = 'www.example.org';
        $actual = $fubar->getAuthority();
        $this->assertSame($expected, $actual);
    }//end testPortOperations()

    /**
     * Test host operations
     *
     * @return void
     */
    public function testHostOperations()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://www.example.org:8080');
        $expected = 'www.example.org';
        $actual = $foo->getHost();
        $this->assertSame($expected, $actual);
        $foo->setHost('173.255.209.112');
        $expected = '173.255.209.112:8080';
        $actual = $foo->getAuthority();
        $this->assertSame($expected, $actual);
        // IPv6 enter upper case w/o compression
        $foo->setHost('2600:3C01:0000:0000:F03C:91FF:FED8:1C0A');
        if (class_exists('\Net_IPv6', false)) {
            $expected = '[2600:3c01::f03c:91ff:fed8:1c0a]:8080';
        } else {
            $expected = '[2600:3c01:0000:0000:f03c:91ff:fed8:1c0a]:8080';
        }
        $actual = $foo->getAuthority();
        $this->assertSame($expected, $actual);
        $bar = $foo->withHost('swpress.dev');
        $bar->setPort(443);
        $bar->setScheme('https');
        $bar->setPath('index.php');
        $expected = 'https://swpress.dev/index.php';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        // test idna
        $fubar = $bar->withHost('進撃の巨人ブログ.com');
        $expected = 'https://xn--u9j4g9dxd292pfbtfskdg9f.com/index.php';
        $actual = $fubar->__toString();
        $this->assertSame($expected, $actual);
        $fubar->setUnicode(true);
        $expected = '進撃の巨人ブログ.com';
        $actual = $fubar->getAuthority();
        $this->assertSame($expected, $actual);
        $fubar->setHost('xn--u9jz52g20bxxt1c509bu60f.jp');
        $expected = '進撃の巨人全巻.jp';
        $actual = $fubar->getHost();
        $this->assertSame($expected, $actual);
        $fubar->setUnicode(false);
        $expected = 'https://xn--u9jz52g20bxxt1c509bu60f.jp/index.php';
        $actual = $fubar->__toString();
        $this->assertSame($expected, $actual);
    }//end testHostOperations()

    /**
     * Test userninfo operations
     *
     * @return void
     */
    public function testUserInfoOperations()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('https://pipfrosch:qwerty@gitlab.com');
        // should NOT show the password
        $expected = 'pipfrosch';
        $actual = $foo->getUserInfo();
        $this->assertSame($expected, $actual);
        $expected = 'pipfrosch@gitlab.com';
        $actual = $foo->getAuthority();
        $this->assertSame($expected, $actual);
        $expected = 'https://pipfrosch@gitlab.com';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        // now it SHOULD show the password
        $foo->setShowPassword(true);
        $expected = 'pipfrosch:qwerty';
        $actual = $foo->getUserInfo();
        $this->assertSame($expected, $actual);
        $expected = 'pipfrosch:qwerty@gitlab.com';
        $actual = $foo->getAuthority();
        $this->assertSame($expected, $actual);
        $expected = 'https://pipfrosch:qwerty@gitlab.com';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo->setUserInfo('john');
        $foo->setShowPassword(true);
        // should not still have old password
        $expected = 'john';
        $actual = $foo->getUserInfo();
        $this->assertSame($expected, $actual);
        $bar = $foo->withUserInfo('malice:NewQwErTy');
        // should NOT carry over the show password
        $expected = 'malice@gitlab.com';
        $actual = $bar->getAuthority();
        $this->assertSame($expected, $actual);
        $bar->setShowPassword(true);
        // now should show it
        $expected = 'malice:NewQwErTy@gitlab.com';
        $actual = $bar->getAuthority();
        $this->assertSame($expected, $actual);
    }//end testUserInfoOperations()

    /**
     * Test Mailto operations
     *  -- These are path operations but IDNA is taken into consideration
     *
     * @return void
     */
    public function testMailtoOperations()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('MAILTO:user@example.net?subject="Tonight I\'m going to party like it\'s 1999"');
        $expected = 'mailto';
        $actual = $foo->getScheme();
        $this->assertSame($expected, $actual);
        $expected = 'user@example.net';
        $actual = $foo->getPath();
        $this->assertSame($expected, $actual);
        $expected='subject="Tonight%20I%27m%20going%20to%20party%20like%20it%27s%201999"';
        $actual = $foo->getQuery();
        $this->assertSame($expected, $actual);
        $expected = 'mailto:user@example.net?subject="Tonight I\'m going to party like it\'s 1999"';
        $actual = rawurldecode($foo->__toString());
        $this->assertSame($expected, $actual);
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->setScheme('mailto');
        $foo->setPath('user@bücher.com');
        $expected = 'mailto:user@xn--bcher-kva.com';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withQuery('subject="Hello World!"');
        $expected = 'mailto:user@xn--bcher-kva.com?subject="Hello%20World%21"';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $bar->setUnicode(true);
        $expected = 'mailto:user@bücher.com?subject="Hello%20World%21"';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $fubar = $bar->withPath('user@example.net,user@bücher.com,user@elsewhere.com');
        // should keep internationalization
        $expected = 'mailto:user@example.net,user@bücher.com,user@elsewhere.com?subject="Hello%20World%21"';
        $actual = $fubar->__toString();
        $this->assertSame($expected, $actual);
        $fubar->setUnicode(false);
        // now should not have internationalization
        $expected = 'mailto:user@example.net,user@xn--bcher-kva.com,user@elsewhere.com?subject="Hello%20World%21"';
        $actual = $fubar->__toString();
        $this->assertSame($expected, $actual);
    }//end testMailtoOperations()

    /**
     * Test Path operations
     *
     * @return void
     */
    public function testPathOperations()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('file:///home/alice/Images/something.jpg');
        $expected = '';
        $actual = $foo->getAuthority();
        $this->assertSame($expected, $actual);
        $expected = '/home/alice/Images/something.jpg';
        $actual = $foo->getPath();
        $this->assertSame($expected, $actual);
        $expected = 'file:///home/alice/Images/something.jpg';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->setScheme('file');
        $foo->setPath('some/path/without/leading/forward/slash');
        $expected = 'file:///some/path/without/leading/forward/slash';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo->setHost('somewhere.net');
        $expected = 'file://somewhere.net/some/path/without/leading/forward/slash';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        // make sure actual path still rootless
        $expected = 'some/path/without/leading/forward/slash';
        $actual = $foo->getPath();
        $this->assertSame($expected, $actual);
        // switch protocols to ftp
        $foo->setScheme('ftp');
        $expected = 'ftp://somewhere.net/some/path/without/leading/forward/slash';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        // switch to http
        $bar = $foo->withScheme('http');
        $expected = 'http://somewhere.net/some/path/without/leading/forward/slash';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
    }//end testPathOperations()

    /**
     * Test relative URLs
     *
     * @return void
     */
    public function testRelativeUrls()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('foo/bar?query=1');
        $expected = "foo/bar?query=1";
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo->parseURI('./foo/bar?query=1');
        $expected = "./foo/bar?query=1";
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo->parseURI('../foo/bar?query=1');
        $expected = "../foo/bar?query=1";
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo->parseURI('../../../foo/bar?query=1');
        $expected = "../../../foo/bar?query=1";
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testRelativeUrls()

    /**
     * Test URN operations
     * -- These are technically path operations
     *
     * @return void
     */
    public function testUrnOperations()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('URN:IETF:RFC:4122');
        $expected = 'ietf:rfc:4122';
        $actual = $foo->getPath();
        $this->assertSame($expected, $actual);
        $random = random_bytes(16);
        $input = bin2hex($random);
        $foo->setPath('UUID:' . $input);
        $expected = 'urn:uuid:';
        $expected .= substr($input, 0, 8);
        $expected .= '-' . substr($input, 8, 4);
        $expected .= '-' . substr($input, 12, 4);
        $expected .= '-' . substr($input, 16, 4);
        $expected .= '-' . substr($input, 20, 12);
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo->setPath('ISBN:1072230526');
        $expected = 'urn:isbn:1-07-223052-6';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo->setPath('ISBN:091302886X');
        $expected = 'urn:isbn:0-913028-86-X';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withPath('ISBN:9781440585913');
        $expected = 'urn:isbn:978-1-4405-8591-3';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
    }//end testUrnOperations()

    /**
     * Test HTTP/HTTPS Query operations
     *
     * @return void
     */
    public function testBasicHttpQueryOperations()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://www.example.org/index.php?foo=bar');
        $expected = 'foo=bar';
        $actual = $foo->getQuery();
        $this->assertSame($expected, $actual);
        $foo->setQuery('fubar=you skank');
        $expected = 'http://www.example.org/index.php?fubar=you%20skank';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withQuery('butter=pancake&vanilla=green');
        $expected = 'butter=pancake&vanilla=green';
        $actual = $bar->getQuery();
        $this->assertSame($expected, $actual);
    }//end testBasicHttpQueryOperations()

    /**
     * Test HTTP/HTTPS Query with duplicate keys
     *
     * @return void
     */
    public function testHttpQueryWithDuplicateKeys()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://www.example.org/index.php');
        $foo->setQuery('foo=Argument One&foo=Argument Two');
        $expected = 'foo=Argument%20One&foo=Argument%20Two';
        $actual = $foo->getQuery();
        $this->assertSame($expected, $actual);
        $bar = $foo->withQuery('foo[]=Argument+One&foo[]=Argument+Two');
        $expected = 'http://www.example.org/index.php?foo[]=Argument+One&foo[]=Argument+Two';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
    }//end testHttpQueryWithDuplicateKeys()

    /**
     * Test with GetQueryBuilder Object
     *
     * @return void
     */
    public function testWithGetQueryBuilderObject()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->setScheme('https');
        $foo->setHost('pipfrosch.com');
        $foo->setPath('/404.php');
        $arrOne = array('foo' => 'Pink Baloon', 'bar' => 'I be drunk');
        $arrTwo = array('foo' => 'Green Banana', 'fubar' => 'boogie');
        $queryArgs = new \Pipfrosch\NetCom\GetQueryBuilder(true, true);
        $queryArgs->setMode('ARRAY');
        $queryArgs->queryFromArray($arrOne);
        $queryArgs->queryFromArray($arrTwo);
        $foo->setGetQuery($queryArgs);
        $expected = 'bar=I+be+drunk&foo=[Pink+Baloon,Green+Banana]&fubar=boogie';
        $actual = $foo->getQuery();
        $this->assertSame($expected, $actual);
        $arrOne = array('nada' => 'Nothing', 'bien' => 'is good');
        $arrTwo = array('Rana' => 'ribbit', 'boylii' => 'yellow');
        $queryArgs = new \Pipfrosch\NetCom\GetQueryBuilder(true);
        $queryArgs->queryFromArray($arrOne);
        $queryArgs->queryFromArray($arrTwo);
        $bar = $foo->withGetQuery($queryArgs);
        $expected = 'https://pipfrosch.com/404.php?Rana=ribbit&bien=is+good&boylii=yellow&nada=Nothing';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
    }//end testWithGetQueryBuilderObject()

    /**
     * Test fragment operations
     *
     * @return void
     */
    public function testFragmentOperations()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://example.net/index.html#fragment');
        $expected = 'fragment';
        $actual = $foo->getFragment();
        $this->assertSame($expected, $actual);
        $foo->setFragment('someOtherFragment');
        $expected = 'http://example.net/index.html#someOtherFragment';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withFragment('beeb@p');
        $expected = 'http://example.net/index.html#beeb%40p';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
    }//end testFragmentOperations()

    /**
     * Test scheme guess
     *
     * @return void
     */
    public function testGuessScheme()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        //with specified scheme
        $foo->setScheme('ftp');
        $expected = 'ftp';
        $actual = $foo->guessScheme();
        $this->assertSame($expected, $actual);
        $foo->setScheme('');
        $actual = $foo->guessScheme();
        $this->assertFalse($actual);
        //with specified port
        $foo->setPath('/fooo/');
        $foo->setPort(80);
        $expected = 'http';
        $actual = $foo->guessScheme();
        $this->assertSame($expected, $actual);
        $foo->setPort(21);
        $expected = 'ftp';
        $actual = $foo->guessScheme();
        $this->assertSame($expected, $actual);
        $foo->setPort(443);
        $expected = 'https';
        $actual = $foo->guessScheme();
        $this->assertSame($expected, $actual);
        //with specified path
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->setPath('paypal@domblogger.net');
        $expected = 'mailto';
        $actual = $foo->guessScheme();
        $this->assertSame($expected, $actual);
        $foo->setPath('urn:isbn:978-0-7607-2245-9');
        $expected = 'urn';
        $actual = $foo->guessScheme();
        $this->assertSame($expected, $actual);
        $foo->setPath('/whatever/foo/');
        $expected = 'https';
        $actual = $foo->guessScheme();
        $this->assertSame($expected, $actual);
        $foo->setPort(8080);
        $expected = 'http';
        $actual = $foo->guessScheme();
        $this->assertSame($expected, $actual);
    }//end testGuessScheme()

    /** Return false scenarios for setCamel tests */

    /**
     * Test setScheme
     *
     * @return void
     */
    public function testInvalidSchemeReturnsFalseWithSetScheme()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $actual = $foo->setScheme('3gp');
        $this->assertFalse($actual);
        $actual = $foo->setScheme('htt:p');
        $this->assertFalse($actual);
        $actual = $foo->setScheme('http://');
        $this->assertFalse($actual);
    }//end testInvalidSchemeReturnsFalseWithSetScheme()

    /**
     * Test setHost
     *
     * @return void
     */
    public function testInvalidHostReturnsFalseWithSetHost()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $actual = $foo->setHost('8.8.8');
        $this->assertFalse($actual);
        $actual = $foo->setHost('goo[]gle.com');
        $this->assertFalse($actual);
    }//end testInvalidHostReturnsFalseWithSetHost()

    /**
     * Test setPort
     *
     * @psalm-suppress InvalidScalarArgument
     *
     * @return null
     */
    public function testInvalidPortReturnsFalseWithSetPort()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $actual = $foo->setPort('8.8.8');
        $this->assertFalse($actual);
        $foo = new \Pipfrosch\NetCom\URI();
        $actual = $foo->setPort(0);
        $this->assertFalse($actual);
    }//end testInvalidPortReturnsFalseWithSetPort()

    /**
     * Test setPath
     *
     * @return void
     */
    public function testInvalidPathReturnsFalseWithSetPath()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->setScheme('urn');
        $actual = $foo->setPath('isbn:rfc:6605');
        $this->assertFalse($actual);
        $foo->parseURI('mailto:paypal@domblogger.net');
        $actual = $foo->setPath('ietf:rfc:6605');
        $this->assertFalse($actual);
    }//end testInvalidPathReturnsFalseWithSetPath()

    /**
     * Test setQuery
     *
     * @return void
     */
    public function testInvalidQueryReturnsFalseWithSetQuery()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->setScheme('email');
        $actual = $foo->setQuery('What The Fuck???');
        $this->assertFalse($actual);
    }//end testInvalidQueryReturnsFalseWithSetQuery()

    /** Exception Tests */

    /**
     * Test invalid scheme.
     *
     * @return void
     */
    public function testInvalidSchemeFromParseUriThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $this->expectException(\InvalidArgumentException::class);
        $foo->parseURI('3tp://whatever.net/');
    }//end testInvalidSchemeFromParseUriThrowsException()

    /**
     * Test invalid scheme.
     *
     * @return void
     */
    public function testInvalidSchemeFromParseUriExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $this->expectExceptionMessage('The specified scheme is not valid');
        $foo->parseURI('3tp://whatever.net/');
    }//end testInvalidSchemeFromParseUriExceptionMessage()

    /**
     * Test invalid scheme
     *
     * @return void
     */
    public function testInvalidSchemeFromWithSchemeThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://www.whatever.net/');
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withScheme('3tp');
    }//end testInvalidSchemeFromWithSchemeThrowsException()

    /**
     * Test invalid scheme
     *
     * @return void
     */
    public function testInvalidSchemeFromWithSchemeExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://www.whatever.net/');
        $this->expectExceptionMessage('does not conform to <code>urn:ietf:rfc:3986<code> section 3.1');
        $bar = $foo->withScheme('3tp');
    }//end testInvalidSchemeFromWithSchemeExceptionMessage()

    /**
     * Valid scheme but not with existing path
     *
     * @return void
     */
    public function testValidWithSchemeMethodWithIncompatibleExistingPathThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://www.whatever.net/');
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withScheme('mailto');
    }//end testValidWithSchemeMethodWithIncompatibleExistingPathThrowsException()

    /**
     * Valid scheme but not with existing path
     *
     * @return void
     */
    public function testValidWithSchemeMethodWithIncompatibleExistingPathExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://www.whatever.net/');
        $this->expectExceptionMessage('The specified scheme <code>mailto</code> is not valid with the specified path');
        $bar = $foo->withScheme('mailto');
    }//end testValidWithSchemeMethodWithIncompatibleExistingPathExceptionMessage()

    /**
     * Valid scheme but not with existing query
     *
     * @return void
     */
    public function testValidWithSchemeWithIncompatibleExistingQueryThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->setScheme('http');
        $foo->setQuery('foo[]=arg1&bar=arg2&foo[]=arg3');
        // verify the query is set
        $expected = 'foo[]=arg1&bar=arg2&foo[]=arg3';
        $actual = $foo->getQuery();
        $this->assertSame($expected, $actual);
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withScheme('mailto');
    }//end testValidWithSchemeWithIncompatibleExistingQueryThrowsException()

    /**
     * Valid scheme but not with existing query
     *
     * @return void
     */
    public function testValidWithSchemeWithIncompatibleExistingQueryExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->setScheme('http');
        $foo->setQuery('foo[]=arg1&bar=arg2&foo[]=arg3');
        // verify the query is set
        $expected = 'foo[]=arg1&bar=arg2&foo[]=arg3';
        $actual = $foo->getQuery();
        $this->assertSame($expected, $actual);
        $this->expectExceptionMessage('The specified scheme <code>mailto</code> is not valid with the existing query');
        $bar = $foo->withScheme('mailto');
    }//end testValidWithSchemeWithIncompatibleExistingQueryExceptionMessage()

    /**
     * Invalid host
     *
     * @return void
     */
    public function testInvalidHostFromParseUriThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $this->expectException(\InvalidArgumentException::class);
        $foo->parseURI('http://www>__.net/index.php');
    }//end testInvalidHostFromParseUriThrowsException()

    /**
     * Invalid host
     *
     * @return void
     */
    public function testInvalidHostFromParseUriExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $this->expectExceptionMessage('is not a valid URI host. See <code>urn:ietf:rfc:3986</code> section 3.2.2');
        $foo->parseURI('http://www>__.net/index.php');
    }//end testInvalidHostFromParseUriExceptionMessage()

    /**
     * Invalid host
     *
     * @return void
     */
    public function testInvalidHostFromWithHostThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://www.google.com/index.html');
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withHost('www.b<>ing#.com');
    }//end testInvalidHostFromWithHostThrowsException()

    /**
     * Invalid host
     *
     * @return void
     */
    public function testInvalidHostFromWithHostExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://www.google.com/index.html');
        $this->expectExceptionMessage('is not a valid URI host. See <code>urn:ietf:rfc:3986</code> section 3.2.2');
        $bar = $foo->withHost('www.b<>ing#.com');
    }//end testInvalidHostFromWithHostExceptionMessage()

    /**
     * Invalid Port
     *
     * @return void
     */
    public function testInvalidPortParseUriThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $this->expectException(\InvalidArgumentException::class);
        $foo->parseURI('http://example.org:9P0/balloon.php');
    }//end testInvalidPortParseUriThrowsException()

    /**
     * Invalid Port
     *
     * @return void
     */
    public function testInvalidPortParseUriExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $this->expectExceptionMessage('is not a valid URI port');
        $foo->parseURI('http://example.org:9P0/balloon.php');
    }//end testInvalidPortParseUriExceptionMessage()

    /**
     * Invalid Port
     *
     * @psalm-suppress InvalidScalarArgument
     *
     * @return void
     */
    public function testInvalidPortFromWithPortThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://example.org/balloon.php');
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withPort('LXXX');
    }//end testInvalidPortFromWithPortThrowsException()

    /**
     * Invalid Port
     *
     * @psalm-suppress InvalidScalarArgument
     *
     * @return void
     */
    public function testInvalidPortFromWithPortExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('http://example.org/balloon.php');
        $this->expectExceptionMessage('is not a valid URI port');
        $bar = $foo->withPort('LXXX');
    }//end testInvalidPortFromWithPortExceptionMessage()

    /**
     * Invalid Path
     *
     * @return void
     */
    public function testInvalidEmailPathFromParseUriThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $this->expectException(\InvalidArgumentException::class);
        $foo->parseURI('mailto://www.foo.bar/thi s:>is:not:a:valid:path');
    }//end testInvalidEmailPathFromParseUriThrowsException()

    /**
     * Invalid Path
     *
     * @return void
     */
    public function testInvalidEmailPathFromParseUriExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $this->expectExceptionMessage('is not valid with the specified scheme');
        $foo->parseURI('mailto://www.foo.bar/thi s:>is:not:a:valid:path');
    }//end testInvalidEmailPathFromParseUriExceptionMessage()

    /**
     * Invalid Path
     *
     * @return void
     */
    public function testUrnSchemeValidEmailFromWithPathThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('urn:isbn:9791090636071');
        // test the URI first
        $expected = 'urn:isbn:979-10-9063607-1';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withPath('paypal@domblogger.net');
    }//end testUrnSchemeValidEmailFromWithPathThrowsException()

    /**
     * Invalid Path
     *
     * @return void
     */
    public function testUrnSchemeValidEmailFromWithPathExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->parseURI('urn:ietf:rfc:8141');
        // test the URI first
        $expected = 'urn:ietf:rfc:8141';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $this->expectExceptionMessage('is not valid with the specified scheme');
        $bar = $foo->withPath('paypal@domblogger.net');
    }//end testUrnSchemeValidEmailFromWithPathExceptionMessage()

    /**
     * Invalid Query
     *
     * @return void
     */
    public function testInvalidQueryFromParseUriThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $this->expectException(\InvalidArgumentException::class);
        $foo->parseURI('http://www.foo.tld/file?foo&bar&ap ple');
    }//end testInvalidQueryFromParseUriThrowsException()

    /**
     * Invalid Query
     *
     * @return void
     */
    public function testInvalidQueryFromParseUriExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $this->expectExceptionMessage('is not a valid query string for the <code>http</code> scheme');
        $foo->parseURI('http://www.foo.tld/file?fo o&bar&apple');
    }//end testInvalidQueryFromParseUriExceptionMessage()

    /**
     * Invalid Query
     *
     * @return void
     */
    public function testInvalidQueryFromWithQueryThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->setScheme('mailto');
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withQuery('foo=argument&bar=hello&foo=anotherargument');
    }//end testInvalidQueryFromWithQueryThrowsException()

    /**
     * Invalid Query
     *
     * @return void
     */
    public function testInvalidQueryFromWithQueryExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\URI();
        $foo->setScheme('mailto');
        $this->expectExceptionMessage('is not a valid query string for the <code>mailto</code> scheme');
        $bar = $foo->withQuery('foo=argument&bar=hello&foo=anotherargument');
    }//end testInvalidQueryFromWithQueryExceptionMessage()
}//end class

?>