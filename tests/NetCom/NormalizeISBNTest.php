<?php
declare(strict_types=1);

/**
 * Unit testing for \Pipfrosch\NetCom\NormalizeISBN
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;

/**
 * Test class for \Pipfrosch\NetCom\NormalizeISBN
 */
// @codingStandardsIgnoreLine
final class NormalizeISBNTest extends TestCase
{
    /**
     * Tests calculation of check digit for 10 digit ISBN
     *
     * @return void
     */
    public function testCalculateCheckDigitForTenDigitIsbn()
    {
        $testCases = array(
            '194493425'   => '1',
            '099055716'   => '2',
            '155861872'   => '4',
            '099889206'   => '8',
            '0-330-28498' => '3',
            '1-58182-008' => '9',
            '2-226-05257' => '7',
            '3-7965-1900' => '8',
            '4-19-830127' => '1',
            '5-85270-001' => '0',
            '605-384-057' => '2',
            '7-301-10299' => '2',
            '80-85983-44' => '3',
            '954-430-603' => 'X',
            '975-293-381' => '5',
            '974-85854-7' => '6',
            '965-376-010' => '6',
            '965-222-779' => 'X',
        );
        foreach ($testCases as $key => $expected) {
            $key = (string) $key;
            $actual = \Pipfrosch\NetCom\NormalizeISBN::calculateChecksum($key);
            $this->assertSame($expected, $actual);
        }
    }//end testCalculateCheckDigitForTenDigitIsbn()

    /**
     * Tests calculation of check digit for 13 digit ISBN
     *
     * @return void
     */
    public function testCalculateCheckDigitForThirteenDigitIsbn()
    {
        $testCases = array(
            '978099055716'    => '6',
            '978099889206'    => '1',
            '978194493425'    => '5',
            '978155861872'    => '5',
            '978-0-06-052085' => '4',
            '978-602-8328-22' => '7',
            '978-607-455-035' => '1',
            '978-622-601-101' => '3',
            '979-10-90636-07' => '1',
            '979-11-86178-14' => '0',
            '979-12-200-0852' => '5',
        );
        foreach ($testCases as $key => $expected) {
            $key = (string) $key;
            $actual = \Pipfrosch\NetCom\NormalizeISBN::calculateChecksum($key);
            $this->assertSame($expected, $actual);
        }
    }//end testCalculateCheckDigitForThirteenDigitISBN()

    /**
     * Tests normalization of Prefix 978 for Group 0
     *
     * @return void
     */
    public function testNormalizePrefixNineSevenEightGroupZero()
    {
        $testCases = array(
            '006052085X' => '0-06-052085-X',
            '0062736841' => '0-06-273684-1',
            '0330284983' => '0-330-28498-3',
            '0517282879' => '0-517-28287-9',
            '0722508123' => '0-7225-0812-3',
            '0913028843' => '0-913028-84-3',
            '091302886X' => '0-913028-86-X',
        );
        foreach ($testCases as $key => $expected) {
            $key = (string) $key;
            $actual = \Pipfrosch\NetCom\NormalizeISBN::normalizeISBN($key);
            $this->assertSame($expected, $actual);
        }
    }//end testNormalizePrefixNineSevenEightGroupZero()

    /**
     * Tests normalization of Prefix 978 for Group 1
     *
     * @return void
     */
    public function testNormalizePrefixNineSevenEightGroupOne()
    {
        $testCases = array(
            '1015027725' => '1-01-502772-5',
            '1025418700' => '1-02-541870-0',
            '1045000450' => '1-04-500045-0',
            '1051002370' => '1-05-100237-0',
            '1068344245' => '1-06-834424-5',
            '1072230526' => '1-07-223052-6',
        );
        foreach ($testCases as $key => $expected) {
            $key = (string) $key;
            $actual = \Pipfrosch\NetCom\NormalizeISBN::normalizeISBN($key);
            $this->assertSame($expected, $actual);
        }
    }//end testNormalizePrefixNineSevenEightGroupOne()

    /**
     * Tests normalization of Prefix 978 for Group 965
     * I currently only know two publisher codes within this group
     *
     * @return void
     */
    public function testNormalizePrefixNineSevenEightGroupNineSixFive()
    {
        $testCases = array(
            '965222779X' => '965-222-779-X',
            '9653760106' => '965-376-010-6',
        );
        foreach ($testCases as $key => $expected) {
            $key = (string) $key;
            $actual = \Pipfrosch\NetCom\NormalizeISBN::normalizeISBN($key);
            $this->assertSame($expected, $actual);
        }
    }//end testNormalizePrefixNineSevenEightGroupNineSixFive()

    /**
     * Test conversion of 10 digit to 13 digit ISBN numbers.
     * The test values come from actual books I looked at with both displayed.
     *
     * @return void
     */
    public function testConvertTenDigitIsbnToThirteenDigitIsbn()
    {
        $testCases = array(
            '0020195508' => '978-0-02-019550-4',
            '032153395X' => '978-0-321-53395-1',
            '0062736841' => '978-0-06-273684-0',
            '0760722455' => '978-0-7607-2245-9',
            '0830731237' => '978-0-8307-3123-7',
            '0884190099' => '978-0-88419-009-7',
            '0927545438' => '978-0-927545-43-3',
            '1440585911' => '978-1-4405-8591-3',
            '1556613210' => '978-1-55661-321-0',
            '1576581365' => '978-1-57658-136-0',
            '162087623X' => '978-1-62087-623-7',
        );
        foreach ($testCases as $key => $expected) {
            $key = (string) $key;
            $actual = \Pipfrosch\NetCom\NormalizeISBN::normalizeISBN($key, true);
            $this->assertSame($expected, $actual);
        }
    }//end testConvertTenDigitIsbnToThirteenDigitIsbn()
}//end class

?>