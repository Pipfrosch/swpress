<?php
declare(strict_types=1);

/**
 * Unit testing for \Pipfrosch\NetCom\UrlEncode
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;

use \Pipfrosch\NetCom\UrlEncode;

/**
 * Test class for \Pipfrosch\NetCom\UrlEncode
 */
// @codingStandardsIgnoreLine
final class UrlEncodeTest extends TestCase
{
    /**
     * These should always be encoded.
     *
     * @var string
     */
    public $alwaysEncode = '% {}\\^`#!$' . "'" . '()*;<>';

    /**
     * These should always be expected.
     *
     * @var string
     */
    public $alwaysExpect = '%25%20%7B%7D%5C%5E%60%23%21%24%27%28%29%2A%3B%3C%3E';

    /**
     * Test case for control characters
     *
     * @return void
     */
    public function testEncodeControlCharacters()
    {
        $string = "\r\n\t" . rawurldecode('%7F');
        $expected = '%0D%0A%09%7F';
        $actual = UrlEncode::fragmentEncode($string);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::queryEncode($string);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::pathEncode($string);
        $this->assertSame($expected, $actual);
    }//end testEncodeControlCharacters()

    /**
     * Test case for reserved characters
     *
     * @return void
     */
    public function testEncodeReservedCharacters()
    {
        $fixme = '=&';
        $exfixme = '%3D%26';
        $string = $this->alwaysEncode ; // . $fixme;
        $expected = $this->alwaysExpect; // . $exfixme;
        $actual = UrlEncode::fragmentEncode($string);
        //var_dump($actual);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::queryEncode($string);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::pathEncode($string);
        $this->assertSame($expected, $actual);
    }//end testEncodeReservedCharacters()

    /**
     * Test case for fragment forbidden characters
     *
     * @return void
     */
    public function testFragmentSpecificEncode()
    {
        $string = $this->alwaysEncode . ':,@+[]|';
        $expected = $this->alwaysExpect . '%3A%2C%40%2B%5B%5D%7C';
        $actual = UrlEncode::fragmentEncode($string);
        $this->assertSame($expected, $actual);
        // adjust expected for query encode
        $expected = $this->alwaysExpect . ':,%40+[]|';
        $actual = UrlEncode::queryEncode($string);
        $this->assertSame($expected, $actual);
        // adjust expected for path encode
        $expected = $this->alwaysExpect . ':,@+%5B%5D%7C';
        $actual = UrlEncode::pathEncode($string);
        $this->assertSame($expected, $actual);
    }//end testFragmentSpecificEncode()

    /**
     * Test case for query forbidden characters
     *
     * @return void
     */
    public function testQuerySpecificEncode()
    {
        $string = $this->alwaysEncode . '@';
        $expected = $this->alwaysExpect . '%40';
        $actual = UrlEncode::queryEncode($string);
        $this->assertSame($expected, $actual);
        // no adjustment needed for fragment
        $actual = UrlEncode::fragmentEncode($string);
        $this->assertSame($expected, $actual);
        // adjust expected for path
        $expected = $this->alwaysExpect . '@';
        $actual = UrlEncode::pathEncode($string);
        $this->assertSame($expected, $actual);
    }//end testQuerySpecificEncode()

    /**
     * Test case for path forbidden characters
     *
     * @return void
     */
    public function testPathSpecificEncode()
    {
        $string = $this->alwaysEncode . '?[]|';
        $expected = $this->alwaysExpect . '%3F%5B%5D%7C';
        $actual = UrlEncode::pathEncode($string);
        $this->assertSame($expected, $actual);
        // adjust for fragment
        $expected = $this->alwaysExpect . '?%5B%5D%7C';
        $actual = UrlEncode::fragmentEncode($string);
        $this->assertSame($expected, $actual);
        // adjust for query
        $expected = $this->alwaysExpect . '?[]|';
        $actual = UrlEncode::queryEncode($string);
        $this->assertSame($expected, $actual);
    }//end testPathSpecificEncode()

    /**
     * Test case for double encoding
     *
     * @psalm-suppress TypeDoesNotContainType
     *
     * @return void
     */
    public function testAvoidDoubleEncoding()
    {
        $string   = '50%%20of%20all%20of%20all%20%23hashtags%20are%20foolish%20%28see reference%29';
        $expected = '50%25%20of%20all%20of%20all%20%23hashtags%20are%20foolish%20%28see%20reference%29';
        // verify string and expected are different
        $test = ($string === $expected);
        $this->assertFalse($test);
        // verify string and expected decode to same
        $test = (rawurldecode($string) === rawurldecode($expected));
        $this->assertTrue($test);
        // test the encoding of string matches expected
        $actual = UrlEncode::fragmentEncode($string);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::queryEncode($string);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::pathEncode($string);
        $this->assertSame($expected, $actual);
    }//end testAvoidDoubleEncoding()

    /**
     * Test case for unicode utf-8 encoding
     *
     * @return void
     */
    public function testEncodeUnicodeNonAscii()
    {
        // Japanese
        $string = '双重关系';
        $expected = '%E5%8F%8C%E9%87%8D%E5%85%B3%E7%B3%BB';
        $actual = UrlEncode::fragmentEncode($string);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::queryEncode($string);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::pathEncode($string);
        $this->assertSame($expected, $actual);
        // Korean
        $string = '박상민 결혼식';
        $expected = '%EB%B0%95%EC%83%81%EB%AF%BC%20%EA%B2%B0%ED%98%BC%EC%8B%9D';
        $actual = UrlEncode::fragmentEncode($string);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::queryEncode($string);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::pathEncode($string);
        $this->assertSame($expected, $actual);
        // Arabic
        $string = 'معارك';
        $expected = '%D9%85%D8%B9%D8%A7%D8%B1%D9%83';
        $actual = UrlEncode::fragmentEncode($string);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::queryEncode($string);
        $this->assertSame($expected, $actual);
        $actual = UrlEncode::pathEncode($string);
        $this->assertSame($expected, $actual);
    }//end testEncodeUnicodeNonAscii()
}//end class

?>