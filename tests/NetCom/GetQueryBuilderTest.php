<?php
declare(strict_types=1);

/**
 * Unit testing for \Pipfrosch\NetCom\GetQueryBuilder
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;

/**
 * Test class for \Pipfrosch\NetCom\URI
 */
// @codingStandardsIgnoreLine
final class GetQueryBuilderTest extends TestCase
{
    /**
     * Test simple queries.
     *
     * @return void
     */
    public function testSimpleQueries()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('foo' => 'value1', 'bar' => 'value2');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&foo=value1';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo->addKeyVal('fubare', 'value three');
        $expected = 'bar=value2&foo=value1&fubare=value%20three';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo->removeKey('foo');
        $expected = 'bar=value2&fubare=value%20three';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testSimpleQueries()

    /**
     * Test serialization of array arguments.
     *
     * @return void
     */
    public function testSerializeArrayValues()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('foo' => array());
        $foo->queryFromArray($arr);
        $arr = array('bar' => array('Value1'));
        $foo->queryFromArray($arr);
        $arr = array('fubar' => array('Value 2', 'Value 3'));
        $foo->queryFromArray($arr);
        $expected = 'bar=Value1&fubar=Value%202&fubar=Value%203';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testSerializeArrayValues()

    /**
     * Test Key Arrays.
     *
     * @return void
     */
    public function testKeyArrays()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $foo->addKeyVal('key[foo]', 'Value1');
        $foo->addKeyVal('key[bar]', 'Value2');
        $foo->addKeyVal('lumpy', 'snakeoil');
        $expected = 'key[bar]=Value2&key[foo]=Value1&lumpy=snakeoil';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testKeyArrays()

    /**
     * Test serialization of array arguments.
     *
     * @return void
     */
    public function testSerializeArrayValuesBrackets()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder(false, true);
        $arr = array('foo' => array());
        $foo->queryFromArray($arr);
        $arr = array('bar' => array('Value1'));
        $foo->queryFromArray($arr);
        $arr = array('fubar' => array('Value 2', 'Value 3'));
        $foo->queryFromArray($arr);
        $expected = 'bar=[Value1]&fubar=[Value%202,Value%203]';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testSerializeArrayValuesBrackets()

    /**
     * Test deserializing of input arguments.
     *
     * @return void
     */
    public function testDeserializeInputStrings()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('foo' => '[value1]', 'bar' => '[value2,value3,value4]');
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value5', 'bar' => '[value6,value7]');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&bar=value3&bar=value4&bar=value6&bar=value7&foo=value1&foo=value5';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testDeserializeInputStrings()

    /**
     * Test deserializing of input arguments.
     *
     * @return void
     */
    public function testDeserializeInputStringsBrackets()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder(false, true);
        $arr = array('foo' => '[value1]', 'bar' => '[value2,value3,value4]');
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value5', 'bar' => '[value6,value7]');
        $foo->queryFromArray($arr);
        $expected = 'bar=[value2,value3,value4,value6,value7]&foo=[value1,value5]';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testDeserializeInputStringsBrackets()

    /**
     * Test FIRST mode where first time a key is defined takes priority.
     *
     * @return void
     */
    public function testFirstMode()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $foo->setMode('FIRST');
        $arr = array('foo' => 'value1', 'bar' => 'value2');
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value3', 'fubar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&foo=value1&fubar=value4';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testFirstMode()

    /**
     * Test LAST mode where last time a key is defined takes priority.
     *
     * @return void
     */
    public function testLastMode()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $foo->setMode('LAST');
        $arr = array('foo' => 'value1', 'bar' => 'value2');
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value3', 'fubar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&foo=value3&fubar=value4';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testLastMode()

    /**
     * Test ARRAY mode where duplicate keys result in array.
     *
     * @return void
     */
    public function testArrayMode()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $foo->setMode('ARRAY');
        $arr = array('foo' => 'value1', 'bar' => 'value2');
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value3', 'fubar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&foo=value1&foo=value3&fubar=value4';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $foo->setMode('ARRAY');
        $arr = array('foo' => '[value1]', 'bar' => 'value2');
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value3', 'fubar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&foo=value1&foo=value3&fubar=value4';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $foo->setMode('ARRAY');
        $arr = array('foo' => 'value1', 'bar' => 'value2');
        $foo->queryFromArray($arr);
        $arr = array('foo' => '[value3,value5]', 'fubar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&foo=value1&foo=value3&foo=value5&fubar=value4';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testArrayMode()

    /**
     * Test ARRAY mode where duplicate keys result in array.
     *
     * @return void
     */
    public function testArrayModeBrackets()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder(false, true);
        $foo->setMode('ARRAY');
        $arr = array('foo' => 'value1', 'bar' => 'value2');
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value3', 'fubar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&foo=[value1,value3]&fubar=value4';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder(false, true);
        $foo->setMode('ARRAY');
        $arr = array('foo' => '[value1]', 'bar' => 'value2');
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value3', 'fubar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&foo=[value1,value3]&fubar=value4';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder(false, true);
        $foo->setMode('ARRAY');
        $arr = array('foo' => 'value1', 'bar' => 'value2');
        $foo->queryFromArray($arr);
        $arr = array('foo' => '[value3,value5]', 'fubar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&foo=[value1,value3,value5]&fubar=value4';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testArrayModeBrackets()

    /**
     * Test OPTIONARRAY mode where duplicate keys are only added to array
     * if first use was an array. This is default mode.
     *
     * @return void
     */
    public function testOptionArrayMode()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('foo' => 'value1', 'bar' => array('value2'));
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value3', 'bar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&bar=value4&foo=value1';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('foo' => '[value1]', 'bar' => array('value2'));
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value3', 'bar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=value2&bar=value4&foo=value1&foo=value3';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testOptionArrayMode()

    /**
     * Test OPTIONARRAY mode where duplicate keys are only added to array
     * if first use was an array. This is default mode.
     *
     * @return void
     */
    public function testOptionArrayModeBrackets()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder(false, true);
        $arr = array('foo' => 'value1', 'bar' => array('value2'));
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value3', 'bar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=[value2,value4]&foo=value1';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder(false, true);
        $arr = array('foo' => '[value1]', 'bar' => array('value2'));
        $foo->queryFromArray($arr);
        $arr = array('foo' => 'value3', 'bar' => 'value4');
        $foo->queryFromArray($arr);
        $expected = 'bar=[value2,value4]&foo=[value1,value3]';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testOptionArrayModeBrackets()

    /**
     * Test plus encoding instead of percent encoding for value.
     *
     * @return void
     */
    public function testPlusEncoding()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder(true);
        $arr = array('foo' => 'value 1', 'bar' => array('value 2', 'value 3'));
        $foo->queryFromArray($arr);
        $foo->addKeyVal('foobar', '[value 4,value 5]');
        $expected = 'bar=value+2&bar=value+3&foo=value+1&foobar=value+4&foobar=value+5';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testPlusEncoding()

    /**
     * Test plus encoding instead of percent encoding for value.
     *
     * @return void
     */
    public function testPlusEncodingBrackets()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder(true, true);
        $arr = array('foo' => 'value 1', 'bar' => array('value 2', 'value 3'));
        $foo->queryFromArray($arr);
        $foo->addKeyVal('foobar', '[value 4,value 5]');
        $expected = 'bar=[value+2,value+3]&foo=value+1&foobar=[value+4,value+5]';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testPlusEncodingBrackets()

    /**
     * Testing compliance with font server API
     *
     * @return void
     */
    public function testWebfontServerGetString()
    {
        $fam = array(
            'Merriweather:400,400i,700,700i,900,900i',
            'Montserrat:400,400i,700,700i',
            'Libre Franklin:400,400i',
            'Inconsolata:400,400i',
        );
        $sub = array(
            'latin',
            'latin-ext'
        );
        $arr = array();
        $arr['family'] = implode('|', $fam);
        $arr['subset'] = implode(',', $sub);
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder(true);
        $foo->queryFromArray($arr);
        $expected = 'family=Merriweather:400,400i,700,700i,900,900i|';
        $expected .= 'Montserrat:400,400i,700,700i|';
        $expected .= 'Libre+Franklin:400,400i|';
        $expected .= 'Inconsolata:400,400i';
        $expected .= '&subset=latin,latin-ext';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }// end testWebfontServerGetString()

    /**
     * Test that [] is allowed for end of key.
     *
     * @return void
     */
    public function testValidBracketsInKey()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $foo->setMode('ARRAY');
        $arr = array('foo[]' => 'value1');
        $foo->queryFromArray($arr);
        $arr = array('foo[]' => 'value2');
        $foo->queryFromArray($arr);
        $expected = 'foo[]=value1&foo[]=value2';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testValidBracketsInKey()


    /** Exception tests */

    /**
     * Test empty key throws exception.
     *
     * @return void
     */
    public function testEmptyKeyInArrayThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('' => 'value1');
        $this->expectException(\InvalidArgumentException::class);
        $foo->queryFromArray($arr);
    }//end testEmptyKeyInArrayThrowsException()

    /**
     * Test empty key throws exception.
     *
     * @return void
     */
    public function testEmptyKeyAsStringThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $this->expectException(\InvalidArgumentException::class);
        $foo->addKeyVal('', 'value1');
    }//end testEmptyKeyAsStringThrowsException()

    /**
     * Test empty key exception message.
     *
     * @return void
     */
    public function testEmptyKeyExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('' => 'value1');
        $this->expectExceptionMessage('parameter key may not be empty');
        $foo->queryFromArray($arr);
    }//end testEmptyKeyExceptionMessage()

    /**
     * Test invalid key throws exception.
     *
     * @return void
     */
    public function testInvalidKeyInArrayThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('foo bar' => 'value1');
        $this->expectException(\InvalidArgumentException::class);
        $foo->queryFromArray($arr);
    }//end testInvalidKeyInArrayThrowsException()

    /**
     * Test invalid key throws exception.
     *
     * @return void
     */
    public function testInvalidKeyAsStringThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $this->expectException(\InvalidArgumentException::class);
        $foo->addKeyVal('foo bar', 'value1');
    }//end testInvalidKeyAsStringThrowsException()

    /**
     * Test invalid key exception message.
     *
     * @return void
     */
    public function testInvalidKeySpaceExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('foo bar' => 'value1');
        $this->expectExceptionMessage('is not a valid key for GET parameters');
        $foo->queryFromArray($arr);
    }//end testInvalidKeySpaceExceptionMessage()

    /**
     * Test invalid key exception message.
     *
     * @return void
     */
    public function testInvalidKeyEqualsExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('foo=bar' => 'value1');
        $this->expectExceptionMessage('is not a valid key for GET parameters');
        $foo->queryFromArray($arr);
    }//end testInvalidKeyEqualsExceptionMessage()

    /**
     * Test invalid key exception message.
     *
     * @return void
     */
    public function testInvalidKeyAmpersandExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('foo&bar' => 'value1');
        $this->expectExceptionMessage('is not a valid key for GET parameters');
        $foo->queryFromArray($arr);
    }//end testInvalidKeyAmpersandExceptionMessage()

    /**
     * Test invalid key exception message.
     *
     * @return void
     */
    public function testInvalidKeyLeftBracketExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('foo[bar' => 'value1');
        $this->expectExceptionMessage('is not a valid key for GET parameters');
        $foo->queryFromArray($arr);
    }//end testInvalidKeyLeftBracketExceptionMessage()

    /**
     * Test invalid key exception message.
     *
     * @return void
     */
    public function testInvalidKeyRightBracketExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $arr = array('foo]bar' => 'value1');
        $this->expectExceptionMessage('is not a valid key for GET parameters');
        $foo->queryFromArray($arr);
    }//end testInvalidKeyRightBracketExceptionMessage()

    /**
     * Test invalid mode.
     *
     * @return void
     */
    public function testInvalidModeThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setMode('foo');
    }//end testInvalidModeThrowsException()

    /**
     * Test invalid mode exception message
     *
     * @return void
     */
    public function testInvalidModeExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $this->expectExceptionMessage('FIRST, LAST, ARRAY, OPTIONARRAY, and EXCEPTION');
        $foo->setMode('foo');
    }//end testInvalidModeExceptionMessage()

    /**
     * Test duplicate key in EXCEPTION mode.
     *
     * @return void
     */
    public function testDuplicateKeyThrowsException()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $foo->setMode('EXCEPTION');
        $arr = array('foo' => 'Value1', 'bar' => 'Value 2');
        $foo->queryFromArray($arr);
        $arr = array('fubar' => 'Value3', 'bar' => 'Value 4');
        $this->expectException(\InvalidArgumentException::class);
        $foo->queryFromArray($arr);
    }//end testDuplicateKeyThrowsException()

    /**
     * Test duplicate key in EXCEPTION mode message.
     *
     * @return void
     */
    public function testDuplicateKeyExceptionMessage()
    {
        $foo = new \Pipfrosch\NetCom\GetQueryBuilder();
        $foo->setMode('EXCEPTION');
        $arr = array('foo' => 'Value1', 'bar' => 'Value 2');
        $foo->queryFromArray($arr);
        $arr = array('fubar' => 'Value3', 'bar' => 'Value 4');
        $this->expectExceptionMessage('already has a value assigned to it');
        $foo->queryFromArray($arr);
    }//end testDuplicateKeyExceptionMessage()
}//end class

?>