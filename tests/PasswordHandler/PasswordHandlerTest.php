<?php
declare(strict_types=1);

/**
 * Unit testing for \Pipfrosch\PasswordHandler\PasswordHandler
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;
use \Pipfrosch\PasswordHandler\PasswordHandler;

/**
 * Test class for \Pipfrosch\PasswordHandler
 */
// @codingStandardsIgnoreLine
final class PasswordHandlerTest extends TestCase
{
    /**
     * Test upgrade of weak hashes to modern hashes
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testWeakHashUpgrade(): void
    {
        $ph = new PasswordHandler();
        $password = 'VeryWeakPassword';
        $weakHash = crypt($password, '$1$qwertyui$');
        $testme = $ph->validatePassword($password, $weakHash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $valid = $ph->validatePassword($password, $testme);
        if (is_string($valid)) {
            $this->assertStringContainsString('$argon2id$', $valid);
        } else {
            $this->assertTrue($valid);
        }
        $weakHash = crypt($password, '$2a$07$qwertyuioplkjhgfds8412$');
        $testme = $ph->validatePassword($password, $weakHash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $valid = $ph->validatePassword($password, $testme);
        if (is_string($valid)) {
            $this->assertStringContainsString('$argon2id$', $valid);
        } else {
            $this->assertTrue($valid);
        }
        $weakHash = crypt($password, '$5$rounds=7000$poiuytrewqasdfgh$');
        $testme = $ph->validatePassword($password, $weakHash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $valid = $ph->validatePassword($password, $testme);
        if (is_string($valid)) {
            $this->assertStringContainsString('$argon2id$', $valid);
        } else {
            $this->assertTrue($valid);
        }
        $weakHash = crypt($password, '$6$rounds=6500$qazxswedcvfrtgbn$');
        $testme = $ph->validatePassword($password, $weakHash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $valid = $ph->validatePassword($password, $testme);
        if (is_string($valid)) {
            $this->assertStringContainsString('$argon2id$', $valid);
        } else {
            $this->assertTrue($valid);
        }
    }//end testWeakHashUpgrade()

    /**
     * Test upgrade of bcrypt cost
     *
     * @psalm-suppress PossiblyInvalidArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testBcryptUpdateHashCost(): void
    {
        $password = 'Password123456';
        $stockCostHash = password_hash($password, PASSWORD_BCRYPT);
        $this->assertStringContainsString('$2y$10$', $stockCostHash);
        $ph = new PasswordHandler('standard', 'bcrypt');
        $standardHash = $ph->validatePassword($password, $stockCostHash);
        $this->assertStringContainsString('$2y$14$', $standardHash);
        $ph = new PasswordHandler('moderate', 'bcrypt');
        $moderateHash = $ph->validatePassword($password, $standardHash);
        $this->assertStringContainsString('$2y$15$', $moderateHash);
        $ph = new PasswordHandler('high', 'bcrypt');
        $highHash = $ph->validatePassword($password, $moderateHash);
        $this->assertStringContainsString('$2y$16$', $highHash);
    }//end testBcryptUpdateHashCost()

    /**
     * Test upgrade of argon2i cost
     *
     * @psalm-suppress PossiblyInvalidArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoUpdateHashCost(): void
    {
        $password = 'Password123456';
        // argon2i
        $stockCostHash = password_hash($password, PASSWORD_ARGON2I);
        $this->assertStringContainsString('$argon2i$v=19$m=1024,t=2,p=2$', $stockCostHash);
        $ph = new PasswordHandler('standard', 'argon2i');
        $standardHash = $ph->validatePassword($password, $stockCostHash);
        $this->assertStringContainsString('$argon2i$v=19$m=131072,t=4,p=2$', $standardHash);
        $ph = new PasswordHandler('moderate', 'argon2i');
        $moderateHash = $ph->validatePassword($password, $standardHash);
        $this->assertStringContainsString('$argon2i$v=19$m=262144,t=6,p=4$', $moderateHash);
        $ph = new PasswordHandler('high', 'argon2i');
        $highHash = $ph->validatePassword($password, $moderateHash);
        $this->assertStringContainsString('$argon2i$v=19$m=524288,t=8,p=6$', $highHash);
        // argon2id
        $stockCostHash = password_hash($password, PASSWORD_ARGON2ID);
        $this->assertStringContainsString('$argon2id$v=19$m=1024,t=2,p=2$', $stockCostHash);
        $ph = new PasswordHandler('standard');
        $standardHash = $ph->validatePassword($password, $stockCostHash);
        $this->assertStringContainsString('$argon2id$v=19$m=131072,t=4,p=2$', $standardHash);
        $ph = new PasswordHandler('moderate');
        $moderateHash = $ph->validatePassword($password, $standardHash);
        $this->assertStringContainsString('$argon2id$v=19$m=262144,t=6,p=4$', $moderateHash);
        $ph = new PasswordHandler('high');
        $highHash = $ph->validatePassword($password, $moderateHash);
        $this->assertStringContainsString('$argon2id$v=19$m=524288,t=8,p=6$', $highHash);
    }//end testArgonTwoUpdateHashCost()

    /**
     * Test bcrypt2 length limitation
     *
     * @return void
     */
    public function testBcryptPasswordTruncationWithoutPasswordNormalization(): void
    {
        $passwordOne='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ00112233445566778899@@';
        $passwordTwo='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ00112233445566778899%%';
        $this->assertNotSame($passwordOne, $passwordTwo);
        $ph = new PasswordHandler('standard', 'bcrypt');
        $hash = $ph->hashPassword($passwordOne);
        $testme = $ph->validatePassword($passwordTwo, $hash);
        if (is_string($testme)) {
            $this->assertStringContainsString('$2y$', $testme);
        } else {
            $this->assertTrue($testme);
        }
    }//end testBcryptPasswordTruncationWithoutPasswordNormalization()

    /**
     * Test bcrypt2 length limitation
     *
     * @return void
     */
    public function testBcryptPasswordNoTruncationWithPasswordNormalization(): void
    {
        define('PASSWORD_HANDLER_SITE_SALT', 'This is a salt string for password normalization');
        $passwordOne='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ00112233445566778899@@';
        $passwordTwo='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ00112233445566778899%%';
        $this->assertNotSame($passwordOne, $passwordTwo);
        $ph = new PasswordHandler('standard', 'bcrypt');
        $hash = $ph->hashPassword($passwordOne);
        $testme = $ph->validatePassword($passwordTwo, $hash);
        $this->assertFalse($testme);
        $testme = $ph->validatePassword($passwordOne, $hash);
        if (is_string($testme)) {
            $this->assertStringContainsString('$2y$', $testme);
        } else {
            $this->assertTrue($testme);
        }
    }//end testBcryptPasswordNoTruncationWithPasswordNormalization()

    /**
     * Test hash made without normalization passes with update to normalized
     *
     * @psalm-suppress PossiblyInvalidArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testBcryptPasswordNotNormalizedPassesWithHashUpgrade(): void
    {
        $passwordOne='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ00112233445566778899@@';
        $passwordTwo='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ00112233445566778899%%';
        $options = array('cost' => 14);
        $NotNormalized = password_hash($passwordOne, PASSWORD_BCRYPT, $options);
        $ph = new PasswordHandler('standard', 'bcrypt');
        $testme = $ph->validatePassword($passwordTwo, $NotNormalized);
        $this->assertStringContainsString('$2y$14$', $testme);
        // PasswordOne should now fail
        $testme = $ph->validatePassword($passwordOne, $testme);
        $this->assertFalse($testme);
    }//end testBcryptPasswordNotNormalizedPassesWithHashUpgrade()

    /**
     * Test invalid archaic md5 hash
     *
     * @return void
     */
    public function testArchaicMdfiveBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = $ph->generatePassword();
        $hash = md5('incorrect');
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testArchaicMdfiveBadPassword()

    /**
     * Test valid archaic md5 hash
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testArchaicMdfiveValidPassword(): void
    {
        $ph = new PasswordHandler('standard', 'bcrypt');
        $password = $ph->generatePassword();
        $hash = md5($password);
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$2y$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$2y$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testArchaicMdfiveValidPassword()

    /**
     * Test invalid archaic sha1 hash
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testArchaicShaoneBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = $ph->generatePassword();
        $hash = hash('sha1', 'incorrect', false);
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testArchaicShaoneBadPassword()

    /**
     * Test valid archaic sha1 hash
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testArchaicShaoneValidPassword(): void
    {
        $ph = new PasswordHandler('standard', 'bcrypt');
        $password = $ph->generatePassword();
        $hash = hash('sha1', $password, false);
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$2y$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$2y$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testArchaicShaoneValidPassword()

    /**
     * Test invalid archaic sha256 hash
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testArchaicShatwoBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = $ph->generatePassword();
        $hash = hash('sha256', 'incorrect', false);
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testArchaicShatwoBadPassword()

    /**
     * Test valid archaic sha256 hash
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testArchaicShatwoValidPassword(): void
    {
        $ph = new PasswordHandler('standard', 'bcrypt');
        $password = $ph->generatePassword();
        $hash = hash('sha256', $password, false);
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$2y$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$2y$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testArchaicShatwoValidPassword()

    /**
     * Test invalid phpass portable
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testPhpassPortableBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'incorrect';
        $hash = '$P$BOgM2Zv7JSgOSTOBnDadiChTYNPKNb0';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testPhpassPortableBadPassword()

    /**
     * Test valid phpass portable
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testPhpassPortableValidPassword(): void
    {
        $ph = new PasswordHandler('standard', 'bcrypt');
        $password = 'secret';
        $hash = '$P$BOgM2Zv7JSgOSTOBnDadiChTYNPKNb0';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$2y$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$2y$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testPhpassPortableValidPassword()

    /**
     * Test invalid blowfish generated via phpass
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testPhpassBlowfishBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'incorrect';
        $hash = '$2a$08$K51apaI6KooXaSF906OVs.DV36pdyhDoYoKT/e2V.FOogO8P.WQaq';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testPhpassBlowfishBadPassword()

    /**
     * Test valid blowfish generated via phpass
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testPhpassBlowfishValidPassword(): void
    {
        $ph = new PasswordHandler('standard', 'bcrypt');
        $password = 'secret';
        $hash = '$2a$08$K51apaI6KooXaSF906OVs.DV36pdyhDoYoKT/e2V.FOogO8P.WQaq';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$2y$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$2y$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testPhpassBlowfishValidPassword()

    /**
     * Test invalid standard DES generated via crypt
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testStandardDesBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'incorrect';
        $hash = 'LarGOdGngF5Wg';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testStandardDesBadPassword()

    /**
     * Test valid standard DES generated via crypt
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testStandardDesValidPassword(): void
    {
        $ph = new PasswordHandler('standard', 'bcrypt');
        $password = 'secret';
        $hash = 'LarGOdGngF5Wg';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$2y$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$2y$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testStandardDesValidPassword()

    /**
     * Test invalid extended DES generated via crypt
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testExtendedDesBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'incorrect';
        $hash = '_J9..saltLXKE4peqNCg';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testExtendedDesBadPassword()

    /**
     * Test valid extended DES generated via crypt
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testExtendedDesValidPassword(): void
    {
        $ph = new PasswordHandler('standard', 'bcrypt');
        $password = 'secret';
        $hash = '_J9..saltLXKE4peqNCg';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$2y$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$2y$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testExtendedDesValidPassword()

    /**
     * Test invalid crypt md5
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testCryptMdfiveBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'incorrect';
        $hash = '$1$dfvsqwel$2zSxYPLTa6oH34yJ2UTBz0';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testCryptMdfiveBadPassword()

    /**
     * Test valid crypt md5
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testCryptMdfiveValidPassword(): void
    {
        $ph = new PasswordHandler('standard', 'bcrypt');
        $password = 'secret';
        $hash = '$1$dfvsqwel$2zSxYPLTa6oH34yJ2UTBz0';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$2y$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$2y$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testCryptMdfiveValidPassword()

    /**
     * Test invalid crypt blowfish
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testCryptBlowfishBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'incorrect';
        $hash = '$2x$10$SodiumChlorideCrystalezM763Oq6JYHeQfuDBstThyNo1rTB2Ra';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testCryptBlowfishBadPassword()

    /**
     * Test valid crypt blowfish
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testCryptBlowfishValidPassword(): void
    {
        $ph = new PasswordHandler('standard', 'bcrypt');
        $password = 'secret';
        $hash = '$2x$10$SodiumChlorideCrystalezM763Oq6JYHeQfuDBstThyNo1rTB2Ra';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$2y$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$2y$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testCryptBlowfishValidPassword()

    /**
     * Test invalid crypt sha256
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testCryptShaTwofiftysixBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'incorrect';
        $hash = '$5$rounds=10000$SodiumChloride$p7s4tuWGblfR6MhPAQUmuzwVfk3w1wD4L8dSLKrk1/8';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testCryptShaTwofiftysixBadPassword()

    /**
     * Test valid crypt sha256
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testCryptShaTwofiftysixValidPassword(): void
    {
        $ph = new PasswordHandler('standard', 'bcrypt');
        $password = 'secret';
        $hash = '$5$rounds=10000$SodiumChloride$p7s4tuWGblfR6MhPAQUmuzwVfk3w1wD4L8dSLKrk1/8';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$2y$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$2y$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testCryptShaTwofiftysixValidPassword()

    /**
     * Test invalid crypt sha512
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testCryptShaFivetwelveBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'incorrect';
        // @codingStandardsIgnoreLine
        $hash = '$6$rounds=10000$SodiumChloride$nFG1ZDFM8WUyNlGUUuPbdrFOFSdpsNF7XAPm6MAM/xFQnNNS8QkixTHm2DCdlbfcVf1MlL.veUV1V4skVoAl9/';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testCryptShaFivetwelveBadPassword()

    /**
     * Test valid crypt sha512
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testCryptShaFivetwelveValidPassword(): void
    {
        $ph = new PasswordHandler('standard', 'bcrypt');
        $password = 'secret';
        // @codingStandardsIgnoreLine
        $hash = '$6$rounds=10000$SodiumChloride$nFG1ZDFM8WUyNlGUUuPbdrFOFSdpsNF7XAPm6MAM/xFQnNNS8QkixTHm2DCdlbfcVf1MlL.veUV1V4skVoAl9/';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$2y$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$2y$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testCryptShaFivetwelveValidPassword()

    /**
     * Test sodium compat mode
     *
     * @return void
     */
    public function testSodiumCompatibilityMode(): void
    {
        $ph = new PasswordHandler('standard', 'argon2id', true);
        $password = $ph->generatePassword();
        $hash = $ph->hashPassword($password);
        $this->assertStringContainsString('$m=262144,t=4,p=1$', $hash);
    }//end testSodiumCompatibilityMode()

    /**
     * Test sodium compat mode forces rehash. As the initial hash is created using
     *  high, it will have parameters large enough that a rehash under normal conditions
     *  would only happen with the 1 in 7 odds, which we account for with a loop.
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testSodiumCompatibilityModeForcesRehash(): void
    {
        for ($i=0; $i<10; $i++) {
            $ph = new PasswordHandler('high', 'argon2id', false);
            $password = $ph->generatePassword();
            $hash = $ph->hashPassword($password);
            $this->assertStringContainsString('$m=524288,t=8,p=6$', $hash);
            $ph = new PasswordHandler('standard', 'argon2id', true);
            $rs = $ph->validatePassword($password, $hash);
            $this->assertStringContainsString('$m=262144,t=4,p=1$', $rs);
            $testagain = $ph->validatePassword($password, $rs);
            if (is_string($testagain)) {
                $this->assertStringContainsString('$m=262144,t=4,p=1$', $testagain);
            } else {
                $this->assertTrue($testagain);
            }
        }
    }//end testSodiumCompatibilityModeForcesRehash()

    /**
     * Test WordPress 2 hash format Bad Password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testWordpressTwoHashBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'invalid';
        $hash = '5ebe2294ecd0e0f08eab7690d2a6ee69';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testWordpressTwoHashBadPassword()

    /**
     * Test WordPress 2 hash format Valid Password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testWordpressTwoHashValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = '5ebe2294ecd0e0f08eab7690d2a6ee69';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testWordpressTwoHashValidPassword()

    /**
     * Test WordPress 3 hash format bad password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testWordpressThreeAndFourHashBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'invalid';
        $hash = '$1$Cc77OJep$AH79AtsmwB8YspThcmvjz/';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testWordpressThreeAndFourHashBadPassword()

    /**
     * Test WordPress 3 hash format valid password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testWordpressThreeAndFourHashValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = '$1$Cc77OJep$AH79AtsmwB8YspThcmvjz/';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testWordpressThreeAndFourHashValidPassword()

    /**
     * Test older Joomla bad password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testJoomlaPreBcryptHashBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'invalid';
        $hash = '6c56e93ed2455a7c9697e4803888694c:KWeFyco69WxAG7UhbddxUoQqCkmxOPs';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testJoomlaPreFiveHashBadPassword()

    /**
     * Test older Joomla valid password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testJoomlaPreBcryptHashValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = '6c56e93ed2455a7c9697e4803888694c:KWeFyco69WxAG7UhbddxUoQqCkmxOPs';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testJoomlaPreFiveHashValidPassword()

    /**
     * Apache md5 bad
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testApacheMdFiveBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'invalid';
        $hash = '$1$PVlaUNLQ$eC50oplLZ4w/elZ3sQgGg0';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testApacheMdFiveBadPassword()

    /**
     * Apache md5 valid
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testApacheMdFiveValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = '$1$PVlaUNLQ$eC50oplLZ4w/elZ3sQgGg0';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testApacheMdFiveValidPassword()

    /**
     * Drupal 6 (same as WP 2) bad password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testDrupalSixBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'invalid';
        $hash = '5ebe2294ecd0e0f08eab7690d2a6ee69';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testDrupalSixBadPassword()

    /**
     * Drupal 6 (same as WP 2) valid password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testDrupalSixValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = '5ebe2294ecd0e0f08eab7690d2a6ee69';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testDrupalSixValidPassword()

    /**
     * Actual Drupal 7 bad password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testDrupalSevenBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'invalid';
        $hash = '$S$5juRVH.IfPeqY6b3r75YZH/f0jiAdUWKeiUCG/Onsz9WqE93OUEN';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testDrupalSevenBadPassword()

    /**
     * Actual Drupal 7 valid password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testDrupalSevenValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = '$S$5juRVH.IfPeqY6b3r75YZH/f0jiAdUWKeiUCG/Onsz9WqE93OUEN';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testDrupalSevenValidPassword()

    /**
     * Emulated Drupal 7 sha512 test
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testEmulatedDrupalSevenShaFiveTwelveValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = '$S$BOgM2Zv7JWBriABUgNY2XLrjfh8OROkEYbFsvcVzpYgEObGbSjq871ribRRbgQiZr5W13c5u.YZTyE0Ad4qXCPt0p0LA//0';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
        $hash = '$S$BOgM2Zv7JWBriABUgNY2XLrjfh8OROkEYbFsvcVzpYgEObGbSjq871ribRRbgQiZr5W13c5u.';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
        $hash = '$S$BOgM2Zv7JWBriABUgNY2XLrjfh8OROkEYbFsvcVzpYgEObGbSjq8';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
        $hash = '$S$BOgM2Zv7JWBriABUgNY2XLrjfh8OROkEYbFs';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
        $hash = '$S$BOgM2Zv7JWBriABUgNY2XLrjfh8OROk';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testEmulatedDrupalSevenShaFiveTwelveValidPassword()

    /**
     * Emulated Drupal 7 sha384 test
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testEmulatedDrupalSevenShaThreeEightyfourValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = '$S$BOgM2Zv7JWBriABUgNY2XLrjfh8OROkEYbFsvcVzpYgEObGbSjq871ribRRbgQiZr5W13c5u.YZTyE0Ad4qXCPt0p0LA//0';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
        $hash = '$S$BOgM2Zv7J3sXFKHlcO5pBcOuIT3jejOFm.SiyoDZvui6.xRO6X83';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
        $hash = '$S$BOgM2Zv7J3sXFKHlcO5pBcOuIT3jejOFm.Si';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
        $hash = '$S$BOgM2Zv7J3sXFKHlcO5pBcOuIT3jejO';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testEmulatedDrupalSevenShaThreeEightyfourValidPassword()

    /**
     * Emulated Drupal 7 sha256 test
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testEmulatedDrupalSevenShaTwoFiftysixValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = '$S$BOgM2Zv7JXRRivP2I3bFhD9hElarnvC1RfMyLlZ5/EA7mKsJsuJ9';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
        $hash = '$S$BOgM2Zv7JXRRivP2I3bFhD9hElarnvC1RfMy';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
        $hash = '$S$BOgM2Zv7JXRRivP2I3bFhD9hElarnvC';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testEmulatedDrupalSevenShaTwoFiftysixValidPassword()

    /**
     * Emulated Drupal 7 sha1 test
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testEmulatedDrupalSevenShaOneValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = '$S$BOgM2Zv7JIw/LGA.DpLdq/OCFPOpTHHYePn2';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
        $hash = '$S$BOgM2Zv7JIw/LGA.DpLdq/OCFPOpTHH';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testEmulatedDrupalSevenShaOneValidPassword()

    /**
     * Emulated Drupal 7 md5 test
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testEmulatedDrupalSevenMdfiveValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = '$S$BOgM2Zv7Jcqh1Fz00D9nD07Q12PouE0';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testEmulatedDrupalSevenMdfiveValidPassword()

    /**
     * Drupal Gallery bad password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testDrupalGalleryBadPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'invalid';
        $hash = '$S$BOgM2Zv7J3sXFKHlcO5pBcOuIT3jejOFm.SiyoDZvui6.xRO6X839uQ2FTyzPZuDgnKgwJXFF';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertFalse($testme);
    }//end testDrupalGalleryBadPassword()

    /**
     * Drupal Gallery valid password
     *
     * @psalm-suppress PossiblyInvalidArgument
     *
     * @return void
     */
    public function testDrupalGalleryValidPassword(): void
    {
        $ph = new PasswordHandler();
        $password = 'secret';
        $hash = 'CyQOCNjvxSl9qQSx6VgyZSev5t4suquf09c34b8c264e044d11c31df57bb3d74';
        $testme = $ph->validatePassword($password, $hash);
        $this->assertStringContainsString('$argon2id$', $testme);
        $testagain = $ph->validatePassword($password, $testme);
        if (is_string($testagain)) {
            $this->assertStringContainsString('$argon2id$', $testagain);
        } else {
            $this->assertTrue($testagain);
        }
    }//end testDrupalGalleryValidPassword()
}//end class

?>