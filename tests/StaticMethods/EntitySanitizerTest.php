<?php
declare(strict_types=1);

/**
 * Unit testing for \SWPress\Core\EntitySanitizer class
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;

/**
 * Test class for \SWPress\Core\EntitySanitizer
 */
// @codingStandardsIgnoreLine
final class EntitySanitizerTest extends TestCase {
    /**
     * HTML Entities
     *
     * @return void
     */
    public function testHtmlNamedEntityToNumberedArraySanity()
    {
        $blacklist = array('&apos;');
        $array = \SWPress\Core\EntitySanitizer::getHtmlEntityMap();
        foreach ($array as $key => $expected) {
            if (! in_array($key, $blacklist)) {
                $actual = \SWPress\Core\EntitySanitizer::namedToNumbered($key);
                $this->assertSame($expected, $actual);
                $a = html_entity_decode($key);
                $b = html_entity_decode($expected);
                $this->assertSame($a, $b);
            }
        }
    }

    /**
     * Normalize numbered entities
     *
     * @return void
     */
    public function testNormalizeNumberedEntities()
    {
        $expected = '&#34;';
        $actual = \SWPress\Core\EntitySanitizer::normalizeNumbereredEntities('&#034;');
        $this->assertSame($expected, $actual);
        $expected = 'This&#160;is a test.';
        $actual = \SWPress\Core\EntitySanitizer::normalizeNumbereredEntities('This&#160is a test.');
        $this->assertSame($expected, $actual);
    }

    /**
     * Make string safe
     *
     * @return void
     */
    public function testStringSanitizer()
    {
        $expected = 'I like pie &amp; ice cream &gt; than I like &#8220;Pie a-la mode&#8221;.';
        $string = 'I like pie & ice cream > than I like &ldquo;Pie a-la mode&rdquo;.';
        $actual = \SWPress\Core\EntitySanitizer::stringSanitizer($string);
        $this->assertSame($expected, $actual);
    }
}