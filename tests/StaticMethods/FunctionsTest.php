<?php
declare(strict_types=1);

/**
 * Unit testing for \SWP (static methods from WP wp-includes/functions.php)
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;

/**
 * Test class for \SWP
 */
// @codingStandardsIgnoreLine
final class FunctionsTest extends TestCase {

    /**
     * The absint static method test
     *
     * @return void
     */
    public function testAbsint()
    {
        $maybeint = 5;
        $expected = 5;
        $actual = \SWP::absint($maybeint);
        $this->assertSame($expected, $actual);
        $maybeint = -5;
        $expected = 5;
        $actual = \SWP::absint($maybeint);
        $this->assertSame($expected, $actual);
        $maybeint = "5.0";
        $expected = 5;
        $actual = \SWP::absint($maybeint);
        $this->assertSame($expected, $actual);
        $maybeint = "-5.0";
        $expected = 5;
        $actual = \SWP::absint($maybeint);
        $this->assertSame($expected, $actual);
        $maybeint = "5.1";
        $expected = 5;
        $actual = \SWP::absint($maybeint);
        $this->assertSame($expected, $actual);
        $maybeint = "-5.1";
        $expected = 5;
        $actual = \SWP::absint($maybeint);
        $this->assertSame($expected, $actual);
        $maybeint = false;
        $expected = 0;
        $actual = \SWP::absint($maybeint);
        $this->assertSame($expected, $actual);
        $maybeint = true;
        $expected = 1;
        $actual = \SWP::absint($maybeint);
        $this->assertSame($expected, $actual);
    }//end testAbsint()

    /**
     * The addQueryArg method - replacement for WP add_query_arg()
     *
     * @return void
     */
    public function testAddQueryArg()
    {
        $existingUri = 'http://example.com/?argA=First%20Argument#somehash';
        $expected = 'http://example.com/?argA=First+Argument&argB=Second+Argument&argC=Third+Argument#somehash';
        $arr = array('argB' => 'Second Argument', 'argC' => 'Third Argument');
        $actual = \SWP::addQueryArg($arr, $existingUri);
        $this->assertSame($expected, $actual);
        $arr = array('argA' => false);
        $expected = 'http://example.com/?argB=Second+Argument&argC=Third+Argument#somehash';
        $actual = \SWP::addQueryArg($arr, $expected);
        $this->assertSame($expected, $actual);
    }//end testAddQueryArg()

    /**
     * The currentTime method - replacement for WP current_time()
     *
     * @return void
     */
    public function testCurrentTime()
    {
        // race condition but should not ever differ by more than a second
        $ts = time();
        $expected = date('Y-m-d H:i:s', $ts);
        $actual = (string) \SWP::currentTime('mysql', true);
        $this->assertRegExp('/^20[1-9][0-9]-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]$/', $actual);
        $diff = \SWP::absint(strtotime($actual) - strtotime($expected));
        $this->assertLessThan(2, $diff);
        $actual = intval(\SWP::currentTime('timestamp', true), 10);
        $diff = \SWP::absint($actual - $ts);
        $this->assertLessThan(2, $diff);
    }//end testCurrentTime()

    /**
     * The getAllowedProtocols method - replacement for WP wp_allowed_protocols()
     *
     * @return void
     */
    public function testGetAllowedProtocols()
    {
        $arr = \SWP::getAllowedProtocols();
        $test = in_array('https', $arr);
        $this->assertTrue($test);
        $test = in_array('ftp', $arr);
        $this->assertTrue($test);
        $test = in_array('skype', $arr);
        $this->assertFalse($test);
    }

    /**
     * The getStatusHeaderDesc method - replacement for WP get_status_header_desc()
     *
     * @return void
     */
    public function testGetStatusHeaderDesc()
    {
        $expected = 'Not Found';
        $actual = \SWP::getStatusHeaderDesc(404);
        $this->assertSame($expected, $actual);
        $expected = 'Non-Authoritative Information';
        $actual = \SWP::getStatusHeaderDesc(203);
        $this->assertSame($expected, $actual);
        $expected = "I'm a teapot";
        $actual = \SWP::getStatusHeaderDesc(418);
        $this->assertSame($expected, $actual);
        $expected = '';
        $actual = \SWP::getStatusHeaderDesc(20);
        $this->assertSame($expected, $actual);
    }//end testGetStatusHeaderDesc()

    /**
     * The httpBuildQuery method - replacement for WP _http_build_query
     *
     * @return void
     */
    public function testHttpBuildQuery()
    {
        $expected = 'greek[alpha]=%CE%91+%CE%B1&greek[beta]=%CE%92+%CE%B2&greek[gamma]=%CE%93+%CE%B3';
        $data = array();
        $data['alpha'] = 'Α α';
        $data['beta']  = 'Β β';
        $data['gamma'] = 'Γ γ';
        $actual = \SWP::httpBuildQuery($data, null, null, 'greek');
        $this->assertSame($expected, $actual);
    }//end testHttpBuildQuery()

    /**
     * The isStream method - replacement for WP wp_is_stream
     *
     * @return void
     */
    public function testIsStream()
    {
        $actual = \SWP::isStream('http://some.url');
        $this->assertTrue($actual);
        $actual = \SWP::isStream('ftp://ftp.warez.net');
        $this->assertTrue($actual);
        $actual = \SWP::isStream('urn:ietf:rfc:8601');
        $this->assertFalse($actual);
    }//end testIsStream()

    /**
     * The mysql2date method - same function name in WP.
     *
     * @return void
     */
    public function testMysqlToDate()
    {
        $expected = 95105880;
        $actual = \SWP::mysql2date('G', '1973-01-05 18:18:00', false);
        $this->assertSame($expected, $actual);
        $expected = 'January 5, 1973 at 6:18 PM UTC';
        $actual = \SWP::mysql2date('F j, Y \a\t g:i A e', '1973-01-05 18:18:00');
        $this->assertSame($expected, $actual);
    }//end testMysqlToDate()

    /**
     * The normalizePath method - replacement for WP wp_normalize_path
     *
     * @return void
     */
    public function testNormalizePath()
    {
        $expected = 'C:/users/alice/wonder/DATA.DOC';
        $actual = \SWP::normalizePath('c:\\users\alice\wonder\DATA.DOC');
        $this->assertSame($expected, $actual);
    }//end testNormalizePath()

    /**
     * The removeQueryArg method - replacement for WP remove_query_arg()
     *
     * @return void
     */
    public function testRemoveQueryArg()
    {
        $existingUri = 'http://example.com/?argA=First+Argument&argB=Second+Argument&argC=Third+Argument#somehash';
        $expected = 'http://example.com/?argC=Third+Argument#somehash';
        $arr = array('argA', 'argB');
        $actual = \SWP::removeQueryArg($arr, $existingUri);
        $this->assertSame($expected, $actual);
        $expected = 'http://example.com/#somehash';
        $actual = \SWP::removeQueryArg('argC', $actual);
        $this->assertSame($expected, $actual);
    }//end testRemoveQueryArg()
}//end class

?>