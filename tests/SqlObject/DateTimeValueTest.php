<?php
declare(strict_types=1);

/**
 * Unit testing for \Pipfrosch\SqlObject\DateTimeValue
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;
use \Pipfrosch\SqlObject\DateTimeValue;
use \Pipfrosch\SqlObject\TimeStampValue;

date_default_timezone_set('UTC');

/**
 * Test class for \Pipfrosch\SqlObject\IntegerValue
 */
// @codingStandardsIgnoreLine
final class DateTimeValueTest extends TestCase {
    /**
     * Test Time Object
     *
     * @return void
     */
    public function testTimeValueObject(): void
    {
        $foo = new DateTimeValue();
        $foo->setValue('19:23:45.234123', 'time');
        $expected = 'TIME';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '19:23:45.234123';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $ts = 1555580886;
        $foo->valueFromEpoch($ts);
        $expected = '09:48:06';
        $utc = $foo->getValue();
        $this->assertSame($expected, $utc);
        $foo->setTimeZone('America/Los_Angeles');
        $foo->valueFromEpoch($ts);
        $expected = '02:48:06';
        $pacific = $foo->getValue();
        $this->assertSame($expected, $pacific);
        $dateTime = $foo->getDateTimeObject();
        $expected = 'PDT';
        $actual = $dateTime->format('T');
        $this->assertSame($expected, $actual);
        $foo->setTimeZone('Australia/Broken_Hill');
        $foo->valueFromDateTime($dateTime);
        $expected = '19:18:06';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withYear();
        $expected = '2019';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withDate();
        $expected = '2019-04-18';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withDateTime();
        $expected = '2019-04-18 19:18:06';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $foo->setTimeZone('America/Los_Angeles');
        // > 32-bit time ts
        $foo->valueFromEpoch(2284924097);
        $expected = '13:08:17';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<value type="TEMPORAL" subtype="TIME" tz="PDT">13:08:17</value>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testTimeValueObject()

    /**
     * Test Time Object Without Time Exception
     *
     * @return void
     */
    public function testTimeObjectWithoutTimeException(): void
    {
        $foo = new DateTimeValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('2008-04-02', 'time');
    }//end testTimeObjectWithoutTimeException()

    /**
     * Test Time Object Without Time Exception
     *
     * @return void
     */
    public function testTimeObjectWithoutTimeExceptionMessage(): void
    {
        $foo = new DateTimeValue();
        $this->expectExceptionMessage('The <code>TIME</code> was specified, but the argument is missing a <code>time</code> component.');
        $foo->setValue('2008-04-02', 'time');
    }//end testTimeObjectWithoutTimeExceptionMessage()

    /**
     * Test Time Object With Invalid Time Exception
     *
     * @return void
     */
    public function testTimeObjectWithInvalidTimeException(): void
    {
        $foo = new DateTimeValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('27:23:19', 'time');
    }//end testTimeObjectWithInvalidTimeException()

    /**
     * Test Time Object With Invalid Time Exception
     *
     * @return void
     */
    public function testTimeObjectWithInvalidTimeExceptionMessage(): void
    {
        $foo = new DateTimeValue();
        $this->expectExceptionMessage('The string <code>27:23:19</code> is not a valid time.');
        $foo->setValue('27:23:19', 'time');
    }//end testTimeObjectWithInvalidTimeExceptionMessage()

    /**
     * Test DATE Object
     *
     * @return void
     */
    public function testDateValueObject(): void
    {
        $foo = new DateTimeValue();
        $foo->setValue('1862-04-16', 'date');
        $expected = 'DATE';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '1862-04-16';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $ts = 1555580886;
        $foo->valueFromEpoch($ts);
        $expected = '2019-04-18';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withTime();
        $expected = '09:48:06';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withYear();
        $expected = '2019';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withDateTime();
        $expected = '2019-04-18 09:48:06';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<value type="TEMPORAL" subtype="DATE" tz="UTC">2019-04-18</value>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testDateValueObject()

    /**
     * Test Date Object Without Date Exception
     *
     * @return void
     */
    public function testDateObjectWithoutDateException(): void
    {
        $foo = new DateTimeValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('04:34:34', 'date');
    }//end testDateObjectWithoutDateException()

    /**
     * Test Date Object Without Date Exception
     *
     * @return void
     */
    public function testDateObjectWithoutDateExceptionMessage(): void
    {
        $foo = new DateTimeValue();
        $this->expectExceptionMessage('The <code>DATE</code> was specified, but the argument is missing a <code>date</code> component.');
        $foo->setValue('04:34:34', 'date');
    }//end testDateObjectWithoutDateExceptionMessage()

    /**
     * Test Date Object With Invalid Date Exception
     *
     * @return void
     */
    public function testDateObjectWithInvalidDateException(): void
    {
        $foo = new DateTimeValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('2017-02-29', 'date');
    }//end testDateObjectWithInvalidDateException()

    /**
     * Test Date Object With Invalid Date Exception
     *
     * @return void
     */
    public function testDateObjectWithInvalidDateExceptionMessage(): void
    {
        $foo = new DateTimeValue();
        $this->expectExceptionMessage('The string <code>2017-02-29</code> is not a valid date.');
        $foo->setValue('2017-02-29', 'date');
    }//end testDateObjectWithInvalidDateExceptionMessage()

    /**
     * Test YEAR object
     *
     * @return void
     */
    public function testYearValueObject(): void
    {
        $foo = new DateTimeValue();
        $foo->setValue('1973', 'year');
        $expected = 'YEAR';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '1973';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $ts = 185580886;
        $foo->valueFromEpoch($ts);
        $expected = '1975';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withTime();
        $expected = '22:14:46';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withDate();
        $expected = '1975-11-18';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withDateTime();
        $expected = '1975-11-18 22:14:46';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<value type="TEMPORAL" subtype="YEAR" tz="UTC">1975</value>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testYearValueObject()

    /**
     * Test Year Object Without Year Exception
     *
     * @return void
     */
    public function testYearObjectWithoutYearException(): void
    {
        $foo = new DateTimeValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('04:34:34', 'year');
    }//end testYearObjectWithoutYearException()

    /**
     * Test Year Object Without Year Exception
     *
     * @return void
     */
    public function testYearObjectWithoutYearExceptionMessage(): void
    {
        $foo = new DateTimeValue();
        $this->expectExceptionMessage('The <code>YEAR</code> was specified, but the argument is missing a <code>year</code> component.');
        $foo->setValue('04:34:34', 'year');
    }//end testYearObjectWithoutYearExceptionMessage()

    /**
     * Test Year Object With Invalid Year Exception
     *
     * @return void
     */
    public function testYearObjectWithInvalidYearException(): void
    {
        $foo = new DateTimeValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('0836', 'year');
    }//end testYearObjectWithInvalidYearException()

    /**
     * Test Year Object With Invalid Year Exception
     *
     * @return void
     */
    public function testYearObjectWithInvalidYearExceptionMessage(): void
    {
        $foo = new DateTimeValue();
        $this->expectExceptionMessage('MariaDB requires a year argument between <code>1000</code> and <code>9999</code> inclusive, but <code>0836</code> was provided.');
        $foo->setValue('0836', 'year');
    }//end testYearObjectWithInvalidYearExceptionMessage()

    /**
     * Test DAYETIME Object
     *
     * @return void
     */
    public function testDateTimeValueObject(): void
    {
        $foo = new DateTimeValue();
        $foo->setValue('1929-10-29 12:17:40.8348', 'datetime');
        $expected = 'DATETIME';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '1929-10-29 12:17:40.8348';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $ts = 45286536884;
        $foo->valueFromEpoch($ts);
        $expected = '3405-01-27 17:34:44';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $expected = 'UTC';
        $actual = $foo->getTimeZone();
        $this->assertSame($expected, $actual);
        $bar = $foo->withYear();
        $expected = '3405';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withDate();
        $expected = '3405-01-27';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $bar = $foo->withTime();
        $expected = '17:34:44';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<value type="TEMPORAL" subtype="DATETIME" tz="UTC">3405-01-27 17:34:44</value>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testDateTimeValueObject()

    /**
     * Test DATETIME object with out time exception
     *
     * @return void
     */
    public function testDatetimeObjectWithoutTimeException(): void
    {
        $foo = new DateTimeValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('1965-09-22');
    }//end testDatetimeObjectWithoutTimeException()

    /**
     * Test DATETIME object with out time exception
     *
     * @return void
     */
    public function testDatetimeObjectWithoutTimeExceptionMessage(): void
    {
        $foo = new DateTimeValue();
        $this->expectExceptionMessage('The <code>DATETIME</code> was specified, but the argument is missing a <code>time</code> component.');
        $foo->setValue('1965-09-22');
    }//end testDatetimeObjectWithoutTimeExceptionMessage()

    /**
     * Test DATETIME object with out date exception
     *
     * @return void
     */
    public function testDatetimeObjectWithoutDateException(): void
    {
        $foo = new DateTimeValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('07:00:00');
    }//end testDatetimeObjectWithoutDateException()

    /**
     * Test DATETIME object with out date exception
     *
     * @return void
     */
    public function testDatetimeObjectWithoutDateExceptionMessage(): void
    {
        $foo = new DateTimeValue();
        $this->expectExceptionMessage('The <code>DATETIME</code> was specified, but the argument is missing a <code>date</code> component.');
        $foo->setValue('07:00:00');
    }//end testDatetimeObjectWithoutDateExceptionMessage()

    /**
     * Test DATETIME object with invalid time exception
     *
     * @return void
     */
    public function testDateTimeObjectWithInvalidTimeException(): void
    {
        $foo = new DateTimeValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('1022-04-12 27:54:32');
    }//end testDateTimeObjectWithInvalidTimeException()

    /**
     * Test DATETIME object with invalid time exception
     *
     * @return void
     */
    public function testDateTimeObjectWithInvalidTimeExceptionMessage(): void
    {
        $foo = new DateTimeValue();
        $this->expectExceptionMessage('The string <code>27:54:32</code> is not a valid time.');
        $foo->setValue('1022-04-12 27:54:32');
    }//end testDateTimeObjectWithInvalidTimeExceptionMessage()

    /**
     * Test DATETIME object with invalid date exception
     *
     * @return void
     */
    public function testDateTimeObjectWithInvalidDateException(): void
    {
        $foo = new DateTimeValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('1022-04-31 20:54:32');
    }//end testDateTimeObjectWithInvalidDateException()

    /**
     * Test DATETIME object with invalid date exception
     *
     * @return void
     */
    public function testDateTimeObjectWithInvalidDateExceptionMessage(): void
    {
        $foo = new DateTimeValue();
        $this->expectExceptionMessage('The string <code>1022-04-31</code> is not a valid date.');
        $foo->setValue('1022-04-31 20:54:32');
    }//end testDateTimeObjectWithInvalidDateExceptionMessage()

    /**
     * Test TIMESTAMP Object
     *
     * @return void
     */
    public function testTimestampValueObject(): void
    {
        $foo = new TimeStampValue();
        // there will be a race condition but should never differ by more than 1 second
        $ts = time();
        $foo->valueFromEpoch();
        $expected = date('Y-m-d H:i:s', $ts);
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $test = new DateTimeValue('America/Los_Angeles');
        $TestTime = 1355632634;
        $test->valueFromEpoch($TestTime);
        $dt = $test->getDateTimeObject();
        $foo->valueFromDateTime($dt);
        // These should be different
        $Pacific = $test->getValue();
        $UTC = $foo->getValue();
        $this->assertNotSame($Pacific, $UTC);
        // these should be the same
        $TestUtcTime = strtotime($UTC);
        $this->assertSame($TestTime, $TestUtcTime);
    }//end testTimestampValueObject()

    /**
     * Test exception with integer too large
     *
     * @return void
     */
    public function testTimestampWithDateTooLargeException(): void
    {
        // fist generate a DateTime object beyond 2038
        $test = new DateTimeValue();
        $test->setValue('2040-03-17 06:33:32');
        $dt = $test->getDateTimeObject();
        // now try to set it as a TIMESTAMP - should fail
        $foo = new TimeStampValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->valueFromDateTime($dt);
    }//end testTimestampWithDateTooLargeException()

    /**
     * Test exception with integer too large
     *
     * @return void
     */
    public function testTimestampWithDateTooLargeExceptionMessage(): void
    {
        // fist generate a DateTime object beyond 2038
        $test = new DateTimeValue();
        $test->setValue('2040-03-17 06:33:32');
        $dt = $test->getDateTimeObject();
        // now try to set it as a TIMESTAMP - should fail
        $foo = new TimeStampValue();
        $this->expectExceptionMessage('The MariaDB TIMESTAMP type requires a <code>32-bit</code> (<code>4 byte</code>) <code>signed integer</code>');
        $foo->valueFromDateTime($dt);
    }//end testTimestampWithDateTooLargeExceptionMessage()

    /**
     * Test exception with reserved 0 timestamp
     *
     * @return void
     */
    public function testTimestampWithReservedZeroSecondsFromEpochException(): void
    {
        $foo = new TimeStampValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->valueFromEpoch(0);
    }//end testTimestampWithReservedZeroSecondsFromEpochException()

    /**
     * Test exception with reserved 0 timestamp
     *
     * @return void
     */
    public function testTimestampWithReservedZeroSecondsFromEpochExceptionMessage(): void
    {
        $foo = new TimeStampValue();
        $this->expectExceptionMessage('The TIMESTAMP value corresponding with the UNIX epoch is reserved in MariaDB and can not be used.');
        $foo->valueFromEpoch(0);
    }//end testTimestampWithReservedZeroSecondsFromEpochExceptionMessage()

    /**
     * Test exception with reserved 0 timestamp
     *
     * @return void
     */
    public function testTimestampWithReservedZeroViaDatetimeException(): void
    {
        // first create a DateTime object
        $test = new DateTimeValue('America/Los_Angeles');
        $test->valueFromEpoch(0);
        $dt = $test->getDateTimeObject();
        // now feed that object to TimeStampValue
        $foo = new TimeStampValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->valueFromDateTime($dt);
    }//end testTimestampWithReservedZeroViaDatetimeException()

    /**
     * Test exception with reserved 0 timestamp
     *
     * @return void
     */
    public function testTimestampWithReservedZeroViaDatetimeExceptionMessage(): void
    {
        // first create a DateTime object
        $test = new DateTimeValue('America/Los_Angeles');
        $test->valueFromEpoch(0);
        $dt = $test->getDateTimeObject();
        // now feed that object to TimeStampValue
        $foo = new TimeStampValue();
        $this->expectExceptionMessage('The TIMESTAMP value corresponding with the UNIX epoch is reserved in MariaDB and can not be used.');
        $foo->valueFromDateTime($dt);
    }//end testTimestampWithReservedZeroViaDatetimeExceptionMessage()

    /**
     * Test exception with reserved 0 timestamp
     *
     * @return void
     */
    public function testTimestampWithReservedZeroViaStringException(): void
    {
        $string = date('Y-m-d H:i:s', 0);
        $foo = new TimeStampValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue($string);
    }//end testTimestampWithReservedZeroViaStringException()

    /**
     * Test exception with reserved 0 timestamp
     *
     * @return void
     */
    public function testTimestampWithReservedZeroViaStringExceptionMessage(): void
    {
        $string = date('Y-m-d H:i:s', 0);
        $foo = new TimeStampValue();
        $this->expectExceptionMessage('The TIMESTAMP value corresponding with the UNIX epoch is reserved in MariaDB and can not be used.');
        $foo->setValue($string);
    }//end testTimestampWithReservedZeroViaStringExceptionMessage()

    /**
     * Test exception with invalid date time string
     *
     * @return void
     */
    public function testTimestampWithInvalidStringException(): void
    {
        $string = '1987-01-23 04:55:23.753459';
        $foo = new TimeStampValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue($string);
    }//end testTimestampWithInvalidStringException()

    /**
     * Test exception with invalid date time string
     *
     * @return void
     */
    public function testTimestampWithInvalidStringExceptionMessage(): void
    {
        $string = '1987-01-23 04:55:23.753459';
        $foo = new TimeStampValue();
        $this->expectExceptionMessage('Expecting <code>YYYY-MM-DD HH:MM:SS</code> style argument');
        $foo->setValue($string);
    }//end testTimestampWithInvalidStringExceptionMessage()

    /**
     * Test exception with invalid date part of string
     *
     * @return void
     */
    public function testTimestampWithInvalidDateException(): void
    {
        $string = '1987-11-31 04:55:23';
        $foo = new TimeStampValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue($string);
    }//end testTimestampWithInvalidDateException()

    /**
     * Test exception with invalid date part of string
     *
     * @return void
     */
    public function testTimestampWithInvalidDateExceptionMessage(): void
    {
        $string = '1987-11-31 04:55:23';
        $foo = new TimeStampValue();
        $this->expectExceptionMessage('The string <code>1987-11-31</code> is not a valid date.');
        $foo->setValue($string);
    }//end testTimestampWithInvalidDateExceptionMessage()

    /**
     * Test TIMESTAMP exception with invalid time part of string
     *
     * @return void
     */
    public function testTimestampWithInvalidTimeException(): void
    {
        $string = '1987-10-31 24:55:23';
        $foo = new TimeStampValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue($string);
    }//end testTimestampWithInvalidTimeException()

    /**
     * Test TIMESTAMP exception with invalid time part of string
     *
     * @return void
     */
    public function testTimestampWithInvalidTimeExceptionMessage(): void
    {
        $string = '1987-10-31 24:55:23';
        $foo = new TimeStampValue();
        $this->expectExceptionMessage('The string <code>24:55:23</code> is not a valid time.');
        $foo->setValue($string);
    }//end testTimestampWithInvalidTimeExceptionMessage()
}//end class

?>