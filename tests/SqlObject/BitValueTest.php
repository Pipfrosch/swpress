<?php
declare(strict_types=1);

/**
 * Unit testing for \Pipfrosch\SqlObject\BitValue
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;
use \Pipfrosch\SqlObject\BitValue;

/**
 * Test class for \Pipfrosch\SqlObject\BitValue
 */
// @codingStandardsIgnoreLine
final class BitValueTest extends TestCase {
    /**
     * BIT value test
     *
     * @return void
     */
    public function testBitObject(): void
    {
        $foo = new BitValue();
        $foo->setValue('0');
        $expected = 'BIT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '0';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $expected = "b'0'";
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo = new BitValue(2);
        $foo->setValue("b'1'");
        $expected = '01';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $expected = "b'01'";
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $foo = new BitValue(8);
        $foo->setValue('1101001');
        $expected = '01101001';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $expected = "b'01101001'";
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testBitObject()

    /**
     * Test Convert Integer
     *
     * @return void
     */
    public function testConvertIntegerToBit(): void
    {
        $foo = new BitValue(8);
        $foo->setFromInteger(107);
        $expected = '01101011';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setFromInteger(255);
        $expected = '11111111';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo = new BitValue(48);
        $foo->setFromInteger(549482903888);
        $expected = '000000000111111111101111101110111011100101010000';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
    }//end testConvertIntegerToBit()

    /**
     * Test Convert Integer
     *
     * @return void
     */
    public function testConvertOctalToBit(): void
    {
        $foo = new BitValue(8);
        $foo->setFromOctal('45');
        $expected = '00100101';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo = new BitValue(48);
        $foo->setFromOctal('7533205713036607');
        $expected = '111101011011010000101111001011000011110110000111';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
    }//end testConvertOctalToBit()

    /**
     * Test Convert Hex
     *
     * @return void
     */
    public function testConvertHexToBit(): void
    {
        $foo = new BitValue(8);
        $foo->setFromHex('c5');
        $expected = '11000101';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo = new BitValue(48);
        $foo->setFromHex('f5b42f2c3d87');
        $expected = '111101011011010000101111001011000011110110000111';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
    }//end testConvertHexToBit()

    /**
     * Test convert binary
     *
     * @return void
     */
    public function testConvertBinaryToBit(): void
    {
        //this is 4 bytes
        $foo = new BitValue(32);
        // first four should _always_ be 0, fifth should sometimes be 1
        $hits = 0;
        for ($i=0; $i<64; $i++) {
            $bin = random_bytes(3);
            $foo->setFromBin($bin);
            $value = $foo->getValue();
            $expected = '00000000';
            $actual = substr($value, 0, 8);
            $this->assertSame($expected, $actual);
            $ninth = substr($value, 8, 1);
            if ($ninth === '1') {
                $hits++;
            }
        }
        // statistically there should be > 8 hits but less than 56
        $this->assertLessThan($hits, 8);
        $this->assertLessThan(56, $hits);
    }//end testConvertBinaryToBit()

    /**
     * Test change bit size
     *
     * @return void
     */
    public function testChangeBitSize(): void
    {
        $foo = new BitValue(8);
        $foo->setFromHex('d3');
        $expected = '11010011';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setBitsize(16);
        $expected = '0000000011010011';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setBitsize(12);
        $expected = '000011010011';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
    }//end testChangeBitSize()

    /**
     * Test DOM output
     *
     * @return void
     */
    public function testDomNodeExport(): void
    {
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $foo = new BitValue(8);
        $foo->setFromHex('e7');
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<value type="NUMERIC" subtype="BIT">11100111</value>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testDomNodeExport()

    /**
     * Test negative integer throws exception
     *
     * @return void
     */
    public function testConvertToBitFromNegativeIntegerException(): void
    {
        $foo = new BitValue(4);
        $this->expectException(\InvalidArgumentException::class);
        $foo->setFromInteger(-12);
    }//end testConvertToBitFromNegativeIntegerException()

    /**
     * Test negative integer throws exception
     *
     * @return void
     */
    public function testConvertToBitFromNegativeIntegerExceptionMessage(): void
    {
        $foo = new BitValue(4);
        $this->expectExceptionMessage('Expecting a non-negative integer, but <code>-12</code> was supplied.');
        $foo->setFromInteger(-12);
    }//end testConvertToBitFromNegativeIntegerExceptionMessage()

    /**
     * Test positive integer that is too large
     *
     * @return void
     */
    public function testConvertToBitFromTooLargeIntegerException(): void
    {
        $int = time();
        $foo = new BitValue(24);
        $this->expectException(\InvalidArgumentException::class);
        $foo->setFromInteger($int);
    }//end testConvertToBitFromTooLargeIntegerException()

    /**
     * Test positive integer that is too large
     *
     * @return void
     */
    public function testConvertToBitFromTooLargeIntegerExceptionMessage(): void
    {
        $int = time();
        $foo = new BitValue(24);
        $this->expectExceptionMessage('Expecting an unsigned integer within <code>24</code> bits, but');
        $foo->setFromInteger($int);
    }//end testConvertToBitFromTooLargeIntegerExceptionMessage()

    /**
     * Test invalid octal exception is thrown
     *
     * @return void
     */
    public function testConvertFromInvalidOctalException(): void
    {
        $foo = new BitValue(24);
        $this->expectException(\InvalidArgumentException::class);
        $foo->setFromOctal('456789');
    }//end testConvertFromInvalidOctalException()

    /**
     * Test invalid octal exception is thrown
     *
     * @return void
     */
    public function testConvertFromInvalidOctalExceptionMessage(): void
    {
        $foo = new BitValue(24);
        $this->expectExceptionMessage('Expecting a valid octal string, but received <code>456789</code>');
        $foo->setFromOctal('456789');
    }//end testConvertFromInvalidOctalExceptionMessage()

    /**
     * Test octal too large
     *
     * @return void
     */
    public function testConvertToBitFromTooLargeOctalException(): void
    {
        $foo = new BitValue(4);
        $this->expectException(\InvalidArgumentException::class);
        $foo->setFromOctal('5371');
    }//end testConvertToBitFromTooLargeOctalException()

    /**
     * Test octal too large
     *
     * @return void
     */
    public function testConvertToBitFromTooLargeOctalExceptionMessage(): void
    {
        $foo = new BitValue(4);
        $this->expectExceptionMessage('Expecting an octal argument within <code>4</code> bits, but <code>5371 (12 bits)</code>');
        $foo->setFromOctal('5371');
    }//end testConvertToBitFromTooLargeOctalExceptionMessage()

    /**
     * Test invalid hex
     *
     * @return void
     */
    public function testConvertFromInvalidHexException(): void
    {
        $foo = new BitValue(32);
        $this->expectException(\InvalidArgumentException::class);
        $foo->setFromHex('ag24a2b6');
    }//end testConvertFromInvalidHexException()

    /**
     * Test invalid hex
     *
     * @return void
     */
    public function testConvertFromInvalidHexExceptionMessage(): void
    {
        $foo = new BitValue(32);
        $this->expectExceptionMessage('Expecting a valid hex string, but received <code>ag24a2b6</code>');
        $foo->setFromHex('ag24a2b6');
    }//end testConvertFromInvalidHexExceptionMessage()

    /**
     * Test hex too large
     *
     * @return void
     */
    public function testConvertToBitFromTooLargeHexException(): void
    {
        $foo = new BitValue(8);
        $this->expectException(\InvalidArgumentException::class);
        $foo->setFromHex('d451a67f');
    }//end testConvertToBitFromTooLargeHexException()

    /**
     * Test hex too large
     *
     * @return void
     */
    public function testConvertToBitFromTooLargeHexExceptionMessage(): void
    {
        $foo = new BitValue(8);
        $this->expectExceptionMessage('Expecting a hexadecimal argument within <code>8</code> bits, but <code>d451a67f (32 bits)</code> was supplied.');
        $foo->setFromHex('d451a67f');
    }//end testConvertToBitFromTooLargeHexExceptionMessage()

    /**
     * Test invalid string
     *
     * @return void
     */
    public function testInvalidStringArgumentException(): void
    {
        $foo = new BitValue(4);
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('pie');
    }//end testInvalidStringArgumentException()

    /**
     * Test invalid string
     *
     * @return void
     */
    public function testInvalidStringArgumentExceptionMessage(): void
    {
        $foo = new BitValue(4);
        $this->expectExceptionMessage('Expecting a BIT argument but received <code>pie</code>.');
        $foo->setValue('pie');
    }//end testInvalidStringArgumentExceptionMessage()

    /**
     * Test BIT string too large
     *
     * @return void
     */
    public function testArgumentTooLargeException(): void
    {
        $foo = new BitValue(4);
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue("b'101010101'");
    }//end testArgumentTooLargeException()

    /**
     * Test BIT string too large
     *
     * @return void
     */
    public function testArgumentTooLargeExceptionMessage(): void
    {
        $foo = new BitValue(4);
        $this->expectExceptionMessage('Expecting a BIT string argument within <code>4</code> bits, but <code>101010101 (9 bits)</code> was supplied');
        $foo->setValue("b'101010101'");
    }//end testArgumentTooLargeExceptionMessage()

    /**
     * Test changing bit size to one too small.
     *
     * @return void
     */
    public function testReducingBitSizeTooMuchException(): void
    {
        $foo = new BitValue(8);
        $foo->setValue("b'100001'");
        $this->expectException(\InvalidArgumentException::class);
        $foo->setBitsize(4);
    }//end testReducingBitSizeTooMuchException()

    /**
     * Test changing bit size to one too small.
     *
     * @return void
     */
    public function testReducingBitSizeTooMuchExceptionMessage(): void
    {
        $foo = new BitValue(8);
        $foo->setValue("b'100001'");
        $this->expectExceptionMessage('You are attempting to set the bit size to <code>4</code> but that is too small for the existing value <code>00100001</code>.');
        $foo->setBitsize(4);
    }//end testReducingBitSizeTooMuchExceptionMessage()
}//end class

?>