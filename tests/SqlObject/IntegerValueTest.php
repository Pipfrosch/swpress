<?php
declare(strict_types=1);

/**
 * Unit testing for \Pipfrosch\SqlObject\IntegerValue
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;
use \Pipfrosch\SqlObject\IntegerValue;

/**
 * Test class for \Pipfrosch\SqlObject\IntegerValue
 */
// @codingStandardsIgnoreLine
final class IntegerValueTest extends TestCase {
    /**
     * Test Boolean Object
     *
     * @return void
     */
    public function testBooleanObject(): void
    {
        $foo = new IntegerValue();
        $foo->setValue('0', 'boolean');
        $expected = 'BOOLEAN';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '0';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setValue('1');
        $expected = 'BOOLEAN';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '1';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withBoolean(false);
        $expected = 'BOOLEAN';
        $actual = $bar->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '0';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withBoolean(true);
        $expected = 'BOOLEAN';
        $actual = $bar->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '1';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
    }//end testBooleanObject()

    /**
     * Test Boolean Object Exceptions
     *
     * @return void
     */
    public function testBooleanObjectException(): void
    {
        $foo = new IntegerValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('2', 'boolean');
    }//end testBooleanObjectException()

    /**
     * Test Boolean Object Exception Message
     *
     * @return void
     */
    public function testBooleanObjectExceptionMessage(): void
    {
        $foo = new IntegerValue();
        $this->expectExceptionMessage('Expecting a string value of <code>0</code> or <code>1</code> but received');
        $foo->setValue('2', 'boolean');
    }//end testBooleanObjectExceptionMessage()

    /**
     * Test TINYINT
     *
     * @return void
     */
    public function testTinyintObject(): void
    {
        $foo = new IntegerValue();
        $foo->setValue('40', 'tinyint');
        $expected = 'TINYINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '40';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setValue('-122', 'tinyint');
        $expected = 'TINYINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '-122';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setValue('190', 'tinyint', false);
        $expected = 'TINYINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '190';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withTinyInt(222, false);
        $expected = 'TINYINT';
        $actual = $bar->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '222';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
    }//end testTinyintObject()

    /**
     * Test TINYINT Object Throws Exception
     *
     * @return void
     */
    public function testTinyintObjectException(): void
    {
        $foo = new IntegerValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('129', 'tinyint');
    }//end testTinyintObjectException()

    /**
     * Test TINYINT Object Exception Message
     *
     * @return void
     */
    public function testTinyintObjectExceptionMessage(): void
    {
        $foo = new IntegerValue();
        $this->expectExceptionMessage('Expecting a string representation of an integer within the valid range for a <code>MariaDB unsigned TINYINT</code>, received');
        $foo->setValue('529', 'tinyint', false);
    }//end testTinyintObjectExceptionMessage()

    /**
     * Test TINYINT Object Throws Exception
     *
     * @return void
     */
    public function testTinyintObjectViaWithException(): void
    {
        $foo = new IntegerValue();
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withTinyInt(602);
    }//end testTinyintObjectViaWithException()

    /**
     * Test TINYINT Object Throws Exception Message
     *
     * @return void
     */
    public function testTinyintObjectViaWithExceptionMessage(): void
    {
        $foo = new IntegerValue();
        $this->expectExceptionMessage('Expecting an integer within the valid range for a <code>MariaDB signed TINYINT</code>');
        $bar = $foo->withTinyInt(670);
    }//end testTinyintObjectViaWithExceptionMessage()

    /**
     * Test SMALLINT
     *
     * @return void
     */
    public function testSmallintObject(): void
    {
        $foo = new IntegerValue();
        $foo->setValue('400', 'smallint');
        $expected = 'SMALLINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '400';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setValue('-800', 'smallint');
        $expected = 'SMALLINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '-800';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setValue('32790', 'smallint', false);
        $expected = 'SMALLINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '32790';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withSmallInt(32720);
        $expected = 'SMALLINT';
        $actual = $bar->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '32720';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
    }//end testSmallintObject()

    /**
     * Test SMALLINT Object Throws Exception
     *
     * @return void
     */
    public function testSmallintObjectException(): void
    {
        $foo = new IntegerValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('32790', 'smallint');
    }//end testSmallintObjectException()

    /**
     * Test SMALLINT Object Exception Message
     *
     * @return void
     */
    public function testSmallintObjectExceptionMessage(): void
    {
        $foo = new IntegerValue();
        $this->expectExceptionMessage('Expecting a string representation of an integer within the valid range for a <code>MariaDB signed SMALLINT</code>');
        $foo->setValue('32790', 'smallint');
    }//end testSmallintObjectExceptionMessage()

    /**
     * Test SMALLINT Object Throws Exception
     *
     * @return void
     */
    public function testSmallintObjectViaWithException(): void
    {
        $foo = new IntegerValue();
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withSmallInt(67000, false);
    }//end testSmallintObjectViaWithException()

    /**
     * Test SMALLINT Object Throws Exception Message
     *
     * @return void
     */
    public function testSmallintObjectViaWithExceptionMessage(): void
    {
        $foo = new IntegerValue();
        $this->expectExceptionMessage('Expecting an integer within the valid range for a <code>MariaDB unsigned SMALLINT</code>');
        $bar = $foo->withSmallInt(67000, false);
    }//end testSmallintObjectViaWithExceptionMessage()

    /**
     * Test MEDIUMINT
     *
     * @return void
     */
    public function testMediumintObject(): void
    {
        $foo = new IntegerValue();
        $foo->setValue('8388607', 'mediumint');
        $expected = 'MEDIUMINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '8388607';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setValue('-388607', 'mediumint');
        $expected = 'MEDIUMINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '-388607';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setValue('14388607', 'mediumint', false);
        $expected = 'MEDIUMINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '14388607';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withMediumInt(327420);
        $expected = 'MEDIUMINT';
        $actual = $bar->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '327420';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
    }//end testMediumintObject()

    /**
     * Test MEDIUMINT Object Throws Exception
     *
     * @return void
     */
    public function testMediumintObjectException(): void
    {
        $foo = new IntegerValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('8388609', 'mediumint');
    }//end testMediumintObjectException()

    /**
     * Test MEDIUMINT Object Exception Message
     *
     * @return void
     */
    public function testMediumintObjectExceptionMessage(): void
    {
        $foo = new IntegerValue();
        $this->expectExceptionMessage('Expecting a string representation of an integer within the valid range for a <code>MariaDB signed MEDIUMINT</code>, received');
        $foo->setValue('8388609', 'mediumint');
    }//end testMediumintObjectExceptionMessage()

    /**
     * Test MEDIUMINT Object Throws Exception
     *
     * @return void
     */
    public function testMediumintObjectViaWithException(): void
    {
        $foo = new IntegerValue();
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withMediumInt(77767055, false);
    }//end testMediumintObjectViaWithException()

    /**
     * Test MEDIUMINT Object Throws Exception Message
     *
     * @return void
     */
    public function testMediumintObjectViaWithExceptionMessage(): void
    {
        $foo = new IntegerValue();
        $this->expectExceptionMessage('Expecting an integer within the valid range for a <code>MariaDB signed MEDIUMINT</code>');
        $bar = $foo->withMediumInt(77767055);
    }//end testMediumintObjectViaWithExceptionMessage()

    /**
     * Test INT
     *
     * @return void
     */
    public function testIntObject(): void
    {
        $foo = new IntegerValue();
        $foo->setValue('2147483640', 'integer');
        $expected = 'INT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '2147483640';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setValue('-21474836', 'integer');
        $expected = 'INT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '-21474836';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setValue('2147483940', 'integer', false);
        $expected = 'INT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '2147483940';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withInt(214748394);
        $expected = 'INT';
        $actual = $bar->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '214748394';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withInt('2547483940', false);
        $expected = 'INT';
        $actual = $bar->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '2547483940';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
    }//end testIntObject()

    /**
     * Test INT Object Throws Exception
     *
     * @return void
     */
    public function testIntObjectException(): void
    {
        $foo = new IntegerValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('838764238609', 'int');
    }//end testIntObjectException()

    /**
     * Test INT Object Exception Message
     *
     * @return void
     */
    public function testIntObjectExceptionMessage(): void
    {
        $foo = new IntegerValue();
        $this->expectExceptionMessage('Expecting a string representation of an integer within the valid range for a <code>MariaDB signed INT</code>, received');
        $foo->setValue('838764238609', 'int');
    }//end testIntObjectExceptionMessage()

    /**
     * Test INT Object Throws Exception
     *
     * @return void
     */
    public function testIntObjectViaWithException(): void
    {
        $foo = new IntegerValue();
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withInt(76437767055, false);
    }//end testIntObjectViaWithException()

    /**
     * Test INT Object Throws Exception Message
     *
     * @return void
     */
    public function testIntObjectViaWithExceptionMessage(): void
    {
        $foo = new IntegerValue();
        $this->expectExceptionMessage('Expecting a string or an integer within the valid range for a <code>MariaDB unsigned INT</code>');
        $bar = $foo->withInt(76437767055, false);
    }//end testIntObjectViaWithExceptionMessage()

    /**
     * Test BIGINT
     *
     * @return void
     */
    public function testBigintObject(): void
    {
        $foo = new IntegerValue();
        $foo->setValue('9223372036854775800', 'bigint');
        $expected = 'BIGINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '9223372036854775800';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setValue('-9223372036854775800', 'bigint');
        $expected = 'BIGINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '-9223372036854775800';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo->setValue('9223372336855775807', 'bigint', false);
        $expected = 'BIGINT';
        $actual = $foo->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '9223372336855775807';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withBigInt(9223372336855775);
        $expected = 'BIGINT';
        $actual = $bar->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '9223372336855775';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        $bar = $foo->withBigInt('18446744073709551610', false);
        $expected = 'BIGINT';
        $actual = $bar->getSubtype();
        $this->assertSame($expected, $actual);
        $expected = '18446744073709551610';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
    }//end testBigintObject()

    /**
     * Test BIGINT Object Throws Exception
     *
     * @return void
     */
    public function testBigintObjectException(): void
    {
        $foo = new IntegerValue();
        $this->expectException(\InvalidArgumentException::class);
        $foo->setValue('18446744093709551615', 'bigint');
    }//end testBigintObjectException()

    /**
     * Test BIGINT Object Exception Message
     *
     * @return void
     */
    public function testBigintObjectExceptionMessage(): void
    {
        $foo = new IntegerValue();
        $this->expectExceptionMessage('Expecting a string representation of an integer within the valid range for a <code>MariaDB unsigned BIGINT</code>, received');
        $foo->setValue('18446844093709551615', 'bigint', false);
    }//end testBigintObjectExceptionMessage()

    /**
     * Test BIGINT Object Throws Exception
     *
     * @return void
     */
    public function testBigintObjectViaWithException(): void
    {
        $foo = new IntegerValue();
        $this->expectException(\InvalidArgumentException::class);
        $bar = $foo->withBigInt('18446844093709551615', false);
    }//end testBigintObjectViaWithException()

    /**
     * Test BIGINT Object Throws Exception Message
     *
     * @return void
     */
    public function testBigintObjectViaWithExceptionMessage(): void
    {
        $foo = new IntegerValue();
        $this->expectExceptionMessage('Expecting a string or an integer within the valid range for a <code>MariaDB unsigned BIGINT</code>');
        $bar = $foo->withBigInt('18446844093709551615', false);
    }//end testBigintObjectViaWithExceptionMessage()

    /**
     * Test DOM node export
     *
     * @return void
     */
    public function testDomNodeExport(): void
    {
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $foo = new IntegerValue();
        $foo->setValue('92', 'tinyint');
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<value type="NUMERIC" subtype="TINYINT" signed="signed">92</value>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testDomNodeExport()
}//end class

?>