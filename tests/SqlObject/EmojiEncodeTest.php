<?php
declare(strict_types=1);

/**
 * Unit testing for EmojiEntity class.
 *
 * Emojis that are not (yet) rendered by my operating system are added via html_entity_decode()
 *
 * The $test string could be created by just assigning its value to the $original string but I
 *  intentionally do not. If you really want to know why, contact me, but do not tell me that I
 *  am doing more work than I need because I can just do $test = $original. I already know that,
 *  there is a reason I do not.
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;
use \Pipfrosch\SqlObject\StaticMethods\EmojiEntity;

/**
 * Test class for \Pipfrosch\SqlObject\StaticMethods\EmojiEntity
 */
// @codingStandardsIgnoreLine
final class EmojiEncodeTest extends TestCase {
    /**
     * Test Face Smiling
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-smiling
     *
     * @return void
     */
    public function testFaceSmilingEmojis(): void
    {
        $original = '😀 😃 😄 😁 😆 😅 🤣 😂 🙂 🙃 😉 😊 😇';
        $test = '😀 😃 😄 😁 😆 😅 🤣 😂 🙂 🙃 😉 😊 😇';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceSmilingEmojis()

    /**
     * Test Face Affection - this set includes a two-byte emoji
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-affection
     *
     * @return void
     */
    public function testFaceAffectionEmojis(): void
    {
        $original = html_entity_decode('&#x1F970;');
        $original .= ' 😍 🤩 😘 😗 ☺ 😚 😙';
        $test = html_entity_decode('&#x1F970;');
        $test .= ' 😍 🤩 😘 😗 ☺ 😚 😙';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceAffectionEmojis()

    /**
     * Test Face Tongue
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-tongue
     *
     * @return void
     */
    public function testFaceTongueEmojis(): void
    {
        $original = '😋 😛 😜 🤪 😝 🤑';
        $test = '😋 😛 😜 🤪 😝 🤑';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceTongueEmojis()

    /**
     * Test Face Hand
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-hand
     *
     * @return void
     */
    public function testFaceHandEmojis(): void
    {
        $original = '🤗 🤭 🤫 🤔';
        $test = '🤗 🤭 🤫 🤔';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceHandEmojis()

    /**
     * Test Face Neutral Skeptical
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-neutral-skeptical
     *
     * @return void
     */
    public function testFaceNeutralSkepticalEmojis(): void
    {
        $original = '🤐 🤨 😐 😑 😶 😏 😒 🙄 😬 🤥';
        $test = '🤐 🤨 😐 😑 😶 😏 😒 🙄 😬 🤥';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceNeutralSkepticalEmojis()

    /**
     * Test Face Sleepy
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-sleepy
     *
     * @return void
     */
    public function testFaceSleepyEmojis(): void
    {
        $original = '😌 😔 😪 🤤 😴';
        $test = '😌 😔 😪 🤤 😴';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceSleepyEmojis()

    /**
     * Test Face Unwell
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-unwell
     *
     * @return void
     */
    public function testFaceUnwellEmojis(): void
    {
        $original = '😷 🤒 🤕 🤢 🤮 🤧 ';
        $original .= html_entity_decode(' &#x1F975; &#x1F976; &#x1F974; ');
        $original .= '😵 🤯';
        $test = '😷 🤒 🤕 🤢 🤮 🤧 ';
        $test .= html_entity_decode(' &#x1F975; &#x1F976; &#x1F974; ');
        $test .= '😵 🤯';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceUnwellEmojis()

    /**
     * Test Face Hat
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-hat
     *
     * @return void
     */
    public function testFaceHatEmojis(): void
    {
        $original = '🤠 ';
        $original .= html_entity_decode('&#x1F973;');
        $test = '🤠 ';
        $test .= html_entity_decode('&#x1F973;');
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceHatEmojis()

    /**
     * Test Face Glasses
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-glasses
     *
     * @return void
     */
    public function testFaceGlassesEmojis(): void
    {
        $original = '😎 🤓 🧐';
        $test = '😎 🤓 🧐';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceGlassesEmojis()

    /**
     * Test Face Concerned
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-concerned
     *
     * @return void
     */
    public function testFaceConcernedEmojis(): void
    {
        $original = '😕 😟 🙁 ☹ 😮 😯 😲 😳 ';
        $original .= html_entity_decode('&#x1F97A; ');
        $original .= '😦 😧 😨 😰 😥 😢 😭 😱 😖 😣 😞 😓 😩 😫 ';
        $original .= html_entity_decode('&#x1F971;');
        $test = '😕 😟 🙁 ☹ 😮 😯 😲 😳 ';
        $test .= html_entity_decode('&#x1F97A; ');
        $test .= '😦 😧 😨 😰 😥 😢 😭 😱 😖 😣 😞 😓 😩 😫 ';
        $test .= html_entity_decode('&#x1F971;');
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceConcernedEmojis()

    /**
     * Test Face Negative
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-negative
     *
     * @return void
     */
    public function testFaceNegativeEmojis(): void
    {
        $original = '😤 😡 😠 🤬 😈 👿 💀 ☠';
        $test = '😤 😡 😠 🤬 😈 👿 💀 ☠';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceNegativeEmojis()

    /**
     * Face Costume
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#face-costume
     *
     * @return void
     */
    public function testFaceCostumeEmojis(): void
    {
        $original = '💩 🤡 👹 👺 👻 👽 👾 🤖';
        $test = '💩 🤡 👹 👺 👻 👽 👾 🤖';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFaceCostumeEmojis()

    /**
     * Cat Face
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#cat-face
     *
     * @return void
     */
    public function testCatFaceEmojis(): void
    {
        $original = '😺 😸 😹 😻 😼 😽 🙀 😿 😾';
        $test = '😺 😸 😹 😻 😼 😽 🙀 😿 😾';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testCatFaceEmojis()

    /**
     * Monkey Face
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#monkey-face
     *
     * @return void
     */
    public function testMonkeyFaceEmojis(): void
    {
        $original = '🙈 🙉 🙊';
        $test = '🙈 🙉 🙊';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testMonkeyFaceEmojis()

    /**
     * Emotion
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#emotion
     *
     * @return void
     */
    public function testEmotionEmojis(): void
    {
        $original = '💋 💌 💘 💝 💖 💗 💓 💞 💕 💟 ❣ 💔 ❤ 🧡 💛 💚 💙 💜 ';
        $original .= html_entity_decode('&#x1F90E; ');
        $original .= '🖤 ';
        $original .= html_entity_decode('&#x1F90D; ');
        $original .= '💯 💢 💥 💫 💦 💨 🕳  💣 💬 👁️‍🗨️ 🗨 🗯 💭 💤';
        $test = '💋 💌 💘 💝 💖 💗 💓 💞 💕 💟 ❣ 💔 ❤ 🧡 💛 💚 💙 💜 ';
        $test .= html_entity_decode('&#x1F90E; ');
        $test .= '🖤 ';
        $test .= html_entity_decode('&#x1F90D; ');
        $test .= '💯 💢 💥 💫 💦 💨 🕳  💣 💬 👁️‍🗨️ 🗨 🗯 💭 💤';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testEmotionEmojis()

    /**
     * Hand Fingers Open
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#hand-fingers-open
     *
     * @return void
     */
    public function testHandFingersOpenEmojis(): void
    {
        $original = '👋 🤚 🖐 ✋ 🖖';
        $test = '👋 🤚 🖐 ✋ 🖖';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testHandFingersOpenEmojis()

    /**
     * Hand Fingers Partial
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#hand-fingers-partial
     *
     * @return void
     */
    public function testHandFingersPartialEmojis(): void
    {
        $original = '👌 ';
        $original .= html_entity_decode('&#x1F90F; ');
        $original .= '✌ 🤞 🤟 🤘 🤙';
        $test = '👌 ';
        $test .= html_entity_decode('&#x1F90F; ');
        $test .= '✌ 🤞 🤟 🤘 🤙';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testHandFingersPartialEmojies()

    /**
     * Hand Single Finger
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#hand-single-finger
     *
     * @return void
     */
    public function testHandSingleFingerEmojis(): void
    {
        $original = '👈 👉 👆 🖕 👇 ☝';
        $test = '👈 👉 👆 🖕 👇 ☝';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testHandSingleFingerEmojis()

    /**
     * Hand Fingers Closed
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#hand-fingers-closed
     *
     * @return void
     */
    public function testHandFingersClosedEmojis(): void
    {
        $original = '👍 👎 ✊ 👊 🤛 🤜';
        $test = '👍 👎 ✊ 👊 🤛 🤜';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testHandFingersClosedEmojis()

    /**
     * Hands
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#hands
     *
     * @return void
     */
    public function testHandsEmojis(): void
    {
        $original = '👏 🙌 👐 🤲 🤝 🙏';
        $test = '👏 🙌 👐 🤲 🤝 🙏';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testHandsEmojis()

    /**
     * Hand Prop
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#hand-prop
     *
     * @return void
     */
    public function testHandPropEmojis(): void
    {
        $original = '✍ 💅 🤳';
        $test = '✍ 💅 🤳';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testHandPropEmojis()

    /**
     * Body Parts
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#body-parts
     *
     * @return void
     */
    public function testBodyPartsEmojis(): void
    {
        $original = '💪 ';
        $original .= html_entity_decode('&#x1F9BE; &#x1F9BF; &#x1F9B5; &#x1F9B6; ');
        $original .= '👂 ';
        $original .= html_entity_decode('&#x1F9BB; ');
        $original .= '👃 🧠 ';
        $original .= html_entity_decode('&#x1F9B7; &#x1F9B4; ');
        $original .= '👀 👁 👅 👄';
        $test = '💪 ';
        $test .= html_entity_decode('&#x1F9BE; &#x1F9BF; &#x1F9B5; &#x1F9B6; ');
        $test .= '👂 ';
        $test .= html_entity_decode('&#x1F9BB; ');
        $test .= '👃 🧠 ';
        $test .= html_entity_decode('&#x1F9B7; &#x1F9B4; ');
        $test .= '👀 👁 👅 👄';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testBodyPartsEmojis()

    /**
     * Person
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#person
     *
     * @return void
     */
    public function testPersonEmojis(): void
    {
        $original = '👶 🧒 👦 👧 🧑 👱 👨 🧔 👱‍♂️ ';
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1F468;&#x200D;&#x1F9B0; &#x1F468;&#x200D;&#x1F9B1; &#x1F468;&#x200D;&#x1F9B3; &#x1F468;&#x200D;&#x1F9B2; ');
        $original .= '👩 👱‍♀️ ';
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1F469;&#x200D;&#x1F9B0; &#x1F469;&#x200D;&#x1F9B1; &#x1F469;&#x200D;&#x1F9B3; &#x1F469;&#x200D;&#x1F9B2; ');
        $original .= '🧓 👴 👵';
        $test = '👶 🧒 👦 👧 🧑 👱 👨 🧔 👱‍♂️ ';
        // @codingStandardsIgnoreLine
        $test .= html_entity_decode('&#x1F468;&#x200D;&#x1F9B0; &#x1F468;&#x200D;&#x1F9B1; &#x1F468;&#x200D;&#x1F9B3; &#x1F468;&#x200D;&#x1F9B2; ');
        $test .= '👩 👱‍♀️ ';
        // @codingStandardsIgnoreLine
        $test .= html_entity_decode('&#x1F469;&#x200D;&#x1F9B0; &#x1F469;&#x200D;&#x1F9B1; &#x1F469;&#x200D;&#x1F9B3; &#x1F469;&#x200D;&#x1F9B2; ');
        $test .= '🧓 👴 👵';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPersonEmojis()

    /**
     * Person Gesture
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#person-gesture
     *
     * @return void
     */
    public function testPersonGestureEmojis(): void
    {
        $original = '🙍 🙍‍♂️ 🙍‍♀️ 🙎 🙎‍♂️ 🙎‍♀️ 🙅 🙅‍♂️ 🙅‍♀️ 🙆 🙆‍♂️ 🙆‍♀️ 💁 💁‍♂️ 💁‍♀️ 🙋 🙋‍♂️ 🙋‍♀️ ';
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1F9CF; &#x1F9CF;&#x200D;&#x2642;&#xFE0F; &#x1F9CF;&#x200D;&#x2640;&#xFE0F; ');
        $original .= '🙇 🙇‍♂️ 🙇‍♀️ 🤦 🤦‍♂️ 🤦‍♀️ 🤷 🤷‍♂️ 🤷‍♀️';
        $test = '🙍 🙍‍♂️ 🙍‍♀️ 🙎 🙎‍♂️ 🙎‍♀️ 🙅 🙅‍♂️ 🙅‍♀️ 🙆 🙆‍♂️ 🙆‍♀️ 💁 💁‍♂️ 💁‍♀️ 🙋 🙋‍♂️ 🙋‍♀️ ';
        $test .= html_entity_decode('&#x1F9CF; &#x1F9CF;&#x200D;&#x2642;&#xFE0F; &#x1F9CF;&#x200D;&#x2640;&#xFE0F; ');
        $test .= '🙇 🙇‍♂️ 🙇‍♀️ 🤦 🤦‍♂️ 🤦‍♀️ 🤷 🤷‍♂️ 🤷‍♀️';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPersonGestureEmojis()

    /**
     * Person Role
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#person-role
     *
     * @return void
     */
    public function testPersonRoleEmojis(): void
    {
        // @codingStandardsIgnoreLine
        $original = '👨‍⚕️ 👩‍⚕️ 👨‍🎓 👩‍🎓 👨‍🏫 👩‍🏫 👨‍⚖️ 👩‍⚖️ 👨‍🌾 👩‍🌾 👨‍🍳 👩‍🍳 👨‍🔧 👩‍🔧 👨‍🏭 👩‍🏭 👨‍💼 👩‍💼 👨‍🔬 👩‍🔬 👨‍💻 👩‍💻 👨‍🎤 👩‍🎤 👨‍🎨 👩‍🎨 👨‍✈️ 👩‍✈️ 👨‍🚀 👩‍🚀 👨‍🚒 👩‍🚒 👮 👮‍♂️ 👮‍♀️ 🕵 🕵️‍♂️ 🕵️‍♀️ 💂 💂‍♂️ 💂‍♀️ 👷 👷‍♂️ 👷‍♀️ 🤴 👸 👳 👳‍♂️ 👳‍♀️ 👲 🧕 🤵 👰 🤰 🤱';
        // @codingStandardsIgnoreLine
        $test = '👨‍⚕️ 👩‍⚕️ 👨‍🎓 👩‍🎓 👨‍🏫 👩‍🏫 👨‍⚖️ 👩‍⚖️ 👨‍🌾 👩‍🌾 👨‍🍳 👩‍🍳 👨‍🔧 👩‍🔧 👨‍🏭 👩‍🏭 👨‍💼 👩‍💼 👨‍🔬 👩‍🔬 👨‍💻 👩‍💻 👨‍🎤 👩‍🎤 👨‍🎨 👩‍🎨 👨‍✈️ 👩‍✈️ 👨‍🚀 👩‍🚀 👨‍🚒 👩‍🚒 👮 👮‍♂️ 👮‍♀️ 🕵 🕵️‍♂️ 🕵️‍♀️ 💂 💂‍♂️ 💂‍♀️ 👷 👷‍♂️ 👷‍♀️ 🤴 👸 👳 👳‍♂️ 👳‍♀️ 👲 🧕 🤵 👰 🤰 🤱';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPersonRoleEmojis()

    /**
     * Person Fantasy
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#person-fantasy
     *
     * @return void
     */
    public function testPersonFantasyEmojis(): void
    {
        $original = '👼 🎅 🤶 ';
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1F9B8; &#x1F9B8;&#x200D;&#x2642;&#xFE0F; &#x1F9B8;&#x200D;&#x2640;&#xFE0F; ');
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1F9B9; &#x1F9B9;&#x200D;&#x2642;&#xFE0F; &#x1F9B9;&#x200D;&#x2640;&#xFE0F; ');
        $original .= '🧙 🧙‍♂️ 🧙‍♀️ 🧚 🧚‍♂️ 🧚‍♀️ 🧛 🧛‍♂️ 🧛‍♀️ 🧜 🧜‍♂️ 🧜‍♀️ 🧝 🧝‍♂️ 🧝‍♀️ 🧞 🧞‍♂️ 🧞‍♀️ 🧟 🧟‍♂️ 🧟‍♀️';
        $test = '👼 🎅 🤶 ';
        $test .= html_entity_decode('&#x1F9B8; &#x1F9B8;&#x200D;&#x2642;&#xFE0F; &#x1F9B8;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B9; &#x1F9B9;&#x200D;&#x2642;&#xFE0F; &#x1F9B9;&#x200D;&#x2640;&#xFE0F; ');
        $test .= '🧙 🧙‍♂️ 🧙‍♀️ 🧚 🧚‍♂️ 🧚‍♀️ 🧛 🧛‍♂️ 🧛‍♀️ 🧜 🧜‍♂️ 🧜‍♀️ 🧝 🧝‍♂️ 🧝‍♀️ 🧞 🧞‍♂️ 🧞‍♀️ 🧟 🧟‍♂️ 🧟‍♀️';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPersonFantasyEmojis()

    /**
     * Person Activity
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#person-activity
     *
     * @return void
     */
    public function testPersonActivityEmojis(): void
    {
        $original = '💆 💆‍♂️ 💆‍♀️ 💇 💇‍♂️ 💇‍♀️ 🚶 🚶‍♂️ 🚶‍♀️ ';
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1F9CD; &#x1F9CD;&#x200D;&#x2642;&#xFE0F;  &#x1F9CD;&#x200D;&#x2640;&#xFE0F; ');
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1F9CE; &#x1F9CE;&#x200D;&#x2642;&#xFE0F;  &#x1F9CE;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F468;&#x200D;&#x1F9AF; &#x1F469;&#x200D;&#x1F9AF; ');
        $original .= html_entity_decode('&#x1F468;&#x200D;&#x1F9BC; &#x1F469;&#x200D;&#x1F9BC; ');
        $original .= html_entity_decode('&#x1F468;&#x200D;&#x1F9BD; &#x1F469;&#x200D;&#x1F9BD; ');
        $original .= '🏃 🏃‍♂️ 🏃‍♀️ 💃 🕺 🕴 👯 👯‍♂️ 👯‍♀️ 🧖 🧖‍♂️ 🧖‍♀️ 🧗 🧗‍♂️ 🧗‍♀️';
        $test = '💆 💆‍♂️ 💆‍♀️ 💇 💇‍♂️ 💇‍♀️ 🚶 🚶‍♂️ 🚶‍♀️ ';
        $test .= html_entity_decode('&#x1F9CD; &#x1F9CD;&#x200D;&#x2642;&#xFE0F;  &#x1F9CD;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CE; &#x1F9CE;&#x200D;&#x2642;&#xFE0F;  &#x1F9CE;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F468;&#x200D;&#x1F9AF; &#x1F469;&#x200D;&#x1F9AF; ');
        $test .= html_entity_decode('&#x1F468;&#x200D;&#x1F9BC; &#x1F469;&#x200D;&#x1F9BC; ');
        $test .= html_entity_decode('&#x1F468;&#x200D;&#x1F9BD; &#x1F469;&#x200D;&#x1F9BD; ');
        $test .= '🏃 🏃‍♂️ 🏃‍♀️ 💃 🕺 🕴 👯 👯‍♂️ 👯‍♀️ 🧖 🧖‍♂️ 🧖‍♀️ 🧗 🧗‍♂️ 🧗‍♀️';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPersonActivityEmojis()

    /**
     * Person Sport
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#person-sport
     *
     * @return void
     */
    public function testPersonSportEmojis(): void
    {
        // @codingStandardsIgnoreLine
        $original = '🤺 🏇 ⛷ 🏂 🏌 🏌️‍♂️ 🏌️‍♀️ 🏄 🏄‍♂️ 🏄‍♀️ 🚣 🚣‍♂️ 🚣‍♀️ 🏊 🏊‍♂️ 🏊‍♀️ ⛹ ⛹️‍♂️ ⛹️‍♀️ 🏋 🏋️‍♂️ 🏋️‍♀️ 🚴 🚴‍♂️ 🚴‍♀️ 🚵 🚵‍♂️ 🚵‍♀️ 🤸 🤸‍♂️ 🤸‍♀️ 🤼 🤼‍♂️ 🤼‍♀️ 🤽 🤽‍♂️ 🤽‍♀️ 🤾 🤾‍♂️ 🤾‍♀️ 🤹 🤹‍♂️ 🤹‍♀️';
        // @codingStandardsIgnoreLine
        $test = '🤺 🏇 ⛷ 🏂 🏌 🏌️‍♂️ 🏌️‍♀️ 🏄 🏄‍♂️ 🏄‍♀️ 🚣 🚣‍♂️ 🚣‍♀️ 🏊 🏊‍♂️ 🏊‍♀️ ⛹ ⛹️‍♂️ ⛹️‍♀️ 🏋 🏋️‍♂️ 🏋️‍♀️ 🚴 🚴‍♂️ 🚴‍♀️ 🚵 🚵‍♂️ 🚵‍♀️ 🤸 🤸‍♂️ 🤸‍♀️ 🤼 🤼‍♂️ 🤼‍♀️ 🤽 🤽‍♂️ 🤽‍♀️ 🤾 🤾‍♂️ 🤾‍♀️ 🤹 🤹‍♂️ 🤹‍♀️';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPersonSportEmojis()

    /**
     * Person Resting
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#person-resting
     *
     * @return void
     */
    public function testPersonRestingEmojis(): void
    {
        $original = '🧘 🧘‍♂️ 🧘‍♀️ 🛀 🛌';
        $test = '🧘 🧘‍♂️ 🧘‍♀️ 🛀 🛌';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPersonRestingEmojis()

    /**
     * Family
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#family
     *
     * @return void
     */
    public function testFamilyEmojis(): void
    {
        $original = html_entity_decode('&#x1F9D1;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;');
        // @codingStandardsIgnoreLine
        $original .= '👭 👫 👬 💏 👩‍❤️‍💋‍👨 👨‍❤️‍💋‍👨 👩‍❤️‍💋‍👩 💑 👩‍❤️‍👨 👨‍❤️‍👨 👩‍❤️‍👩 👪 👨‍👩‍👦 👨‍👩‍👧 👨‍👩‍👧‍👦 👨‍👩‍👦‍👦 👨‍👩‍👧‍👧 👨‍👨‍👦 👨‍👨‍👧 👨‍👨‍👧‍👦 👨‍👨‍👦‍👦 👨‍👨‍👧‍👧 👩‍👩‍👦 👩‍👩‍👧 👩‍👩‍👧‍👦 👩‍👩‍👦‍👦 👩‍👩‍👧‍👧 👨‍👦 👨‍👦‍👦 👨‍👧 👨‍👧‍👦 👨‍👧‍👧 👩‍👦 👩‍👦‍👦 👩‍👧 👩‍👧‍👦 👩‍👧‍👧';
        $test = html_entity_decode('&#x1F9D1;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;');
        // @codingStandardsIgnoreLine
        $test .= '👭 👫 👬 💏 👩‍❤️‍💋‍👨 👨‍❤️‍💋‍👨 👩‍❤️‍💋‍👩 💑 👩‍❤️‍👨 👨‍❤️‍👨 👩‍❤️‍👩 👪 👨‍👩‍👦 👨‍👩‍👧 👨‍👩‍👧‍👦 👨‍👩‍👦‍👦 👨‍👩‍👧‍👧 👨‍👨‍👦 👨‍👨‍👧 👨‍👨‍👧‍👦 👨‍👨‍👦‍👦 👨‍👨‍👧‍👧 👩‍👩‍👦 👩‍👩‍👧 👩‍👩‍👧‍👦 👩‍👩‍👦‍👦 👩‍👩‍👧‍👧 👨‍👦 👨‍👦‍👦 👨‍👧 👨‍👧‍👦 👨‍👧‍👧 👩‍👦 👩‍👦‍👦 👩‍👧 👩‍👧‍👦 👩‍👧‍👧';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFamilyEmojis()

    /**
     * Person Symbol
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#person-symbol
     *
     * @return void
     */
    public function testPersonSymbolEmojis(): void
    {
        $original = '🗣 👤 👥 👣';
        $test = '🗣 👤 👥 👣';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPersonSymbolEmojis()

    /**
     * Hair Style
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#hair-style
     *
     * @return void
     */
    public function testHairStyleEmojis(): void
    {
        $original = html_entity_decode('&#x1F9B0; &#x1F9B1; &#x1F9B3; &#x1F9B2;');
        $test = html_entity_decode('&#x1F9B0; &#x1F9B1; &#x1F9B3; &#x1F9B2;');
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testHairStyleEmojis()

    /**
     * Animal Mammal
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#animal-mammal
     *
     * @return void
     */
    public function testAnimalMammalEmojis(): void
    {
        $original = '🐵 🐒 🦍 ';
        $original .= html_entity_decode('&#x1F9A7; ');
        $original .= '🐶 🐕 ';
        $original .= html_entity_decode('&#x1F9AE; &#x1F415;&#x200D;&#x1F9BA; ');
        $original .= '🐩 🐺 🦊 ';
        $original .= html_entity_decode('&#x1F99D; ');
        $original .= '🐱 🐈 🦁 🐯 🐅 🐆 🐴 🐎 🦄 🦓 🦌 🐮 🐂 🐃 🐄 🐷 🐖 🐗 🐽 🐏 🐑 🐐 🐪 🐫 ';
        $original .= html_entity_decode('&#x1F999; ');
        $original .= '🦒 🐘 🦏 ';
        $original .= html_entity_decode('&#x1F99B; ');
        $original .= '🐭 🐁 🐀 🐹 🐰 🐇 🐿 🦔 🦇 🐻 🐨 🐼 ';
        $original .= html_entity_decode('&#x1F9A5; &#x1F9A6; &#x1F9A8; &#x1F998; &#x1F9A1; ');
        $original .= '🐾';
        $test = '🐵 🐒 🦍 ';
        $test .= html_entity_decode('&#x1F9A7; ');
        $test .= '🐶 🐕 ';
        $test .= html_entity_decode('&#x1F9AE; &#x1F415;&#x200D;&#x1F9BA; ');
        $test .= '🐩 🐺 🦊 ';
        $test .= html_entity_decode('&#x1F99D; ');
        $test .= '🐱 🐈 🦁 🐯 🐅 🐆 🐴 🐎 🦄 🦓 🦌 🐮 🐂 🐃 🐄 🐷 🐖 🐗 🐽 🐏 🐑 🐐 🐪 🐫 ';
        $test .= html_entity_decode('&#x1F999; ');
        $test .= '🦒 🐘 🦏 ';
        $test .= html_entity_decode('&#x1F99B; ');
        $test .= '🐭 🐁 🐀 🐹 🐰 🐇 🐿 🦔 🦇 🐻 🐨 🐼 ';
        $test .= html_entity_decode('&#x1F9A5; &#x1F9A6; &#x1F9A8; &#x1F998; &#x1F9A1; ');
        $test .= '🐾';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testAnimalMammalEmojis()

    /**
     * Animal Bird
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#animal-bird
     *
     * @return void
     */
    public function testAnimalBirdEmojis(): void
    {
        $original = '🦃 🐔 🐓 🐣 🐤 🐥 🐦 🐧 🕊 🦅 🦆 ';
        $original .= html_entity_decode('&#x1F9A2; ');
        $original .= '🦉 ';
        $original .= html_entity_decode('&#x1F9A9; &#x1F99A; &#x1F99C;');
        $test = '🦃 🐔 🐓 🐣 🐤 🐥 🐦 🐧 🕊 🦅 🦆 ';
        $test .= html_entity_decode('&#x1F9A2; ');
        $test .= '🦉 ';
        $test .= html_entity_decode('&#x1F9A9; &#x1F99A; &#x1F99C;');
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testAnimalBirdEmojis()

    /**
     * Animal Amphibian
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#animal-amphibian
     *
     * @return void
     */
    public function testAnimalAmphibianEmojis(): void
    {
        $original = '🐸';
        $test = '🐸';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testAnimalAmphibianEmojis()

    /**
     * Animal Reptile
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#animal-reptile
     *
     * @return void
     */
    public function testAnimalReptileEmojis(): void
    {
        $original = '🐊 🐢 🦎 🐍 🐲 🐉 🦕 🦖';
        $test = '🐊 🐢 🦎 🐍 🐲 🐉 🦕 🦖';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testAnimalReptileEmojis()

    /**
     * Animal Marine
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#animal-marine
     *
     * @return void
     */
    public function testAnimalMarineEmojis(): void
    {
        $original = '🐳 🐋 🐬 🐟 🐠 🐡 🦈 🐙 🐚';
        $test = '🐳 🐋 🐬 🐟 🐠 🐡 🦈 🐙 🐚';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testAnimalMarineEmojis()

    /**
     * Animal Bug
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#animal-bug
     *
     * @return void
     */
    public function testAnimalBugEmojis(): void
    {
        $original = '🐌 🦋 🐛 🐜 🐝 🐞 🦗 🕷 🕸 🦂 ';
        $original .= html_entity_decode('&#x1F99F; &#x1F9A0;');
        $test = '🐌 🦋 🐛 🐜 🐝 🐞 🦗 🕷 🕸 🦂 ';
        $test .= html_entity_decode('&#x1F99F; &#x1F9A0;');
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testAnimalBugEmojis()

    /**
     * Plant Flower
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#plant-flower
     *
     * @return void
     */
    public function testPlantFlowerEmojis(): void
    {
        $original = '💐 🌸 💮 🏵 🌹 🥀 🌺 🌻 🌼 🌷';
        $test = '💐 🌸 💮 🏵 🌹 🥀 🌺 🌻 🌼 🌷';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPlantFlowerEmojis()

    /**
     * Plant Other
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#plant-other
     *
     * @return void
     */
    public function testPlantOtherEmojis(): void
    {
        $original = '🌱 🌲 🌳 🌴 🌵 🌾 🌿 ☘ 🍀 🍁 🍂 🍃';
        $test = '🌱 🌲 🌳 🌴 🌵 🌾 🌿 ☘ 🍀 🍁 🍂 🍃';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPlantOtherEmojis()

    /**
     * Food and Fruit
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#food-fruit
     *
     * @return void
     */
    public function testFoodFruitEmojis(): void
    {
        $original = '🍇 🍈 🍉 🍊 🍋 🍌 🍍 ';
        $original .= html_entity_decode('&#x1F96D; ');
        $original .= '🍎 🍏 🍐 🍑 🍒 🍓 🥝 🍅 🥥';
        $test = '🍇 🍈 🍉 🍊 🍋 🍌 🍍 ';
        $test .= html_entity_decode('&#x1F96D; ');
        $test .= '🍎 🍏 🍐 🍑 🍒 🍓 🥝 🍅 🥥';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFoodFruitEmojis()

    /**
     * Food Vegetable
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#food-vegetable
     *
     * @return void
     */
    public function testFoodVegetableEmojis(): void
    {
        $original = '🥑 🍆 🥔 🥕 🌽 🌶 🥒 ';
        $original .= html_entity_decode('&#x1F96C; ');
        $original .= '🥦 ';
        $original .= html_entity_decode('&#x1F9C4; &#x1F9C5 ');
        $original .= '🍄 🥜 🌰';
        $test = '🥑 🍆 🥔 🥕 🌽 🌶 🥒 ';
        $test .= html_entity_decode('&#x1F96C; ');
        $test .= '🥦 ';
        $test .= html_entity_decode('&#x1F9C4; &#x1F9C5 ');
        $test .= '🍄 🥜 🌰';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFoodVegetableEmojis()

    /**
     * Food Prepared
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#food-prepared
     *
     * @return void
     */
    public function testFoodPreparedEmojis(): void
    {
        $original = '🍞 🥐 🥖 🥨 ';
        $original .= html_entity_decode('&#x1F96F; ');
        $original .= '🥞 ';
        $original .= html_entity_decode('&#x1F9C7; ');
        $original .= '🧀 🍖 🍗 🥩 🥓 🍔 🍟 🍕 🌭 🥪 🌮 🌯 🥙 ';
        $original .= html_entity_decode('&#x1F9C6; ');
        $original .= '🥚 🍳 🥘 🍲 🥣 🥗 🍿 ';
        $original .= html_entity_decode('&#x1F9C8; &#x1F9C2; ');
        $original .= '🥫';
        $test = '🍞 🥐 🥖 🥨 ';
        $test .= html_entity_decode('&#x1F96F; ');
        $test .= '🥞 ';
        $test .= html_entity_decode('&#x1F9C7; ');
        $test .= '🧀 🍖 🍗 🥩 🥓 🍔 🍟 🍕 🌭 🥪 🌮 🌯 🥙 ';
        $test .= html_entity_decode('&#x1F9C6; ');
        $test .= '🥚 🍳 🥘 🍲 🥣 🥗 🍿 ';
        $test .= html_entity_decode('&#x1F9C8; &#x1F9C2; ');
        $test .= '🥫';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFoodPreparedEmojis()

    /**
     * Food Asian
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#food-asian
     *
     * @return void
     */
    public function testFoodAsianEmojis(): void
    {
        $original = '🍱 🍘 🍙 🍚 🍛 🍜 🍝 🍠 🍢 🍣 🍤 🍥 ';
        $original .= html_entity_decode('&#x1F96E; ');
        $original .= '🍡 🥟 🥠 🥡';
        $test = '🍱 🍘 🍙 🍚 🍛 🍜 🍝 🍠 🍢 🍣 🍤 🍥 ';
        $test .= html_entity_decode('&#x1F96E; ');
        $test .= '🍡 🥟 🥠 🥡';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFoodAsianEmojis()

    /**
     * Food Marine
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#food-marine
     *
     * @return void
     */
    public function testFoodMarineEmojis(): void
    {
        $original = '🦀 ';
        $original .= html_entity_decode('&#x1F99E; ');
        $original .= '🦐 🦑 ';
        $original .= html_entity_decode('&#x1F9AA;');
        $test = '🦀 ';
        $test .= html_entity_decode('&#x1F99E; ');
        $test .= '🦐 🦑 ';
        $test .= html_entity_decode('&#x1F9AA;');
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFoodMarineEmojis()

    /**
     * Food Sweet
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#food-sweet
     *
     * @return void
     */
    public function testFoodSweetEmojis(): void
    {
        $original = '🍦 🍧 🍨 🍩 🍪 🎂 🍰 ';
        $original .= html_entity_decode('&#x1F9C1; ');
        $original .= '🥧 🍫 🍬 🍭 🍮 🍯';
        $test = '🍦 🍧 🍨 🍩 🍪 🎂 🍰 ';
        $test .= html_entity_decode('&#x1F9C1; ');
        $test .= '🥧 🍫 🍬 🍭 🍮 🍯';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFoodSweetEmojis()

    /**
     * Drink
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#drink
     *
     * @return void
     */
    public function testDrinkEmojis(): void
    {
        $original = '🍼 🥛 ☕ 🍵 🍶 🍾 🍷 🍸 🍹 🍺 🥂 🥃 🥤 ';
        $original .= html_entity_decode('&#x1F9C3; &#x1F9C9; &#x1F9CA');
        $test = '🍼 🥛 ☕ 🍵 🍶 🍾 🍷 🍸 🍹 🍺 🥂 🥃 🥤 ';
        $test .= html_entity_decode('&#x1F9C3; &#x1F9C9; &#x1F9CA');
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testDrinkEmojis()

    /**
     * Dishware
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#dishware
     *
     * @return void
     */
    public function testDishwareEmojis(): void
    {
        $original = '🥢 🍽 🍴 🥄 🔪 🏺';
        $test = '🥢 🍽 🍴 🥄 🔪 🏺';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testDishwareEmojis()

    /**
     * Place Map
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#place-map
     *
     * @return void
     */
    public function testPlaceMapEmojis(): void
    {
        $original = '🌍 🌎 🌏 🌐 🗺 🗾 ';
        $original .= html_entity_decode('&#x1F9ED;');
        $test = '🌍 🌎 🌏 🌐 🗺 🗾 ';
        $test .= html_entity_decode('&#x1F9ED;');
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPlaceMapEmojis()

    /**
     * Place Geographic
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#place-geographic
     *
     * @return void
     */
    public function testPlaceGeographicEmojis(): void
    {
        $original = '🏔 ⛰ 🌋 🗻 🏕 🏖 🏜 🏝 🏞';
        $test = '🏔 ⛰ 🌋 🗻 🏕 🏖 🏜 🏝 🏞';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPlaceGeographicEmojis()

    /**
     * Place Building
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#place-building
     *
     * @return void
     */
    public function testPlaceBuildingEmojis(): void
    {
        $original = '🏟 🏛 🏗 ';
        $original .= html_entity_decode('&#x1F9F1; ');
        $original .= '🏘 🏚 🏠 🏡 🏢 🏣 🏤 🏥 🏦 🏨 🏩 🏪 🏫 🏬 🏭 🏯 🏰 💒 🗼 🗽';
        $test = '🏟 🏛 🏗 ';
        $test .= html_entity_decode('&#x1F9F1; ');
        $test .= '🏘 🏚 🏠 🏡 🏢 🏣 🏤 🏥 🏦 🏨 🏩 🏪 🏫 🏬 🏭 🏯 🏰 💒 🗼 🗽';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPlaceBuildingEmojis()

    /**
     * Place Religious
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#place-religious
     *
     * @return void
     */
    public function testPlaceReligiousEmojis(): void
    {
        $original = '⛪ 🕌 ';
        $original .= html_entity_decode('&#x1F6D5; ');
        $original .= '🕍 ⛩ 🕋';
        $test = '⛪ 🕌 ';
        $test .= html_entity_decode('&#x1F6D5; ');
        $test .= '🕍 ⛩ 🕋';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPlaceReligiousEmojis()

    /**
     * Place Other
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#place-other
     *
     * @return void
     */
    public function testPlaceOtherEmojis(): void
    {
        $original = '⛲ ⛺ 🌁 🌃 🏙 🌄 🌅 🌆 🌇 🌉 ♨ 🎠 🎡 🎢 💈 🎪';
        $test = '⛲ ⛺ 🌁 🌃 🏙 🌄 🌅 🌆 🌇 🌉 ♨ 🎠 🎡 🎢 💈 🎪';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPlaceOtherEmojis()

    /**
     * Transport Ground
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#transport-ground
     *
     * @return void
     */
    public function testTransportGroundEmojis(): void
    {
        $original = '🚂 🚃 🚄 🚅 🚆 🚇 🚈 🚉 🚊 🚝 🚞 🚋 🚌 🚍 🚎 🚐 🚑 🚒 🚓 🚔 🚕 🚖 🚗 🚘 🚙 🚚 🚛 🚜 🏎 🏍 🛵 ';
        $original .= html_entity_decode('&#x1F9BD; &#x1F9BC; &#x1F6FA; ');
        $original .= '🚲 🛴 ';
        $original .= html_entity_decode('&#x1F6F9; ');
        $original .= '🚏 🛣 🛤 🛢 ⛽ 🚨 🚥 🚦 🛑 🚧';
        $test = '🚂 🚃 🚄 🚅 🚆 🚇 🚈 🚉 🚊 🚝 🚞 🚋 🚌 🚍 🚎 🚐 🚑 🚒 🚓 🚔 🚕 🚖 🚗 🚘 🚙 🚚 🚛 🚜 🏎 🏍 🛵 ';
        $test .= html_entity_decode('&#x1F9BD; &#x1F9BC; &#x1F6FA; ');
        $test .= '🚲 🛴 ';
        $test .= html_entity_decode('&#x1F6F9; ');
        $test .= '🚏 🛣 🛤 🛢 ⛽ 🚨 🚥 🚦 🛑 🚧';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testTransportGroundEmojis()

    /**
     * Transport Water
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#transport-water
     *
     * @return void
     */
    public function testTransportWaterEmojis(): void
    {
        $original = '⚓ ⛵ 🛶 🚤 🛳 ⛴ 🛥 🚢';
        $test = '⚓ ⛵ 🛶 🚤 🛳 ⛴ 🛥 🚢';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testTransportWaterEmojis()

    /**
     * Transport Air
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#transport-air
     *
     * @return void
     */
    public function testTransportAirEmojis(): void
    {
        $original = '✈ 🛩 🛫 🛬 ';
        $original .= html_entity_decode('&#x1FA82; ');
        $original .= '💺 🚁 🚟 🚠 🚡 🛰 🚀 🛸';
        $test = '✈ 🛩 🛫 🛬 ';
        $test .= html_entity_decode('&#x1FA82; ');
        $test .= '💺 🚁 🚟 🚠 🚡 🛰 🚀 🛸';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testTransportAirEmojis()

    /**
     * Hotel
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#hotel
     *
     * @return void
     */
    public function testHotelEmojis(): void
    {
        $original = '🛎 ';
        $original .= html_entity_decode('&#x1F9F3;');
        $test = '🛎 ';
        $test .= html_entity_decode('&#x1F9F3;');
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testHotelEmojis()

    /**
     * Time
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#time
     *
     * @return void
     */
    public function testTimeEmojis(): void
    {
        $original = '⌛ ⏳ ⌚ ⏰ ⏱ ⏲ 🕰 🕛 🕧 🕐 🕜 🕑 🕝 🕒 🕞 🕓 🕟 🕔 🕠 🕕 🕡 🕖 🕢 🕗 🕣 🕘 🕤 🕙 🕥 🕚 🕦';
        $test = '⌛ ⏳ ⌚ ⏰ ⏱ ⏲ 🕰 🕛 🕧 🕐 🕜 🕑 🕝 🕒 🕞 🕓 🕟 🕔 🕠 🕕 🕡 🕖 🕢 🕗 🕣 🕘 🕤 🕙 🕥 🕚 🕦';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testTimeEmojis()

    /**
     * Sky and Weather
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#sky_&_weather
     *
     * @return void
     */
    public function testSkyAndWeatherEmojis(): void
    {
        $original = '🌑 🌒 🌓 🌔 🌕 🌖 🌗 🌘 🌙 🌚 🌛 🌜 🌡 ☀ 🌝 🌞 ';
        $original .= html_entity_decode('&#x1FA90; ');
        $original .= '⭐ 🌟 🌠 🌌 ☁ ⛅ ⛈ 🌤 🌥 🌦 🌧 🌨 🌩 🌪 🌫 🌬 🌀 🌈 🌂 ☂ ☔ ⛱ ⚡ ❄ ☃ ⛄ ☄ 🔥 💧 🌊';
        $test = '🌑 🌒 🌓 🌔 🌕 🌖 🌗 🌘 🌙 🌚 🌛 🌜 🌡 ☀ 🌝 🌞 ';
        $test .= html_entity_decode('&#x1FA90; ');
        $test .= '⭐ 🌟 🌠 🌌 ☁ ⛅ ⛈ 🌤 🌥 🌦 🌧 🌨 🌩 🌪 🌫 🌬 🌀 🌈 🌂 ☂ ☔ ⛱ ⚡ ❄ ☃ ⛄ ☄ 🔥 💧 🌊';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkyAndWeatherEmojis()

    /**
     * Event
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#event
     *
     * @return void
     */
    public function testEventEmojis(): void
    {
        $original = '🎃 🎄 🎆 🎇 ';
        $original .= html_entity_decode('&#x1F9E8; ');
        $original .= '✨ 🎈 🎉 🎊 🎋 🎍 🎎 🎏 🎐 ';
        $original .= html_entity_decode('&#x1F9E7; ');
        $original .= '🎀 🎁 🎗 🎟 🎫';
        $test = '🎃 🎄 🎆 🎇 ';
        $test .= html_entity_decode('&#x1F9E8; ');
        $test .= '✨ 🎈 🎉 🎊 🎋 🎍 🎎 🎏 🎐 ';
        $test .= html_entity_decode('&#x1F9E7; ');
        $test .= '🎀 🎁 🎗 🎟 🎫';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testEventEmojis()

    /**
     * Award Medal
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#award-medal
     *
     * @return void
     */
    public function testAwardMedalEmojis(): void
    {
        $original = '🎖 🏆 🏅 🥇 🥈 🥉';
        $test = '🎖 🏆 🏅 🥇 🥈 🥉';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testAwardMedalEmojis()

    /**
     * Sport
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#sport
     *
     * @return void
     */
    public function testSportEmojis(): void
    {
        $original = '⚽ ⚾ ';
        $original .= html_entity_decode('&#x1F94E; ');
        $original .= '🏀 🏐 🏈 🏉 🎾 ';
        $original .= html_entity_decode('&#x1F94F; ');
        $original .= '🎳 🏏 🏑 🏒 ';
        $original .= html_entity_decode('&#x1F94D; ');
        $original .= '🏓 🏸 🥊 🥋 🥅 ⛳ ⛸ 🎣 ';
        $original .= html_entity_decode('&#x1F93F; ');
        $original .= '🎽 🎿 🛷 🥌';
        $test = '⚽ ⚾ ';
        $test .= html_entity_decode('&#x1F94E; ');
        $test .= '🏀 🏐 🏈 🏉 🎾 ';
        $test .= html_entity_decode('&#x1F94F; ');
        $test .= '🎳 🏏 🏑 🏒 ';
        $test .= html_entity_decode('&#x1F94D; ');
        $test .= '🏓 🏸 🥊 🥋 🥅 ⛳ ⛸ 🎣 ';
        $test .= html_entity_decode('&#x1F93F; ');
        $test .= '🎽 🎿 🛷 🥌';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSportEmojis()

    /**
     * Game
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#game
     *
     * @return void
     */
    public function testGameEmojis(): void
    {
        $original = '🎯 ';
        $original .= html_entity_decode('&#x1FA80; &#x1FA81; ');
        $original .= '🎱 🔮 ';
        $original .= html_entity_decode('&#x1F9FF; ');
        $original .= '🎮 🕹 🎰 🎲 ';
        $original .= html_entity_decode('&#x1F9E9; &#x1F9F8; ');
        $original .= '♠ ♥ ♦ ♣ ♟ 🃏 🀄 🎴';
        $test = '🎯 ';
        $test .= html_entity_decode('&#x1FA80; &#x1FA81; ');
        $test .= '🎱 🔮 ';
        $test .= html_entity_decode('&#x1F9FF; ');
        $test .= '🎮 🕹 🎰 🎲 ';
        $test .= html_entity_decode('&#x1F9E9; &#x1F9F8; ');
        $test .= '♠ ♥ ♦ ♣ ♟ 🃏 🀄 🎴';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test, true);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testGameEmojis()

    /**
     * Arts and Crafts
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#arts_&_crafts
     *
     * @return void
     */
    public function testArtsAndCraftsEmojis(): void
    {
        $original = '🎭 🖼 🎨 ';
        $original .= html_entity_decode('&#x1F9F5; &#x1F9F6;');
        $test = '🎭 🖼 🎨 ';
        $test .= html_entity_decode('&#x1F9F5; &#x1F9F6;');
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testArtsAndCraftsEmojis()

    /**
     * Clothing
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#clothing
     *
     * @return void
     */
    public function testClothingEmojis(): void
    {
        $original = '👓 🕶 ';
        $original .= html_entity_decode('&#x1F97D; &#x1F97C; &#x1F9BA; ');
        $original .= '👔 👕 👖 🧣 🧤 🧥 🧦 👗 👘 ';
        $original .= html_entity_decode('&#x1F97B; &#x1FA71; &#x1FA72; &#x1FA73; ');
        $original .= '👙 👚 👛 👜 👝 🛍 🎒 👞 👟 ';
        $original .= html_entity_decode('&#x1F97E; &#x1F97F; ');
        $original .= '👠 👡 ';
        $original .= html_entity_decode('&#x1FA70; ');
        $original .= '👢 👑 👒 🎩 🎓 🧢 ⛑ 📿 💄 💍 💎';
        $test = '👓 🕶 ';
        $test .= html_entity_decode('&#x1F97D; &#x1F97C; &#x1F9BA; ');
        $test .= '👔 👕 👖 🧣 🧤 🧥 🧦 👗 👘 ';
        $test .= html_entity_decode('&#x1F97B; &#x1FA71; &#x1FA72; &#x1FA73; ');
        $test .= '👙 👚 👛 👜 👝 🛍 🎒 👞 👟 ';
        $test .= html_entity_decode('&#x1F97E; &#x1F97F; ');
        $test .= '👠 👡 ';
        $test .= html_entity_decode('&#x1FA70; ');
        $test .= '👢 👑 👒 🎩 🎓 🧢 ⛑ 📿 💄 💍 💎';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testClothingEmojis()

    /**
     * Sound
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#sound
     *
     * @return void
     */
    public function testSoundEmojis(): void
    {
        $original = '🔇 🔈 🔉 🔊 📢 📣 📯 🔔 🔕';
        $test = '🔇 🔈 🔉 🔊 📢 📣 📯 🔔 🔕';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSoundEmojis()

    /**
     * Music
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#music
     *
     * @return void
     */
    public function testMusicEmojis(): void
    {
        $original = '🎼 🎵 🎶 🎙 🎚 🎛 🎤 🎧 📻';
        $test = '🎼 🎵 🎶 🎙 🎚 🎛 🎤 🎧 📻';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testMusicEmojis()

    /**
     * Musical Instruments
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#musical-instrument
     *
     * @return void
     */
    public function testMusicalInstrumentEmojis(): void
    {
        $original = '🎷 🎸 🎹 🎺 🎻 ';
        $original .= html_entity_decode('&#x1FA95; ');
        $original .= '🥁';
        $test = '🎷 🎸 🎹 🎺 🎻 ';
        $test .= html_entity_decode('&#x1FA95; ');
        $test .= '🥁';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testMusicalInstrumentEmojis()

    /**
     * Phone
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#phone
     *
     * @return void
     */
    public function testPhoneEmojis(): void
    {
        $original = '📱 📲 ☎ 📞 📟 📠';
        $test = '📱 📲 ☎ 📞 📟 📠';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testPhoneEmojis()

    /**
     * Computer
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#computer
     *
     * @return void
     */
    public function testComputerEmojis(): void
    {
        $original = '🔋 🔌 💻 🖥 🖨 ⌨ 🖱 🖲 💽 💾 💿 📀 ';
        $original .= html_entity_decode('&#x1F9EE;');
        $test = '🔋 🔌 💻 🖥 🖨 ⌨ 🖱 🖲 💽 💾 💿 📀 ';
        $test .= html_entity_decode('&#x1F9EE;');
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testComputerEmojis()

    /**
     * Light and Video
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#light_&_video
     *
     * @return void
     */
    public function testLightAndVideoEmojis(): void
    {
        $original = '🎥 🎞 📽 🎬 📺 📷 📸 📹 📼 🔍 🔎 🕯 💡 🔦 🏮 ';
        $original .= html_entity_decode('&#x1FA94;');
        $test = '🎥 🎞 📽 🎬 📺 📷 📸 📹 📼 🔍 🔎 🕯 💡 🔦 🏮 ';
        $test .= html_entity_decode('&#x1FA94;');
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testLightAndVideoEmojis()

    /**
     * Book-Paper
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#book-paper
     *
     * @return void
     */
    public function testBookPaperEmojis(): void
    {
        $original = '📔 📕 📖 📗 📘 📙 📚 📓 📒 📃 📜 📄 📰 🗞 📑 🔖 🏷';
        $test = '📔 📕 📖 📗 📘 📙 📚 📓 📒 📃 📜 📄 📰 🗞 📑 🔖 🏷';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testBookPaperEmojis()

    /**
     * Money Emojis
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#money
     *
     * @return void
     */
    public function testMoneyEmojis(): void
    {
        $original = '💰 💴 💵 💶 💷 💸 💳 ';
        $original .= html_entity_decode('&#x1F9FE; ');
        $original .= '💹 💱 💲';
        $test = '💰 💴 💵 💶 💷 💸 💳 ';
        $test .= html_entity_decode('&#x1F9FE; ');
        $test .= '💹 💱 💲';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testMoneyEmojis()

    /**
     * Mail Emojis
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#mail
     *
     * @return void
     */
    public function testMailEmojis(): void
    {
        $original = '✉ 📧 📨 📩 📤 📥 📦 📫 📪 📬 📭 📮 🗳';
        $test = '✉ 📧 📨 📩 📤 📥 📦 📫 📪 📬 📭 📮 🗳';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testMailEmojis()

    /**
     * Writing Emojis
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#writing
     *
     * @return void
     */
    public function testWritingEmojis(): void
    {
        $original = '✏ ✒ 🖋 🖊 🖌 🖍 📝';
        $test = '✏ ✒ 🖋 🖊 🖌 🖍 📝';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testWritingEmojis()

    /**
     * Office Emojis
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#office
     *
     * @return void
     */
    public function testOfficeEmojis(): void
    {
        $original = '💼 📁 📂 🗂 📅 📆 🗒 🗓 📇 📈 📉 📊 📋 📌 📍 📎 🖇 📏 📐 ✂ 🗃 🗄 🗑';
        $test = '💼 📁 📂 🗂 📅 📆 🗒 🗓 📇 📈 📉 📊 📋 📌 📍 📎 🖇 📏 📐 ✂ 🗃 🗄 🗑';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testOfficeEmojis()

    /**
     * Lock
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#lock
     *
     * @return void
     */
    public function testLockEmojis(): void
    {
        $original = '🔒 🔓 🔏 🔐 🔑 🗝';
        $test = '🔒 🔓 🔏 🔐 🔑 🗝';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testLockEmojis()

    /**
     * Tool
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#tool
     *
     * @return void
     */
    public function testToolEmojis(): void
    {
        $original = '🔨 ';
        $original .= html_entity_decode('&#x1FA93; ');
        $original .= '⛏ ⚒ 🛠 🗡 ⚔ 🔫 🏹 🛡 🔧 🔩 ⚙ 🗜 ⚖ ';
        $original .= html_entity_decode('&#x1F9AF; ');
        $original .= '🔗 ⛓ ';
        $original .= html_entity_decode('&#x1F9F0; &#x1F9F2;');
        $test = '🔨 ';
        $test .= html_entity_decode('&#x1FA93; ');
        $test .= '⛏ ⚒ 🛠 🗡 ⚔ 🔫 🏹 🛡 🔧 🔩 ⚙ 🗜 ⚖ ';
        $test .= html_entity_decode('&#x1F9AF; ');
        $test .= '🔗 ⛓ ';
        $test .= html_entity_decode('&#x1F9F0; &#x1F9F2;');
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testToolEmojis()

    /**
     * Science
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#science
     *
     * @return void
     */
    public function testScienceEmojis(): void
    {
        $original = '⚗ ';
        $original .= html_entity_decode('&#x1F9EA; &#x1F9EB; &#x1F9EC; ');
        $original .= '🔬 🔭 📡';
        $test = '⚗ ';
        $test .= html_entity_decode('&#x1F9EA; &#x1F9EB; &#x1F9EC; ');
        $test .= '🔬 🔭 📡';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testScienceEmojis()

    /**
     * Medical
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#medical
     *
     * @return void
     */
    public function testMedicalEmojis(): void
    {
        $original = '💉 ';
        $original .= html_entity_decode('&#x1FA78; ');
        $original .= '💊 ';
        $original .= html_entity_decode('&#x1FA79; &#x1FA7A;');
        $test = '💉 ';
        $test .= html_entity_decode('&#x1FA78; ');
        $test .= '💊 ';
        $test .= html_entity_decode('&#x1FA79; &#x1FA7A;');
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testMedicalEmojis()

    /**
     * Household
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#household
     *
     * @return void
     */
    public function testHouseholdEmojis(): void
    {
        $original = '🚪 🛏 🛋 ';
        $original .= html_entity_decode('&#x1FA91; ');
        $original .= '🚽 🚿 🛁 ';
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1FA92; &#x1F9F4; &#x1F9F7; &#x1F9F9; &#x1F9FA; &#x1F9FB; &#x1F9FC; &#x1F9FD; &#x1F9EF; ');
        $original .= '🛒';
        $test = '🚪 🛏 🛋 ';
        $test .= html_entity_decode('&#x1FA91; ');
        $test .= '🚽 🚿 🛁 ';
        // @codingStandardsIgnoreLine
        $test .= html_entity_decode('&#x1FA92; &#x1F9F4; &#x1F9F7; &#x1F9F9; &#x1F9FA; &#x1F9FB; &#x1F9FC; &#x1F9FD; &#x1F9EF; ');
        $test .= '🛒';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testHouseholdEmojis()

    /**
     * Other Object
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#other-object
     *
     * @return void
     */
    public function testOtherObjectEmojis(): void
    {
        $original = '🚬 ⚰ ⚱ 🗿';
        $test = '🚬 ⚰ ⚱ 🗿';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testOtherObjectEmojis()

    /**
     * Transportation Sign
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#transport-sign
     *
     * @return void
     */
    public function testTransportationSignEmojis(): void
    {
        $original = '🏧 🚮 🚰 ♿ 🚹 🚺 🚻 🚼 🚾 🛂 🛃 🛄 🛅';
        $test = '🏧 🚮 🚰 ♿ 🚹 🚺 🚻 🚼 🚾 🛂 🛃 🛄 🛅';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testTransportationSignEmojis()

    /**
     * Warning
     *
     * #see https://unicode.org/emoji/charts/full-emoji-list.html#warning
     *
     * @return void
     */
    public function testWarningEmojis(): void
    {
        $original = '⚠ 🚸 ⛔ 🚫 🚳 🚭 🚯 🚱 🚷 📵 🔞 ☢ ☣';
        $test = '⚠ 🚸 ⛔ 🚫 🚳 🚭 🚯 🚱 🚷 📵 🔞 ☢ ☣';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testWarningEmojis()

    /**
     * Arrow
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#arrow
     *
     * @return void
     */
    public function testArrowEmojis(): void
    {
        $original = '⬆ ↗ ➡ ↘ ⬇ ↙ ⬅ ↖ ↕ ↔ ↩ ↪ ⤴ ⤵ 🔃 🔄 🔙 🔚 🔛 🔜 🔝';
        $test = '⬆ ↗ ➡ ↘ ⬇ ↙ ⬅ ↖ ↕ ↔ ↩ ↪ ⤴ ⤵ 🔃 🔄 🔙 🔚 🔛 🔜 🔝';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testArrowEmojis()

    /**
     * Religion
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#religion
     *
     * @return void
     */
    public function testReligionEmojis(): void
    {
        $original = '🛐 ⚛ 🕉 ✡ ☸ ☯ ✝ ☦ ☪ ☮ 🕎 🔯';
        $test = '🛐 ⚛ 🕉 ✡ ☸ ☯ ✝ ☦ ☪ ☮ 🕎 🔯';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testReligionEmojis()

    /**
     * Zodiac
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#zodiac
     *
     * @return void
     */
    public function testZodiacEmojis(): void
    {
        $original = '♈ ♉ ♊ ♋ ♌ ♍ ♎ ♏ ♐ ♑ ♒ ♓ ⛎';
        $test = '♈ ♉ ♊ ♋ ♌ ♍ ♎ ♏ ♐ ♑ ♒ ♓ ⛎';
        //EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testZodiacEmojis()

    /**
     * AV Symbol
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#av-symbol
     *
     * @return void
     */
    public function testAvSymbolEmojis(): void
    {
        $original = '🔀 🔁 🔂 ▶ ⏩ ⏭ ⏯ ◀ ⏪ ⏮ 🔼 ⏫ 🔽 ⏬ ⏸ ⏹ ⏺ ⏏ 🎦 🔅 🔆 📶 📳 📴';
        $test = '🔀 🔁 🔂 ▶ ⏩ ⏭ ⏯ ◀ ⏪ ⏮ 🔼 ⏫ 🔽 ⏬ ⏸ ⏹ ⏺ ⏏ 🎦 🔅 🔆 📶 📳 📴';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testAvSymbolEmojis()

    /**
     * Gender
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#gender
     *
     * @return void
     */
    public function testGenderEmojis(): void
    {
        $original = '♀ ♂';
        $test = '♀ ♂';
        //EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testGenderEmojis()

    /**
     * Other Symbol
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#other-symbol
     *
     * @return void
     */
    public function testOtherSymbolEmojis(): void
    {
        $original = '⚕ ♾ ♻ ⚜ 🔱 📛 🔰 ⭕ ✅ ☑ ✔ ✖ ❌ ❎ ➕ ➖ ➗ ➰ ➿ 〽 ✳ ✴ ❇ ‼ ⁉ ❓ ❔ ❕ ❗ 〰 © ® ™';
        $test = '⚕ ♾ ♻ ⚜ 🔱 📛 🔰 ⭕ ✅ ☑ ✔ ✖ ❌ ❎ ➕ ➖ ➗ ➰ ➿ 〽 ✳ ✴ ❇ ‼ ⁉ ❓ ❔ ❕ ❗ 〰 © ® ™';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test, true);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testOtherSymbolEmojis()

    /**
     * Keycap
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#keycap
     *
     * @return void
     */
    public function testKeycapEmojis(): void
    {
        $original = '#️⃣ *️⃣ 0️⃣ 1️⃣ 2️⃣ 3️⃣ 4️⃣ 5️⃣ 6️⃣ 7️⃣ 8️⃣ 9️⃣ 🔟';
        $test = '#️⃣ *️⃣ 0️⃣ 1️⃣ 2️⃣ 3️⃣ 4️⃣ 5️⃣ 6️⃣ 7️⃣ 8️⃣ 9️⃣ 🔟';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testKeycapEmojis()

    /**
     * Alphanum
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#alphanum
     *
     * @return void
     */
    public function testAlphanumEmojis(): void
    {
        $original = '🔠 🔡 🔢 🔣 🔤 🅰 🆎 🅱 🆑 🆒 🆓 ℹ 🆔 Ⓜ 🆕 🆖 🅾 🆗 🅿 🆘 🆙 🆚 🈁 🈂 🈷 🈶 🈯 🉐 🈹 🈚 🈲 🉑 🈸 🈴 🈳 ㊗ ㊙ 🈺 🈵';
        $test = '🔠 🔡 🔢 🔣 🔤 🅰 🆎 🅱 🆑 🆒 🆓 ℹ 🆔 Ⓜ 🆕 🆖 🅾 🆗 🅿 🆘 🆙 🆚 🈁 🈂 🈷 🈶 🈯 🉐 🈹 🈚 🈲 🉑 🈸 🈴 🈳 ㊗ ㊙ 🈺 🈵';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testAlphanumEmojis()

    /**
     * Geometric
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#geometric
     *
     * @return void
     */
    public function testGeometricEmojis(): void
    {
        $original = '🔴 ';
        $original .= html_entity_decode('&#x1F7E0; &#x1F7E1; &#x1F7E2; ');
        $original .= '🔵 ';
        $original .= html_entity_decode('&#x1F7E3; &#x1F7E4; ');
        $original .= '⚫ ⚪ ';
        $original .= html_entity_decode('&#x1F7E5; &#x1F7E7; &#x1F7E8; &#x1F7E9; &#x1F7E6; &#x1F7EA; &#x1F7EB; ');
        $original .= '⬛ ⬜ ◼ ◻ ◾ ◽ ▪ ▫ 🔶 🔷 🔸 🔹 🔺 🔻 💠 🔘 🔳 🔲';
        $test = '🔴 ';
        $test .= html_entity_decode('&#x1F7E0; &#x1F7E1; &#x1F7E2; ');
        $test .= '🔵 ';
        $test .= html_entity_decode('&#x1F7E3; &#x1F7E4; ');
        $test .= '⚫ ⚪ ';
        $test .= html_entity_decode('&#x1F7E5; &#x1F7E7; &#x1F7E8; &#x1F7E9; &#x1F7E6; &#x1F7EA; &#x1F7EB; ');
        $test .= '⬛ ⬜ ◼ ◻ ◾ ◽ ▪ ▫ 🔶 🔷 🔸 🔹 🔺 🔻 💠 🔘 🔳 🔲';
        EmojiEntity::threeByteEncode($test);
        EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testGeometricEmojis()

    /**
     * Flag
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#flag
     *
     * @return void
     */
    public function testFlagEmojis(): void
    {
        $original = '🏁 🚩 🎌 🏴 🏳 🏳️‍🌈';
        $original .= html_entity_decode('&#x1F3F4;&#x200D;&#x2620;&#xFE0F;');
        $test = '🏁 🚩 🎌 🏴 🏳 🏳️‍🌈';
        $test .= html_entity_decode('&#x1F3F4;&#x200D;&#x2620;&#xFE0F;');
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testFlagEmojis()

    /**
     * Country Flags
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#country-flag
     *
     * @return void
     */
    public function testCountryFlagEmojis(): void
    {
        $original = '🇦🇨 🇦🇩 🇦🇪 🇦🇫 🇦🇬 🇦🇮 🇦🇱 🇦🇲 🇦🇴 🇦🇶 🇦🇷 🇦🇸 🇦🇹 🇦🇺 🇦🇼 🇦🇽 🇦🇿 🇧🇦 🇧🇧 🇧🇩 🇧🇪 🇧🇫 🇧🇬 🇧🇭 🇧🇮 🇧🇯 ';
        $original .= html_entity_decode('&#x1F1E7;&#x1F1F1; ');
        $original .= '🇧🇲 🇧🇳 🇧🇴 ';
        $original .= html_entity_decode('&#x1F1E7;&#x1F1F6; ');
        $original .= '🇧🇷 🇧🇸 🇧🇹 🇧🇻 🇧🇼 🇧🇾 🇧🇿 🇨🇦 🇨🇨 🇨🇩 🇨🇫 🇨🇬 🇨🇭 🇨🇮 🇨🇰 🇨🇱 🇨🇲 🇨🇳 🇨🇴 🇨🇵 🇨🇷 🇨🇺 🇨🇻 🇨🇼 🇨🇽 🇨🇾 🇨🇿 🇩🇪 ';
        $original .= html_entity_decode('&#x1F1E9;&#x1F1EC; ');
        $original .= '🇩🇯 🇩🇰 🇩🇲 🇩🇴 🇩🇿 ';
        $original .= html_entity_decode('&#x1F1EA;&#x1F1E6; ');
        $original .= '🇪🇨 🇪🇪 🇪🇬 ';
        $original .= html_entity_decode('&#x1F1EA;&#x1F1ED; ');
        $original .= '🇪🇷 🇪🇸 🇪🇹 🇪🇺 🇫🇮 🇫🇯 ';
        $original .= html_entity_decode('&#x1F1EB;&#x1F1F0; ');
        $original .= '🇫🇲 🇫🇴 🇫🇷 🇬🇦 🇬🇧 🇬🇩 🇬🇪 ';
        $original .= html_entity_decode('&#x1F1EC;&#x1F1EB; ');
        $original .= '🇬🇬 🇬🇭 🇬🇮 🇬🇱 🇬🇲 🇬🇳 ';
        $original .= html_entity_decode('&#x1F1EC;&#x1F1F5; ');
        $original .= '🇬🇶 🇬🇷 ';
        $original .= html_entity_decode('&#x1F1EC;&#x1F1F8; ');
        $original .= '🇬🇹 🇬🇺 🇬🇼 🇬🇾 🇭🇰 🇭🇲 🇭🇳 🇭🇷 🇭🇹 🇭🇺 🇮🇨 🇮🇩 🇮🇪 🇮🇱 🇮🇲 🇮🇳 🇮🇴 🇮🇶 🇮🇷 🇮🇸 🇮🇹 🇯🇪 🇯🇲 🇯🇴 🇯🇵 🇰🇪 🇰🇬 🇰🇭 🇰🇮 🇰🇲 🇰🇳 🇰🇵 ';
        $original .= '🇰🇷 🇰🇼 🇰🇾 🇰🇿 🇱🇦 🇱🇧 🇱🇨 🇱🇮 🇱🇰 🇱🇷 🇱🇸 🇱🇹 🇱🇺 🇱🇻 🇱🇾 🇲🇦 🇲🇨 🇲🇩 🇲🇪 ';
        $original .= html_entity_decode('&#x1F1F2;&#x1F1EB; ');
        $original .= '🇲🇬 🇲🇭 🇲🇰 🇲🇱 🇲🇲 🇲🇳 🇲🇴 🇲🇵 ';
        $original .= html_entity_decode('&#x1F1F2;&#x1F1F6; ');
        $original .= '🇲🇷 🇲🇸 🇲🇹 🇲🇺 🇲🇻 🇲🇼 🇲🇽 🇲🇾 🇲🇿 🇳🇦 ';
        $original .= html_entity_decode('&#x1F1F3;&#x1F1E8; ');
        $original .= '🇳🇪 🇳🇫 🇳🇬 🇳🇮 🇳🇱 🇳🇴 🇳🇵 🇳🇷 🇳🇺 🇳🇿 🇴🇲 🇵🇦 🇵🇪 🇵🇫 🇵🇬 🇵🇭 🇵🇰 🇵🇱 ';
        $original .= html_entity_decode('&#x1F1F5;&#x1F1F2; ');
        $original .= '🇵🇳 🇵🇷 🇵🇸 🇵🇹 🇵🇼 🇵🇾 🇶🇦 ';
        $original .= html_entity_decode('&#x1F1F7;&#x1F1EA; ');
        $original .= '🇷🇴 🇷🇸 🇷🇺 🇷🇼 🇸🇦 🇸🇧 🇸🇨 🇸🇩 🇸🇪 🇸🇬 🇸🇭 🇸🇮 🇸🇯 🇸🇰 🇸🇱 🇸🇲 🇸🇳 🇸🇴 🇸🇷 🇸🇸 🇸🇹 🇸🇻 🇸🇽 🇸🇾 🇸🇿 🇹🇦 🇹🇨 🇹🇩 ';
        $original .= html_entity_decode('&#x1F1F9;&#x1F1EB; ');
        $original .= '🇹🇬 🇹🇭 🇹🇯 🇹🇰 🇹🇱 🇹🇲 🇹🇳 🇹🇴 🇹🇷 🇹🇹 🇹🇻 🇹🇼 🇹🇿 🇺🇦 🇺🇬 🇺🇲 🇺🇳 🇺🇸 🇺🇾 🇺🇿 🇻🇦 🇻🇨 🇻🇪 🇻🇬 🇻🇮 🇻🇳 🇻🇺 ';
        $original .= html_entity_decode('&#x1F1FC;&#x1F1EB; ');
        $original .= '🇼🇸 ';
        $original .= html_entity_decode('&#x1F1FD;&#x1F1F0; ');
        $original .= '🇾🇪 ';
        $original .= html_entity_decode('&#x1F1FE;&#x1F1F9; ');
        $original .= '🇿🇦 🇿🇲 🇿🇼';
        $test = '🇦🇨 🇦🇩 🇦🇪 🇦🇫 🇦🇬 🇦🇮 🇦🇱 🇦🇲 🇦🇴 🇦🇶 🇦🇷 🇦🇸 🇦🇹 🇦🇺 🇦🇼 🇦🇽 🇦🇿 🇧🇦 🇧🇧 🇧🇩 🇧🇪 🇧🇫 🇧🇬 🇧🇭 🇧🇮 🇧🇯 ';
        $test .= html_entity_decode('&#x1F1E7;&#x1F1F1; ');
        $test .= '🇧🇲 🇧🇳 🇧🇴 ';
        $test .= html_entity_decode('&#x1F1E7;&#x1F1F6; ');
        $test .= '🇧🇷 🇧🇸 🇧🇹 🇧🇻 🇧🇼 🇧🇾 🇧🇿 🇨🇦 🇨🇨 🇨🇩 🇨🇫 🇨🇬 🇨🇭 🇨🇮 🇨🇰 🇨🇱 🇨🇲 🇨🇳 🇨🇴 🇨🇵 🇨🇷 🇨🇺 🇨🇻 🇨🇼 🇨🇽 🇨🇾 🇨🇿 🇩🇪 ';
        $test .= html_entity_decode('&#x1F1E9;&#x1F1EC; ');
        $test .= '🇩🇯 🇩🇰 🇩🇲 🇩🇴 🇩🇿 ';
        $test .= html_entity_decode('&#x1F1EA;&#x1F1E6; ');
        $test .= '🇪🇨 🇪🇪 🇪🇬 ';
        $test .= html_entity_decode('&#x1F1EA;&#x1F1ED; ');
        $test .= '🇪🇷 🇪🇸 🇪🇹 🇪🇺 🇫🇮 🇫🇯 ';
        $test .= html_entity_decode('&#x1F1EB;&#x1F1F0; ');
        $test .= '🇫🇲 🇫🇴 🇫🇷 🇬🇦 🇬🇧 🇬🇩 🇬🇪 ';
        $test .= html_entity_decode('&#x1F1EC;&#x1F1EB; ');
        $test .= '🇬🇬 🇬🇭 🇬🇮 🇬🇱 🇬🇲 🇬🇳 ';
        $test .= html_entity_decode('&#x1F1EC;&#x1F1F5; ');
        $test .= '🇬🇶 🇬🇷 ';
        $test .= html_entity_decode('&#x1F1EC;&#x1F1F8; ');
        $test .= '🇬🇹 🇬🇺 🇬🇼 🇬🇾 🇭🇰 🇭🇲 🇭🇳 🇭🇷 🇭🇹 🇭🇺 🇮🇨 🇮🇩 🇮🇪 🇮🇱 🇮🇲 🇮🇳 🇮🇴 🇮🇶 🇮🇷 🇮🇸 🇮🇹 🇯🇪 🇯🇲 🇯🇴 🇯🇵 🇰🇪 🇰🇬 🇰🇭 🇰🇮 🇰🇲 🇰🇳 🇰🇵 ';
        $test .= '🇰🇷 🇰🇼 🇰🇾 🇰🇿 🇱🇦 🇱🇧 🇱🇨 🇱🇮 🇱🇰 🇱🇷 🇱🇸 🇱🇹 🇱🇺 🇱🇻 🇱🇾 🇲🇦 🇲🇨 🇲🇩 🇲🇪 ';
        $test .= html_entity_decode('&#x1F1F2;&#x1F1EB; ');
        $test .= '🇲🇬 🇲🇭 🇲🇰 🇲🇱 🇲🇲 🇲🇳 🇲🇴 🇲🇵 ';
        $test .= html_entity_decode('&#x1F1F2;&#x1F1F6; ');
        $test .= '🇲🇷 🇲🇸 🇲🇹 🇲🇺 🇲🇻 🇲🇼 🇲🇽 🇲🇾 🇲🇿 🇳🇦 ';
        $test .= html_entity_decode('&#x1F1F3;&#x1F1E8; ');
        $test .= '🇳🇪 🇳🇫 🇳🇬 🇳🇮 🇳🇱 🇳🇴 🇳🇵 🇳🇷 🇳🇺 🇳🇿 🇴🇲 🇵🇦 🇵🇪 🇵🇫 🇵🇬 🇵🇭 🇵🇰 🇵🇱 ';
        $test .= html_entity_decode('&#x1F1F5;&#x1F1F2; ');
        $test .= '🇵🇳 🇵🇷 🇵🇸 🇵🇹 🇵🇼 🇵🇾 🇶🇦 ';
        $test .= html_entity_decode('&#x1F1F7;&#x1F1EA; ');
        $test .= '🇷🇴 🇷🇸 🇷🇺 🇷🇼 🇸🇦 🇸🇧 🇸🇨 🇸🇩 🇸🇪 🇸🇬 🇸🇭 🇸🇮 🇸🇯 🇸🇰 🇸🇱 🇸🇲 🇸🇳 🇸🇴 🇸🇷 🇸🇸 🇸🇹 🇸🇻 🇸🇽 🇸🇾 🇸🇿 🇹🇦 🇹🇨 🇹🇩 ';
        $test .= html_entity_decode('&#x1F1F9;&#x1F1EB; ');
        $test .= '🇹🇬 🇹🇭 🇹🇯 🇹🇰 🇹🇱 🇹🇲 🇹🇳 🇹🇴 🇹🇷 🇹🇹 🇹🇻 🇹🇼 🇹🇿 🇺🇦 🇺🇬 🇺🇲 🇺🇳 🇺🇸 🇺🇾 🇺🇿 🇻🇦 🇻🇨 🇻🇪 🇻🇬 🇻🇮 🇻🇳 🇻🇺 ';
        $test .= html_entity_decode('&#x1F1FC;&#x1F1EB; ');
        $test .= '🇼🇸 ';
        $test .= html_entity_decode('&#x1F1FD;&#x1F1F0; ');
        $test .= '🇾🇪 ';
        $test .= html_entity_decode('&#x1F1FE;&#x1F1F9; ');
        $test .= '🇿🇦 🇿🇲 🇿🇼';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testCountryFlagEmojis()

    /**
     * Subdivision flag
     *
     * @see https://unicode.org/emoji/charts/full-emoji-list.html#subdivision-flag
     *
     * @return void
     */
    public function testSubdivisionFlagEmojis(): void
    {
        $original = '🏴󠁧󠁢󠁥󠁮󠁧󠁿 🏴󠁧󠁢󠁳󠁣󠁴󠁿 🏴󠁧󠁢󠁷󠁬󠁳󠁿';
        $test = '🏴󠁧󠁢󠁥󠁮󠁧󠁿 🏴󠁧󠁢󠁳󠁣󠁴󠁿 🏴󠁧󠁢󠁷󠁬󠁳󠁿';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSubdivisionFlagEmojis()

    /**
     * Skin Tone Hand Fingers Open
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#hand-fingers-open
     *
     * @return void
     */
    public function testSkinToneHandFingersOpenEmojis(): void
    {
        $original = '👋🏻 👋🏼 👋🏽 👋🏾 👋🏿 🤚🏻 🤚🏼 🤚🏽 🤚🏾 🤚🏿 🖐🏻 🖐🏼 🖐🏽 🖐🏾 🖐🏿 ✋🏻 ✋🏼 ✋🏽 ✋🏾 ✋🏿 🖖🏻 🖖🏼 🖖🏽 🖖🏾 🖖🏿';
        $test = '👋🏻 👋🏼 👋🏽 👋🏾 👋🏿 🤚🏻 🤚🏼 🤚🏽 🤚🏾 🤚🏿 🖐🏻 🖐🏼 🖐🏽 🖐🏾 🖐🏿 ✋🏻 ✋🏼 ✋🏽 ✋🏾 ✋🏿 🖖🏻 🖖🏼 🖖🏽 🖖🏾 🖖🏿';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinToneHandFingersOpenEmojis()

    /**
     * Skin Tone Hand Fingers Partial
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#hand-fingers-partial
     *
     * @return void
     */
    public function testSkinToneHandFingersPartialEmojis(): void
    {
        $original = '👌🏻 👌🏼 👌🏽 👌🏾 👌🏿 ';
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1F90F;&#x1F3FB; &#x1F90F;&#x1F3FC; &#x1F90F;&#x1F3FD; &#x1F90F;&#x1F3FE; &#x1F90F;&#x1F3FF; ');
        $original .= '✌🏻 ✌🏼 ✌🏽 ✌🏾 ✌🏿 🤞🏻 🤞🏼 🤞🏽 🤞🏾 🤞🏿 🤟🏻 🤟🏼 🤟🏽 🤟🏾 🤟🏿 🤘🏻 🤘🏼 🤘🏽 🤘🏾 🤘🏿 🤙🏻 🤙🏼 🤙🏽 🤙🏾 🤙🏿';
        $test = '👌🏻 👌🏼 👌🏽 👌🏾 👌🏿 ';
        // @codingStandardsIgnoreLine
        $test .= html_entity_decode('&#x1F90F;&#x1F3FB; &#x1F90F;&#x1F3FC; &#x1F90F;&#x1F3FD; &#x1F90F;&#x1F3FE; &#x1F90F;&#x1F3FF; ');
        $test .= '✌🏻 ✌🏼 ✌🏽 ✌🏾 ✌🏿 🤞🏻 🤞🏼 🤞🏽 🤞🏾 🤞🏿 🤟🏻 🤟🏼 🤟🏽 🤟🏾 🤟🏿 🤘🏻 🤘🏼 🤘🏽 🤘🏾 🤘🏿 🤙🏻 🤙🏼 🤙🏽 🤙🏾 🤙🏿';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinToneHandFingersPartialEmojis()

    /**
     * Skin Tone Hand Single Finger
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#hand-single-finger
     *
     * @return void
     */
    public function testSkinToneHandSingleFingerEmojis(): void
    {
        $original = '👈🏻 👈🏼 👈🏽 👈🏾 👈🏿 👉🏻 👉🏼 👉🏽 👉🏾 👉🏿 👆🏻 👆🏼 👆🏽 👆🏾 👆🏿 🖕🏻 🖕🏼 🖕🏽 🖕🏾 🖕🏿 👇🏻 👇🏼 👇🏽 👇🏾 👇🏿 ☝🏻 ☝🏼 ☝🏽 ☝🏾 ☝🏿';
        $test = '👈🏻 👈🏼 👈🏽 👈🏾 👈🏿 👉🏻 👉🏼 👉🏽 👉🏾 👉🏿 👆🏻 👆🏼 👆🏽 👆🏾 👆🏿 🖕🏻 🖕🏼 🖕🏽 🖕🏾 🖕🏿 👇🏻 👇🏼 👇🏽 👇🏾 👇🏿 ☝🏻 ☝🏼 ☝🏽 ☝🏾 ☝🏿';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinToneHandSingleFingerEmojis()

    /**
     * Skin Tone Hand Fingers Closed
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#hand-fingers-closed
     *
     * @return void
     */
    public function testSkinToneHandFingersClosedEmojis(): void
    {
        $original = '👍🏻 👍🏼 👍🏽 👍🏾 👍🏿 👎🏻 👎🏼 👎🏽 👎🏾 👎🏿 ✊🏻 ✊🏼 ✊🏽 ✊🏾 ✊🏿 👊🏻 👊🏼 👊🏽 👊🏾 👊🏿 🤛🏻 🤛🏼 🤛🏽 🤛🏾 🤛🏿 🤜🏻 🤜🏼 🤜🏽 🤜🏾 🤜🏿';
        $test = '👍🏻 👍🏼 👍🏽 👍🏾 👍🏿 👎🏻 👎🏼 👎🏽 👎🏾 👎🏿 ✊🏻 ✊🏼 ✊🏽 ✊🏾 ✊🏿 👊🏻 👊🏼 👊🏽 👊🏾 👊🏿 🤛🏻 🤛🏼 🤛🏽 🤛🏾 🤛🏿 🤜🏻 🤜🏼 🤜🏽 🤜🏾 🤜🏿';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinToneHandFingersClosedEmojis()

    /**
     * Skin Tone Hands
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#hands
     *
     * @return void
     */
    public function testSkinToneHandsEmojis(): void
    {
        $original = '👏🏻 👏🏼 👏🏽 👏🏾 👏🏿 🙌🏻 🙌🏼 🙌🏽 🙌🏾 🙌🏿 👐🏻 👐🏼 👐🏽 👐🏾 👐🏿 🤲🏻 🤲🏼 🤲🏽 🤲🏾 🤲🏿 🙏🏻 🙏🏼 🙏🏽 🙏🏾 🙏🏿';
        $test = '👏🏻 👏🏼 👏🏽 👏🏾 👏🏿 🙌🏻 🙌🏼 🙌🏽 🙌🏾 🙌🏿 👐🏻 👐🏼 👐🏽 👐🏾 👐🏿 🤲🏻 🤲🏼 🤲🏽 🤲🏾 🤲🏿 🙏🏻 🙏🏼 🙏🏽 🙏🏾 🙏🏿';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinToneHandsEmojis()

    /**
     * Skin Tone Hand Prop
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#hand-prop
     *
     * @return void
     */
    public function testSkinToneHandPropEmojis(): void
    {
        $original = '✍🏻 ✍🏼 ✍🏽 ✍🏾 ✍🏿 💅🏻 💅🏼 💅🏽 💅🏾 💅🏿 🤳🏻 🤳🏼 🤳🏽 🤳🏾 🤳🏿';
        $test = '✍🏻 ✍🏼 ✍🏽 ✍🏾 ✍🏿 💅🏻 💅🏼 💅🏽 💅🏾 💅🏿 🤳🏻 🤳🏼 🤳🏽 🤳🏾 🤳🏿';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinToneHandPropEmojis()

    /**
     * Skin Tone Body Parts
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#body-parts
     *
     * @return void
     */
    public function testSkinToneBodyPartsEmojis(): void
    {
        $original = '💪🏻 💪🏼 💪🏽 💪🏾 💪🏿 ';
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1F9B5;&#x1F3FB; &#x1F9B5;&#x1F3FC; &#x1F9B5;&#x1F3FD; &#x1F9B5;&#x1F3FE; &#x1F9B5;&#x1F3FF; ');
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1F9B6;&#x1F3FB; &#x1F9B6;&#x1F3FC; &#x1F9B6;&#x1F3FD; &#x1F9B6;&#x1F3FE; &#x1F9B6;&#x1F3FF; ');
        $original .= '👂🏻 👂🏼 👂🏽 👂🏾 👂🏿 ';
        // @codingStandardsIgnoreLine
        $original .= html_entity_decode('&#x1F9BB;&#x1F3FB; &#x1F9BB;&#x1F3FC; &#x1F9BB;&#x1F3FD; &#x1F9BB;&#x1F3FE; &#x1F9BB;&#x1F3FF; ');
        $original .= '👃🏻 👃🏼 👃🏽 👃🏾 👃🏿';
        $test = '💪🏻 💪🏼 💪🏽 💪🏾 💪🏿 ';
        // @codingStandardsIgnoreLine
        $test .= html_entity_decode('&#x1F9B5;&#x1F3FB; &#x1F9B5;&#x1F3FC; &#x1F9B5;&#x1F3FD; &#x1F9B5;&#x1F3FE; &#x1F9B5;&#x1F3FF; ');
        // @codingStandardsIgnoreLine
        $test .= html_entity_decode('&#x1F9B6;&#x1F3FB; &#x1F9B6;&#x1F3FC; &#x1F9B6;&#x1F3FD; &#x1F9B6;&#x1F3FE; &#x1F9B6;&#x1F3FF; ');
        $test .= '👂🏻 👂🏼 👂🏽 👂🏾 👂🏿 ';
        // @codingStandardsIgnoreLine
        $test .= html_entity_decode('&#x1F9BB;&#x1F3FB; &#x1F9BB;&#x1F3FC; &#x1F9BB;&#x1F3FD; &#x1F9BB;&#x1F3FE; &#x1F9BB;&#x1F3FF; ');
        $test .= '👃🏻 👃🏼 👃🏽 👃🏾 👃🏿';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinToneBodyPartsEmojis()

    /**
     * Skin Tone Person
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#person
     *
     * @return void
     */
    public function testSkinTonePersonEmojis(): void
    {
        $original = '👶🏻 👶🏼 👶🏽 👶🏾 👶🏿 🧒🏻 🧒🏼 🧒🏽 🧒🏾 🧒🏿 👦🏻 👦🏼 👦🏽 👦🏾 👦🏿 👧🏻 👧🏼 👧🏽 👧🏾 👧🏿 🧑🏻 🧑🏼 🧑🏽 🧑🏾 🧑🏿 👱🏻 👱🏼 👱🏽 👱🏾 👱🏿 ';
        $original .= '👨🏻 👨🏼 👨🏽 👨🏾 👨🏿 🧔🏻 🧔🏼 🧔🏽 🧔🏾 🧔🏿 👱🏻‍♂️ 👱🏼‍♂️ 👱🏽‍♂️ 👱🏾‍♂️ 👱🏿‍♂️ ';
        $original .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9B0; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9B0; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9B0; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9B0; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9B0; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9B1; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9B1; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9B1; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9B1; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9B1; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9B3; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9B3; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9B3; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9B3; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9B3; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9B2; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9B2; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9B2; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9B2; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9B2; ');
        $original .= '👩🏻 👩🏼 👩🏽 👩🏾 👩🏿 👱🏻‍♀️ 👱🏼‍♀️ 👱🏽‍♀️ 👱🏾‍♀️ 👱🏿‍♀️ ';
        $original .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9B0; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9B0; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9B0; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9B0; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9B0; ');

        $original .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9B1; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9B1; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9B1; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9B1; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9B1; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9B3; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9B3; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9B3; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9B3; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9B3; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9B2; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9B2; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9B2; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9B2; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9B2; ');
        $original .= '🧓🏻 🧓🏼 🧓🏽 🧓🏾 🧓🏿 👴🏻 👴🏼 👴🏽 👴🏾 👴🏿 👵🏻 👵🏼 👵🏽 👵🏾 👵🏿';
        $test = '👶🏻 👶🏼 👶🏽 👶🏾 👶🏿 🧒🏻 🧒🏼 🧒🏽 🧒🏾 🧒🏿 👦🏻 👦🏼 👦🏽 👦🏾 👦🏿 👧🏻 👧🏼 👧🏽 👧🏾 👧🏿 🧑🏻 🧑🏼 🧑🏽 🧑🏾 🧑🏿 👱🏻 👱🏼 👱🏽 👱🏾 👱🏿 ';
        $test .= '👨🏻 👨🏼 👨🏽 👨🏾 👨🏿 🧔🏻 🧔🏼 🧔🏽 🧔🏾 🧔🏿 👱🏻‍♂️ 👱🏼‍♂️ 👱🏽‍♂️ 👱🏾‍♂️ 👱🏿‍♂️ ';
        $test .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9B0; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9B0; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9B0; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9B0; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9B0; ');

        $test .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9B1; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9B1; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9B1; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9B1; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9B1; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9B3; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9B3; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9B3; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9B3; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9B3; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9B2; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9B2; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9B2; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9B2; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9B2; ');
        $test .= '👩🏻 👩🏼 👩🏽 👩🏾 👩🏿 👱🏻‍♀️ 👱🏼‍♀️ 👱🏽‍♀️ 👱🏾‍♀️ 👱🏿‍♀️ ';
        $test .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9B0; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9B0; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9B0; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9B0; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9B0; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9B1; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9B1; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9B1; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9B1; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9B1; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9B3; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9B3; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9B3; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9B3; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9B3; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9B2; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9B2; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9B2; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9B2; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9B2; ');
        $test .= '🧓🏻 🧓🏼 🧓🏽 🧓🏾 🧓🏿 👴🏻 👴🏼 👴🏽 👴🏾 👴🏿 👵🏻 👵🏼 👵🏽 👵🏾 👵🏿';
        EmojiEntity::threeByteEncode($test);
        //EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinTonePersonEmojis()

    /**
     * Skin Tone Person Gesture
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#person-gesture
     *
     * @return void
     */
    public function testSkinTonePersonGestureEmojis(): void
    {
        // @codingStandardsIgnoreLine
        $original = '🙍🏻 🙍🏼 🙍🏽 🙍🏾 🙍🏿 🙍🏻‍♂️ 🙍🏼‍♂️ 🙍🏽‍♂️ 🙍🏾‍♂️ 🙍🏿‍♂️ 🙍🏻‍♀️ 🙍🏼‍♀️ 🙍🏽‍♀️ 🙍🏾‍♀️ 🙍🏿‍♀️ 🙎🏻 🙎🏼 🙎🏽 🙎🏾 🙎🏿 🙎🏻‍♂️ 🙎🏼‍♂️ 🙎🏽‍♂️ 🙎🏾‍♂️ 🙎🏿‍♂️ 🙎🏻‍♀️ 🙎🏼‍♀️ 🙎🏽‍♀️ 🙎🏾‍♀️ 🙎🏿‍♀️ 🙅🏻 🙅🏼 🙅🏽 🙅🏾 🙅🏿 🙅🏻‍♂️ 🙅🏼‍♂️ 🙅🏽‍♂️ 🙅🏾‍♂️ 🙅🏿‍♂️ 🙅🏻‍♀️ 🙅🏼‍♀️ 🙅🏽‍♀️ 🙅🏾‍♀️ 🙅🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $original .= '🙆🏻 🙆🏼 🙆🏽 🙆🏾 🙆🏿 🙆🏻‍♂️ 🙆🏼‍♂️ 🙆🏽‍♂️ 🙆🏾‍♂️ 🙆🏿‍♂️ 🙆🏻‍♀️ 🙆🏼‍♀️ 🙆🏽‍♀️ 🙆🏾‍♀️ 🙆🏿‍♀️ 💁🏻 💁🏼 💁🏽 💁🏾 💁🏿 💁🏻‍♂️ 💁🏼‍♂️ 💁🏽‍♂️ 💁🏾‍♂️ 💁🏿‍♂️ 💁🏻‍♀️ 💁🏼‍♀️ 💁🏽‍♀️ 💁🏾‍♀️ 💁🏿‍♀️ 🙋🏻 🙋🏼 🙋🏽 🙋🏾 🙋🏿 🙋🏻‍♂️ 🙋🏼‍♂️ 🙋🏽‍♂️ 🙋🏾‍♂️ 🙋🏿‍♂️ 🙋🏻‍♀️ 🙋🏼‍♀️ 🙋🏽‍♀️ 🙋🏾‍♀️ 🙋🏿‍♀️ ';
        // deaf person
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FB; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FC; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FD; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FE; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FF; ');
        // deaf man
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FB;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FC;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FD;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FE;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FF;&#x200D;&#x2642;&#xFE0F; ');
        // deaf woman
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FB;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FC;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FD;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FE;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CF;&#x1F3FF;&#x200D;&#x2640;&#xFE0F; ');
        // @codingStandardsIgnoreLine
        $original .= '🙇🏻 🙇🏼 🙇🏽 🙇🏾 🙇🏿 🙇🏻‍♂️ 🙇🏼‍♂️ 🙇🏽‍♂️ 🙇🏾‍♂️ 🙇🏿‍♂️ 🙇🏻‍♀️ 🙇🏼‍♀️ 🙇🏽‍♀️ 🙇🏾‍♀️ 🙇🏿‍♀️ 🤦🏻 🤦🏼 🤦🏽 🤦🏾 🤦🏿 🤦🏻‍♂️ 🤦🏼‍♂️ 🤦🏽‍♂️ 🤦🏾‍♂️ 🤦🏿‍♂️ 🤦🏻‍♀️ 🤦🏼‍♀️ 🤦🏽‍♀️ 🤦🏾‍♀️ 🤦🏿‍♀️ 🤷🏻 🤷🏼 🤷🏽 🤷🏾 🤷🏿 🤷🏻‍♂️ 🤷🏼‍♂️ 🤷🏽‍♂️ 🤷🏾‍♂️ 🤷🏿‍♂️ 🤷🏻‍♀️ 🤷🏼‍♀️ 🤷🏽‍♀️ 🤷🏾‍♀️ 🤷🏿‍♀️';
        // @codingStandardsIgnoreLine
        $test = '🙍🏻 🙍🏼 🙍🏽 🙍🏾 🙍🏿 🙍🏻‍♂️ 🙍🏼‍♂️ 🙍🏽‍♂️ 🙍🏾‍♂️ 🙍🏿‍♂️ 🙍🏻‍♀️ 🙍🏼‍♀️ 🙍🏽‍♀️ 🙍🏾‍♀️ 🙍🏿‍♀️ 🙎🏻 🙎🏼 🙎🏽 🙎🏾 🙎🏿 🙎🏻‍♂️ 🙎🏼‍♂️ 🙎🏽‍♂️ 🙎🏾‍♂️ 🙎🏿‍♂️ 🙎🏻‍♀️ 🙎🏼‍♀️ 🙎🏽‍♀️ 🙎🏾‍♀️ 🙎🏿‍♀️ 🙅🏻 🙅🏼 🙅🏽 🙅🏾 🙅🏿 🙅🏻‍♂️ 🙅🏼‍♂️ 🙅🏽‍♂️ 🙅🏾‍♂️ 🙅🏿‍♂️ 🙅🏻‍♀️ 🙅🏼‍♀️ 🙅🏽‍♀️ 🙅🏾‍♀️ 🙅🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $test .= '🙆🏻 🙆🏼 🙆🏽 🙆🏾 🙆🏿 🙆🏻‍♂️ 🙆🏼‍♂️ 🙆🏽‍♂️ 🙆🏾‍♂️ 🙆🏿‍♂️ 🙆🏻‍♀️ 🙆🏼‍♀️ 🙆🏽‍♀️ 🙆🏾‍♀️ 🙆🏿‍♀️ 💁🏻 💁🏼 💁🏽 💁🏾 💁🏿 💁🏻‍♂️ 💁🏼‍♂️ 💁🏽‍♂️ 💁🏾‍♂️ 💁🏿‍♂️ 💁🏻‍♀️ 💁🏼‍♀️ 💁🏽‍♀️ 💁🏾‍♀️ 💁🏿‍♀️ 🙋🏻 🙋🏼 🙋🏽 🙋🏾 🙋🏿 🙋🏻‍♂️ 🙋🏼‍♂️ 🙋🏽‍♂️ 🙋🏾‍♂️ 🙋🏿‍♂️ 🙋🏻‍♀️ 🙋🏼‍♀️ 🙋🏽‍♀️ 🙋🏾‍♀️ 🙋🏿‍♀️ ';
        // deaf person
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FB; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FC; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FD; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FE; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FF; ');
        // deaf man
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FB;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FC;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FD;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FE;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FF;&#x200D;&#x2642;&#xFE0F; ');
        // deaf woman
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FB;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FC;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FD;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FE;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CF;&#x1F3FF;&#x200D;&#x2640;&#xFE0F; ');
        // @codingStandardsIgnoreLine
        $test .= '🙇🏻 🙇🏼 🙇🏽 🙇🏾 🙇🏿 🙇🏻‍♂️ 🙇🏼‍♂️ 🙇🏽‍♂️ 🙇🏾‍♂️ 🙇🏿‍♂️ 🙇🏻‍♀️ 🙇🏼‍♀️ 🙇🏽‍♀️ 🙇🏾‍♀️ 🙇🏿‍♀️ 🤦🏻 🤦🏼 🤦🏽 🤦🏾 🤦🏿 🤦🏻‍♂️ 🤦🏼‍♂️ 🤦🏽‍♂️ 🤦🏾‍♂️ 🤦🏿‍♂️ 🤦🏻‍♀️ 🤦🏼‍♀️ 🤦🏽‍♀️ 🤦🏾‍♀️ 🤦🏿‍♀️ 🤷🏻 🤷🏼 🤷🏽 🤷🏾 🤷🏿 🤷🏻‍♂️ 🤷🏼‍♂️ 🤷🏽‍♂️ 🤷🏾‍♂️ 🤷🏿‍♂️ 🤷🏻‍♀️ 🤷🏼‍♀️ 🤷🏽‍♀️ 🤷🏾‍♀️ 🤷🏿‍♀️';
        EmojiEntity::threeByteEncode($test);
        // EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinTonePersonGestureEmojis()

    /**
     * Skin Tone Person Role
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#person-role
     *
     * @return void
     */
    public function testSkinTonePersonRoleEmojis(): void
    {
        // @codingStandardsIgnoreLine
        $original = '👨🏻‍⚕️ 👨🏼‍⚕️ 👨🏽‍⚕️ 👨🏾‍⚕️ 👨🏿‍⚕️ 👩🏻‍⚕️ 👩🏼‍⚕️ 👩🏽‍⚕️ 👩🏾‍⚕️ 👩🏿‍⚕️ 👨🏻‍🎓 👨🏼‍🎓 👨🏽‍🎓 👨🏾‍🎓 👨🏿‍🎓 👩🏻‍🎓 👩🏼‍🎓 👩🏽‍🎓 👩🏾‍🎓 👩🏿‍🎓 👨🏻‍🏫 👨🏼‍🏫 👨🏽‍🏫 👨🏾‍🏫 👨🏿‍🏫 👩🏻‍🏫 👩🏼‍🏫 👩🏽‍🏫 👩🏾‍🏫 👩🏿‍🏫 👨🏻‍⚖️ 👨🏼‍⚖️ 👨🏽‍⚖️ 👨🏾‍⚖️ 👨🏿‍⚖️ 👩🏻‍⚖️ 👩🏼‍⚖️ 👩🏽‍⚖️ 👩🏾‍⚖️ 👩🏿‍⚖️ ';
        // @codingStandardsIgnoreLine
        $original .= '👨🏻‍🌾 👨🏼‍🌾 👨🏽‍🌾 👨🏾‍🌾 👨🏿‍🌾 👩🏻‍🌾 👩🏼‍🌾 👩🏽‍🌾 👩🏾‍🌾 👩🏿‍🌾 👨🏻‍🍳 👨🏼‍🍳 👨🏽‍🍳 👨🏾‍🍳 👨🏿‍🍳 👩🏻‍🍳 👩🏼‍🍳 👩🏽‍🍳 👩🏾‍🍳 👩🏿‍🍳 👨🏻‍🔧 👨🏼‍🔧 👨🏽‍🔧 👨🏾‍🔧 👨🏿‍🔧 👩🏻‍🔧 👩🏼‍🔧 👩🏽‍🔧 👩🏾‍🔧 👩🏿‍🔧 👨🏻‍🏭 👨🏼‍🏭 👨🏽‍🏭 👨🏾‍🏭 👨🏿‍🏭 👩🏻‍🏭 👩🏼‍🏭 👩🏽‍🏭 👩🏾‍🏭 👩🏿‍🏭 ';
        // @codingStandardsIgnoreLine
        $original .= '👨🏻‍💼 👨🏼‍💼 👨🏽‍💼 👨🏾‍💼 👨🏿‍💼 👩🏻‍💼 👩🏼‍💼 👩🏽‍💼 👩🏾‍💼 👩🏿‍💼 👨🏻‍🔬 👨🏼‍🔬 👨🏽‍🔬 👨🏾‍🔬 👨🏿‍🔬 👩🏻‍🔬 👩🏼‍🔬 👩🏽‍🔬 👩🏾‍🔬 👩🏿‍🔬 👨🏻‍💻 👨🏼‍💻 👨🏽‍💻 👨🏾‍💻 👨🏿‍💻 👩🏻‍💻 👩🏼‍💻 👩🏽‍💻 👩🏾‍💻 👩🏿‍💻 👨🏻‍🎤 👨🏼‍🎤 👨🏽‍🎤 👨🏾‍🎤 👨🏿‍🎤 👩🏻‍🎤 👩🏼‍🎤 👩🏽‍🎤 👩🏾‍🎤 👩🏿‍🎤 ';
        // @codingStandardsIgnoreLine
        $original .= '👨🏻‍🎨 👨🏼‍🎨 👨🏽‍🎨 👨🏾‍🎨 👨🏿‍🎨 👩🏻‍🎨 👩🏼‍🎨 👩🏽‍🎨 👩🏾‍🎨 👩🏿‍🎨 👨🏻‍✈️ 👨🏼‍✈️ 👨🏽‍✈️ 👨🏾‍✈️ 👨🏿‍✈️ 👩🏻‍✈️ 👩🏼‍✈️ 👩🏽‍✈️ 👩🏾‍✈️ 👩🏿‍✈️ 👨🏻‍🚀 👨🏼‍🚀 👨🏽‍🚀 👨🏾‍🚀 👨🏿‍🚀 👩🏻‍🚀 👩🏼‍🚀 👩🏽‍🚀 👩🏾‍🚀 👩🏿‍🚀 👨🏻‍🚒 👨🏼‍🚒 👨🏽‍🚒 👨🏾‍🚒 👨🏿‍🚒 👩🏻‍🚒 👩🏼‍🚒 👩🏽‍🚒 👩🏾‍🚒 👩🏿‍🚒 ';
        // @codingStandardsIgnoreLine
        $original .= '👮🏻 👮🏼 👮🏽 👮🏾 👮🏿 👮🏻‍♂️ 👮🏼‍♂️ 👮🏽‍♂️ 👮🏾‍♂️ 👮🏿‍♂️ 👮🏻‍♀️ 👮🏼‍♀️ 👮🏽‍♀️ 👮🏾‍♀️ 👮🏿‍♀️ 🕵🏻 🕵🏼 🕵🏽 🕵🏾 🕵🏿 🕵🏻‍♂️ 🕵🏼‍♂️ 🕵🏽‍♂️ 🕵🏾‍♂️ 🕵🏿‍♂️ 🕵🏻‍♀️ 🕵🏼‍♀️ 🕵🏽‍♀️ 🕵🏾‍♀️ 🕵🏿‍♀️ 💂🏻 💂🏼 💂🏽 💂🏾 💂🏿 💂🏻‍♂️ 💂🏼‍♂️ 💂🏽‍♂️ 💂🏾‍♂️ 💂🏿‍♂️ 💂🏻‍♀️ 💂🏼‍♀️ 💂🏽‍♀️ 💂🏾‍♀️ 💂🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $original .= '👷🏻 👷🏼 👷🏽 👷🏾 👷🏿 👷🏻‍♂️ 👷🏼‍♂️ 👷🏽‍♂️ 👷🏾‍♂️ 👷🏿‍♂️ 👷🏻‍♀️ 👷🏼‍♀️ 👷🏽‍♀️ 👷🏾‍♀️ 👷🏿‍♀️ 🤴🏻 🤴🏼 🤴🏽 🤴🏾 🤴🏿 👸🏻 👸🏼 👸🏽 👸🏾 👸🏿 👳🏻 👳🏼 👳🏽 👳🏾 👳🏿 👳🏻‍♂️ 👳🏼‍♂️ 👳🏽‍♂️ 👳🏾‍♂️ 👳🏿‍♂️ 👳🏻‍♀️ 👳🏼‍♀️ 👳🏽‍♀️ 👳🏾‍♀️ 👳🏿‍♀️ ';
        $original .= '👲🏻 👲🏼 👲🏽 👲🏾 👲🏿 🧕🏻 🧕🏼 🧕🏽 🧕🏾 🧕🏿 🤵🏻 🤵🏼 🤵🏽 🤵🏾 🤵🏿 👰🏻 👰🏼 👰🏽 👰🏾 👰🏿 🤰🏻 🤰🏼 🤰🏽 🤰🏾 🤰🏿 🤱🏻 🤱🏼 🤱🏽 🤱🏾 🤱🏿';
        // @codingStandardsIgnoreLine
        $test = '👨🏻‍⚕️ 👨🏼‍⚕️ 👨🏽‍⚕️ 👨🏾‍⚕️ 👨🏿‍⚕️ 👩🏻‍⚕️ 👩🏼‍⚕️ 👩🏽‍⚕️ 👩🏾‍⚕️ 👩🏿‍⚕️ 👨🏻‍🎓 👨🏼‍🎓 👨🏽‍🎓 👨🏾‍🎓 👨🏿‍🎓 👩🏻‍🎓 👩🏼‍🎓 👩🏽‍🎓 👩🏾‍🎓 👩🏿‍🎓 👨🏻‍🏫 👨🏼‍🏫 👨🏽‍🏫 👨🏾‍🏫 👨🏿‍🏫 👩🏻‍🏫 👩🏼‍🏫 👩🏽‍🏫 👩🏾‍🏫 👩🏿‍🏫 👨🏻‍⚖️ 👨🏼‍⚖️ 👨🏽‍⚖️ 👨🏾‍⚖️ 👨🏿‍⚖️ 👩🏻‍⚖️ 👩🏼‍⚖️ 👩🏽‍⚖️ 👩🏾‍⚖️ 👩🏿‍⚖️ ';
        // @codingStandardsIgnoreLine
        $test .= '👨🏻‍🌾 👨🏼‍🌾 👨🏽‍🌾 👨🏾‍🌾 👨🏿‍🌾 👩🏻‍🌾 👩🏼‍🌾 👩🏽‍🌾 👩🏾‍🌾 👩🏿‍🌾 👨🏻‍🍳 👨🏼‍🍳 👨🏽‍🍳 👨🏾‍🍳 👨🏿‍🍳 👩🏻‍🍳 👩🏼‍🍳 👩🏽‍🍳 👩🏾‍🍳 👩🏿‍🍳 👨🏻‍🔧 👨🏼‍🔧 👨🏽‍🔧 👨🏾‍🔧 👨🏿‍🔧 👩🏻‍🔧 👩🏼‍🔧 👩🏽‍🔧 👩🏾‍🔧 👩🏿‍🔧 👨🏻‍🏭 👨🏼‍🏭 👨🏽‍🏭 👨🏾‍🏭 👨🏿‍🏭 👩🏻‍🏭 👩🏼‍🏭 👩🏽‍🏭 👩🏾‍🏭 👩🏿‍🏭 ';
        // @codingStandardsIgnoreLine
        $test .= '👨🏻‍💼 👨🏼‍💼 👨🏽‍💼 👨🏾‍💼 👨🏿‍💼 👩🏻‍💼 👩🏼‍💼 👩🏽‍💼 👩🏾‍💼 👩🏿‍💼 👨🏻‍🔬 👨🏼‍🔬 👨🏽‍🔬 👨🏾‍🔬 👨🏿‍🔬 👩🏻‍🔬 👩🏼‍🔬 👩🏽‍🔬 👩🏾‍🔬 👩🏿‍🔬 👨🏻‍💻 👨🏼‍💻 👨🏽‍💻 👨🏾‍💻 👨🏿‍💻 👩🏻‍💻 👩🏼‍💻 👩🏽‍💻 👩🏾‍💻 👩🏿‍💻 👨🏻‍🎤 👨🏼‍🎤 👨🏽‍🎤 👨🏾‍🎤 👨🏿‍🎤 👩🏻‍🎤 👩🏼‍🎤 👩🏽‍🎤 👩🏾‍🎤 👩🏿‍🎤 ';
        // @codingStandardsIgnoreLine
        $test .= '👨🏻‍🎨 👨🏼‍🎨 👨🏽‍🎨 👨🏾‍🎨 👨🏿‍🎨 👩🏻‍🎨 👩🏼‍🎨 👩🏽‍🎨 👩🏾‍🎨 👩🏿‍🎨 👨🏻‍✈️ 👨🏼‍✈️ 👨🏽‍✈️ 👨🏾‍✈️ 👨🏿‍✈️ 👩🏻‍✈️ 👩🏼‍✈️ 👩🏽‍✈️ 👩🏾‍✈️ 👩🏿‍✈️ 👨🏻‍🚀 👨🏼‍🚀 👨🏽‍🚀 👨🏾‍🚀 👨🏿‍🚀 👩🏻‍🚀 👩🏼‍🚀 👩🏽‍🚀 👩🏾‍🚀 👩🏿‍🚀 👨🏻‍🚒 👨🏼‍🚒 👨🏽‍🚒 👨🏾‍🚒 👨🏿‍🚒 👩🏻‍🚒 👩🏼‍🚒 👩🏽‍🚒 👩🏾‍🚒 👩🏿‍🚒 ';
        // @codingStandardsIgnoreLine
        $test .= '👮🏻 👮🏼 👮🏽 👮🏾 👮🏿 👮🏻‍♂️ 👮🏼‍♂️ 👮🏽‍♂️ 👮🏾‍♂️ 👮🏿‍♂️ 👮🏻‍♀️ 👮🏼‍♀️ 👮🏽‍♀️ 👮🏾‍♀️ 👮🏿‍♀️ 🕵🏻 🕵🏼 🕵🏽 🕵🏾 🕵🏿 🕵🏻‍♂️ 🕵🏼‍♂️ 🕵🏽‍♂️ 🕵🏾‍♂️ 🕵🏿‍♂️ 🕵🏻‍♀️ 🕵🏼‍♀️ 🕵🏽‍♀️ 🕵🏾‍♀️ 🕵🏿‍♀️ 💂🏻 💂🏼 💂🏽 💂🏾 💂🏿 💂🏻‍♂️ 💂🏼‍♂️ 💂🏽‍♂️ 💂🏾‍♂️ 💂🏿‍♂️ 💂🏻‍♀️ 💂🏼‍♀️ 💂🏽‍♀️ 💂🏾‍♀️ 💂🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $test .= '👷🏻 👷🏼 👷🏽 👷🏾 👷🏿 👷🏻‍♂️ 👷🏼‍♂️ 👷🏽‍♂️ 👷🏾‍♂️ 👷🏿‍♂️ 👷🏻‍♀️ 👷🏼‍♀️ 👷🏽‍♀️ 👷🏾‍♀️ 👷🏿‍♀️ 🤴🏻 🤴🏼 🤴🏽 🤴🏾 🤴🏿 👸🏻 👸🏼 👸🏽 👸🏾 👸🏿 👳🏻 👳🏼 👳🏽 👳🏾 👳🏿 👳🏻‍♂️ 👳🏼‍♂️ 👳🏽‍♂️ 👳🏾‍♂️ 👳🏿‍♂️ 👳🏻‍♀️ 👳🏼‍♀️ 👳🏽‍♀️ 👳🏾‍♀️ 👳🏿‍♀️ ';
        $test .= '👲🏻 👲🏼 👲🏽 👲🏾 👲🏿 🧕🏻 🧕🏼 🧕🏽 🧕🏾 🧕🏿 🤵🏻 🤵🏼 🤵🏽 🤵🏾 🤵🏿 👰🏻 👰🏼 👰🏽 👰🏾 👰🏿 🤰🏻 🤰🏼 🤰🏽 🤰🏾 🤰🏿 🤱🏻 🤱🏼 🤱🏽 🤱🏾 🤱🏿';
        EmojiEntity::threeByteEncode($test);
        // EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinTonePersonRoleEmojis()

    /**
     * Skin Tone Person Fantasy
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#person-fantasy
     *
     * @return void
     */
    public function testSkinTonePersonFantasyEmojis(): void
    {
        $original = '👼🏻 👼🏼 👼🏽 👼🏾 👼🏿 🎅🏻 🎅🏼 🎅🏽 🎅🏾 🎅🏿 🤶🏻 🤶🏼 🤶🏽 🤶🏾 🤶🏿 ';
        // superhero genderless
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FB; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FC; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FD; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FE; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FF; ');
        // man superhero
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FB;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FC;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FD;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FE;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FF;&#x200D;&#x2642;&#xFE0F; ');
        // woman superhero
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FB;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FC;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FD;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FE;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B8;&#x1F3FF;&#x200D;&#x2640;&#xFE0F; ');
        // supervillian genderless
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FB; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FC; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FD; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FE; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FF; ');
        // man supervillian
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FB;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FC;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FD;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FE;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FF;&#x200D;&#x2642;&#xFE0F; ');
        // woman supervillian
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FB;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FC;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FD;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FE;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9B9;&#x1F3FF;&#x200D;&#x2640;&#xFE0F; ');
        // @codingStandardsIgnoreLine
        $original .= '🧙🏻 🧙🏼 🧙🏽 🧙🏾 🧙🏿 🧙🏻‍♂️ 🧙🏼‍♂️ 🧙🏽‍♂️ 🧙🏾‍♂️ 🧙🏿‍♂️ 🧙🏻‍♀️ 🧙🏼‍♀️ 🧙🏽‍♀️ 🧙🏾‍♀️ 🧙🏿‍♀️ 🧚🏻 🧚🏼 🧚🏽 🧚🏾 🧚🏿 🧚🏻‍♂️ 🧚🏼‍♂️ 🧚🏽‍♂️ 🧚🏾‍♂️ 🧚🏿‍♂️ 🧚🏻‍♀️ 🧚🏼‍♀️ 🧚🏽‍♀️ 🧚🏾‍♀️ 🧚🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $original .= '🧛🏻 🧛🏼 🧛🏽 🧛🏾 🧛🏿 🧛🏻‍♂️ 🧛🏼‍♂️ 🧛🏽‍♂️ 🧛🏾‍♂️ 🧛🏿‍♂️ 🧛🏻‍♀️ 🧛🏼‍♀️ 🧛🏽‍♀️ 🧛🏾‍♀️ 🧛🏿‍♀️ 🧜🏻 🧜🏼 🧜🏽 🧜🏾 🧜🏿 🧜🏻‍♂️ 🧜🏼‍♂️ 🧜🏽‍♂️ 🧜🏾‍♂️ 🧜🏿‍♂️ 🧜🏻‍♀️ 🧜🏼‍♀️ 🧜🏽‍♀️ 🧜🏾‍♀️ 🧜🏿‍♀️ ';
        $original .= '🧝🏻 🧝🏼 🧝🏽 🧝🏾 🧝🏿 🧝🏻‍♂️ 🧝🏼‍♂️ 🧝🏽‍♂️ 🧝🏾‍♂️ 🧝🏿‍♂️ 🧝🏻‍♀️ 🧝🏼‍♀️ 🧝🏽‍♀️ 🧝🏾‍♀️ 🧝🏿‍♀️';
        $test = '👼🏻 👼🏼 👼🏽 👼🏾 👼🏿 🎅🏻 🎅🏼 🎅🏽 🎅🏾 🎅🏿 🤶🏻 🤶🏼 🤶🏽 🤶🏾 🤶🏿 ';
        // superhero genderless
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FB; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FC; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FD; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FE; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FF; ');
        // man superhero
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FB;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FC;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FD;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FE;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FF;&#x200D;&#x2642;&#xFE0F; ');
        // woman superhero
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FB;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FC;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FD;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FE;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B8;&#x1F3FF;&#x200D;&#x2640;&#xFE0F; ');
        // supervillian genderless
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FB; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FC; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FD; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FE; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FF; ');
        // man supervillian
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FB;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FC;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FD;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FE;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FF;&#x200D;&#x2642;&#xFE0F; ');
        // woman supervillian
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FB;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FC;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FD;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FE;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9B9;&#x1F3FF;&#x200D;&#x2640;&#xFE0F; ');
        // @codingStandardsIgnoreLine
        $test .= '🧙🏻 🧙🏼 🧙🏽 🧙🏾 🧙🏿 🧙🏻‍♂️ 🧙🏼‍♂️ 🧙🏽‍♂️ 🧙🏾‍♂️ 🧙🏿‍♂️ 🧙🏻‍♀️ 🧙🏼‍♀️ 🧙🏽‍♀️ 🧙🏾‍♀️ 🧙🏿‍♀️ 🧚🏻 🧚🏼 🧚🏽 🧚🏾 🧚🏿 🧚🏻‍♂️ 🧚🏼‍♂️ 🧚🏽‍♂️ 🧚🏾‍♂️ 🧚🏿‍♂️ 🧚🏻‍♀️ 🧚🏼‍♀️ 🧚🏽‍♀️ 🧚🏾‍♀️ 🧚🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $test .= '🧛🏻 🧛🏼 🧛🏽 🧛🏾 🧛🏿 🧛🏻‍♂️ 🧛🏼‍♂️ 🧛🏽‍♂️ 🧛🏾‍♂️ 🧛🏿‍♂️ 🧛🏻‍♀️ 🧛🏼‍♀️ 🧛🏽‍♀️ 🧛🏾‍♀️ 🧛🏿‍♀️ 🧜🏻 🧜🏼 🧜🏽 🧜🏾 🧜🏿 🧜🏻‍♂️ 🧜🏼‍♂️ 🧜🏽‍♂️ 🧜🏾‍♂️ 🧜🏿‍♂️ 🧜🏻‍♀️ 🧜🏼‍♀️ 🧜🏽‍♀️ 🧜🏾‍♀️ 🧜🏿‍♀️ ';
        $test .= '🧝🏻 🧝🏼 🧝🏽 🧝🏾 🧝🏿 🧝🏻‍♂️ 🧝🏼‍♂️ 🧝🏽‍♂️ 🧝🏾‍♂️ 🧝🏿‍♂️ 🧝🏻‍♀️ 🧝🏼‍♀️ 🧝🏽‍♀️ 🧝🏾‍♀️ 🧝🏿‍♀️';
        EmojiEntity::threeByteEncode($test);
        // EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinTonePersonFantasyEmojis()

    /**
     * Skin Tone Person Activity
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#person-activity
     *
     * @return void
     */
    public function testSkinTonePersonActivityEmojis(): void
    {
        // @codingStandardsIgnoreLine
        $original = '💆🏻 💆🏼 💆🏽 💆🏾 💆🏿 💆🏻‍♂️ 💆🏼‍♂️ 💆🏽‍♂️ 💆🏾‍♂️ 💆🏿‍♂️ 💆🏻‍♀️ 💆🏼‍♀️ 💆🏽‍♀️ 💆🏾‍♀️ 💆🏿‍♀️ 💇🏻 💇🏼 💇🏽 💇🏾 💇🏿 💇🏻‍♂️ 💇🏼‍♂️ 💇🏽‍♂️ 💇🏾‍♂️ 💇🏿‍♂️ 💇🏻‍♀️ 💇🏼‍♀️ 💇🏽‍♀️ 💇🏾‍♀️ 💇🏿‍♀️ ';
        $original .= '🚶🏻 🚶🏼 🚶🏽 🚶🏾 🚶🏿 🚶🏻‍♂️ 🚶🏼‍♂️ 🚶🏽‍♂️ 🚶🏾‍♂️ 🚶🏿‍♂️ 🚶🏻‍♀️ 🚶🏼‍♀️ 🚶🏽‍♀️ 🚶🏾‍♀️ 🚶🏿‍♀️ ';
        // generic person standing
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FB; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FC; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FD; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FE; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FF; ');
        // man standing
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FB;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FC;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FD;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FE;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FF;&#x200D;&#x2642;&#xFE0F; ');
        // woman standing
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FB;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FC;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FD;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FE;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CD;&#x1F3FF;&#x200D;&#x2640;&#xFE0F; ');
        // generic person kneeling
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FB; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FC; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FD; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FE; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FF; ');
        // man kneeling
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FB;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FC;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FD;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FE;&#x200D;&#x2642;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FF;&#x200D;&#x2642;&#xFE0F; ');
        // woman kneeling
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FB;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FC;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FD;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FE;&#x200D;&#x2640;&#xFE0F; ');
        $original .= html_entity_decode('&#x1F9CE;&#x1F3FF;&#x200D;&#x2640;&#xFE0F; ');
        // man with probing cane
        $original .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9AF; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9AF; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9AF; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9AF; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9AF; ');
        // woman with probing cane
        $original .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9AF; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9AF; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9AF; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9AF; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9AF; ');
        // man with motorized wheelchair
        $original .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9BC; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9BC; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9BC; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9BC; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9BC; ');
        // woman with motorized wheelchair
        $original .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9BC; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9BC; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9BC; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9BC; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9BC; ');
        // man with manual wheelchair
        $original .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9BD; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9BD; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9BD; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9BD; ');
        $original .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9BD; ');
        // woman with manual wheelchair
        $original .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9BD; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9BD; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9BD; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9BD; ');
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9BD; ');
        // @codingStandardsIgnoreLine
        $original .= '🏃🏻 🏃🏼 🏃🏽 🏃🏾 🏃🏿 🏃🏻‍♂️ 🏃🏼‍♂️ 🏃🏽‍♂️ 🏃🏾‍♂️ 🏃🏿‍♂️ 🏃🏻‍♀️ 🏃🏼‍♀️ 🏃🏽‍♀️ 🏃🏾‍♀️ 🏃🏿‍♀️ 💃🏻 💃🏼 💃🏽 💃🏾 💃🏿 🕺🏻 🕺🏼 🕺🏽 🕺🏾 🕺🏿 🕴🏻 🕴🏼 🕴🏽 🕴🏾 🕴🏿 ';
        // @codingStandardsIgnoreLine
        $original .= '🧖🏻 🧖🏼 🧖🏽 🧖🏾 🧖🏿 🧖🏻‍♂️ 🧖🏼‍♂️ 🧖🏽‍♂️ 🧖🏾‍♂️ 🧖🏿‍♂️ 🧖🏻‍♀️ 🧖🏼‍♀️ 🧖🏽‍♀️ 🧖🏾‍♀️ 🧖🏿‍♀️ 🧗🏻 🧗🏼 🧗🏽 🧗🏾 🧗🏿 🧗🏻‍♂️ 🧗🏼‍♂️ 🧗🏽‍♂️ 🧗🏾‍♂️ 🧗🏿‍♂️ 🧗🏻‍♀️ 🧗🏼‍♀️ 🧗🏽‍♀️ 🧗🏾‍♀️ 🧗🏿‍♀️';
        // @codingStandardsIgnoreLine
        $test = '💆🏻 💆🏼 💆🏽 💆🏾 💆🏿 💆🏻‍♂️ 💆🏼‍♂️ 💆🏽‍♂️ 💆🏾‍♂️ 💆🏿‍♂️ 💆🏻‍♀️ 💆🏼‍♀️ 💆🏽‍♀️ 💆🏾‍♀️ 💆🏿‍♀️ 💇🏻 💇🏼 💇🏽 💇🏾 💇🏿 💇🏻‍♂️ 💇🏼‍♂️ 💇🏽‍♂️ 💇🏾‍♂️ 💇🏿‍♂️ 💇🏻‍♀️ 💇🏼‍♀️ 💇🏽‍♀️ 💇🏾‍♀️ 💇🏿‍♀️ ';
        $test .= '🚶🏻 🚶🏼 🚶🏽 🚶🏾 🚶🏿 🚶🏻‍♂️ 🚶🏼‍♂️ 🚶🏽‍♂️ 🚶🏾‍♂️ 🚶🏿‍♂️ 🚶🏻‍♀️ 🚶🏼‍♀️ 🚶🏽‍♀️ 🚶🏾‍♀️ 🚶🏿‍♀️ ';
        // generic person standing
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FB; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FC; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FD; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FE; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FF; ');
        // man standing
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FB;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FC;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FD;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FE;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FF;&#x200D;&#x2642;&#xFE0F; ');
        // woman standing
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FB;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FC;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FD;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FE;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CD;&#x1F3FF;&#x200D;&#x2640;&#xFE0F; ');
        // generic person kneeling
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FB; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FC; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FD; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FE; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FF; ');
        // man kneeling
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FB;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FC;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FD;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FE;&#x200D;&#x2642;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FF;&#x200D;&#x2642;&#xFE0F; ');
        // woman kneeling
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FB;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FC;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FD;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FE;&#x200D;&#x2640;&#xFE0F; ');
        $test .= html_entity_decode('&#x1F9CE;&#x1F3FF;&#x200D;&#x2640;&#xFE0F; ');
        // man with probing cane
        $test .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9AF; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9AF; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9AF; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9AF; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9AF; ');
        // woman with probing cane
        $test .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9AF; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9AF; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9AF; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9AF; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9AF; ');
        // man with motorized wheelchair
        $test .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9BC; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9BC; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9BC; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9BC; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9BC; ');
        // woman with motorized wheelchair
        $test .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9BC; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9BC; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9BC; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9BC; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9BC; ');
        // man with manual wheelchair
        $test .= html_entity_decode('&#x1F468;&#x1F3FB;&#x200D;&#x1F9BD; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F9BD; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F9BD; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F9BD; ');
        $test .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F9BD; ');
        // woman with manual wheelchair
        $test .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F9BD; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F9BD; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F9BD; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F9BD; ');
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F9BD; ');
        // @codingStandardsIgnoreLine
        $test .= '🏃🏻 🏃🏼 🏃🏽 🏃🏾 🏃🏿 🏃🏻‍♂️ 🏃🏼‍♂️ 🏃🏽‍♂️ 🏃🏾‍♂️ 🏃🏿‍♂️ 🏃🏻‍♀️ 🏃🏼‍♀️ 🏃🏽‍♀️ 🏃🏾‍♀️ 🏃🏿‍♀️ 💃🏻 💃🏼 💃🏽 💃🏾 💃🏿 🕺🏻 🕺🏼 🕺🏽 🕺🏾 🕺🏿 🕴🏻 🕴🏼 🕴🏽 🕴🏾 🕴🏿 ';
        // @codingStandardsIgnoreLine
        $test .= '🧖🏻 🧖🏼 🧖🏽 🧖🏾 🧖🏿 🧖🏻‍♂️ 🧖🏼‍♂️ 🧖🏽‍♂️ 🧖🏾‍♂️ 🧖🏿‍♂️ 🧖🏻‍♀️ 🧖🏼‍♀️ 🧖🏽‍♀️ 🧖🏾‍♀️ 🧖🏿‍♀️ 🧗🏻 🧗🏼 🧗🏽 🧗🏾 🧗🏿 🧗🏻‍♂️ 🧗🏼‍♂️ 🧗🏽‍♂️ 🧗🏾‍♂️ 🧗🏿‍♂️ 🧗🏻‍♀️ 🧗🏼‍♀️ 🧗🏽‍♀️ 🧗🏾‍♀️ 🧗🏿‍♀️';
        EmojiEntity::threeByteEncode($test);
        // EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinTonePersonActivityEmojis()

    /**
     * Skin Tone Person Sport
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#person-sport
     *
     * @return void
     */
    public function testSkinTonePersonSportEmojis(): void
    {
        // @codingStandardsIgnoreLine
        $original = '🏇🏻 🏇🏼 🏇🏽 🏇🏾 🏇🏿 🏂🏻 🏂🏼 🏂🏽 🏂🏾 🏂🏿 🏌🏻 🏌🏼 🏌🏽 🏌🏾 🏌🏿 🏌🏻‍♂️ 🏌🏼‍♂️ 🏌🏽‍♂️ 🏌🏾‍♂️ 🏌🏿‍♂️ 🏌🏻‍♀️ 🏌🏼‍♀️ 🏌🏽‍♀️ 🏌🏾‍♀️ 🏌🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $original .= '🏄🏻 🏄🏼 🏄🏽 🏄🏾 🏄🏿 🏄🏻‍♂️ 🏄🏼‍♂️ 🏄🏽‍♂️ 🏄🏾‍♂️ 🏄🏿‍♂️ 🏄🏻‍♀️ 🏄🏼‍♀️ 🏄🏽‍♀️ 🏄🏾‍♀️ 🏄🏿‍♀️ 🚣🏻 🚣🏼 🚣🏽 🚣🏾 🚣🏿 🚣🏻‍♂️ 🚣🏼‍♂️ 🚣🏽‍♂️ 🚣🏾‍♂️ 🚣🏿‍♂️ 🚣🏻‍♀️ 🚣🏼‍♀️ 🚣🏽‍♀️ 🚣🏾‍♀️ 🚣🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $original .= '🏊🏻 🏊🏼 🏊🏽 🏊🏾 🏊🏿 🏊🏻‍♂️ 🏊🏼‍♂️ 🏊🏽‍♂️ 🏊🏾‍♂️ 🏊🏿‍♂️ 🏊🏻‍♀️ 🏊🏼‍♀️ 🏊🏽‍♀️ 🏊🏾‍♀️ 🏊🏿‍♀️ ⛹🏻 ⛹🏼 ⛹🏽 ⛹🏾 ⛹🏿 ⛹🏻‍♂️ ⛹🏼‍♂️ ⛹🏽‍♂️ ⛹🏾‍♂️ ⛹🏿‍♂️ ⛹🏻‍♀️ ⛹🏼‍♀️ ⛹🏽‍♀️ ⛹🏾‍♀️ ⛹🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $original .= '🏋🏻 🏋🏼 🏋🏽 🏋🏾 🏋🏿 🏋🏻‍♂️ 🏋🏼‍♂️ 🏋🏽‍♂️ 🏋🏾‍♂️ 🏋🏿‍♂️ 🏋🏻‍♀️ 🏋🏼‍♀️ 🏋🏽‍♀️ 🏋🏾‍♀️ 🏋🏿‍♀️ 🚴🏻 🚴🏼 🚴🏽 🚴🏾 🚴🏿 🚴🏻‍♂️ 🚴🏼‍♂️ 🚴🏽‍♂️ 🚴🏾‍♂️ 🚴🏿‍♂️ 🚴🏻‍♀️ 🚴🏼‍♀️ 🚴🏽‍♀️ 🚴🏾‍♀️ 🚴🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $original .= '🚵🏻 🚵🏼 🚵🏽 🚵🏾 🚵🏿 🚵🏻‍♂️ 🚵🏼‍♂️ 🚵🏽‍♂️ 🚵🏾‍♂️ 🚵🏿‍♂️ 🚵🏻‍♀️ 🚵🏼‍♀️ 🚵🏽‍♀️ 🚵🏾‍♀️ 🚵🏿‍♀️ 🤸🏻 🤸🏼 🤸🏽 🤸🏾 🤸🏿 🤸🏻‍♂️ 🤸🏼‍♂️ 🤸🏽‍♂️ 🤸🏾‍♂️ 🤸🏿‍♂️ 🤸🏻‍♀️ 🤸🏼‍♀️ 🤸🏽‍♀️ 🤸🏾‍♀️ 🤸🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $original .= '🤽🏻 🤽🏼 🤽🏽 🤽🏾 🤽🏿 🤽🏻‍♂️ 🤽🏼‍♂️ 🤽🏽‍♂️ 🤽🏾‍♂️ 🤽🏿‍♂️ 🤽🏻‍♀️ 🤽🏼‍♀️ 🤽🏽‍♀️ 🤽🏾‍♀️ 🤽🏿‍♀️ 🤾🏻 🤾🏼 🤾🏽 🤾🏾 🤾🏿 🤾🏻‍♂️ 🤾🏼‍♂️ 🤾🏽‍♂️ 🤾🏾‍♂️ 🤾🏿‍♂️ 🤾🏻‍♀️ 🤾🏼‍♀️ 🤾🏽‍♀️ 🤾🏾‍♀️ 🤾🏿‍♀️ ';
        $original .= '🤹🏻 🤹🏼 🤹🏽 🤹🏾 🤹🏿 🤹🏻‍♂️ 🤹🏼‍♂️ 🤹🏽‍♂️ 🤹🏾‍♂️ 🤹🏿‍♂️ 🤹🏻‍♀️ 🤹🏼‍♀️ 🤹🏽‍♀️ 🤹🏾‍♀️ 🤹🏿‍♀️';
        // @codingStandardsIgnoreLine
        $test = '🏇🏻 🏇🏼 🏇🏽 🏇🏾 🏇🏿 🏂🏻 🏂🏼 🏂🏽 🏂🏾 🏂🏿 🏌🏻 🏌🏼 🏌🏽 🏌🏾 🏌🏿 🏌🏻‍♂️ 🏌🏼‍♂️ 🏌🏽‍♂️ 🏌🏾‍♂️ 🏌🏿‍♂️ 🏌🏻‍♀️ 🏌🏼‍♀️ 🏌🏽‍♀️ 🏌🏾‍♀️ 🏌🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $test .= '🏄🏻 🏄🏼 🏄🏽 🏄🏾 🏄🏿 🏄🏻‍♂️ 🏄🏼‍♂️ 🏄🏽‍♂️ 🏄🏾‍♂️ 🏄🏿‍♂️ 🏄🏻‍♀️ 🏄🏼‍♀️ 🏄🏽‍♀️ 🏄🏾‍♀️ 🏄🏿‍♀️ 🚣🏻 🚣🏼 🚣🏽 🚣🏾 🚣🏿 🚣🏻‍♂️ 🚣🏼‍♂️ 🚣🏽‍♂️ 🚣🏾‍♂️ 🚣🏿‍♂️ 🚣🏻‍♀️ 🚣🏼‍♀️ 🚣🏽‍♀️ 🚣🏾‍♀️ 🚣🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $test .= '🏊🏻 🏊🏼 🏊🏽 🏊🏾 🏊🏿 🏊🏻‍♂️ 🏊🏼‍♂️ 🏊🏽‍♂️ 🏊🏾‍♂️ 🏊🏿‍♂️ 🏊🏻‍♀️ 🏊🏼‍♀️ 🏊🏽‍♀️ 🏊🏾‍♀️ 🏊🏿‍♀️ ⛹🏻 ⛹🏼 ⛹🏽 ⛹🏾 ⛹🏿 ⛹🏻‍♂️ ⛹🏼‍♂️ ⛹🏽‍♂️ ⛹🏾‍♂️ ⛹🏿‍♂️ ⛹🏻‍♀️ ⛹🏼‍♀️ ⛹🏽‍♀️ ⛹🏾‍♀️ ⛹🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $test .= '🏋🏻 🏋🏼 🏋🏽 🏋🏾 🏋🏿 🏋🏻‍♂️ 🏋🏼‍♂️ 🏋🏽‍♂️ 🏋🏾‍♂️ 🏋🏿‍♂️ 🏋🏻‍♀️ 🏋🏼‍♀️ 🏋🏽‍♀️ 🏋🏾‍♀️ 🏋🏿‍♀️ 🚴🏻 🚴🏼 🚴🏽 🚴🏾 🚴🏿 🚴🏻‍♂️ 🚴🏼‍♂️ 🚴🏽‍♂️ 🚴🏾‍♂️ 🚴🏿‍♂️ 🚴🏻‍♀️ 🚴🏼‍♀️ 🚴🏽‍♀️ 🚴🏾‍♀️ 🚴🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $test .= '🚵🏻 🚵🏼 🚵🏽 🚵🏾 🚵🏿 🚵🏻‍♂️ 🚵🏼‍♂️ 🚵🏽‍♂️ 🚵🏾‍♂️ 🚵🏿‍♂️ 🚵🏻‍♀️ 🚵🏼‍♀️ 🚵🏽‍♀️ 🚵🏾‍♀️ 🚵🏿‍♀️ 🤸🏻 🤸🏼 🤸🏽 🤸🏾 🤸🏿 🤸🏻‍♂️ 🤸🏼‍♂️ 🤸🏽‍♂️ 🤸🏾‍♂️ 🤸🏿‍♂️ 🤸🏻‍♀️ 🤸🏼‍♀️ 🤸🏽‍♀️ 🤸🏾‍♀️ 🤸🏿‍♀️ ';
        // @codingStandardsIgnoreLine
        $test .= '🤽🏻 🤽🏼 🤽🏽 🤽🏾 🤽🏿 🤽🏻‍♂️ 🤽🏼‍♂️ 🤽🏽‍♂️ 🤽🏾‍♂️ 🤽🏿‍♂️ 🤽🏻‍♀️ 🤽🏼‍♀️ 🤽🏽‍♀️ 🤽🏾‍♀️ 🤽🏿‍♀️ 🤾🏻 🤾🏼 🤾🏽 🤾🏾 🤾🏿 🤾🏻‍♂️ 🤾🏼‍♂️ 🤾🏽‍♂️ 🤾🏾‍♂️ 🤾🏿‍♂️ 🤾🏻‍♀️ 🤾🏼‍♀️ 🤾🏽‍♀️ 🤾🏾‍♀️ 🤾🏿‍♀️ ';
        $test .= '🤹🏻 🤹🏼 🤹🏽 🤹🏾 🤹🏿 🤹🏻‍♂️ 🤹🏼‍♂️ 🤹🏽‍♂️ 🤹🏾‍♂️ 🤹🏿‍♂️ 🤹🏻‍♀️ 🤹🏼‍♀️ 🤹🏽‍♀️ 🤹🏾‍♀️ 🤹🏿‍♀️';
        EmojiEntity::threeByteEncode($test);
        // EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinTonePersonSportEmojis()

    /**
     * Skin Tone Person Resting
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#person-resting
     *
     * @return void
     */
    public function testSkinTonePersonRestingEmojis(): void
    {
        // @codingStandardsIgnoreLine
        $original = '🧘🏻 🧘🏼 🧘🏽 🧘🏾 🧘🏿 🧘🏻‍♂️ 🧘🏼‍♂️ 🧘🏽‍♂️ 🧘🏾‍♂️ 🧘🏿‍♂️ 🧘🏻‍♀️ 🧘🏼‍♀️ 🧘🏽‍♀️ 🧘🏾‍♀️ 🧘🏿‍♀️ 🛀🏻 🛀🏼 🛀🏽 🛀🏾 🛀🏿 🛌🏻 🛌🏼 🛌🏽 🛌🏾 🛌🏿';
        // @codingStandardsIgnoreLine
        $test = '🧘🏻 🧘🏼 🧘🏽 🧘🏾 🧘🏿 🧘🏻‍♂️ 🧘🏼‍♂️ 🧘🏽‍♂️ 🧘🏾‍♂️ 🧘🏿‍♂️ 🧘🏻‍♀️ 🧘🏼‍♀️ 🧘🏽‍♀️ 🧘🏾‍♀️ 🧘🏿‍♀️ 🛀🏻 🛀🏼 🛀🏽 🛀🏾 🛀🏿 🛌🏻 🛌🏼 🛌🏽 🛌🏾 🛌🏿';
        EmojiEntity::threeByteEncode($test);
        // EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinTonePersonRestingEmojis()

    /**
     * Skin Tone Person Family
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#family
     *
     * @return void
     */
    public function testSkinToneFamilyEmojis(): void
    {
        // holding hands
        $original = html_entity_decode('&#x1F9D1;&#x1F3FB;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FB; ');  // 1226
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FB; '); // 1227
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FC; '); // 1228
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FB; '); // 1229
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FC; '); // 1230
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FD; '); // 1231
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FB; '); // 1232
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FC; '); // 1233
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FD; '); // 1234
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FE; '); // 1235
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FB; '); // 1236
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FC; '); // 1237
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FD; '); // 1238
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FE; '); // 1239
        $original .= html_entity_decode('&#x1F9D1;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FF; '); // 1240
        // women holding hands
        $original .= html_entity_decode('&#x1F46D;&#x1F3FB '); // 1241
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FB; '); // 1242
        $original .= html_entity_decode('&#x1F46D;&#x1F3FC '); // 1243
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FB; '); // 1244
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FC; '); // 1245
        $original .= html_entity_decode('&#x1F46D;&#x1F3FD '); // 1246
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FB; '); // 1247
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FC; '); // 1248
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FD; '); // 1249
        $original .= html_entity_decode('&#x1F46D;&#x1F3FE '); // 1250
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FB; '); // 1251
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FC; '); // 1252
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FD; '); // 1253
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FE; '); // 1254
        $original .= html_entity_decode('&#x1F46D;&#x1F3FF '); // 1255
        // woman man holding hands
        $original .= html_entity_decode('&#x1F46B;&#x1F3FB; '); // 1256
        $original .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1257
        $original .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1258
        $original .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FE; '); // 1259
        $original .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FF; '); // 1260
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1261
        $original .= html_entity_decode('&#x1F46B;&#x1F3FC; '); // 1262
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1263
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FE; '); // 1264
        $original .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FF; '); // 1265
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1266
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1267
        $original .= html_entity_decode('&#x1F46B;&#x1F3FD; '); // 1268
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FE; '); // 1269
        $original .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FF; '); // 1270
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1271
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1272
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1273
        $original .= html_entity_decode('&#x1F46B;&#x1F3FE; '); // 1274
        $original .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FF; '); // 1275
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1276
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1277
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1278
        $original .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FE; '); // 1279
        $original .= html_entity_decode('&#x1F46B;&#x1F3FF; '); // 1280
        // men holding hands
        $original .= html_entity_decode('&#x1F46C;&#x1F3FB; '); // 1281
        $original .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1282
        $original .= html_entity_decode('&#x1F46C;&#x1F3FC; '); // 1283
        $original .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1284
        $original .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1285
        $original .= html_entity_decode('&#x1F46C;&#x1F3FD; '); // 1286
        $original .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1287
        $original .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1288
        $original .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1289
        $original .= html_entity_decode('&#x1F46C;&#x1F3FE; '); // 1290
        $original .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1291
        $original .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1292
        $original .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1293
        $original .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FE; '); // 1294
        $original .= html_entity_decode('&#x1F46C;&#x1F3FF;'); // 1295
        // holding hands
        $test = html_entity_decode('&#x1F9D1;&#x1F3FB;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FB; ');  // 1226
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FB; '); // 1227
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FC; '); // 1228
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FB; '); // 1229
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FC; '); // 1230
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FD; '); // 1231
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FB; '); // 1232
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FC; '); // 1233
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FD; '); // 1234
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FE; '); // 1235
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FB; '); // 1236
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FC; '); // 1237
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FD; '); // 1238
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FE; '); // 1239
        $test .= html_entity_decode('&#x1F9D1;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F9D1;&#x1F3FF; '); // 1240
        // women holding hands
        $test .= html_entity_decode('&#x1F46D;&#x1F3FB '); // 1241
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FB; '); // 1242
        $test .= html_entity_decode('&#x1F46D;&#x1F3FC '); // 1243
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FB; '); // 1244
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FC; '); // 1245
        $test .= html_entity_decode('&#x1F46D;&#x1F3FD '); // 1246
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FB; '); // 1247
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FC; '); // 1248
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FD; '); // 1249
        $test .= html_entity_decode('&#x1F46D;&#x1F3FE '); // 1250
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FB; '); // 1251
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FC; '); // 1252
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FD; '); // 1253
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F469;&#x1F3FE; '); // 1254
        $test .= html_entity_decode('&#x1F46D;&#x1F3FF '); // 1255
        // woman man holding hands
        $test .= html_entity_decode('&#x1F46B;&#x1F3FB; '); // 1256
        $test .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1257
        $test .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1258
        $test .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FE; '); // 1259
        $test .= html_entity_decode('&#x1F469;&#x1F3FB;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FF; '); // 1260
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1261
        $test .= html_entity_decode('&#x1F46B;&#x1F3FC; '); // 1262
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1263
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FE; '); // 1264
        $test .= html_entity_decode('&#x1F469;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FF; '); // 1265
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1266
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1267
        $test .= html_entity_decode('&#x1F46B;&#x1F3FD; '); // 1268
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FE; '); // 1269
        $test .= html_entity_decode('&#x1F469;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FF; '); // 1270
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1271
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1272
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1273
        $test .= html_entity_decode('&#x1F46B;&#x1F3FE; '); // 1274
        $test .= html_entity_decode('&#x1F469;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FF; '); // 1275
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1276
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1277
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1278
        $test .= html_entity_decode('&#x1F469;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FE; '); // 1279
        $test .= html_entity_decode('&#x1F46B;&#x1F3FF; '); // 1280
        // men holding hands
        $test .= html_entity_decode('&#x1F46C;&#x1F3FB; '); // 1281
        $test .= html_entity_decode('&#x1F468;&#x1F3FC;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1282
        $test .= html_entity_decode('&#x1F46C;&#x1F3FC; '); // 1283
        $test .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1284
        $test .= html_entity_decode('&#x1F468;&#x1F3FD;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1285
        $test .= html_entity_decode('&#x1F46C;&#x1F3FD; '); // 1286
        $test .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1287
        $test .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1288
        $test .= html_entity_decode('&#x1F468;&#x1F3FE;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1289
        $test .= html_entity_decode('&#x1F46C;&#x1F3FE; '); // 1290
        $test .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FB; '); // 1291
        $test .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FC; '); // 1292
        $test .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FD; '); // 1293
        $test .= html_entity_decode('&#x1F468;&#x1F3FF;&#x200D;&#x1F91D;&#x200D;&#x1F468;&#x1F3FE; '); // 1294
        $test .= html_entity_decode('&#x1F46C;&#x1F3FF;'); // 1295
        EmojiEntity::threeByteEncode($test);
        // EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinToneFamilyEmojis()

    /**
     * Skin Tone Component
     *
     * @see https://unicode.org/emoji/charts/full-emoji-modifiers.html#skin-tone
     *
     * @return void
     */
    public function testSkinToneComponent(): void
    {
        $original = '🏻 🏼 🏽 🏾 🏿';
        $test = '🏻 🏼 🏽 🏾 🏿';
        EmojiEntity::threeByteEncode($test);
        // EmojiEntity::twoByteEncode($test);
        // $test should now only contain ASCII range characters.
        $valid = false;
        if (preg_match('/[^\x{00}-\x{7F}]/u', $test) === 0) {
            $valid = true;
        }
        $this->assertTrue($valid);
        $test = html_entity_decode($test);
        $this->assertSame($original, $test);
    }//end testSkinToneComponent()
}//end class

?>