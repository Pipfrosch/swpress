<?php
declare(strict_types=1);

/**
 * Unit testing for TIS620 class
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

define('TIS620_ENCODE_MODE', 'win874');
use PHPUnit\Framework\TestCase;
use \Pipfrosch\SqlObject\StaticMethods\TIS620;

/**
 * Test class for \Pipfrosch\SqlObject\StaticMethods\TIS620
 */
// @codingStandardsIgnoreLine
final class ThaiTestWin874 extends TestCase {
    /**
     * Test conversion
     *
     * @return void
     */
    public function testThaiIndustrialStandardSixTwenty(): void
    {
        $originalUTF8 = 'เมืองสามหมอก';
        // validate that above really is UTF-8
        $test = mb_detect_encoding($originalUTF8, 'UTF-8', true);
        $this->assertSame('UTF-8', $test);
        // convert to TIS-620
        $foo = TIS620::encodeTo8bit($originalUTF8);
        $test = is_string($foo);
        $this->assertTrue($test);
        $this->assertNotSame($originalUTF8, $foo);
        $bar = TIS620::encodeTo8bit($foo);
        $this->assertSame($foo, $bar);
        $newUTF8 = TIS620::encodeToUTF8($bar);
        $this->assertSame($originalUTF8, $newUTF8);
    }//end testThaiIndustrialStandardSixTwenty()

    /**
     * Test Unicode -> entity map.
     *
     * @return void()
     */
    public function testUnicodeToEntityConversionMap(): void
    {
        $arr = TIS620::returnEntityConversionArray();
        $s = array('/^\/\\\x\{/', '/\}\/u$/');
        $r = array('&#x', ';');
        foreach ($arr as $key => $value) {
            $key = preg_replace($s, $r, $key);
            $a = html_entity_decode($key);
            $b = html_entity_decode($value);
            $this->assertSame($a, $b);
        }
    }//end testUnicodeToEntityConversionMap()

    /**
     * Test string with non-Thai characters
     *
     * @return void
     */
    public function testThaiStringWithCurlyQuotes(): void
    {
        $originalUTF8 = '[“เมืองสามหมอก”]';
        // should return string
        $foo = TIS620::encodeTo8bit($originalUTF8, true);
        $test = is_string($foo);
        $this->assertTrue($test);
        // convert back to UTF8
        $actual = TIS620::encodeToUTF8($foo);
        $expected = '[“เมืองสามหมอก”]';
        $this->assertSame($expected, $actual);
    }//end testThaiStringWithCurlyQuotes()

    /**
     * Test string with Japanese characters. They should be replaced
     *  with ? characters.
     *
     * @return void
     */
    public function testThaiStringWithJapaneseCharacters(): void
    {
        $originalUTF8 = '[“เมืองสามหมอก”][マウンテンフィジー]';
        // should return false
        $foo = TIS620::encodeTo8bit($originalUTF8, true);
        $this->assertFalse($foo);
        // now should return a string
        $foo = TIS620::encodeTo8bit($originalUTF8, true, true);
        $test = is_string($foo);
        $this->assertTrue($test);
        // convert back to UTF8
        $actual = TIS620::encodeToUTF8($foo);
        $expected = '[“เมืองสามหมอก”][?????????]';
        $this->assertSame($expected, $actual);
    }//end testThaiStringWithJapaneseCharacters()

    /**
     * Test string with Emoji
     *
     * @return void
     */
    public function testThaiStringWithEmoji(): void
    {
        $originalUTF8 = 'เมืองสามหมอก[😵]';
        // should return false
        $foo = TIS620::encodeTo8bit($originalUTF8);
        $this->assertFalse($foo);
        // now should return a string
        $foo = TIS620::encodeTo8bit($originalUTF8, true);
        $test = is_string($foo);
        $this->assertTrue($test);
        // convert back to UTF8
        $actual = TIS620::encodeToUTF8($foo);
        $expected = 'เมืองสามหมอก[&#x1F635;]';
        $this->assertSame($expected, $actual);
    }//end testThaiStringWithEmoji()

    /**
     * Test string with Emoji sequence
     *
     * @return void
     */
    public function testThaiStringWithSequenceEmoji(): void
    {
        $originalUTF8 = 'เมืองสามหมอก[🧟‍♂️]';
        // should return false
        $foo = TIS620::encodeTo8bit($originalUTF8);
        $this->assertFalse($foo);
        // now should return a string
        $foo = TIS620::encodeTo8bit($originalUTF8, true);
        $test = is_string($foo);
        $this->assertTrue($test);
        // convert back to UTF8
        $actual = TIS620::encodeToUTF8($foo);
        $expected = 'เมืองสามหมอก[&#x1F9DF;&#x200D;&#x2642;&#xFE0F;]';
        $this->assertSame($expected, $actual);
    }//end testThaiStringWithSequenceEmoji()

    /**
     * Test string with non-breaking space
     *
     * @return void
     */
    public function testThaiStringWithNonBreakingSpace(): void
    {
        $originalUTF8 = 'เมืองสามหมอก[' . html_entity_decode('&nbsp;') . ']';
        // should return a string
        $foo = TIS620::encodeTo8bit($originalUTF8);
        $test = is_string($foo);
        $this->assertTrue($test);
        // convert back to UTF8
        $actual = TIS620::encodeToUTF8($foo);
        $expected = 'เมืองสามหมอก[ ]';
        $this->assertSame($expected, $actual);
    }//end testThaiStringWithNonBreakingSpace()
}//end class

?>