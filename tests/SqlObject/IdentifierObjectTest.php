<?php
declare(strict_types=1);

/**
 * Unit testing for classes that extend \Pipfrosch\SqlObject\AbstractIdentifier
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;
use \Pipfrosch\SqlObject\ColumnIdentifier;
use \Pipfrosch\SqlObject\TableIdentifier;
use \Pipfrosch\SqlObject\DatabaseIdentifier;

/**
 * Test class for \Pipfrosch\SqlObject\IntegerValue
 */
// @codingStandardsIgnoreLine
final class IdentifierObjectTest extends TestCase {
    /**
     * Test basic creation of Database Identified Object
     *
     * @return void
     */
    public function testCreateDatabaseIdentifier(): void
    {
        $foo = new DatabaseIdentifier('fubar');
        $expected = 'fubar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<identifier type="DATABASE">fubar</identifier>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testCreateDatabaseIdentifier()

    /**
     * Test that space in identifier triggers quote
     *
     * @return void
     */
    public function testSpaceInIdentifierRequiresQuote(): void
    {
        $foo = new DatabaseIdentifier('foo bar');
        $expected = 'foo bar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // this is where quote should be applied
        $expected = '`foo bar`';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<identifier type="DATABASE">`foo bar`</identifier>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
        // now with ANSI
        $foo = new DatabaseIdentifier('foo bar', true);
        $expected = 'foo bar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // this is where quote should be applied
        $expected = '"foo bar"';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<identifier type="DATABASE" quote="ANSI">"foo bar"</identifier>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testSpaceInIdentifierRequiresQuote()

    /**
     * Test that initial quotes are stripped
     *
     * @return void
     */
    public function testStrippingOfInitialQuotesFromIdentifier(): void
    {
        $foo = new DatabaseIdentifier('`fubar`');
        $expected = 'fubar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        $foo = new DatabaseIdentifier('"fubar"', true);
        $expected = 'fubar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
    }//end testStrippingOfInitialQuotesFromIdentifier()

    /**
     * Test that reserved word gets quoted.
     *
     * @return void
     */
    public function testReservedWordGetsQuoted(): void
    {
        $foo = new DatabaseIdentifier('insert');
        $expected = 'insert';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should be quotes
        $actual = $foo->__toString();
        $expected = '`insert`';
        $this->assertSame($expected, $actual);
        // now test from exceptions set
        $foo = new DatabaseIdentifier('timestamp');
        $expected = 'timestamp';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should be quotes
        $actual = $foo->__toString();
        $expected = '`timestamp`';
        // now test from oracle set
        $foo = new DatabaseIdentifier('period');
        $expected = 'period';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should be quotes
        $actual = $foo->__toString();
        $expected = '`period`';
    }//end testReservedWordGetsQuoted()

    /**
     * Test completely numeric gets quoted
     *
     * @return void
     */
    public function testFullyDecimalIdentifierGetsQuoted(): void
    {
        $foo = new DatabaseIdentifier('1234567890');
        $expected = '1234567890';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should be quotes
        $actual = $foo->__toString();
        $expected = '`1234567890`';
        $this->assertSame($expected, $actual);
    }//end testFullyDecimalIdentifierGetsQuoted()

    /**
     * Test partially numeric does not get quoted
     *
     * @return void
     */
    public function testPartiallyNumericRestAlphaDoesNotGetQuoted(): void
    {
        $foo = new DatabaseIdentifier('1234567890abcdefg');
        $expected = '1234567890abcdefg';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should be quotes
        $actual = $foo->__toString();
        $expected = '1234567890abcdefg';
        $this->assertSame($expected, $actual);
    }//end testPartiallyNumericRestAlphaDoesNotGetQuoted()

    /**
     * Test that string looks like float gets quoted.
     *
     * @return void
     */
    public function testIdentifierLooksLikeFloatGetsQuoted(): void
    {
        $foo = new DatabaseIdentifier('4e5678');
        $expected = '4e5678';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should be quotes
        $actual = $foo->__toString();
        $expected = '`4e5678`';
        $this->assertSame($expected, $actual);
    }//end testIdentifierLooksLikeFloatGetsQuoted()

    /**
     * Test that identifier that contains period gets quoted
     *
     * @return void
     */
    public function testIdentifierThatContainsPeriodGetsQuoted(): void
    {
        $foo = new DatabaseIdentifier('foo.bar');
        $expected = 'foo.bar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should be quotes
        $actual = $foo->__toString();
        $expected = '`foo.bar`';
        $this->assertSame($expected, $actual);
    }//end testIdentifierThatContainsPeriodGetsQuoted()

    /**
     * Test that identifier that contains backslash gets quoted
     *
     * @return void
     */
    public function testIdentifierWithBackslashGetsQuoted(): void
    {
        $foo = new DatabaseIdentifier('foo\bar');
        $expected = 'foo\bar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should be quotes
        $actual = $foo->__toString();
        $expected = '`foo\bar`';
        $this->assertSame($expected, $actual);
    }//end testIdentifierWithBackslashGetsQuoted()

    /**
     * Test that identifier that contains forwardslash gets quoted
     *
     * @return void
     */
    public function testIdentifierWithForwardslashGetsQuoted(): void
    {
        $foo = new DatabaseIdentifier('foo/bar');
        $expected = 'foo/bar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should be quotes
        $actual = $foo->__toString();
        $expected = '`foo/bar`';
        $this->assertSame($expected, $actual);
    }//end testIdentifierWithForwardslashGetsQuoted()

    /**
     * Test two-byte unicode does not get quoted
     *
     * @return void
     */
    public function testTwobyteUnicodeDoesNotGetQuoted(): void
    {
        // character is cfa2
        $foo = new DatabaseIdentifier('fooϢbar');
        $expected = 'fooϢbar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should not be quotes
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testTwobyteUnicodeDoesNotGetQuoted()

    /**
     * Test dollar sign and underscore do not get quoted
     *
     * @return void
     */
    public function testIdentifierWithDollarSignAndUnderscoreDoesNotGetQuoted(): void
    {
        $foo = new DatabaseIdentifier('fo$o_bar');
        $expected = 'fo$o_bar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should not be quotes
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
    }//end testIdentifierWithDollarSignAndUnderscoreDoesNotGetQuoted()

    /**
     * Test backtick gets escaped
     *
     * @return void
     */
    public function testIdentifierWithBacktickGetsEscaped(): void
    {
        $foo = new DatabaseIdentifier('foo`bar');
        $expected = 'foo`bar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should not be quotes
        $actual = $foo->__toString();
        $expected = '`foo``bar`';
        $this->assertSame($expected, $actual);
    }//end testIdentifierWithBacktickGetsEscaped()

    /**
     * Test backtick not escaped in ANSI mode
     *
     * @return void
     */
    public function testIdentifierWithBacktickNotEscapedInAnsiMode(): void
    {
        $foo = new DatabaseIdentifier('foo`bar', true);
        $expected = 'foo`bar';
        $actual = $foo->getValue();
        $this->assertSame($expected, $actual);
        // This is where there should not be quotes
        $actual = $foo->__toString();
        $expected = '"foo`bar"';
        $this->assertSame($expected, $actual);
    }//end testIdentifierWithBacktickNotEscapedInAnsiMode()

    /**
     * Test Table identifier with database identifier.
     *
     * @return void
     */
    public function testTableIdentifierWithDatabaseIdentiferNeitherQuoted(): void
    {
        $foo = new DatabaseIdentifier('foo');
        $bar = new TableIdentifier('bar');
        $bar->setDatabaseIdentifier($foo);
        $expected = 'bar';
        $actual = $bar->getValue();
        $this->assertSame($expected, $actual);
        // now test __toString includes database
        $expected = 'foo.bar';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
    }

    /**
     * Test Table identifier with quoted database identifier
     *
     * @return void
     */
    public function testTableIdentifierWithQuotedDatabaseIdentifier(): void
    {
        $foo = new DatabaseIdentifier('123');
        $bar = new TableIdentifier('bar');
        $bar->setDatabaseIdentifier($foo);
        // test __toString includes database
        $expected = '`123`.bar';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
    }

    /**
     * Test quoted table identifier with unquoted database identifier
     *
     * @return void
     */
    public function testTableIdentifierQuotedWithUnquotedDatabaseIdentifier(): void
    {
        $foo = new DatabaseIdentifier('foo');
        $bar = new TableIdentifier('456');
        $bar->setDatabaseIdentifier($foo);
        // test __toString includes database
        $expected = 'foo.`456`';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
    }

    /**
     * Test both database and table quoted
     *
     * @return void
     */
    public function testTableIdentifierWithDatabaseIdentifierBothQuoted(): void
    {
        $foo = new DatabaseIdentifier('123');
        $bar = new TableIdentifier('456');
        $bar->setDatabaseIdentifier($foo);
        // test __toString includes database
        $expected = '`123`.`456`';
        $actual = $bar->__toString();
        $this->assertSame($expected, $actual);
    }

    /**
     * Test column identifier with table identifier
     *
     * @return void
     */
    public function testColumnIdentifierWithTableIdentifierNeitherQuoted(): void
    {
        $bar = new TableIdentifier('bar');
        $fubar = new ColumnIdentifier('fubar');
        $fubar->setTableIdentifier($bar);
        // test __toString includes database
        $expected = 'bar.fubar';
        $actual = $fubar->__toString();
        $this->assertSame($expected, $actual);
    }

    /**
     * Test column identifier with both database and table with column quoted
     *
     * @return void
     */
    public function testColumnIdentifierQuotedWithUnquotedTableAndDatabaseIdentifiers(): void
    {
        $foo = new DatabaseIdentifier('foo');
        $bar = new TableIdentifier('bar');
        $bar->setDatabaseIdentifier($foo);
        $fubar = new ColumnIdentifier('fu bar');
        $fubar->setTableIdentifier($bar);
        // test __toString includes database
        $expected = 'foo.bar.`fu bar`';
        $actual = $fubar->__toString();
        $this->assertSame($expected, $actual);
        // throw in a DOMNode test
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $fubar->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<identifier type="COLUMN" nullable="null ok">foo.bar.`fu bar`</identifier>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }

    /**
     * Test with non-UTF8 input
     *
     * @return void
     */
    public function testConvertNonUtfeightEncodingToUtfeight(): void
    {
        $utf8 = 'エッチ';
        $eucjpWin = mb_convert_encoding($utf8, 'eucjp-win', 'UTF-8');
        // verify the two are different - test will not continue if encoding failed
        $this->assertNotSame($utf8, $eucjpWin);
        // Now create column identifier using the eicjp-win string
        $foo = new ColumnIdentifier($eucjpWin);
        $actual = $foo->getValue();
        //it should identical to the original utf8
        $this->assertSame($utf8, $actual);
    }//end testConvertNonUtfeightEncodingToUtfeight()
}//end class

?>