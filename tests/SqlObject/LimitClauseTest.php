<?php
declare(strict_types=1);

/**
 * Unit testing for \Pipfrosch\SqlObject\IntegerValue
 *
 * @package    SWPress
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;
use \Pipfrosch\SqlObject\LimitClause;

/**
 * Test class for \Pipfrosch\SqlObject\LimitClause
 */
// @codingStandardsIgnoreLine
final class LimitClauseTest extends TestCase {
    /**
     * Test will just row count
     *
     * @return void
     */
    public function testLimitClauseWithJustRowCount(): void
    {
        $foo = new LimitClause();
        $foo->setRowcount(16);
        $expected = '16';
        $actual = $foo->getRowcount();
        $this->assertSame($expected, $actual);
        $expected = 'LIMIT 16';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<clause type="LIMIT"><rowcount><value type="NUMERIC" subtype="INT">16</value></rowcount></clause>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testLimitClauseWithJustRowCount()

    /**
     * Test with just rows examined
     *
     * @return void
     */
    public function testLimitClauseWithJustRowsExamined(): void
    {
        $foo = new LimitClause();
        $foo->setRowsExamined(168);
        $expected = '168';
        $actual = $foo->getRowsExamined();
        $this->assertSame($expected, $actual);
        $expected = 'LIMIT ROWS EXAMINED 168';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<clause type="LIMIT"><rowsexamined><value type="NUMERIC" subtype="INT">168</value></rowsexamined></clause>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testLimitClauseWithJustRowsExamined()

    /**
     * Test with just offset
     *
     * @return void
     */
    public function testLimitClauseWithJustOffset(): void
    {
        $foo = new LimitClause();
        $foo->setOffset(5);
        $expected = '5';
        $actual = $foo->getOffset();
        $this->assertSame($expected, $actual);
        $expected = '';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<!--Invalid LIMIT clause-->';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testLimitClauseWithJustOffset()

    /**
     * Test with offset and rows
     *
     * @return void
     */
    public function testLimitClauseWithOffsetAndRowCount(): void
    {
        $foo = new LimitClause();
        $foo->setOffset(75);
        $foo->setRowcount(10);
        $expected = 'LIMIT 75,10';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<clause type="LIMIT"><offset><value type="NUMERIC" subtype="INT">75</value></offset><rowcount><value type="NUMERIC" subtype="INT">10</value></rowcount></clause>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testLimitClauseWithOffsetAndRowCount()

    /**
     * Test with offset and rows and rows examined
     *
     * @return void
     */
    public function testLimitClauseWithOffsetAndRowCountAndRowsExamined(): void
    {
        $foo = new LimitClause();
        $foo->setOffset(75);
        $foo->setRowcount(10);
        $foo->setRowsExamined(400);
        $expected = 'LIMIT 75,10 ROWS EXAMINED 400';
        $actual = $foo->__toString();
        $this->assertSame($expected, $actual);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->loadXML('<?xml version="1.0" encoding="UTF-8"?><xml />');
        $xml = $dom->getElementsByTagName('xml')->item(0);
        $node = $foo->toDomNode($dom);
        $xml->appendChild($node);
        $expected = '<clause type="LIMIT"><offset><value type="NUMERIC" subtype="INT">75</value></offset><rowcount><value type="NUMERIC" subtype="INT">10</value></rowcount><rowsexamined><value type="NUMERIC" subtype="INT">400</value></rowsexamined></clause>';
        $actual = $dom->saveXML();
        $this->assertStringContainsString($expected, $actual);
    }//end testLimitClauseWithOffsetAndRowCountAndRowsExamined()

    /**
     * Test false with invalid argument
     *
     * @return void
     */
    public function testLimitClauseSetReturnsFalseWithNegativeArgument(): void
    {
        $foo = new LimitClause();
        $res = $foo->setOffset(-7);
        $this->assertFalse($res);
        $res = $foo->setRowcount(-7);
        $this->assertFalse($res);
        $res = $foo->setRowsExamined(-7);
        $this->assertFalse($res);
    }//end testLimitClauseSetReturnsFalseWithNegativeArgument()
}//end class

?>