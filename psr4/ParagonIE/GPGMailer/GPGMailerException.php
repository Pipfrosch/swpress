<?php

/**
 * This class allows sending messages encrypted with and/or signed with GnuPG.
 *
 * Unit tests not yet imported from upstream project.
 *
 * @package ParagonIE\GPGMailer
 * @author  Paragon Initiative Enterprises <info@paragonie.com>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://github.com/paragonie/gpg-mailer
 *
 * @see https://paragonie.com
 */

namespace ParagonIE\GPGMailer;

/**
 * Class GPGMailerException
 *
 * @package ParagonIE\GPGMailer
 */
class GPGMailerException extends \Exception
{

}//end class

