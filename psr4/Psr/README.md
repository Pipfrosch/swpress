PHP-FIG Interfaces
==================

These interfaces are for planned future classes that implement them.


HTTP Messaging Interfaces (PSR-7)
---------------------------------

Long term I am planning to replace the `\Requests` and `\SWPress\HTTP` set of
classes with classes that implement
[PSR-7](https://www.php-fig.org/psr/psr-7/).

The `\Requests` class is old, much of what it does I believe can be done better
using modern methods. The `\SWPress\HTTP` set of classes is based on WordPress
classes that make use of the `\Requests` class.


SimpleCache Interfaces (PSR-16)
-------------------------------

Performance can be improved by intelligent caching. I already have classes that
implement [PSR-16](https://www.php-fig.org/psr/psr-16) using APCu and Redis,
which allow the system administrator to choose which they prefer.

Those classes are not yet brought in to this project, as how to best implement
them here has not yet been determined.
