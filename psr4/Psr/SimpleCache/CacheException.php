<?php
/**
 * @package SimpleCache
 * @author  PHP-FIG
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://www.php-fig.org/psr/psr-16/
 */

namespace Psr\SimpleCache;

/**
 * Interface used for all types of exceptions thrown by the implementing library.
 */
interface CacheException
{
}//end interface

