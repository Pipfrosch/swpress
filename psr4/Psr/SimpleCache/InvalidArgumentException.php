<?php
/**
 * @package SimpleCache
 * @author  PHP-FIG
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://www.php-fig.org/psr/psr-16/
 */

namespace Psr\SimpleCache;

/**
 * Exception interface for invalid cache arguments.
 *
 * When an invalid argument is passed it must throw an exception which implements
 * this interface
 */
interface InvalidArgumentException extends CacheException
{
}//end interface

