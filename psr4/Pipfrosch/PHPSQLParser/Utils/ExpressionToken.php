<?php
declare(strict_types=1);

/**
 * Expression Token PHPDoc file comment missing in upstream. This is based on other files
 *
 * @package PHP-SQL-Parser
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link    https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Utils;

/*
use PHPSQLParser\Options;
use PHPSQLParser\processors\DefaultProcessor;
*/

/**
 * ExpressionToken Class
 */
class ExpressionToken
{
    /**
     * @var bool|array
     */
    private $subTree;

    /**
     * @var string
     */
    private $expression;

    /**
     * @var int|string
     */
    private $key;

    /**
     * @var string
     */
    private $token;

    /**
     * @var bool|string
     */
    private $tokenType;

    /**
     * @var string
     */
    private $trim;

    /**
     * @var string
     */
    private $upper;

    /**
     * @var null|array
     */
    private $noQuotes;

    /**
     * @var string
     */
    private $delim = '';

    /**
     * The constructor
     *
     * @param int|string $key   The key value.
     *
     * @param string $token The token value.
     */
    public function __construct($key = "", $token = "")
    {
        $this->subTree = false;
        $this->expression = "";
        $this->key = $key;
        $this->token = $token;
        $this->tokenType = false;
        $this->trim = trim($token);
        $this->upper = strtoupper($this->trim);
        $this->noQuotes = null;
    }//end __construct()

    /**
     * AddToken
     *
     * TODO: we could replace it with a constructor new ExpressionToken(this, "*")
     *
     * @param string $string String to add to token.
     *
     * @return void
     */
    public function addToken(string $string): void
    {
        $this->token .= $string;
    }//end addToken()

    /**
     * Checks if enclosed within parenthesis.
     *
     * @return bool
     */
    public function isEnclosedWithinParenthesis(): bool
    {
        return (!empty($this->upper) && $this->upper[0] === '(' && substr($this->upper, -1) === ')');
    }//end isEnclosedWithinParenthesis()

    /**
     * Sets the SubTree
     *
     * @param bool|array $tree The tree.
     *
     * @return void
     */
    public function setSubTree($tree): void
    {
        $this->subTree = $tree;
    }//end setSubTree

    /**
     * Gets the SubTree
     *
     * @return array
     */
    public function getSubTree(): array
    {
        if (is_array($this->subTree)) {
            return $this->subTree;
        }
        return array();
    }//end getSubTree()

    /**
     * Gets the upper idx
     *
     * @param bool|int $idx The index.
     *
     * @return string
     */
    public function getUpper($idx = false): string
    {
        if (is_int($idx)) {
            $this->upper[$idx];
        }
        return $this->upper;
    }//end getUpper()

    /**
     * Gets the trim
     *
     * @param bool|int $idx The index.
     *
     * @return string
     */
    public function getTrim($idx = false): string
    {
        if (is_int($idx)) {
            return $this->trim[$idx];
        }
        return $this->trim;
    }//end getTrim()

    /**
     * Gets the token
     *
     * @param bool|int $idx The idx key.
     *
     * @return string
     */
    public function getToken($idx = false): string
    {
        if (is_int($idx)) {
            return $this->token[$idx];
        }
        return $this->token;
    }//end getToken()

    /**
     * Sets NoQuotes
     *
     * @param null|string                     $token   The token.
     * @param \Pipfrosch\PHPSQLParser\Options $options The options.
     * @param mixed                           $qchars  No idea what it does, does not seem to be used.
     *
     * @return void
     */
    public function setNoQuotes($token, \Pipfrosch\PHPSQLParser\Options $options, $qchars = null): void
    {
        $this->noQuotes = ($token === null) ? null : $this->revokeQuotation($token, $options);
    }//end setNoQuotes()

    /**
     * Set the token type,
     *
     * @param string $type The token type.
     *
     * @return void
     */
    public function setTokenType($type): void
    {
        $this->tokenType = $type;
    }//end setTokenType()

    /**
     * Sets the delimiter
     *
     * @param string $delim The delimiter to set.
     *
     * @return void
     */
    public function setDelim(string $delim): void
    {
        $this->delim = $delim;
    }//end setDelim()

    /**
     * Check is token ends with specified needle
     *
     * @param string $needle The needle to check.
     *
     * @return bool
     */
    public function endsWith($needle): bool
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        $start = $length * -1;
        return (substr($this->token, $start) === $needle);
    }//end endsWith()

    /**
     * Tests if whitespace token.
     *
     * @return bool
     */
    public function isWhitespaceToken(): bool
    {
        return ($this->trim === "");
    }//end isWhitespaceToken()

    /**
     * Tests if comma token.
     *
     * @return bool
     */
    public function isCommaToken(): bool
    {
        return ($this->trim === ",");
    }//end isCommaToken()

    /**
     * Tests if variable token.
     *
     * @return bool
     */
    public function isVariableToken(): bool
    {
        return $this->upper[0] === '@';
    }//end isVariableToken()

    /**
     * Tests if subquery token.
     *
     * @return bool
     */
    public function isSubQueryToken(): bool
    {
        if (preg_match("/^\\(\\s*(-- [\\w\\s]+\\n)?\\s*SELECT/i", $this->trim) === 1) {
            return true;
        }
        return false;
    }//end isSubQueryToken()

    /**
     * Tests if token type is expression.
     *
     * @return bool
     */
    public function isExpression(): bool
    {
        return $this->tokenType === ExpressionType::EXPRESSION;
    }//end isExpression()

    /**
     * Tests if token type is bracket expression.
     *
     * @return bool
     */
    public function isBracketExpression(): bool
    {
        return $this->tokenType === ExpressionType::BRACKET_EXPRESSION;
    }//end isBracketExpression()

    /**
     * Tests if token type is operator.
     *
     * @return bool
     */
    public function isOperator(): bool
    {
        return $this->tokenType === ExpressionType::OPERATOR;
    }//end isOperator()

    /**
     * Tests if token type is in the list of types.
     *
     * @return bool
     */
    public function isInList(): bool
    {
        return $this->tokenType === ExpressionType::IN_LIST;
    }//end isInList()

    /**
     * Tests if token type is a function
     *
     * @return bool
     */
    public function isFunction(): bool
    {
        return $this->tokenType === ExpressionType::SIMPLE_FUNCTION;
    }//end isFunction()

    /**
     * Tests if token type is unspecified.
     *
     * @return bool
     */
    public function isUnspecified(): bool
    {
        return ($this->tokenType === false);
    }//end isUnspecified()

    /**
     * Tests if token type is variable
     *
     * @return bool
     */
    public function isVariable(): bool
    {
        return $this->tokenType === ExpressionType::GLOBAL_VARIABLE
        || $this->tokenType === ExpressionType::LOCAL_VARIABLE
        || $this->tokenType === ExpressionType::USER_VARIABLE;
    }//end isVariable()

    /**
     * Tests if token type is aggregate function.
     *
     * @return bool
     */
    public function isAggregateFunction(): bool
    {
        return $this->tokenType === ExpressionType::AGGREGATE_FUNCTION;
    }//end isAggregateFunction()

    /**
     * Tests if token type is custom function.
     *
     * @return bool
     */
    public function isCustomFunction(): bool
    {
        return $this->tokenType === ExpressionType::CUSTOM_FUNCTION;
    }//end isCustomFunction()

    /**
     * Tests if token type is column reference.
     *
     * @return bool
     */
    public function isColumnReference(): bool
    {
        return $this->tokenType === ExpressionType::COLREF;
    }//end isColumnReference()

    /**
     * Tests if token type is constant
     *
     * @return bool
     */
    public function isConstant(): bool
    {
        return $this->tokenType === ExpressionType::CONSTANT;
    }//end isConstant()

    /**
     * Test if token type is sign
     *
     * @return bool
     */
    public function isSign(): bool
    {
        return $this->tokenType === ExpressionType::SIGN;
    }//end isSign()

    /**
     * Test if token type is subquery.
     *
     * @return bool
     */
    public function isSubQuery()
    {
        return $this->tokenType === ExpressionType::SUBQUERY;
    }//end isSubQuery()

    /**
     * Revoke quotation
     *
     * @param string                          $token   The token.
     * @param \Pipfrosch\PHPSQLParser\Options $options The options.
     *
     * @return array
     */
    private function revokeQuotation(string $token, \Pipfrosch\PHPSQLParser\Options $options): array
    {
        $defProc = new \Pipfrosch\PHPSQLParser\Processors\DefaultProcessor($options);
        return $defProc->revokeQuotation($token);
    }//end revokeQuotation()

    /**
     * To Array
     *
     * @return array
     */
    public function toArray(): array
    {
        $result = array();
        $result['expr_type'] = $this->tokenType;
        $result['base_expr'] = $this->token;
        if (!empty($this->noQuotes)) {
            $result['no_quotes'] = $this->noQuotes;
        }
        $result['sub_tree'] = $this->subTree;
        if (! empty($this->delim)) {
            $result['delim'] = $this->delim;
        }
        return $result;
    }//end toArray()
}//end class

?>
