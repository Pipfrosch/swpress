<?php
declare(strict_types=1);

/**
 * ExplainProcessor.php
 *
 * This file implements the processor for the EXPLAIN statements.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Processors;

/**
 * This class processes the EXPLAIN statements.
 */
class ExplainProcessor extends AbstractProcessorArray
{
    /**
     * Checks if statement is the needle
     *
     * @param array  $keys   The keys.
     * @param string $needle The needle.
     *
     * @return bool
     */
    protected function isStatement(array $keys, string $needle = "EXPLAIN"): bool
    {
        $pos = array_search($needle, $keys);
        if (is_int($pos) && isset($keys[$pos + 1])) {
            return in_array($keys[$pos + 1], array('SELECT', 'DELETE', 'INSERT', 'REPLACE', 'UPDATE'), true);
        }
        return false;
    }//end isStatement()

    // TODO: refactor that function
    /**
     * Process function
     *
     * @param array $tokens The query tokens.
     * @param array $keys   Array of keys.
     *
     * @return array
     */
    public function process(array $tokens, array $keys = array()): array
    {
        $base_expr = "";
        $expr = array();
        $currCategory = "";
        if ($this->isStatement($keys)) {
            foreach ($tokens as $token) {
                $trim = trim($token);
                $base_expr .= $token;
                if ($trim === '') {
                    continue;
                }
                $upper = strtoupper($trim);
                switch ($upper) {
                    case 'EXTENDED':
                    case 'PARTITIONS':
                        return array(
                            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::RESERVED,
                            'base_expr' => $token
                        );
                        break;
                    case 'FORMAT':
                        if ($currCategory === '') {
                            $currCategory = $upper;
                            $expr[] = array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::RESERVED,
                                'base_expr' => $trim
                            );
                        }
                        // else?
                        break;
                    case '=':
                        if ($currCategory === 'FORMAT') {
                            $expr[] = array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::OPERATOR,
                                'base_expr' => $trim
                            );
                        }
                        // else?
                        break;
                    case 'TRADITIONAL':
                    case 'JSON':
                        if ($currCategory === 'FORMAT') {
                            $expr[] = array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::RESERVED,
                                'base_expr' => $trim
                            );
                            return array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::EXPRESSION,
                                'base_expr' => trim($base_expr),
                                'sub_tree' => $expr
                            );
                        }
                        // else?
                        break;
                    default:
                        // ignore the other stuff
                        break;
                }
            }
            //do not return null
            //return empty($expr) ? null : $expr;
            return $expr;
        }
        foreach ($tokens as $token) {
            $trim = trim($token);
            if ($trim === '') {
                continue;
            }
            switch ($currCategory) {
                case 'TABLENAME':
                    $currCategory = 'WILD';
                    $expr[] = array(
                        'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::COLREF,
                        'base_expr' => $trim,
                        'no_quotes' => $this->revokeQuotation($trim)
                    );
                    break;
                case '':
                    $currCategory = 'TABLENAME';
                    $expr[] = array(
                        'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::TABLE,
                        'table' => $trim,
                        'no_quotes' => $this->revokeQuotation($trim),
                        'alias' => false,
                        'base_expr' => $trim
                    );
                    break;
                default:
                    break;
            }
        }
        //do not return null
        //return empty($expr) ? null : $expr;
        return $expr;
    }//end process()
}//class

?>
