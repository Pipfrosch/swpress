<?php
declare(strict_types=1);

/**
 * AbstractProcessor.php
 *
 * This file implements an abstract processor, which implements some helper functions.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Processors;

/**
 * This class contains some general functions for a processor.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 *
 */
abstract class AbstractProcessor
{
    /**
     * @var null|\Pipfrosch\PHPSQLParser\Options
     */
    protected $options;

    /**
     * AbstractProcessor constructor.
     *
     * @param \Pipfrosch\PHPSQLParser\Options $options The options object.
     */
    public function __construct(\Pipfrosch\PHPSQLParser\Options $options = null)
    {
        $this->options = $options;
    }//end __construct()

    /**
     * This function implements the main functionality of a processor class.
     * Always use default values for additional parameters within overridden functions.
     *
     * @param array|string $tokens The parsed query as tokens.
     *
     * @return array
     */
    abstract public function process($tokens): array;

    /**
     * This function splits up a SQL statement into easy to "parse"
     * tokens for the SQL processor
     *
     * @param string $sql The SQL query string.
     *
     * @return array
     */
    public function splitSQLIntoTokens(string $sql): array
    {
        $lexer = new \Pipfrosch\PHPSQLParser\Lexer\PHPSQLLexer();
        return $lexer->split($sql);
    }//end splitSQLIntoTokens()

    /**
     * Revokes the quoting characters from an expression
     * Possibibilies:
     *   `a`
     *   'a'
     *   "a"
     *   `a`.`b`
     *   `a.b`
     *   a.`b`
     *   `a`.b
     * It is also possible to have escaped quoting characters
     * within an expression part:
     *   `a``b` => a`b
     * And you can use whitespace between the parts:
     *   a  .  `b` => [a,b]
     *
     * @param string $sql The SQL string.
     *
     * @return array
     */
    protected function revokeQuotation(string $sql): array
    {
        $tmp = trim($sql);
        $result = array();

        $quote = false;
        $start = 0;
        $i = 0;
        $len = strlen($tmp);

        while ($i < $len) {
            $char = $tmp[$i];
            switch ($char) {
                case '`':
                case '\'':
                case '"':
                    if ($quote === false) {
                        // start
                        $quote = $char;
                        $start = $i + 1;
                        break;
                    }
                    if ($quote !== $char) {
                        break;
                    }
                    if (isset($tmp[$i + 1]) && ($quote === $tmp[$i + 1])) {
                        // escaped
                        $i++;
                        break;
                    }
                    // end
                    $char = substr($tmp, $start, $i - $start);
                    $result[] = str_replace($quote . $quote, $quote, $char);
                    $start = $i + 1;
                    $quote = false;
                    break;
                case '.':
                    if ($quote === false) {
                        // we have found a separator
                        $char = trim(substr($tmp, $start, $i - $start));
                        if ($char !== '') {
                            $result[] = $char;
                        }
                        $start = $i + 1;
                    }
                    break;
                default:
                // ignore
                    break;
            }
            $i++;
        }
        if ($quote === false && ($start < $len)) {
            $char = trim(substr($tmp, $start, $i - $start));
            if ($char !== '') {
                $result[] = $char;
            }
        }
        return array('delim' => (count($result) === 1 ? false : '.'), 'parts' => $result);
    }//end revokeQuotation()

    /**
     * This method removes parenthesis from start of the given string.
     * It removes also the associated closing parenthesis.
     *
     * @param string $token The token.
     *
     * @return string
     */
    protected function removeParenthesisFromStart(string $token): string
    {
        $parenthesisRemoved = 0;
        $trim = trim($token);
        if ($trim !== '' && $trim[0] === '(') { // remove only one parenthesis pair now!
            $parenthesisRemoved++;
            $trim[0] = ' ';
            $trim = trim($trim);
        }
        $parenthesis = $parenthesisRemoved;
        $i = 0;
        $string = 0;
        // Whether a string was opened or not, and with which character it was open (' or ")
        $stringOpened = '';
        while ($i < strlen($trim)) {
            if ($trim[$i] === "\\") {
                $i += 2; // an escape character, the next character is irrelevant
                continue;
            }
            if ($trim[$i] === "'") {
                if ($stringOpened === '') {
                    $stringOpened = "'";
                } elseif ($stringOpened === "'") {
                    $stringOpened = '';
                }
            }
            if ($trim[$i] === '"') {
                if ($stringOpened === '') {
                    $stringOpened = '"';
                } elseif ($stringOpened === '"') {
                    $stringOpened = '';
                }
            }
            if (($stringOpened === '') && ($trim[$i] === '(')) {
                $parenthesis++;
            }
            if (($stringOpened === '') && ($trim[$i] === ')')) {
                if ($parenthesis == $parenthesisRemoved) {
                    $trim[$i] = ' ';
                    $parenthesisRemoved--;
                }
                $parenthesis--;
            }
            $i++;
        }
        return trim($trim);
    }//end removeParenthesisFromStart()

    /**
     * Get Variable Type
     *
     * @param string $expression The expression.
     *
     * @return string
     */
    protected function getVariableType(string $expression): string
    {
        // $expression must contain only upper-case characters
        if ($expression[1] !== '@') {
            return \Pipfrosch\PHPSQLParser\Utils\ExpressionType::USER_VARIABLE;
        }
        $pos = strpos($expression, '.', 2);
        if ($pos === false) {
            $type = 'DEFAULT';
        } else {
            $type = substr($expression, 2, $pos);
        }
        switch ($type) {
            case 'GLOBAL':
                $type = \Pipfrosch\PHPSQLParser\Utils\ExpressionType::GLOBAL_VARIABLE;
                break;
            case 'LOCAL':
                $type = \Pipfrosch\PHPSQLParser\Utils\ExpressionType::LOCAL_VARIABLE;
                break;
            case 'SESSION':
            default:
                $type = \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SESSION_VARIABLE;
                break;
        }
        return $type;
    }//end getVariableType()

    /**
     * Check if token is a comma
     *
     * @param string $token The token.
     *
     * @return bool
     */
    protected function isCommaToken(string $token): bool
    {
        return (trim($token) === ',');
    }//end isCommaToken()

    /**
     * Check it token is whitespace
     *
     * @param string $token The token.
     *
     * @return bool
     */
    protected function isWhitespaceToken(string $token): bool
    {
        return (trim($token) === '');
    }//end isWhitespaceToken()

    /**
     * Check if token is comment
     *
     * @param string $token The token.
     *
     * @return bool
     */
    protected function isCommentToken(string $token): bool
    {
        return isset($token[0]) && isset($token[1])
                && (($token[0] === '-' && $token[1] === '-') || ($token[0] === '/' && $token[1] === '*'));
    }//end isCommentToken()

    /**
     * Check if out is a column reference
     *
     * @param array $out The out to check.
     *
     * @return bool
     */
    protected function isColumnReference(array $out): bool
    {
        return (isset($out['expr_type']) && $out['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::COLREF);
    }//end isColumnReference()

    /**
     * Check if out is reserved
     *
     * @param array $out The out to check.
     *
     * @return bool
     */
    protected function isReserved(array $out): bool
    {
        return (isset($out['expr_type'])
        && $out['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::RESERVED);
    }//end isReserved()

    /**
     * Check if out is constant
     *
     * @param array $out The out to check.
     *
     * @return bool
     */
    protected function isConstant(array $out): bool
    {
        return (isset($out['expr_type'])
        && $out['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::CONSTANT);
    }//end isConstant()

    /**
     * Check if out is aggregate function
     *
     * @param array $out The out to check.
     *
     * @return bool
     */
    protected function isAggregateFunction(array $out): bool
    {
        return (isset($out['expr_type'])
        && $out['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::AGGREGATE_FUNCTION);
    }//end isAggregateFunction()

    /**
     * Check if out is custom function
     *
     * @param array $out The out to check.
     *
     * @return bool
     */
    protected function isCustomFunction(array $out): bool
    {
        return (isset($out['expr_type'])
        && $out['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::CUSTOM_FUNCTION);
    }//end isCustomFunction()

    /**
     * Check if out is a function
     *
     * @param array $out The out to check.
     *
     * @return bool
     */
    protected function isFunction(array $out): bool
    {
        return (isset($out['expr_type'])
        && $out['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SIMPLE_FUNCTION);
    }//end isFunction()

    /**
     * Check if out is an expression
     *
     * @param array $out The out to check.
     *
     * @return bool
     */
    protected function isExpression(array $out): bool
    {
        return (isset($out['expr_type'])
        && $out['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::EXPRESSION);
    }//end isExpression()

    /**
     * Check if out is a bracket expression
     *
     * @param array $out The out to check.
     *
     * @return bool
     */
    protected function isBracketExpression(array $out): bool
    {
        return (isset($out['expr_type'])
        && $out['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::BRACKET_EXPRESSION);
    }//end isBracketExpression()

    /**
     * Check if out is a subquery
     *
     * @param array $out The out to check.
     *
     * @return bool
     */
    protected function isSubQuery(array $out): bool
    {
        return (isset($out['expr_type'])
        && $out['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBQUERY);
    }//end isSubQuery()

    /**
     * Check if out is comment
     *
     * @param array $out The out to check.
     *
     * @return bool
     */
    protected function isComment(array $out): bool
    {
        return (isset($out['expr_type'])
        && $out['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::COMMENT);
    }//end isComment()

    /**
     * Process comment
     *
     * @param string $expression The expression.
     *
     * @return array
     */
    public function processComment(string $expression): array
    {
        $result = array();
        $result['expr_type'] = \Pipfrosch\PHPSQLParser\Utils\ExpressionType::COMMENT;
        $result['value'] = $expression;
        return $result;
    }//end processComment()

    /**
     * Translates an array of objects into an associative array
     *
     * @param array $tokenList Array of token objects.
     *
     * @return array
     */
    public function toArray(array $tokenList): array
    {
        $expr = array();
        foreach ($tokenList as $token) {
            if ($token instanceof \Pipfrosch\PHPSQLParser\Utils\ExpressionToken) {
                $expr[] = $token->toArray();
            } else {
                $expr[] = $token;
            }
        }
        return $expr;
    }//end toArray()

    /**
     * Array insert after entry
     *
     * @param array $array The array.
     * @param mixed $key   The key to insert after.
     * @param mixed $entry The entry to make.
     *
     * @return array
     */
    protected function array_insert_after($array, $key, $entry): array
    {
        $idx = array_search($key, array_keys($array));
        if (is_int($idx)) {
            $array = array_slice($array, 0, $idx + 1, true) + $entry
                + array_slice($array, $idx + 1, count($array) - 1, true);
        }
        return $array;
    }//end array_insert_after()
}//end class

?>
