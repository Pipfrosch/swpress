<?php
declare(strict_types=1);

/**
 * HavingProcessor.php
 *
 * Parses the HAVING statements.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Processors;

/**
 * This class implements the processor for the HAVING statement.
 * You can overwrite all functions to achieve another handling.
 */
class HavingProcessor extends ExpressionListProcessor
{
/**
 * Code block below commented out by Alice Wonder <paypal@domblogger.net> to avoid dependence on
 *   on the \Analog\Analog class as I do not see this being needed for the PHPSQLParser use case
 *   in SWPress.
 *
 * TODO - look at class and see if it is of use to SWPress
 *
 *   // helper function to find an example for issue 10 on GitHub.com
 *   protected function logError($sql) {
 *       \Analog\Analog::log(
 *           "The alias clause of a colref is empty, this should not occur.",
 *           \Analog\Analog::ALERT
 *       );
 *       \Analog\Analog::log(
 *           "Please create an issue on GitHub or GoogleCode with your SQL statement.",
 *           \Analog\Analog::ALERT
 *       );
 *       \Analog\Analog::log(print_r($sql, true), Analog::ALERT);
 *   }
 */

    /**
     * Process tokens.
     *
     * @param array $tokens The tokens to process.
     * @param array $select The select array.
     *
     * @return array
     */
    public function process(array $tokens, array $select = array()): array
    {
        $parsed = parent::process($tokens);
        foreach ($parsed as $k => $v) {
            if ($v['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::COLREF) {
                foreach ($select as $clause) {
                    if (!isset($clause['alias'])) {
                    //$this->logError($select);
                        continue;
                    }
                    if (!$clause['alias']) {
                        continue;
                    }
                    if ($clause['alias']['no_quotes'] === $v['no_quotes']) {
                        $parsed[$k]['expr_type'] = \Pipfrosch\PHPSQLParser\Utils\ExpressionType::ALIAS;
                        break;
                    }
                }
            }
        }
        return $parsed;
    }//end process()
}//end class

?>
