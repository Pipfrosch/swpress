<?php
declare(strict_types=1);

/**
 * SubpartitionDefinitionProcessor.php
 *
 * This file implements the processor for the SUBPARTITION statements
 * within CREATE TABLE.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Processors;

/**
 * This class processes the SUBPARTITION statements within CREATE TABLE.
 */
class SubpartitionDefinitionProcessor extends AbstractProcessorArray
{
    /**
     * Get reserved type
     *
     * @param string $token The token.
     *
     * @return array
     */
    protected function getReservedType(string $token): array
    {
        return array(
            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::RESERVED,
            'base_expr' => $token
        );
    }//end getReservedType()

    /**
     * Get constant type
     *
     * @param string $token The token.
     *
     * @return array
     */
    protected function getConstantType(string $token): array
    {
        return array(
            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::CONSTANT,
            'base_expr' => $token
        );
    }//end getConstantType()

    /**
     * Get operator type
     *
     * @param string $token The token.
     *
     * @return array
     */
    protected function getOperatorType(string $token): array
    {
        return array(
            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::OPERATOR,
            'base_expr' => $token
        );
    }//end getOperatorType()

    /**
     * Get bracket expression type
     *
     * @param string $token The token.
     *
     * @return array
     */
    protected function getBracketExpressionType(string $token): array
    {
        return array(
            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::BRACKET_EXPRESSION,
            'base_expr' => $token,
            'sub_tree' => false
        );
    }//end getBracketExpressionType()

    /**
     * Process tokens
     *
     * @psalm-suppress TypeDoesNotContainType
     *
     * @param array $tokens The tokens to process.
     *
     * @return array
     */
    public function process(array $tokens): array
    {
        $result = array();
        $prevCategory = '';
        $currCategory = '';
        $parsed = array();
        $expr = array();
        $base_expr = '';
        $skip = 0;
        foreach ($tokens as $tokenKey => $token) {
            $trim = trim($token);
            $base_expr .= $token;
            if ($skip > 0) {
                $skip--;
                continue;
            }
            if ($skip < 0) {
                break;
            }
            if ($trim === '') {
                continue;
            }
            $upper = strtoupper($trim);
            switch ($upper) {
                case 'SUBPARTITION':
                    if ($currCategory === '') {
                        $expr[] = $this->getReservedType($trim);
                        $parsed = array(
                            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_DEF,
                            'base_expr' => trim($base_expr),
                            'sub_tree' => false
                        );
                        $currCategory = $upper;
                        continue 2;
                    }//else ?
                    break;
                case 'COMMENT':
                    if ($prevCategory === 'SUBPARTITION') {
                        $expr[] = array(
                            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_COMMENT,
                            'base_expr' => false,
                            'sub_tree' => false,
                            'storage' => substr($base_expr, 0, -strlen($token))
                        );
                        $parsed['sub_tree'] = $expr;
                        $base_expr = $token;
                        $expr = array($this->getReservedType($trim));
                        $currCategory = $upper;
                        continue 2;
                    }//else ?
                    break;
                case 'STORAGE':
                    if ($prevCategory === 'SUBPARTITION') {
                        // followed by ENGINE
                        $expr[] = array(
                            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::ENGINE,
                            'base_expr' => false,
                            'sub_tree' => false,
                            'storage' => substr($base_expr, 0, -strlen($token))
                        );
                        $parsed['sub_tree'] = $expr;
                        $base_expr = $token;
                        $expr = array($this->getReservedType($trim));
                        $currCategory = $upper;
                        continue 2;
                    }//else ?
                    break;
                case 'ENGINE':
                    if ($currCategory === 'STORAGE') {
                        $expr[] = $this->getReservedType($trim);
                        $currCategory = $upper;
                        continue 2;
                    }
                    if ($prevCategory === 'SUBPARTITION') {
                        $expr[] = array(
                            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::ENGINE,
                            'base_expr' => false,
                            'sub_tree' => false,
                            'storage' => substr($base_expr, 0, -strlen($token))
                        );
                        $parsed['sub_tree'] = $expr;
                        $base_expr = $token;
                        $expr = array($this->getReservedType($trim));
                        $currCategory = $upper;
                        continue 2;
                    }//else ?
                    break;
                case '=':
                    if (in_array($currCategory, array('ENGINE', 'COMMENT', 'DIRECTORY', 'MAX_ROWS', 'MIN_ROWS'))) {
                        $expr[] = $this->getOperatorType($trim);
                        continue 2;
                    }//else ?
                    break;
                case ',':
                    if ($prevCategory === 'SUBPARTITION' && $currCategory === '') {
                        // it separates the subpartition-definitions
                        $result[] = $parsed;
                        $parsed = array();
                        $base_expr = '';
                        $expr = array();
                    }
                    break;
                case 'DATA':
                case 'INDEX':
                    if ($prevCategory === 'SUBPARTITION') {
                        // followed by DIRECTORY
                        $expr[] = array(
                            'expr_type' => constant(
                                '\Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_' . $upper . '_DIR'
                            ),
                            'base_expr' => false,
                            'sub_tree' => false,
                            'storage' => substr($base_expr, 0, -strlen($token))
                        );
                        $parsed['sub_tree'] = $expr;
                        $base_expr = $token;
                        $expr = array($this->getReservedType($trim));
                        $currCategory = $upper;
                        continue 2;
                    }//else ?
                    break;
                case 'DIRECTORY':
                    if ($currCategory === 'DATA' || $currCategory === 'INDEX') {
                        $expr[] = $this->getReservedType($trim);
                        $currCategory = $upper;
                        continue 2;
                    }//else ?
                    break;
                case 'MAX_ROWS':
                case 'MIN_ROWS':
                    if ($prevCategory === 'SUBPARTITION') {
                        $expr[] = array(
                            'expr_type' => constant(
                                '\Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_' . $upper
                            ),
                            'base_expr' => false,
                            'sub_tree' => false,
                            'storage' => substr($base_expr, 0, -strlen($token))
                        );
                        $parsed['sub_tree'] = $expr;
                        $base_expr = $token;
                        $expr = array($this->getReservedType($trim));
                        $currCategory = $upper;
                        continue 2;
                    }//else ?
                    break;
                default:
                    switch ($currCategory) {
                        case 'MIN_ROWS':
                        case 'MAX_ROWS':
                        case 'ENGINE':
                        case 'DIRECTORY':
                        case 'COMMENT':
                            $expr[] = $this->getConstantType($trim);
                            if (! isset($parsed['sub_tree']) || ! is_array($parsed['sub_tree'])) {
                                $parsed['sub_tree'] = array();
                            }
                            $last = array_pop($parsed['sub_tree']);
                            $last['sub_tree'] = $expr;
                            $last['base_expr'] = trim($base_expr);
                            $base_expr = $last['storage'] . $base_expr;
                            unset($last['storage']);
                            $parsed['sub_tree'][] = $last;
                            $parsed['base_expr'] = trim($base_expr);
                            $expr = $parsed['sub_tree'];
                            unset($last);
                            $currCategory = $prevCategory;
                            break;
                        case 'SUBPARTITION':
                            // that is the subpartition name
                            $last = array_pop($expr);
                            $last['name'] = $trim;
                            $expr[] = $last;
                            $expr[] = $this->getConstantType($trim);
                            $parsed['sub_tree'] = $expr;
                            $parsed['base_expr'] = trim($base_expr);
                            break;
                        default:
                            break;
                    }
                    break;
            }
            $prevCategory = $currCategory;
            $currCategory = '';
        }
        $result[] = $parsed;
        return $result;
    }//end process()
}//end class

?>
