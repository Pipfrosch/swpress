<?php
declare(strict_types=1);

/**
 * RecordProcessor.php
 *
 * This file implements a processor, which processes records of data
 * for an INSERT statement.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Processors;

/**
 * This class processes records of an INSERT statement.
 */
class RecordProcessor extends AbstractProcessorString
{
    /**
     * Process Expression List
     *
     * @param array $unparsed The unparsed query.
     *
     * @return array
     */
    protected function processExpressionList(array $unparsed): array
    {
        $processor = new ExpressionListProcessor($this->options);
        return $processor->process($unparsed);
    }//end processExpressionList()

    /**
     * Process unparsed query
     *
     * @param string $unparsed The unparsed query.
     *
     * @return array
     */
    public function process(string $unparsed): array
    {
        $unparsed = $this->removeParenthesisFromStart($unparsed);
        $values = $this->splitSQLIntoTokens($unparsed);
        foreach ($values as $k => $v) {
            if ($this->isCommaToken($v)) {
                $values[$k] = "";
            }
        }
        return $this->processExpressionList($values);
    }//end process()
}//end class

?>
