<?php
declare(strict_types=1);

/**
 * InsertProcessor.php
 *
 * This file implements the processor for the INSERT statements.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Processors;

/**
 * This class processes the INSERT statements.
 */
class InsertProcessor extends AbstractProcessorArray
{
    /**
     * Process Options
     *
     * @param array $tokenList The token list.
     *
     * @return array
     */
    protected function processOptions(array $tokenList): array
    {
        if (! isset($tokenList['OPTIONS'])) {
            return array();
        }
        $result = array();
        foreach ($tokenList['OPTIONS'] as $token) {
            $result[] = array(
                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::RESERVED,
                'base_expr' => trim($token)
            );
        }
        return $result;
    }//end processOptions()

    /**
     * Process keyword
     *
     * @param string $keyword   The keyword.
     * @param array  $tokenList The token array.
     *
     * @return array
     */
    protected function processKeyword(string $keyword, array $tokenList): array
    {
        if (! isset($tokenList[$keyword])) {
            return array(
                '',
                false,
                array()
            );
        }
        $table = '';
        $cols = false;
        $result = array();
        foreach ($tokenList[$keyword] as $token) {
            $trim = trim($token);
            if ($trim === '') {
                continue;
            }
            $upper = strtoupper($trim);
            switch ($upper) {
                case 'INTO':
                    $result[] = array(
                        'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::RESERVED,
                        'base_expr' => $trim
                    );
                    continue;
                case 'INSERT':
                case 'REPLACE':
                    continue;
                default:
                    if ($table === '') {
                        $table = $trim;
                        continue;
                    }
                    if ($cols === false) {
                        $cols = $trim;
                    }
                    break;
            }
        }
        return array(
            $table,
            $cols,
            $result
        );
    }//end processKeyword()

    /**
     * Process Columns
     *
     * @param string $cols Columns to process.
     *
     * @return array
     */
    protected function processColumns(string $cols): array
    {
        if (substr($cols, 0, 1) === '(' && substr($cols, -1) === ')') {
            $parsed = array(
                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::BRACKET_EXPRESSION,
                'base_expr' => $cols,
                'sub_tree' => false
            );
        } else {
            $parsed = array();
        }
        $cols = $this->removeParenthesisFromStart($cols);
        if (stripos($cols, 'SELECT') === 0) {
            $processor = new DefaultProcessor($this->options);
            $parsed['sub_tree'] = array(
                array(
                    'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::QUERY,
                    'base_expr' => $cols,
                    'sub_tree' => $processor->process($cols)
                )
            );
        } else {
            $processor = new ColumnListProcessor($this->options);
            $parsed['sub_tree'] = $processor->process($cols);
            $parsed['expr_type'] = \Pipfrosch\PHPSQLParser\Utils\ExpressionType::COLUMN_LIST;
        }
        return $parsed;
    }//end processColumns()

    /**
     * Process Token List
     *
     * @param array  $tokenList The token list.
     * @param string $token_category The token category.
     *
     * @return array
     */
    public function process(array $tokenList, string $token_category = 'INSERT'): array
    {
        $table = '';
        $cols = false;
        $comments = array();
        foreach ($tokenList as $key => &$token) {
            if ($key == 'VALUES') {
                continue;
            }
            foreach ($token as &$value) {
                if ($this->isCommentToken($value)) {
                     $comments[] = parent::processComment($value);
                     $value = '';
                }
            }
        }
        $parsed = $this->processOptions($tokenList);
        unset($tokenList['OPTIONS']);
        list($table, $cols, $key) = $this->processKeyword('INTO', $tokenList);
        $parsed = array_merge($parsed, $key);
        unset($tokenList['INTO']);
        if ($table === '' && in_array($token_category, array('INSERT', 'REPLACE'))) {
            list($table, $cols, $key) = $this->processKeyword($token_category, $tokenList);
        }
        $parsed[] = array(
            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::TABLE,
            'table' => $table,
            'no_quotes' => $this->revokeQuotation($table),
            'alias' => false,
            'base_expr' => $table
        );
        if ($cols !== false) {
            $cols = $this->processColumns($cols);
            $parsed[] = $cols;
        }
        $parsed = array_merge($parsed, $comments);
        $tokenList[$token_category] = $parsed;
        return $tokenList;
    }//end process()
}//end class

?>
