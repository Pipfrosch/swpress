<?php
declare(strict_types=1);

/**
 * TableProcessor.php
 *
 * This file implements the processor for the TABLE statements.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Processors;

/**
 * This class processes the TABLE statements.
 */
class TableProcessor extends AbstractProcessorArray
{
    /**
     * Get reserved type
     *
     * @param string $token The token.
     *
     * @return array
     */
    protected function getReservedType(string $token): array
    {
        return array(
            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::RESERVED,
            'base_expr' => $token
        );
    }//end getReservedType()

    /**
     * Get constant type
     *
     * @param string $token The token.
     *
     * @return array
     */
    protected function getConstantType(string $token): array
    {
        return array(
            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::CONSTANT,
            'base_expr' => $token
        );
    }//end getConstantType()

    /**
     * Get operator type
     *
     * @param string $token The token.
     *
     * @return array
     */
    protected function getOperatorType(string $token): array
    {
        return array(
            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::OPERATOR,
            'base_expr' => $token
        );
    }//end getOperatorType()

    /**
     * Process partition options
     *
     * @param array $tokens The tokens array.
     *
     * @return array
     */
    protected function processPartitionOptions(array $tokens): array
    {
        $processor = new PartitionOptionsProcessor($this->options);
        return $processor->process($tokens);
    }//end processPartitionOptions()

    /**
     * Process create definition
     *
     * @param array $tokens The tokens array.
     *
     * @return array
     */
    protected function processCreateDefinition(array $tokens): array
    {
        $processor = new CreateDefinitionProcessor($this->options);
        return $processor->process($tokens);
    }//end processCreateDefinition()

    /**
     * Clear
     *
     * @param array  $expr      Expression array.
     * @param string $base_expr Base expression string.
     * @param string $category  Category string.
     *
     * @return void
     */
    protected function clear(array &$expr, string &$base_expr, string &$category): void
    {
        $expr = array();
        $base_expr = '';
        $category = 'CREATE_DEF';
    }//end clear()

    /**
     * Process tokens
     *
     * @psalm-suppress TypeDoesNotContainType
     *
     * @param array $tokens The tokens array.
     *
     * @return array
     */
    public function process(array $tokens): array
    {
        $currCategory = 'TABLE_NAME';
        $result = array(
            'base_expr'     => false,
            'name'          => false,
            'no_quotes'     => false,
            'create-def'    => false,
            'options'       => false,
            'like'          => false,
            'select-option' => false
        );
        $expr = array();
        $base_expr = '';
        $skip = 0;
        foreach ($tokens as $tokenKey => $token) {
            $trim = trim($token);
            $base_expr .= $token;
            if ($skip > 0) {
                $skip--;
                continue;
            }
            if ($skip < 0) {
                break;
            }
            if ($trim === '') {
                continue;
            }
            $upper = strtoupper($trim);
            switch ($upper) {
                case ',':
                    // it is possible to separate the table options with comma!
                    if (isset($prevCategory) && $prevCategory === 'CREATE_DEF') {
                        if (! isset($result['options']) || ! is_array($result['options'])) {
                            $result['options'] = array();
                        }
                        $last = array_pop($result['options']);
                        $last['delim'] = ',';
                        $result['options'][] = $last;
                        $base_expr = '';
                    }
                    continue 2;
                case 'UNION':
                    if (isset($prevCategory) && $prevCategory === 'CREATE_DEF') {
                        $expr[] = $this->getReservedType($trim);
                        $currCategory = 'UNION';
                        continue 2;
                    }
                    break;
                case 'LIKE':
                    // like without parenthesis
                    if (isset($prevCategory) && $prevCategory === 'TABLE_NAME') {
                        $currCategory = $upper;
                        continue 2;
                    }
                    break;
                case '=':
                    // the optional operator
                    if (isset($prevCategory) && $prevCategory === 'TABLE_OPTION') {
                        $expr[] = $this->getOperatorType($trim);
                        continue 2; // don't change the category
                    }
                    break;
                case 'CHARACTER':
                    if (isset($prevCategory) && $prevCategory === 'CREATE_DEF') {
                        $expr[] = $this->getReservedType($trim);
                        $currCategory = 'TABLE_OPTION';
                    }
                    if (isset($prevCategory) && $prevCategory === 'TABLE_OPTION') {
                        // add it to the previous DEFAULT
                        $expr[] = $this->getReservedType($trim);
                        continue 2;
                    }
                    break;
                case 'SET':
                case 'CHARSET':
                    if (isset($prevCategory) && $prevCategory === 'TABLE_OPTION') {
                        // add it to a previous CHARACTER
                        $expr[] = $this->getReservedType($trim);
                        $currCategory = 'CHARSET';
                        continue 2;
                    }
                    break;
                case 'COLLATE':
                    if (isset($prevCategory) && ($prevCategory === 'TABLE_OPTION' || $prevCategory === 'CREATE_DEF')) {
                        // add it to the previous DEFAULT
                        $expr[] = $this->getReservedType($trim);
                        $currCategory = 'COLLATE';
                        continue 2;
                    }
                    break;
                case 'DIRECTORY':
                    if ($currCategory === 'INDEX_DIRECTORY' || $currCategory === 'DATA_DIRECTORY') {
                        // after INDEX or DATA
                        $expr[] = $this->getReservedType($trim);
                        continue 2;
                    }
                    break;
                case 'INDEX':
                    if (isset($prevCategory) && $prevCategory === 'CREATE_DEF') {
                        $expr[] = $this->getReservedType($trim);
                        $currCategory = 'INDEX_DIRECTORY';
                        continue 2;
                    }
                    break;
                case 'DATA':
                    if (isset($prevCategory) && $prevCategory === 'CREATE_DEF') {
                        $expr[] = $this->getReservedType($trim);
                        $currCategory = 'DATA_DIRECTORY';
                        continue 2;
                    }
                    break;
                case 'INSERT_METHOD':
                case 'DELAY_KEY_WRITE':
                case 'ROW_FORMAT':
                case 'PASSWORD':
                case 'MAX_ROWS':
                case 'MIN_ROWS':
                case 'PACK_KEYS':
                case 'CHECKSUM':
                case 'COMMENT':
                case 'CONNECTION':
                case 'AUTO_INCREMENT':
                case 'AVG_ROW_LENGTH':
                case 'ENGINE':
                case 'TYPE':
                case 'STATS_AUTO_RECALC':
                case 'STATS_PERSISTENT':
                case 'KEY_BLOCK_SIZE':
                    if (isset($prevCategory) && $prevCategory === 'CREATE_DEF') {
                        $expr[] = $this->getReservedType($trim);
                        $currCategory = $prevCategory = 'TABLE_OPTION';
                        continue 2;
                    }
                    break;
                case 'DYNAMIC':
                case 'FIXED':
                case 'COMPRESSED':
                case 'REDUNDANT':
                case 'COMPACT':
                case 'NO':
                case 'FIRST':
                case 'LAST':
                case 'DEFAULT':
                    if (isset($prevCategory) && $prevCategory === 'CREATE_DEF') {
                        // DEFAULT before CHARACTER SET and COLLATE
                        $expr[] = $this->getReservedType($trim);
                        $currCategory = 'TABLE_OPTION';
                    }
                    if (isset($prevCategory) && $prevCategory === 'TABLE_OPTION') {
                        // all assignments with the keywords
                        $expr[] = $this->getReservedType($trim);
                        if (! isset($result['options']) || ! is_array($result['options'])) {
                            $result['options'] = array();
                        }
                        $result['options'][] = array(
                            'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::EXPRESSION,
                            'base_expr' => trim($base_expr),
                            'delim' => ' ',
                            'sub_tree' => $expr
                        );
                        $this->clear($expr, $base_expr, $currCategory);
                    }
                    break;
                case 'IGNORE':
                case 'REPLACE':
                    $expr[] = $this->getReservedType($trim);
                    $result['select-option'] = array(
                        'base_expr' => trim($base_expr),
                        'duplicates' => $trim,
                        'as' => false,
                        'sub_tree' => $expr
                    );
                    continue 2;
                case 'AS':
                    $expr[] = $this->getReservedType($trim);
                    if (! isset($result['select-option']) || ! is_array($result['select-option'])) {
                        $result['select-option'] = array();
                    }
                    if (!isset($result['select-option']['duplicates'])) {
                        $result['select-option']['duplicates'] = false;
                    }
                    $result['select-option']['as'] = true;
                    $result['select-option']['base_expr'] = trim($base_expr);
                    $result['select-option']['sub_tree'] = $expr;
                    continue 2;
                case 'PARTITION':
                    if (isset($prevCategory) && $prevCategory === 'CREATE_DEF') {
                        $part = $this->processPartitionOptions(
                            array_slice(
                                $tokens,
                                intval($tokenKey, 10) - 1,
                                null,
                                true
                            )
                        );
                        $skip = $part['last-parsed'] - $tokenKey;
                        $result['partition-options'] = $part['partition-options'];
                        continue 2;
                    }// else
                    break;
                default:
                    switch ($currCategory) {
                        case 'CHARSET':
                            // the charset name
                            $expr[] = $this->getConstantType($trim);
                            if (! isset($result['options']) || ! is_array($result['options'])) {
                                $result['options'] = array();
                            }
                            $result['options'][] = array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::CHARSET,
                                'base_expr' => trim($base_expr),
                                'delim' => ' ',
                                'sub_tree' => $expr
                            );
                            $this->clear($expr, $base_expr, $currCategory);
                            break;
                        case 'COLLATE':
                            // the collate name
                            $expr[] = $this->getConstantType($trim);
                            if (! isset($result['options']) || ! is_array($result['options'])) {
                                $result['options'] = array();
                            }
                            $result['options'][] = array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::COLLATE,
                                'base_expr' => trim($base_expr),
                                'delim' => ' ',
                                'sub_tree' => $expr
                            );
                            $this->clear($expr, $base_expr, $currCategory);
                            break;
                        case 'DATA_DIRECTORY':
                            // we have the directory name
                            $expr[] = $this->getConstantType($trim);
                            if (! isset($result['options']) || ! is_array($result['options'])) {
                                $result['options'] = array();
                            }
                            $result['options'][] = array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::DIRECTORY,
                                'kind' => 'DATA',
                                'base_expr' => trim($base_expr),
                                'delim' => ' ',
                                'sub_tree' => $expr
                            );
                            if (! isset($prevCategory)) {
                                // it's getting cleared anyway...
                                $prevCategory = '';
                            }
                            $this->clear($expr, $base_expr, $prevCategory);
                            continue 3;
                        case 'INDEX_DIRECTORY':
                            // we have the directory name
                            $expr[] = $this->getConstantType($trim);
                            if (! isset($result['options']) || ! is_array($result['options'])) {
                                $result['options'] = array();
                            }
                            $result['options'][] = array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::DIRECTORY,
                                'kind' => 'INDEX',
                                'base_expr' => trim($base_expr),
                                'delim' => ' ',
                                'sub_tree' => $expr
                            );
                            $this->clear($expr, $base_expr, $prevCategory);
                            continue 3;
                        case 'TABLE_NAME':
                            $result['base_expr'] = $result['name'] = $trim;
                            $result['no_quotes'] = $this->revokeQuotation($trim);
                            $this->clear($expr, $base_expr, $prevCategory);
                            break;
                        case 'LIKE':
                            $result['like'] = array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::TABLE,
                                'table' => $trim,
                                'base_expr' => $trim,
                                'no_quotes' => $this->revokeQuotation($trim)
                            );
                            if (! isset($prevCategory)) {
                                // it's getting cleared anyway...
                                $prevCategory = '';
                            }
                            $this->clear($expr, $base_expr, $currCategory);
                            break;
                        case '':
                            // after table name
                            if (isset($prevCategory)) {
                                /**
                                 * @psalm-suppress RedundantCondition
                                 */
                                if ($prevCategory === 'TABLE_NAME' && $upper[0] === '(' && substr($upper, -1) === ')') {
                                    $unparsed = $this->splitSQLIntoTokens($this->removeParenthesisFromStart($trim));
                                    $coldef = $this->processCreateDefinition($unparsed);
                                    $result['create-def'] = array(
                                        'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::BRACKET_EXPRESSION,
                                        'base_expr' => $base_expr,
                                        'sub_tree' => $coldef['create-def']
                                    );
                                    $expr = array();
                                    $base_expr = '';
                                    $currCategory = 'CREATE_DEF';
                                }
                            }
                            break;
                        case 'UNION':
                            // TODO: this token starts and ends with parenthesis
                            // and contains a list of table names (comma-separated)
                            // split the token and add the list as subtree
                            // we must change the DefaultProcessor
                            $unparsed = $this->splitSQLIntoTokens($this->removeParenthesisFromStart($trim));
                            $expr[] = array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::BRACKET_EXPRESSION,
                                'base_expr' => $trim,
                                'sub_tree' => '***TODO***'
                            );
                            if (! isset($result['options']) || ! is_array($result['options'])) {
                                $result['options'] = array();
                            }
                            $result['options'][] = array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::UNION,
                                'base_expr' => trim($base_expr),
                                'delim' => ' ',
                                'sub_tree' => $expr
                            );
                            $this->clear($expr, $base_expr, $currCategory);
                            break;
                        default:
                            // strings and numeric constants
                            $expr[] = $this->getConstantType($trim);
                            if (! isset($result['options']) || ! is_array($result['options'])) {
                                $result['options'] = array();
                            }
                            $result['options'][] = array(
                                'expr_type' => \Pipfrosch\PHPSQLParser\Utils\ExpressionType::EXPRESSION,
                                'base_expr' => trim($base_expr),
                                'delim' => ' ',
                                'sub_tree' => $expr
                            );
                            $this->clear($expr, $base_expr, $currCategory);
                            break;
                    }
                    break;
            }
            $prevCategory = $currCategory;
            $currCategory = '';
        }
        if ($result['like'] === false) {
            unset($result['like']);
        }
        if ($result['select-option'] === false) {
            unset($result['select-option']);
        }
        return $result;
    }//end process()
}//end class

?>
