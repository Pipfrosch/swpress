<?php
declare(strict_types=1);

/**
 * Alter Statement Builder
 *
 * PHPDoc comment added by Alice Wonder using info from other files.
 *
 * @package PHP-SQL-Parser
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link    https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * Alter Statement Builder
 */
class AlterStatementBuilder implements Builder
{
    /**
     * Subtree builder
     *
     * @param array $parsed The parsed array.
     *
     * @return string The query string.
     */
    protected function buildSubTree(array $parsed): string
    {
        $builder = new SubTreeBuilder();
        return $builder->build($parsed);
    }//end buildSubTree()

    /**
     * Alter builder
     *
     * @param array $parsed The parsed array.
     *
     * @return string The query string.
     */
    private function buildAlter(array $parsed): string
    {
        $builder = new AlterBuilder();
        return $builder->build($parsed);
    }//end buildAlter()

    /**
     * The Alter Statement Builder.
     *
     * @param array $parsed The parsed array.
     *
     * @return string The query string.
     */
    public function build(array $parsed): string
    {
        $alter = $parsed['ALTER'];
        $sql = $this->buildAlter($alter);
        return $sql;
    }//end build()
}//end class

?>
