<?php
declare(strict_types=1);

/**
 * BracketStatementBuilder.php
 *
 * Builds the parentheses around a statement.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for the parentheses around a statement.
 * You can overwrite all functions to achieve another handling.
 */
class BracketStatementBuilder implements Builder
{
    /**
     * Build Select Bracket Expression.
     *
     * @param array $parsed The parsed array.
     *
     * @return string The query.
     */
    protected function buildSelectBracketExpression(array $parsed): string
    {
        $builder = new SelectBracketExpressionBuilder();
        // Too many arguments
        //return $builder->build($parsed, " ");
        return $builder->build($parsed);
    }//end buildSelectBracketExpression()

    /**
     * Build select statement.
     *
     * @param array $parsed The parsed query array.
     *
     * @return string The query string.
     */
    protected function buildSelectStatement(array $parsed): string
    {
        $builder = new SelectStatementBuilder();
        return $builder->build($parsed);
    }//end buildSelectStatement()

    /**
     * Build the query.
     *
     * @param array $parsed The parsed query array.
     *
     * @return string The query string.
     */
    public function build(array $parsed): string
    {
        $sql = "";
        foreach ($parsed['BRACKET'] as $k => $v) {
            $len = strlen($sql);
            $sql .= $this->buildSelectBracketExpression($v);
            if ($len == strlen($sql)) {
                throw new \Pipfrosch\PHPSQLParser\Exceptions\UnableToCreateSQLException('BRACKET', $k, $v, 'expr_type');
            }
        }
        return trim($sql . " " . trim($this->buildSelectStatement($parsed)));
    }//end build()
}//end class

?>
