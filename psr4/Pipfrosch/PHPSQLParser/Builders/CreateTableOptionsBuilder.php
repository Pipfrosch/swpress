<?php
declare(strict_types=1);

/**
 * CreateTableOptionsBuilder.php
 *
 * Builds the table-options statement part of CREATE TABLE.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for the table-options statement part of CREATE TABLE.
 * You can overwrite all functions to achieve another handling.
 */
class CreateTableOptionsBuilder implements Builder
{
    /**
     * Build Expression
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildExpression(array $parsed): string
    {
        $builder = new SelectExpressionBuilder();
        return $builder->build($parsed);
    }//end buildExpression()

    /**
     * Build Character Set
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildCharacterSet(array $parsed): string
    {
        $builder = new CharacterSetBuilder();
        return $builder->build($parsed);
    }//end buildCharacterSet()

    /**
     * Build Collation
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildCollation(array $parsed): string
    {
        $builder = new CollationBuilder();
        return $builder->build($parsed);
    }//end buildCollation()

    /**
     * Returns a well-formatted delimiter string. If you don't need nice SQL,
     * you could simply return $parsed['delim'].
     *
     * @param array $parsed The part of the output array, which contains the current expression.
     *
     * @return string String which is added right after the expression.
     */
    protected function getDelimiter(array $parsed): string
    {
        return ($parsed['delim'] === false ? '' : (trim($parsed['delim']) . ' '));
    }//end getDelimiter()

    /**
     * Build
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    public function build(array $parsed): string
    {
        if (!isset($parsed['options']) || $parsed['options'] === false) {
            return "";
        }
        $options = $parsed['options'];
        $sql = "";
        foreach ($options as $k => $v) {
            $len = strlen($sql);
            $sql .= $this->buildExpression($v);
            $sql .= $this->buildCharacterSet($v);
            $sql .= $this->buildCollation($v);
            if ($len == strlen($sql)) {
                throw new \Pipfrosch\PHPSQLParser\Exceptions\UnableToCreateSQLException(
                    'CREATE TABLE options',
                    $k,
                    $v,
                    'expr_type'
                );
            }
            $sql .= $this->getDelimiter($v);
        }
        return " " . substr($sql, 0, -1);
    }//end build()
}//end class

?>
