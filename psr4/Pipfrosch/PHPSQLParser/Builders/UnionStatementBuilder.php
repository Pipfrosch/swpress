<?php
declare(strict_types=1);

/**
 * UnionAllStatementBuilder.php
 *
 * @package PHP-SQL-Parser
 * @author  George Schneeloch <george_schneeloch@hms.harvard.edu>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link    https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for the whole Union statement. You can overwrite
 * all functions to achieve another handling.
 */
class UnionStatementBuilder implements Builder
{
    /**
     * Build
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    public function build(array $parsed): string
    {
        $sql = '';
        $select_builder = new SelectStatementBuilder();
        $first = true;
        foreach ($parsed['UNION'] as $clause) {
            if (! $first) {
                $sql .= " UNION ";
            } else {
                $first = false;
            }
            $sql .= $select_builder->build($clause);
        }
        return $sql;
    }//end build()
}//end class

?>
