<?php
declare(strict_types=1);

/**
 * SubQueryBuilder.php
 *
 * Builds the statements for sub-queries.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for sub-queries.
 * You can overwrite all functions to achieve another handling.
 */
class SubQueryBuilder implements Builder
{
    /**
     * Build Ref Clause
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildRefClause(array $parsed): string
    {
        $builder = new RefClauseBuilder();
        return $builder->build($parsed);
    }//end buildRefClause()

    /**
     * Build Ref Type
     *
     * @param string $parsedString The parsed query string.
     *
     * @return string
     */
    protected function buildRefType(string $parsedString): string
    {
        $builder = new RefTypeBuilder();
        return $builder->build($parsedString);
    }//end buildRefType()

    /**
     * Build Join
     *
     * @param string $parsedString The parsed query string.
     *
     * @return string
     */
    protected function buildJoin(string $parsedString): string
    {
        $builder = new JoinBuilder();
        return $builder->build($parsedString);
    }//end buildJoin()

    /**
     * Build Alias
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildAlias(array $parsed): string
    {
        $builder = new AliasBuilder();
        return $builder->build($parsed);
    }//end buildAlias()

    /**
     * Build Select Statement
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildSelectStatement(array $parsed): string
    {
        $builder = new SelectStatementBuilder();
        return $builder->build($parsed);
    }//end buildSelectStatement()

    /**
     * Build
     *
     * @param array $parsed The parsed query.
     * @param int   $index  The index.
     *
     * @return string
     */
    public function build(array $parsed, int $index = 0): string
    {
        if ($parsed['expr_type'] !== \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBQUERY) {
            return '';
        }
        // TODO: should we add a numeric level (0) between sub_tree and SELECT?
        $sql = $this->buildSelectStatement($parsed['sub_tree']);
        $sql = '(' . $sql . ')';
        $sql .= $this->buildAlias($parsed);
        if ($index !== 0) {
            if (isset($parsed['join_type']) && is_string($parsed['join_type'])) {
                $sql = $this->buildJoin($parsed['join_type']) . $sql;
                if (isset($parsed['ref_type']) && is_string($parsed['ref_type'])) {
                    $sql .= $this->buildRefType($parsed['ref_type']);
                }
            }
            $sql .= $parsed['ref_clause'] === false ? '' : $this->buildRefClause($parsed['ref_clause']);
        }
        return $sql;
    }//end build()
}//end class

?>
