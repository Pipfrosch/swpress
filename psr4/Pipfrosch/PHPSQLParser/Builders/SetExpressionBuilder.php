<?php
declare(strict_types=1);

/**
 * SetExpressionBuilder.php
 *
 * Builds the SET part of the INSERT statement.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for the SET part of INSERT statement.
 * You can overwrite all functions to achieve another handling.
 */
class SetExpressionBuilder implements Builder
{
    /**
     * Build Col Ref
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildColRef(array $parsed): string
    {
        $builder = new ColumnReferenceBuilder();
        return $builder->build($parsed);
    }//end buildColRed()

    /**
     * Build Constant
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildConstant(array $parsed): string
    {
        $builder = new ConstantBuilder();
        return $builder->build($parsed);
    }//end buildConstant()

    /**
     * Build Operator
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildOperator(array $parsed): string
    {
        $builder = new OperatorBuilder();
        return $builder->build($parsed);
    }//end buildOperator()

    /**
     * Build Function
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildFunction(array $parsed): string
    {
        $builder = new FunctionBuilder();
        return $builder->build($parsed);
    }//end buildFunction()

    /**
     * Build Bracket Expression
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildBracketExpression(array $parsed): string
    {
        $builder = new SelectBracketExpressionBuilder();
        return $builder->build($parsed);
    }//end buildBracketExpression()

    /**
     * Build Sign
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildSign(array $parsed): string
    {
        $builder = new SignBuilder();
        return $builder->build($parsed);
    }//end buildSign()

    /**
     * Build
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    public function build(array $parsed): string
    {
        if ($parsed['expr_type'] !== \Pipfrosch\PHPSQLParser\Utils\ExpressionType::EXPRESSION) {
            return '';
        }
        $sql = '';
        foreach ($parsed['sub_tree'] as $k => $v) {
            $delim = ' ';
            $len = strlen($sql);
            $sql .= $this->buildColRef($v);
            $sql .= $this->buildConstant($v);
            $sql .= $this->buildOperator($v);
            $sql .= $this->buildFunction($v);
            $sql .= $this->buildBracketExpression($v);
            // we don't need whitespace between the sign and
            // the following part
            if ($this->buildSign($v) !== '') {
                $delim = '';
            }
            $sql .= $this->buildSign($v);
            if ($len == strlen($sql)) {
                throw new \Pipfrosch\PHPSQLParser\Exceptions\UnableToCreateSQLException(
                    'SET expression subtree',
                    $k,
                    $v,
                    'expr_type'
                );
            }
            $sql .= $delim;
        }
        $sql = substr($sql, 0, -1);
        return $sql;
    }//end build()
}//end class

?>
