<?php
declare(strict_types=1);

/**
 * ReplaceBuilder.php
 *
 * Builds the [REPLACE] statement part.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for the [REPLACE] statement parts.
 * You can overwrite all functions to achieve another handling.
 */
class ReplaceBuilder implements Builder
{
    /**
     * Build Table
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildTable(array $parsed): string
    {
        $builder = new TableBuilder();
        return $builder->build($parsed, 0);
    }//end buildTable()

    /**
     * Build SubQuery
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildSubQuery(array $parsed): string
    {
        $builder = new SubQueryBuilder();
        return $builder->build($parsed, 0);
    }//end buildSubQuery()

    /**
     * Build Reserved
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildReserved(array $parsed): string
    {
        $builder = new ReservedBuilder();
        return $builder->build($parsed);
    }//end buildReserved()

    /**
     * Build Bracket Expression
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildBracketExpression(array $parsed): string
    {
        $builder = new SelectBracketExpressionBuilder();
        return $builder->build($parsed);
    }//end buildBracketExpression()

    /**
     * Build Column List
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildColumnList(array $parsed): string
    {
        $builder = new ReplaceColumnListBuilder();
        //return $builder->build($parsed, 0);
        return $builder->build($parsed);
    }//end buildColumnList()

    /**
     * Build
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    public function build(array $parsed): string
    {
        $sql = '';
        foreach ($parsed as $k => $v) {
            $len = strlen($sql);
            $sql .= $this->buildTable($v);
            $sql .= $this->buildSubQuery($v);
            $sql .= $this->buildColumnList($v);
            $sql .= $this->buildReserved($v);
            $sql .= $this->buildBracketExpression($v);
            if ($len == strlen($sql)) {
                throw new \Pipfrosch\PHPSQLParser\Exceptions\UnableToCreateSQLException(
                    'REPLACE',
                    $k,
                    $v,
                    'expr_type'
                );
            }
            $sql .= " ";
        }
        return 'REPLACE ' . substr($sql, 0, -1);
    }//end build()
}//end class

?>
