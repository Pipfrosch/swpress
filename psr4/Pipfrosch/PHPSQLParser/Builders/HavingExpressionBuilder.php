<?php
declare(strict_types=1);

/**
 * HavingExpressionBuilder.php
 *
 * Builds expressions within the HAVING part.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for expressions within the HAVING part.
 * You can overwrite all functions to achieve another handling.
 */
class HavingExpressionBuilder extends WhereExpressionBuilder
{
    /**
     * Build Having Expression
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildHavingExpression(array $parsed): string
    {
        return $this->build($parsed);
    }//end buildHavingExpression()

    /**
     * Build Having Bracket Expression
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildHavingBracketExpression(array $parsed): string
    {
        $builder = new HavingBracketExpressionBuilder();
        return $builder->build($parsed);
    }//end buildHavingBracketExpression()

    /**
     * Build
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    public function build(array $parsed): string
    {
        if ($parsed['expr_type'] !== \Pipfrosch\PHPSQLParser\Utils\ExpressionType::EXPRESSION) {
            return "";
        }
        $sql = "";
        foreach ($parsed['sub_tree'] as $k => $v) {
            $len = strlen($sql);
            $sql .= $this->buildColRef($v);
            $sql .= $this->buildConstant($v);
            $sql .= $this->buildOperator($v);
            $sql .= $this->buildInList($v);
            $sql .= $this->buildFunction($v);
            $sql .= $this->buildHavingExpression($v);
            $sql .= $this->buildHavingBracketExpression($v);
            $sql .= $this->buildUserVariable($v);
            if ($len == strlen($sql)) {
                throw new \Pipfrosch\PHPSQLParser\Exceptions\UnableToCreateSQLException(
                    'HAVING expression subtree',
                    $k,
                    $v,
                    'expr_type'
                );
            }
            $sql .= " ";
        }
        $sql = substr($sql, 0, -1);
        return $sql;
    }//end build()
}//end class

?>
