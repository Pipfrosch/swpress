<?php
declare(strict_types=1);

/**
 * DropExpressionBuilder.php
 *
 * Builds the object list of a DROP statement.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for the object list of a DROP statement.
 * You can overwrite all functions to achieve another handling.
 */
class DropExpressionBuilder implements Builder
{
    /**
     * Build Table
     *
     * @param array $parsed The parsed query.
     * @param int   $index  The index.
     *
     * @return string
     */
    protected function buildTable(array $parsed, int $index): string
    {
        $builder = new TableBuilder();
        return $builder->build($parsed, $index);
    }//end buildTable()

    /**
     * Build Database
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildDatabase(array $parsed): string
    {
        $builder = new DatabaseBuilder();
        return $builder->build($parsed);
    }//end buildDatabase()

    /**
     * Build Schema
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildSchema(array $parsed): string
    {
        $builder = new SchemaBuilder();
        return $builder->build($parsed);
    }//end buildSchema()

    /**
     * Build Temporary Table
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildTemporaryTable(array $parsed): string
    {
        $builder = new TempTableBuilder();
        return $builder->build($parsed);
    }//end buildTemporaryTable()

    /**
     * Build View
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildView(array $parsed): string
    {
        $builder = new ViewBuilder();
        return $builder->build($parsed);
    }//end buildView()

    /**
     * Build
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    public function build(array $parsed): string
    {
        if ($parsed['expr_type'] !== \Pipfrosch\PHPSQLParser\Utils\ExpressionType::EXPRESSION) {
            return "";
        }
        $sql = '';
        foreach ($parsed['sub_tree'] as $k => $v) {
            $len = strlen($sql);
            $sql .= $this->buildTable($v, 0);
            $sql .= $this->buildView($v);
            $sql .= $this->buildSchema($v);
            $sql .= $this->buildDatabase($v);
            //$sql .= $this->buildTemporaryTable($v, 0);
            $sql .= $this->buildTemporaryTable($v);
            if ($len == strlen($sql)) {
                throw new \Pipfrosch\PHPSQLParser\Exceptions\UnableToCreateSQLException(
                    'DROP object-list subtree',
                    $k,
                    $v,
                    'expr_type'
                );
            }
            $sql .= ', ';
        }
        return substr($sql, 0, -2);
    }//end build()
}//end class

?>
