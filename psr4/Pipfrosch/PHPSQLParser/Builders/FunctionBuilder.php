<?php
declare(strict_types=1);

/**
 * FunctionBuilder.php
 *
 * Builds function statements.
 *
 * LICENSE:
 * Copyright (c) 2010-2015 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2015 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for function calls.
 * You can overwrite all functions to achieve another handling.
 */
class FunctionBuilder implements Builder
{
    /**
     * Build Alias
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildAlias(array $parsed): string
    {
        $builder = new AliasBuilder();
        return $builder->build($parsed);
    }//end buildAlias()

    /**
     * Build Col Ref
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildColRef(array $parsed): string
    {
        $builder = new ColumnReferenceBuilder();
        return $builder->build($parsed);
    }//end buildColRef()

    /**
     * BuildConstant
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildConstant(array $parsed): string
    {
        $builder = new ConstantBuilder();
        return $builder->build($parsed);
    }//end buildConstant()

    /**
     * Build Reserved
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildReserved(array $parsed): string
    {
        $builder = new ReservedBuilder();
        return $builder->build($parsed);
    }//end buildReserved()

    /**
     * Is Reserved
     *
     * @param array $parsed The parsed query.
     *
     * @return bool
     */
    protected function isReserved(array $parsed): bool
    {
        $builder = new ReservedBuilder();
        return $builder->isReserved($parsed);
    }//end isReserved()

    /**
     * Build Select Expression
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildSelectExpression(array $parsed): string
    {
        $builder = new SelectExpressionBuilder();
        return $builder->build($parsed);
    }//end buildSelectExpression()

    /**
     * Build Select Bracket Expression
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildSelectBracketExpression(array $parsed): string
    {
        $builder = new SelectBracketExpressionBuilder();
        return $builder->build($parsed);
    }//end buildSelectBracketExpression()

    /**
     * Build SubQuery
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildSubQuery(array $parsed): string
    {
        $builder = new SubQueryBuilder();
        return $builder->build($parsed);
    }//end buildSubQuery()

    /**
     * Build
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    public function build(array $parsed): string
    {
        if (($parsed['expr_type'] !== \Pipfrosch\PHPSQLParser\Utils\ExpressionType::AGGREGATE_FUNCTION)
        && ($parsed['expr_type'] !== \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SIMPLE_FUNCTION)
        && ($parsed['expr_type'] !== \Pipfrosch\PHPSQLParser\Utils\ExpressionType::CUSTOM_FUNCTION)) {
            return "";
        }
        if ($parsed['sub_tree'] === false) {
            return $parsed['base_expr'] . "()" . $this->buildAlias($parsed);
        }
        $sql = "";
        foreach ($parsed['sub_tree'] as $k => $v) {
            $len = strlen($sql);
            $sql .= $this->build($v);
            $sql .= $this->buildConstant($v);
            $sql .= $this->buildSubQuery($v);
            $sql .= $this->buildColRef($v);
            $sql .= $this->buildReserved($v);
            $sql .= $this->buildSelectBracketExpression($v);
            $sql .= $this->buildSelectExpression($v);
            if ($len === strlen($sql)) {
                throw new \Pipfrosch\PHPSQLParser\Exceptions\UnableToCreateSQLException(
                    'function subtree',
                    $k,
                    $v,
                    'expr_type'
                );
            }
            $sql .= ($this->isReserved($v) ? " " : ",");
        }
        return $parsed['base_expr'] . "(" . substr($sql, 0, -1) . ")" . $this->buildAlias($parsed);
    }//end build()
}//end class

?>
