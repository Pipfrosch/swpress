<?php
declare(strict_types=1);

/**
 * CreateBuilder.php
 *
 * Builds the CREATE statement
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for the [CREATE] part. You can overwrite
 * all functions to achieve another handling.
 */
class CreateBuilder implements Builder
{
    /**
     * Build Create Table
     *
     * @param array $parsed The parsed array.
     *
     * @return string
     */
    protected function buildCreateTable(array $parsed): string
    {
        $builder = new CreateTableBuilder();
        return $builder->build($parsed);
    }//end buildCreateTable()

    /**
     * Build Create Index
     *
     * @param array $parsed The parsed array.
     *
     * @return string
     */
    protected function buildCreateIndex(array $parsed): string
    {
        $builder = new CreateIndexBuilder();
        return $builder->build($parsed);
    }//end buildCreateIndex()

    /**
     * Build SubTree
     *
     * @param array $parsed The parsed array.
     *
     * @return string
     */
    protected function buildSubTree(array $parsed): string
    {
        $builder = new SubTreeBuilder();
        return $builder->build($parsed);
    }//end buildSubTree()

    /**
     * Build
     *
     * @param array $parsed The parsed array.
     *
     * @return string
     */
    public function build(array $parsed): string
    {
        $create = $parsed['CREATE'];
        $sql = $this->buildSubTree($create);
        if (($create['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::TABLE)
            || ($create['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::TEMPORARY_TABLE)) {
            $sql .= ' ' . $this->buildCreateTable($parsed['TABLE']);
        }
        if ($create['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::INDEX) {
            $sql .= ' ' . $this->buildCreateIndex($parsed['INDEX']);
        }
        // TODO: add more expr_types here (like VIEW), if available in parser output
        return "CREATE " . $sql;
    }//end build()
}//end class

?>
