<?php
declare(strict_types=1);

/**
 * DropBuilder.php
 *
 * Builds the CREATE statement
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for the [DROP] part. You can overwrite
 * all functions to achieve another handling.
 */
class DropBuilder implements Builder
{
    /**
     * Build Drop Index
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildDropIndex(array $parsed): string
    {
        $builder = new DropIndexBuilder();
        return $builder->build($parsed);
    }//end buildDropIndex()

    /**
     * Build Reserved
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildReserved(array $parsed): string
    {
        $builder = new ReservedBuilder();
        return $builder->build($parsed);
    }//end buildReserved()

    /**
     * Build Expression
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildExpression(array $parsed): string
    {
        $builder = new DropExpressionBuilder();
        return $builder->build($parsed);
    }//end buildExpression()

    /**
     * Build SubTree
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    protected function buildSubTree(array $parsed): string
    {
        $sql = '';
        foreach ($parsed['sub_tree'] as $k => $v) {
            $len = strlen($sql);
            $sql .= $this->buildReserved($v);
            $sql .= $this->buildExpression($v);
            if ($len === strlen($sql)) {
                throw new \Pipfrosch\PHPSQLParser\Exceptions\UnableToCreateSQLException(
                    'DROP subtree',
                    $k,
                    $v,
                    'expr_type'
                );
            }
            $sql .= ' ';
        }
        return $sql;
    }//end buildSubTree()

    /**
     * Build
     *
     * @param array $parsed The parsed query.
     *
     * @return string
     */
    public function build(array $parsed): string
    {
        $drop = $parsed['DROP'];
        $sql  = $this->buildSubTree($drop);
        if ($drop['expr_type'] === \Pipfrosch\PHPSQLParser\Utils\ExpressionType::INDEX) {
            $sql .= '' . $this->buildDropIndex($parsed['INDEX']) . ' ';
        }
        return 'DROP ' . substr($sql, 0, -1);
    }//end build()
}//end class

?>
