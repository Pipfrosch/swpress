<?php
declare(strict_types=1);

/**
 * AlterBuilder for [DELETE] part.
 *
 * @package PHP-SQL-Parser
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link    https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Builders;

/**
 * This class implements the builder for the [DELETE] part. You can overwrite
 * all functions to achieve another handling.
 */
class AlterBuilder implements Builder
{
    /**
     * Build the query part.
     *
     * @param array $parsed The parsed array.
     *
     * @return string The query string.
     */
    public function build(array $parsed): string
    {
        $sql = '';
        foreach ($parsed as $term) {
            if ($term === ' ') {
                continue;
            }
            if (substr($term, 0, 1) === '(' ||
                strpos($term, "\n") !== false) {
                $sql = rtrim($sql);
            }
            $sql .= $term . ' ';
        }
        $sql = rtrim($sql);
        return $sql;
    }//end build()
}//end class

?>
