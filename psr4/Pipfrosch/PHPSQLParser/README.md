PHP-SQL-Parser README
=====================

This is an attempt to port the project at https://github.com/greenlion/PHP-SQL-Parser
to `strict_types=1` compliant code with PSR-4 style autoload capabilities.

The `@link` tags in the PHP files point to the parent project as that is where
most of the logic comes from, but I have made some changes.

The primary purpose of this project is to assist in sanitizing raw SQL queries
in the SW Press fork of WordPress.
