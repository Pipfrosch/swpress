<?php
declare(strict_types=1);

/**
 * PositionCalculator.php
 *
 * This class implements the calculator for the string positions of the
 * base_expr elements within the output of the PHPSQLParser.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 Justin Swanhart and André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2015 Justin Swanhart and André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Positions;

/*
use PHPSQLParser\utils\PHPSQLParserConstants;
use PHPSQLParser\exceptions\UnableToCalculatePositionException;
use PHPSQLParser\utils\ExpressionType;
*/

/**
 * This class implements the calculator for the string positions of the
 * base_expr elements within the output of the PHPSQLParser.
 */
class PositionCalculator
{
    /**
     * @var array
     */
    protected static $allowedOnOperator = array(
        "\t",
        "\n",
        "\r",
        " ",
        ",",
        "(",
        ")",
        "_",
        "'",
        "\"",
        "?",
        "@",
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9"
    );

    /**
     * @var array
     */
    protected static $allowedOnOther = array(
        "\t",
        "\n",
        "\r",
        " ",
        ",",
        "(",
        ")",
        "<",
        ">",
        "*",
        "+",
        "-",
        "/",
        "|",
        "&",
        "=",
        "!",
        ";"
    );

    /**
     * Something goes here
     */
    protected $flippedBacktrackingTypes;

    /**
     * @var array
     */
    protected static $backtrackingTypes = array(
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::EXPRESSION,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBQUERY,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::BRACKET_EXPRESSION,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::TABLE_EXPRESSION,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::RECORD,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::IN_LIST,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::MATCH_ARGUMENTS,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::TABLE,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::TEMPORARY_TABLE,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::COLUMN_TYPE,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::COLDEF,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PRIMARY_KEY,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::CONSTRAINT,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::COLUMN_LIST,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::CHECK,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::COLLATE,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::LIKE,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::INDEX,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::INDEX_TYPE,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::INDEX_SIZE,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::INDEX_PARSER,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::FOREIGN_KEY,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::REFERENCE,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_HASH,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_COUNT,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_KEY,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_KEY_ALGORITHM,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_RANGE,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_LIST,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_DEF,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_VALUES,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_DEF,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_DATA_DIR,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_INDEX_DIR,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_COMMENT,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_MAX_ROWS,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::PARTITION_MIN_ROWS,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_COMMENT,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_DATA_DIR,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_INDEX_DIR,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_KEY,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_KEY_ALGORITHM,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_MAX_ROWS,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_MIN_ROWS,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_HASH,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBPARTITION_COUNT,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::CHARSET,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::ENGINE,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::QUERY,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::INDEX_ALGORITHM,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::INDEX_LOCK,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::SUBQUERY_FACTORING,
        \Pipfrosch\PHPSQLParser\Utils\ExpressionType::CUSTOM_FUNCTION
    );

    /**
     * Constructor.
     *
     * It initializes some fields.
     */
    public function __construct()
    {
        $this->flippedBacktrackingTypes = array_flip(self::$backtrackingTypes);
    }//end __construct()

    /**
     * Print Position
     *
     * @param string $text         Text.
     * @param string $sql          SQL string.
     * @param int    $charPos      Character Position.
     * @param string $key          Key.
     * @param string $parsed       The parsed string.
     * @param array  $backtracking Backtracking.
     *
     * @return void
     */
    protected function printPos(string $text, string $sql, int $charPos, string $key, string $parsed, array $backtracking): void
    {
        if (!isset($_SERVER['DEBUG'])) {
            return;
        }
        $spaces = "";
        $caller = debug_backtrace();
        $i = 1;
        while ($caller[$i]['function'] === 'lookForBaseExpression') {
            $spaces .= "   ";
            $i++;
        }
        $holdem = substr($sql, 0, $charPos) . "^" . substr($sql, $charPos);
        echo $spaces . $text . " key:" . $key . "  parsed:" . $parsed . " back:" . serialize($backtracking) . " "
            . $holdem . "\n";
    }//end printPos()

    /**
     * Set position within SQL
     *
     * @param string $sql The SQL string.
     * @param array  $parsed The parsed SQL.
     *
     * @return array|bool
     */
    public function setPositionsWithinSQL(string $sql, array $parsed)
    {
        $charPos = 0;
        $backtracking = array();
        $this->lookForBaseExpression($sql, $charPos, $parsed, 0, $backtracking);
        return $parsed;
    }//end setPositionsWithinSQL()

    /**
     * Find position within string
     *
     * @param string $sql       The SQL string.
     * @param string $value     The value.
     * @param string $expr_type Expression type.
     *
     * @return int|bool The position or false if not found.
     */
    protected function findPositionWithinString(string $sql, string $value, string $expr_type)
    {
        $offset = 0;
        $ok = false;
        while (true) {
            $pos = strpos($sql, $value, $offset);
            // error_log("pos:$pos value:$value sql:$sql");
            if ($pos === false) {
                break;
            }
            $before = "";
            if ($pos > 0) {
                $before = $sql[$pos - 1];
            }
            // if we have a quoted string, we every character is allowed after it
            // see issue 137
            $quoted = ($sql[$pos + strlen($value) - 1] === '`');
            $after = "";
            if (isset($sql[$pos + strlen($value)])) {
                $after = $sql[$pos + strlen($value)];
            }
            // if we have an operator, it should be surrounded by
            // whitespace, comma, parenthesis, digit or letter, end_of_string
            // an operator should not be surrounded by another operator
            if (in_array($expr_type, array('operator','column-list'), true)) {
                $ok = ($before === "" || in_array($before, self::$allowedOnOperator, true))
                    || (strtolower($before) >= 'a' && strtolower($before) <= 'z');
                $ok = $ok
                    && ($after === "" || in_array($after, self::$allowedOnOperator, true)
                        || (strtolower($after) >= 'a' && strtolower($after) <= 'z'));
                if (! $ok) {
                    $offset = $pos + 1;
                    continue;
                }
                break;
            }
            // in all other cases we accept
            // whitespace, comma, operators, parenthesis and end_of_string
            $ok = ($before === "" || in_array($before, self::$allowedOnOther, true));
            $ok = $ok
                && ($after === "" || in_array($after, self::$allowedOnOther, true)
                || ($quoted && (strtolower($after) >= 'a' && strtolower($after) <= 'z')));

            if ($ok) {
                break;
            }

            $offset = $pos + 1;
        }
        return $pos;
    }//end findPositionWithinString()

    /**
     * Look for base expression
     *
     * @param string     $sql          The SQL string.
     * @param int        $charPos      Character Position.
     * @param array|bool $parsed       The parsed query array.
     * @param string|int $key          The key.
     * @param array      $backtracking Backtracking array.
     *
     * @return void
     */
    protected function lookForBaseExpression(string $sql, int &$charPos, &$parsed, $key, array &$backtracking): void
    {
        if (! is_numeric($key)) {
            /** To make static analysis tools type happy.*/
            $flippedBacktrackingTypesIsSet = false;
            if (! is_bool($parsed) && isset($this->flippedBacktrackingTypes[$parsed])) {
                $flippedBacktrackingTypesIsSet = true;
            }
            /** End make static analysis tools type happy.*/
            if (($key === 'UNION' || $key === 'UNION ALL')
                || ($key === 'expr_type' && $flippedBacktrackingTypesIsSet)
                || ($key === 'select-option' && $parsed !== false) || ($key === 'alias' && $parsed !== false)) {
                // we hold the current position and come back after the next base_expr
                // we do this, because the next base_expr contains the complete expression/subquery/record
                // and we have to look into it too
                $backtracking[] = $charPos;
            } elseif (($key === 'ref_clause' || $key === 'columns') && $parsed !== false) {
                // we hold the current position and come back after n base_expr(s)
                // there is an array of sub-elements before (!) the base_expr clause of the current element
                // so we go through the sub-elements and must come at the end
                $backtracking[] = $charPos;
                if (is_array($parsed)) {
                    //we know $parsed is not bool at this point but make vimeo/psalm happy
                    for ($i = 1; $i < count($parsed); $i++) {
                        $backtracking[] = false; // backtracking only after n base_expr!
                    }
                }
            } elseif (($key === 'sub_tree' && $parsed !== false) || ($key === 'options' && $parsed !== false)) {
                // we prevent wrong backtracking on subtrees (too much array_pop())
                // there is an array of sub-elements after(!) the base_expr clause of the current element
                // so we go through the sub-elements and must not come back at the end
                if (is_array($parsed)) {
                    //we know $parsed is not bool at this point but make vimeo/psalm happy
                    for ($i = 1; $i < count($parsed); $i++) {
                        $backtracking[] = false;
                    }
                }
            } elseif (($key === 'TABLE') || ($key === 'create-def' && $parsed !== false)) {
                // do nothing
            } else {
                // move the current pos after the keyword
                // SELECT, WHERE, INSERT etc.
                if (\Pipfrosch\PHPSQLParser\Utils\PHPSQLParserConstants::getInstance()->isReserved($key)) {
                    $charPos = stripos($sql, $key, $charPos);
                    if (! is_int($charPos)) {
                        //should never happen but this is wrong
                        $charPos = 0;
                    }
                    $charPos += strlen($key);
                }
            }
        }
        if (!is_array($parsed)) {
            return;
        }

        foreach ($parsed as $key => $value) {
            if ($key === 'base_expr') {
                //$this->printPos("0", $sql, $charPos, $key, $value, $backtracking);
                $subject = substr($sql, $charPos);
                $pos = $this->findPositionWithinString(
                    $subject,
                    $value,
                    isset($parsed['expr_type']) ? $parsed['expr_type'] : 'alias'
                );
                if ($pos === false) {
                    throw new \Pipfrosch\PHPSQLParser\Exceptions\UnableToCalculatePositionException($value, $subject);
                }
                if (is_bool($pos)) {
                    //never will happen
                    $pos = 0;
                }

                $parsed['position'] = $charPos + $pos;
                $charPos += $pos + strlen($value);

                //$this->printPos("1", $sql, $charPos, $key, $value, $backtracking);
                $oldPos = array_pop($backtracking);
                if (isset($oldPos) && $oldPos !== false) {
                    $charPos = $oldPos;
                }
                //$this->printPos("2", $sql, $charPos, $key, $value, $backtracking);
            } else {
                $this->lookForBaseExpression($sql, $charPos, $parsed[$key], $key, $backtracking);
            }
        }
    }//end lookForBaseExpression()
}//end class

?>
