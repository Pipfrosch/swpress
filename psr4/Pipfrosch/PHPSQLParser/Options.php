<?php
declare(strict_types=1);

/**
 * Options class
 *
 * @package PHP-SQL-Parser
 * @author  mfris <unknown@unknown.tld>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link    https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser;

/**
 * Options class
 */
final class Options
{

    /**
     * @var array
     */
    private $options;

    /**
     * @const string
     */
    const CONSISTENT_SUB_TREES = 'consistent_sub_trees';

    /**
     * @const string
     */
    const ANSI_QUOTES = 'ansi_quotes';

    /**
     * Options constructor.
     *
     * @param array $options The options array.
     */
    public function __construct(array $options)
    {
        $this->options = $options;
    }//end __construct()

    /**
     * Get consistent subtree
     *
     * @return bool
     */
    public function getConsistentSubtrees(): bool
    {
        return (isset($this->options[self::CONSISTENT_SUB_TREES]) && $this->options[self::CONSISTENT_SUB_TREES]);
    }//end getConsistentSubtrees()

    /**
     * Get ANSI Quotes
     *
     * @return bool
     */
    public function getANSIQuotes(): bool
    {
        return (isset($this->options[self::ANSI_QUOTES]) && $this->options[self::ANSI_QUOTES]);
    }//end getANSIQuotes()
}//end class

?>
