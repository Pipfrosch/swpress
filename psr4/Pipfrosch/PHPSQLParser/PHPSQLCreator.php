<?php
declare(strict_types=1);

/**
 * PHPSQLCreator.php
 *
 * A creator, which generates SQL from the output of PHPSQLParser.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser;

/**
 * This class generates SQL from the output of the PHPSQLParser.
 */
class PHPSQLCreator
{
    /**
     * @var null|string
     */
    protected $created = null;

    /**
     * The constructor function.
     *
     * @param bool|array $parsed The array to build a query from, or false.
     */
    public function __construct($parsed = false)
    {
        if (is_array($parsed)) {
            $this->create($parsed);
        }
    }//end __construct()

    /**
     * Create query from parsed array.
     *
     * @param array $parsed The array to build a query from.
     *
     * @return string The constructed query.
     */
    public function create(array $parsed): string
    {
        $k = key($parsed);
        switch ($k) {
            case 'UNION':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\UnionStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'UNION ALL':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\UnionAllStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'SELECT':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\SelectStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'INSERT':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\InsertStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'REPLACE':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\ReplaceStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'DELETE':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\DeleteStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'TRUNCATE':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\TruncateStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'UPDATE':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\UpdateStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'RENAME':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\RenameStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'SHOW':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\ShowStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'CREATE':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\CreateStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'BRACKET':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\BracketStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'DROP':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\DropStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            case 'ALTER':
                $builder = new \Pipfrosch\PHPSQLParser\Builders\AlterStatementBuilder();
                $this->created = $builder->build($parsed);
                break;
            default:
                throw new \Pipfrosch\PHPSQLParser\Exceptions\UnsupportedFeatureException($k);
                break;
        }
        return $this->created;
    }//end create()
}//end class

?>
