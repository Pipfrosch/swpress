<?php
declare(strict_types=1);

/**
 * UnableToCalculatePositionException.php
 *
 * This file implements the UnableToCalculatePositionException class which is used within the
 * PHPSQLParser package.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */

namespace Pipfrosch\PHPSQLParser\Exceptions;

/*
use Exception;
*/

/**
 * This exception will occur, if the PositionCalculator can not find the token
 * defined by a base_expr field within the original SQL statement. Please create
 * an issue in such a case, it is an application error.
 *
 * @author  André Rothe <andre.rothe@phosco.info>
 * @license http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 */
class UnableToCalculatePositionException extends \Exception
{
    /**
     * @var string
     */
    protected $needle;

    /**
     * @var string
     */
    protected $haystack;

    /**
     * Filter special html characters.
     *
     * @author Alice Wonder <paypal@domblogger.net>
     *
     * @param string $input The string to filter.
     *
     * @return string The filtered string.
     */
    protected function specialCharacterFilter(string $input): string
    {
        $s = array();
        $r = array();
        $s[] = '/&/';
        $r[] = '&amp;';
        $s[] = '/</';
        $r[] = '&lt;';
        $s[] = '/>/';
        $r[] = '&gt;';
        return preg_replace($s, $r, $input);
    }//end specialCharacterFilter()

    /**
     * The constructor
     *
     * @param mixed $needle   The needle to find.
     * @param mixed $haystack The haystack the need should be in.
     */
    public function __construct($needle, $haystack)
    {
        $needle = (string) $needle;
        $needle = $this->specialCharacterFilter($needle);
        $haystack = (string) $haystack;
        $haystack = $this->specialCharacterFilter($haystack);
        $this->needle = $needle;
        $this->haystack = $haystack;
        parent::__construct(
            "cannot calculate position of <code>" . $needle . "</code> within <code>" . $haystack . "</code>",
            5
        );
    }//end __construct()

    /**
     * Get Needle
     *
     * @return string The needle.
     */
    public function getNeedle(): string
    {
        return $this->needle;
    }//end getNeedle()

    /**
     * Get Haystack
     *
     * @return string The haystack.
     */
    public function getHaystack(): string
    {
        return $this->haystack;
    }//end getHaystack()
}//end class

?>
