<?php
declare(strict_types=1);

/**
 * UnableToCreateSQLException.php
 *
 * This file implements the UnableToCreateSQLException class which is used within the
 * PHPSQLParser package.
 *
 * LICENSE:
 * Copyright (c) 2010-2014 André Rothe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package   PHP-SQL-Parser
 * @author    André Rothe <andre.rothe@phosco.info>
 * @copyright 2010-2014 André Rothe
 * @license   http://www.debian.org/misc/bsd.license  BSD License (3 Clause)
 * @link      https://github.com/greenlion/PHP-SQL-Parser
 */
namespace Pipfrosch\PHPSQLParser\Exceptions;

/*
use Exception;
*/

/**
 * This exception will occur within the PHPSQLCreator, if the creator can not find a
 * method, which can handle the current expr_type field. It could be an error within the parser
 * output or a special case has not been modelled within the creator. Please create an issue
 * in such a case.
 */
class UnableToCreateSQLException extends \Exception
{
    /**
     * @var string
     */
    protected $part;

    /**
     * @var string
     */
    protected $partkey;

    /**
     * @var array
     */
    protected $entry;

    /**
     * @var string
     */
    protected $entrykey;

    /**
     * Filter special html characters.
     *
     * @author Alice Wonder <paypal@domblogger.net>
     *
     * @param string $input The string to filter.
     *
     * @return string The filtered string.
     */
    protected function specialCharacterFilter(string $input): string
    {
        $s = array();
        $r = array();
        $s[] = '/&/';
        $r[] = '&amp;';
        $s[] = '/</';
        $r[] = '&lt;';
        $s[] = '/>/';
        $r[] = '&gt;';
        return preg_replace($s, $r, $input);
    }//end specialCharacterFilter()

    /**
     * The constructor
     *
     * @param mixed $part     The part array.
     * @param mixed $partkey  The part key.
     * @param array $entry    The entry array.
     * @param mixed $entrykey The entry key.
     */
    public function __construct($part, $partkey, $entry, $entrykey)
    {
        $partkey = (string) $partkey;
        $partkey = $this->specialCharacterFilter($partkey);
        $entrykey = (string) $entrykey;
        $entrykey = $this->specialCharacterFilter($entrykey);
        $this->part = $part;
        $this->partkey = $partkey;
        $this->entry = $entry;
        $this->entrykey = $entrykey;
        parent::__construct(
            "unknown [" . $entrykey . "] = " . $entry[$entrykey] . " in \"" . $part . "\" [" . $partkey . "] ",
            15
        );
    }//end __construct

    /**
     * Get the entry value.
     *
     * @return array
     */
    public function getEntry(): array
    {
        return $this->entry;
    }//end getEntry()

    /**
     * Get the entry value.
     *
     * @return string
     */
    public function getEntryKey(): string
    {
        return $this->entrykey;
    }//end getEntryKey()

    /**
     * Get the SQL part
     *
     * @return string
     */
    public function getSQLPart(): string
    {
        return $this->part;
    }//end getSQLPart()

    /**
     * Get the SQL Part Key
     *
     * @return string
     */
    public function getSQLPartKey(): string
    {
        return $this->partkey;
    }//end getSQLPartKey()
}//class

?>
