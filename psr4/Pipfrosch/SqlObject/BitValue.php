<?php
declare(strict_types=1);

/**
 * Integer Value Object
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The IntegerValue object for MariaDB numeric types
 */
class BitValue extends AbstractNumericValue implements DataValueInterface
{
    /**
     * @var string
     */
    protected $subtype = 'BIT';

    /**
     * The M number of bits. 1 to max 64.
     *
     * @var int
     */
    protected $bitsize = 1;

    /**
     * @var null|string
     */
    protected $value = null;

    /**
     * Large numbers (> 32 bit) need gmp to properly convert base.
     *
     * @param string|int $input The number to convert.
     * @param int        $base  The base number.
     *
     * @return string The base 2 number.
     */
    protected function gmpBaseConvert($input, int $base): string
    {
        return gmp_strval(gmp_init($input, $base), 2);
    }

    /**
     * Convert Base 10 integer to Base 2 string
     *
     * @param int $integer The integer to convert.
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     */
    public function setFromInteger(int $integer): bool
    {
        if ($integer < 0) {
            throw Exceptions\InvalidNumericArgument::negativeInteger($integer);
        }
        if ($integer > 65536 && function_exists('gmp_strval')) {
            $bit = $this->gmpBaseConvert($integer, 10);
        } else {
            $str = (string) $integer;
            $bit = base_convert($str, 10, 2);
        }
        if (strlen($bit) > $this->bitsize) {
            throw Exceptions\InvalidNumericArgument::integerTooLarge($integer, strlen($bit), $this->bitsize);
        }
        $this->value = str_pad($bit, $this->bitsize, '0', STR_PAD_LEFT);
        return true;
    }//end setFromInteger()

    /**
     * Convert Octal String to Base 2 string
     *
     * @param string $hex The hex string to convert.
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     */
    public function setFromOctal(string $octal): bool
    {
        $octal = trim(strtolower($octal));
        if (preg_match('/^[0-7]+$/', $octal) !== 1) {
            throw Exceptions\InvalidNumericArgument::expectingOctalArgument($octal);
        }
        if (strlen($octal) > 16 && function_exists('gmp_strval')) {
            $bit = $this->gmpBaseConvert($octal, 8);
        } else {
            $bit = base_convert($octal, 8, 2);
        }
        if (strlen($bit) > $this->bitsize) {
            throw Exceptions\InvalidNumericArgument::octalTooLarge($octal, strlen($bit), $this->bitsize);
        }
        $this->value = str_pad($bit, $this->bitsize, '0', STR_PAD_LEFT);
        return true;
    }//end setFromOctal()

    /**
     * Convert Hex to Base 2 string
     *
     * @param string $hex The hex string to convert.
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     */
    public function setFromHex(string $hex): bool
    {
        $hex = trim(strtolower($hex));
        if (! ctype_xdigit($hex)) {
            throw Exceptions\InvalidNumericArgument::expectingHexArgument($hex);
        }
        if (strlen($hex) > 8 && function_exists('gmp_strval')) {
            $bit = $this->gmpBaseConvert($hex, 16);
        } else {
            $bit = base_convert($hex, 16, 2);
        }
        if (strlen($bit) > $this->bitsize) {
            throw Exceptions\InvalidNumericArgument::hexTooLarge($hex, strlen($bit), $this->bitsize);
        }
        $this->value = str_pad($bit, $this->bitsize, '0', STR_PAD_LEFT);
        return true;
    }//end setFromHex()

    /**
     * Convert binary data to Base 2 string
     *
     * @param string $bin The binary data to convert.
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     */
    public function setFromBin(string $bin): bool
    {
        $hex = bin2hex($bin);
        return $this->setFromHex($hex);
    }//end setFromBin()

    /**
     * Set the value from string
     *
     * @param string      $value   The value to set.
     * @param null|string $subtype Ignored as always BIT for this class.
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     */
    public function setValue(string $value, $subtype = null):bool
    {
        $string = trim($value);
        $valid = false;
        if (preg_match('/^[0-1]{1,64}$/', $string) === 1) {
            $valid = true;
        } elseif (preg_match('/^b\'[0-1]{1,64}\'$/', $string) === 1) {
            $n = strlen($string) - 3;
            $string = substr($string, 2, $n);
            $valid = true;
        }
        if (! $valid) {
            throw Exceptions\InvalidNumericArgument::invalidBitString($value);
        }
        if (strlen($string) > $this->bitsize) {
            throw Exceptions\InvalidNumericArgument::bitStringTooLarge($string, strlen($string), $this->bitsize);
        }
        $this->value = str_pad($string, $this->bitsize, '0', STR_PAD_LEFT);
        return true;
    }//end setValue()

    /**
     * Set the bitsize
     *
     * @param int $bitsize The bitsize to use.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function setBitsize(int $bitsize): void
    {
        if ($bitsize < 1 || $bitsize > 64) {
            throw Exceptions\InvalidNumericArgument::invalidBitSize($bitsize);
        }
        if (! is_string($this->value)) {
            $this->bitsize = $bitsize;
            return;
        }
        $test = preg_replace('/^0+/', '', $this->value);
        if (strlen($test) > $bitsize) {
            throw Exceptions\InvalidNumericArgument::existingBitValueTooLarge($bitsize, $this->value);
        }
        $this->bitsize = $bitsize;
        $this->value = str_pad($test, $bitsize, '0', STR_PAD_LEFT);
    }//end setBitsize()

    /**
     * To string
     *
     * @return string
     */
    public function __toString(): string
    {
        if (! is_string($this->value)) {
            return '';
        }
        return "b'" . $this->value . "'";
    }//end __toString()

    /**
     * The constructor
     *
     * @param int $bitsize Defaults to 1.
     */
    public function __construct(int $bitsize = 1)
    {
        if ($bitsize < 1) {
            $this->bitsize = 1;
        } elseif ($bitsize > 64) {
            $this->bitsize = 64;
        } else {
            $this->bitsize = $bitsize;
        }
    }//end __construct()
}//end class

?>