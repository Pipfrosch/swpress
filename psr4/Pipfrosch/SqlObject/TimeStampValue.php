<?php
declare(strict_types=1);

/**
 * The TimeStamp object for MariaDB temporal types.
 *
 * WARNING: The TIMESTAMP data type should not be used to store arbitrary temporal values. It should really
 *  only be used when you want the database itself to automatically record when a record is created or modified.
 *
 *  Internally, MariaDB stores this data type as a 32-bit signed integer representing seconds from UNIX epoch.
 *  Even on 64-bit systems that normally use a 64-bit signed integer for dates, MariaDB uses a 32-bit signed
 *  integer.
 *
 *  When queried, the result is shown as a YYYY-MM-DD string localized to the timezone of either the MariaDB
 *  server or client (which often are the same host), but it is displayed without the localized timezone information.
 *  This means the identical record with the identical TIMESTAMP will display differently depending on what the
 *  timezone settings are. I do not like it, but it is what it is.
 *
 *  It is highly recommended that your MariaDB server and client be configured to use UTC and that the default
 *  timezone in your PHP scripts also be configured to use UTC so that the displayed result is always the same.
 *
 *  It is also highly recommended to only use the TIMESTAMP temporal type in the context of columns that are maintained
 *  by the database itself on insert or on update, rather than set by INSERT or UPDATE queries. Use the DATETIME
 *  type when you want to use an INSERT or UPDATE query to specify a date and time. This data type really is only
 *  intended for what it is called, a TIMESTAMP, it is not intended for general Date and Time data.
 *
 * This class does not allow you to specify the timezone used when creating an object of this type. That is
 *  intentional. The generated string should match the timezone of the client used to insert or update the row,
 *  so that hopefully when MariaDB converts the string to a 32-bit signed integer, it will take the timezone of the
 *  connection CLIENT into consideration and do the right thing.
 *
 *  But seriously, do not use INSERT or UPDATE to specify a value, set up the table to automatically use the
 *  current timestamp on insert or update. Use DATETIME for columns where you want to specify a value.
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 *
 * @see https://mariadb.com/kb/en/library/timestamp/
 */

namespace Pipfrosch\SqlObject;

/**
 * The TimeStamp object for MariaDB temporal types.
 */
class TimeStampValue extends AbstractTemporalValue implements DataValueInterface
{
    /**
     * The subtype which will always be TIMESTAMP
     *
     * @var string
     */
    protected $subtype = 'TIMESTAMP';

    /**
     * A 32-bit signed integer that represents seconds from UNIX epoch
     *
     * @var null|int
     */
    protected $timestamp = null;

    /**
     * Set the value from a UNIX timestamp (seconds from epoch)
     *
     * @param null|int $timestamp The UNIX timestamp to convert. If null, uses current.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function valueFromEpoch($timestamp = null): void
    {
        if (! is_int($timestamp)) {
            $timestamp = time();
        }
        if ($timestamp < -2147483648 || $timestamp > 2147483647) {
            throw Exceptions\InvalidTemporalArgument::invalidTimestampBytes($timestamp);
        }
        if ($timestamp === 0) {
            throw Exceptions\InvalidTemporalArgument::epochTimestamp();
        }
        $this->timestamp = $timestamp;
    }//end valueFromEpoch()

    /**
     * Returns the value appropriate to the type
     *
     * @return string|null
     */
    public function getValue()
    {
        if (! is_int($this->timestamp)) {
            return null;
        }
        $dt = new \DateTime();
        $dt->setTimeZone($this->dateTimeZone);
        $dt->setTimestamp($this->timestamp);
        return $dt->format('Y-m-d H:i:s');
    }//end getValue()

    /**
     * Set the value from conforming string.
     *
     * @param string      $value   The value as a string.
     * @param string|null $subtype The subtype associated with the value. Meaningless
     *                             in the context of this class, it is ignored.
     *
     * @return bool True on success, should throw exception on failure.
     *
     * @throws \InvalidArgumentException
     */
    public function setValue(string $value, $subtype = null): bool
    {
        $value = trim($value);
        if (preg_match('/^[0-9]{4,4}-[0-1][0-9]-[0-3][0-9] [0-5][0-9]:[0-5][0-9]:[0-5][0-9]$/', $value) !== 1) {
            throw Exceptions\InvalidTemporalArgument::invalidTemporalString($value, false);
        }
        $year = substr($value, 0, 10);
        $time = substr($value, 11);
        $intyear = intval(substr($value, 0, 4), 10);
        $intmonth = intval(substr($value, 5, 2), 10);
        $intday = intval(substr($value, 8), 10);
        if (! checkdate($intmonth, $intday, $intyear)) {
            throw Exceptions\InvalidTemporalArgument::bogusDate(substr($value, 0, 10));
        }
        $hour = intval(substr($value, 11, 2), 10);
        $minutes = intval(substr($value, 14, 2), 10);
        $seconds = intval(substr($value, 17), 10);
        if ($hour > 23 || $minutes > 59 || $seconds > 59) {
            throw Exceptions\InvalidTemporalArgument::bogusTime(substr($value, 11));
        }
        $dt = new \DateTime($value, $this->dateTimeZone);
        $epoch = intval($dt->format('U'), 10);
        $this->valueFromEpoch($epoch);
        return true;
    }//end setValue()

    /**
     * String representation of value
     *
     * @return string
     */
    public function __toString(): string
    {
        $value = $this->getValue();
        if (is_string($value)) {
            return $value;
        }
        return '';
    }//end __toString()

    /**
     * Return value as DOMNode
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode|\DOMComment
     */
    public function toDomNode(\DOMDocument $dom)
    {
        $value = $this->getValue();
        if (! is_string($value)) {
            $node = $dom->createComment('Invalid ' . $this->subtype . ' value');
            return $node;
        }
        $node = $dom->createElement('value', $value);
        $node->setAttribute('type', $this->type);
        $node->setAttribute('subtype', $this->subtype);
        $node->setAttribute('tz', $this->getTimeZone(true));
        return $node;
    }//end toDomNode()

    /**
     * The constructor. Always use the system timezone.
     */
    public function __construct()
    {
        $tzi = date_default_timezone_get();
        $this->dateTimeZone = new \DateTimeZone($tzi);
    }//end __construct()
}//end class

?>