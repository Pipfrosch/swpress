<?php
declare(strict_types=1);

/**
 * Integer Value Object
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The IntegerValue object for MariaDB numeric types
 */
class IntegerValue extends AbstractNumericValue implements DataValueInterface
{
    /**
     * Signed int or not.
     *
     * @var bool
     */
    protected $signed = true;

    /**
     * Validate value being set is an integer
     *
     * @param string $string The string to test.
     *
     * @return bool True if string representation of an integer.
     */
    protected function isIntegerString(string $string): bool
    {
        $first = substr($string, 0, 1);
        if ($first === '-') {
            return ctype_digit(substr($string, 1));
        }
        return ctype_digit($string);
    }//end isIntegerString()

    /**
     * Validated that the integer is within inclusive range.
     *
     * @param int $n   The integer to test.
     * @param int $min The smallest value the integer can be.
     * @param int $max The largest value the integer can be.
     *
     * @return bool True if within range, inclusive.
     */
    protected function inRange(int $n, int $min, int $max): bool
    {
        if (($n < $min) || ($n > $max)) {
            return false;
        }
        return true;
    }//end inRange()

    /**
     * Validate that a very large integer is within inclusive range.
     *
     * @param string $n   String representative of very large integer.
     * @param string $min String representative of smallest value can be.
     * @param string $max String representative of largest value can be.
     *
     * @return bool True if within range, inclusive.
     */
    protected function gmpInRange(string $n, string $min, string $max): bool
    {
        if (! function_exists('gmp_cmp')) {
            // be nice if they do not have GMP functions.
            $message = 'Please compile PHP with the GMP functions.';
            $message .= ' See https://www.php.net/manual/en/gmp.installation.php';
            error_log($message);
            return true;
        }
        if (gmp_cmp($min, $n) === 1) {
            return false;
        }
        if (gmp_cmp($n, $max) === 1) {
            return false;
        }
        return true;
    }//end gmpInRange()

    /**
     * Check if string represents a 0 or 1
     *
     * @param string $string The string to test.
     *
     * @return bool
     */
    protected function isBool(string $string): bool
    {
        $int = intval($string, 10);
        return $this->inRange($int, 0, 1);
    }//end isBool()

    /**
     * Check is string is valid tinyint
     *
     * @see https://mariadb.com/kb/en/library/tinyint/
     *
     * @param string $string The string to check.
     * @param bool   $signed If signed tinyint.
     *
     * @return bool
     */
    protected function isTinyInt(string $string, bool $signed): bool
    {
        $int = intval($string, 10);
        if ($signed) {
            return $this->inRange($int, -128, 127);
        }
        return $this->inRange($int, 0, 255);
    }//end isTinyInt()

    /**
     * Check if string is valid smallint
     *
     * @see https://mariadb.com/kb/en/library/smallint/
     *
     * @param string $string The string to check.
     * @param bool   $signed If signed tinyint.
     *
     * @return bool
     */
    protected function isSmallInt(string $string, bool $signed): bool
    {
        $int = intval($string, 10);
        if ($signed) {
            return $this->inRange($int, -32768, 32767);
        }
        return $this->inRange($int, 0, 65535);
    }//end isSmallInt()

    /**
     * Check if string is valid mediumint
     *
     * @see https://mariadb.com/kb/en/library/mediumint/
     *
     * @param string $string The string to check.
     * @param bool   $signed If signed tinyint.
     *
     * @return bool
     */
    protected function isMediumInt(string $string, bool $signed): bool
    {
        $int = intval($string, 10);
        if ($signed) {
            return $this->inRange($int, -8388608, 8388607);
        }
        return $this->inRange($int, 0, 16777215);
    }//end isMediumInt()

    /**
     * Check if string is valid int
     *
     * @see https://mariadb.com/kb/en/library/int/
     *
     * @param string $string The string to check.
     * @param bool   $signed If signed tinyint.
     *
     * @return bool
     */
    protected function isInt(string $string, bool $signed): bool
    {
        $intable = false;
        $test = intval($string, 10) + 0;
        if (is_int($test)) {
            $intable = true;
        }
        if (! $signed && PHP_INT_SIZE === 4) {
            //32-bit system
            $intable = false;
        }
        if ($intable) {
            $int = intval($string, 10);
            if ($signed) {
                return $this->inRange($int, -2147483648, 2147483647);
            }
            return $this->inRange($int, 0, 4294967295);
        }
        if ($signed) {
            return $this->gmpInRange($string, "-2147483648", "2147483647");
        }
        return $this->gmpInRange($string, "0", "4294967295");
    }//end isInt()

    /**
     * Check if string is valid bigint
     *
     * @see https://mariadb.com/kb/en/library/bigint/
     *
     * @param string $string The string to check.
     * @param bool   $signed If signed tinyint.
     *
     * @return bool
     */
    protected function isBigInt(string $string, bool $signed): bool
    {
        $intable = false;
        $test = intval($string, 10) + 0;
        if (is_int($test)) {
            $intable = true;
        }
        if ($intable && $signed && PHP_INT_SIZE === 4) {
            //will be within BIGINT for 32-bit or 64-bit
            return true;
        }
        if ($signed) {
            // 32-bit systems need this
            return $this->gmpInRange($string, "-9223372036854775808", "9223372036854775807");
        }
        return $this->gmpInRange($string, "0", "18446744073709551615");
    }//end isBigInt()

    /**
     * Set the value.
     *
     * @param string      $value   The string value.
     * @param null|string $subtype The integer subtype.
     * @param null|bool   $signed  Whether or not integer is signed.
     *
     * @return bool
     *
     * @throws \InvalidArgumentException for invalid values.
     */
    public function setValue(string $value, $subtype = null, $signed = null): bool
    {
        if (! is_bool($signed)) {
            $signed = $this->signed;
        }
        $value = trim($value);
        if (is_null($subtype)) {
            $subtype = $this->subtype;
        } else {
            $subtype = trim($subtype);
        }
        if ($subtype === '') {
            $subtype = null;
        }
        $validSubtype = $this->validateSubType($subtype);
        if (is_bool($validSubtype)) {
            //never will happen but static analysis tools don't understand
            if (is_null($subtype)) {
                $subtype = 'null';
            }
            throw Exceptions\InvalidSubtype::genericInvalidSubtype($subtype);
        }
        if (is_string($validSubtype) && ! in_array($validSubtype, $this->validIntTypes)) {
            throw Exceptions\InvalidSubtype::invalidIntegerSubtype($validSubtype);
        }
        if (! $this->isIntegerString($value)) {
            throw Exceptions\InvalidNumericArgument::expectingIntegerString($value);
        }
        switch ($validSubtype) {
            case 'BOOLEAN':
                if (! $this->isBool($value)) {
                    throw Exceptions\InvalidNumericArgument::expectingBooleanString($value);
                }
                break;
            case 'TINYINT':
                if (! $this->isTinyInt($value, $signed)) {
                    throw Exceptions\InvalidNumericArgument::integerStringOutOfRange($value, $validSubtype, $signed);
                }
                break;
            case 'SMALLINT':
                if (! $this->isSmallInt($value, $signed)) {
                    throw Exceptions\InvalidNumericArgument::integerStringOutOfRange($value, $validSubtype, $signed);
                }
                break;
            case 'MEDIUMINT':
                if (! $this->isMediumInt($value, $signed)) {
                    throw Exceptions\InvalidNumericArgument::integerStringOutOfRange($value, $validSubtype, $signed);
                }
                break;
            
            case 'BIGINT':
                if (! $this->isBigInt($value, $signed)) {
                    throw Exceptions\InvalidNumericArgument::integerStringOutOfRange($value, $validSubtype, $signed);
                }
                break;
            default:
                if (! $this->isInt($value, $signed)) {
                    throw Exceptions\InvalidNumericArgument::integerStringOutOfRange($value, 'INT', $signed);
                }
                break;
        }
        // still here, we are good to go.
        $this->signed = $signed;
        $this->subtype = $validSubtype;
        $this->value = $value;
        return true;
    }//end setValue()

    /**
     * Set the subtype
     *
     * @param null|string $subtype The subtype to set.
     * @param null|bool   $signed  Optional. Signed or unsigned. Defaults to current class property.
     *
     * @return bool
     *
     * @throws \InvalidArgumentException for invalid values.
     */
    public function setSubType($subtype, $signed = null)
    {
        if (! is_bool($signed)) {
            $signed = $this->signed;
        }
        $validSubtype = $this->validateSubType($subtype);
        if (is_bool($validSubtype)) {
            //never will happen but static analysis tools don't understand
            if (is_null($subtype)) {
                $subtype = 'null';
            }
            throw Exceptions\InvalidSubtype::genericInvalidSubtype($subtype);
        }
        if (is_string($validSubtype) && ! in_array($validSubtype, $this->validIntTypes)) {
            throw Exceptions\InvalidSubtype::invalidIntegerSubtype($validSubtype);
        }
        if (is_null($this->value)) {
            $this->subtype = $validSubtype;
            return true;
        }
        return $this->setValue($this->value, $validSubtype, $signed);
    }//end setSubType()

    /**
     * Return BOOLEAN object
     *
     * @param bool $value The boolean equivalent.
     *
     * @return static A new instance with specified value.
     */
    public function withBoolean(bool $value)
    {
        $return = new static();
        if ($value) {
            $return->setValue('1', 'BOOLEAN');
        } else {
            $return->setValue('0', 'BOOLEAN');
        }
        return $return;
    }// end withBoolean()

    /**
     * Return TINYINT object
     *
     * @param int  $value  The integer equivalent.
     * @param bool $signed Signed or not.
     *
     * @return static A new instance with specified value.
     *
     * @throws \InvalidArgumentException for invalid values.
     */
    public function withTinyInt(int $value, bool $signed = true)
    {
        $str = (string) $value;
        if (! $this->isTinyInt($str, $signed)) {
            throw Exceptions\InvalidNumericArgument::integerOutOfRange($value, 'TINYINT', $signed);
        }
        $return = new static();
        $return->setValue($str, 'TINYINT', $signed);
        return $return;
    }//end withTinyInt()

    /**
     * Return SMALLINT object
     *
     * @param int  $value  The integer equivalent.
     * @param bool $signed Signed or not.
     *
     * @return static A new instance with specified value.
     *
     * @throws \InvalidArgumentException for invalid values.
     */
    public function withSmallInt(int $value, bool $signed = true)
    {
        $str = (string) $value;
        if (! $this->isSmallInt($str, $signed)) {
            throw Exceptions\InvalidNumericArgument::integerOutOfRange($value, 'SMALLINT', $signed);
        }
        $return = new static();
        $return->setValue($str, 'SMALLINT', $signed);
        return $return;
    }//end withSmallInt()

    /**
     * Return MEDIUMINT object
     *
     * @param int  $value  The integer equivalent.
     * @param bool $signed Signed or not.
     *
     * @return static A new instance with specified value.
     *
     * @throws \InvalidArgumentException for invalid values.
     */
    public function withMediumInt(int $value, bool $signed = true)
    {
        $str = (string) $value;
        if (! $this->isMediumInt($str, $signed)) {
            throw Exceptions\InvalidNumericArgument::integerOutOfRange($value, 'MEDIUMINT', $signed);
        }
        $return = new static();
        $return->setValue($str, 'MEDIUMINT', $signed);
        return $return;
    }//end withMediumInt()

    /**
     * Return INT object
     *
     * @param string|int  $value  The integer equivalent.
     * @param bool        $signed Signed or not.
     *
     * @return static A new instance with specified value.
     *
     * @throws \InvalidArgumentException for invalid values.
     */
    public function withInt($value, bool $signed = true)
    {
        if (is_int($value)) {
            $str = (string) $value;
        } else {
            $str = $value;
        }
        if (! $this->isInt($str, $signed)) {
            throw Exceptions\InvalidNumericArgument::integerValueOutOfRange($str, 'INT', $signed);
        }
        $return = new static();
        $return->setValue($str, 'INT', $signed);
        return $return;
    }//end withInt()

    /**
     * Return BIGINT object
     *
     * @param string|int  $value  The integer equivalent.
     * @param bool        $signed Signed or not.
     *
     * @return static A new instance with specified value.
     *
     * @throws \InvalidArgumentException for invalid values.
     */
    public function withBigInt($value, bool $signed = true)
    {
        if (is_int($value)) {
            $str = (string) $value;
        } else {
            $str = $value;
        }
        if (! $this->isBigInt($str, $signed)) {
            throw Exceptions\InvalidNumericArgument::integerValueOutOfRange($str, 'BIGINT', $signed);
        }
        $return = new static();
        $return->setValue($str, 'BIGINT', $signed);
        return $return;
    }//end withBigInt()

    /**
     * The constructor
     */
    public function __construct()
    {
    }//end __construct()
}//end class

?>