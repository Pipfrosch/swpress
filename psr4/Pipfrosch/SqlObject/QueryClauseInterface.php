<?php
/**
 * Interface for query clauses
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The QueryClause Interface
 */
interface QueryClauseInterface
{
    /**
     * Get the type.
     * Must return the type. Should be upper case ASCII.
     *
     * @return string
     */
    public function getType();

    /**
     * Should return the clause as a string, empty string
     * if not a valid clause.
     *
     * @return string
     */
    public function __toString(): string;

    /**
     * Export the value to a DOMNode
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode|\DOMComment
     */
    public function toDomNode(\DOMDocument $dom);
}//end interface

?>