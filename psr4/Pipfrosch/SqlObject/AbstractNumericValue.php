<?php
declare(strict_types=1);

/**
 * Numeric data value
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The NumericValue object for MariaDB numeric types
 */
abstract class AbstractNumericValue implements DataValueInterface
{
    /**
     * @var string
     */
    protected $type = 'NUMERIC';

    /**
     * Should be upper case.
     *
     * @var null|string
     */
    protected $subtype = null;

    /**
     * @var null|string
     */
    protected $value = null;

    /**
     * @var bool
     */
    protected $parameterize = true;

    /**
     * Array of value subtypes other than null
     *
     * @see https://mariadb.com/kb/en/library/data-types-numeric-data-types/
     */
    protected $validSubtype = array(
        'TINYINT',
        'BOOLEAN',
        'SMALLINT',
        'MEDIUMINT',
        'INT',
        'BIGINT',
        'DECIMAL',
        'FLOAT',
        'DOUBLE',
        'BIT'
    );

    /**
     * Integer subtypes
     *
     * @var array
     */
    protected $validIntTypes = array(
        'TINYINT',
        'BOOLEAN',
        'SMALLINT',
        'MEDIUMINT',
        'INT',
        'BIGINT'
    );

    /**
     * Synonyms
     */
    protected $subtypeSynonyms = array(
        'INTEGER'          => 'INT',
        'DEC'              => 'DECIMAL',
        'NUMERIC'          => 'DECIMAL',
        'FIXED'            => 'DECIMAL',
        'REAL'             => 'DOUBLE',
        'DOUBLE PRECISION' => 'DOUBLE'
    );

    /**
     * Validate the subtype. This should be called by setSubType()
     *
     * @param null|string $subtype The subType to set.
     *
     * @return string|bool|null
     */
    protected function validateSubType($subtype)
    {
        if (is_null($subtype)) {
            return null;
        }
        $subtype = strtoupper(trim($subtype));
        if (isset($this->subtypeSynonyms[$subtype])) {
            return $this->subtypeSynonyms[$subtype];
        }
        if (! in_array($subtype, $this->validSubtype)) {
            return false;
        }
        return $subtype;
    }//end validateSubType()

    /**
     * Return the type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }//end getType()

    /**
     * Return the subtype
     *
     * @return null|string
     */
    public function getSubtype()
    {
        return $this->subtype;
    }//end getSubtype()

    /**
     * Return the value
     *
     * @return null|string
     */
    public function getValue()
    {
        return $this->value;
    }//end getValue()

    /**
     * Whether or not to parameterize the value when created a prepared
     * statement with the value.
     *
     * @return bool True if it should be parameterized
     */
    public function getParametized(): bool
    {
        return $this->parameterize;
    }//end getParametized()

    /**
     * Set whether or not to paramatize the value when creating a prepared
     * statement.
     *
     * @param bool $parameterize True if set to parameterize.
     *
     * @return void
     */
    public function setParametize(bool $parameterize): void
    {
        $this->parameterize = $parameterize;
    }//end setParametize()

    /**
     * Return the value or empty string if null
     *
     * @return string
     */
    public function __toString()
    {
        if (is_null($this->value)) {
            return 'NULL';
        }
        return $this->value;
    }//end __toString()

    /**
     * Return a DOMDocument Node
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode|\DOMComment
     */
    public function toDomNode(\DOMDocument $dom)
    {
        if (is_string($this->value)) {
            $node = $dom->createElement('value', $this->value);
            $node->setAttribute('type', $this->type);
            if (is_string($this->subtype)) {
                $node->setAttribute('subtype', $this->subtype);
            }
            if (isset($this->signed) && $this->signed) {
                $node->setAttribute('signed', 'signed');
            }
        } else {
            $node = $dom->createElement('value');
            $node->setAttribute('type', 'null');
        }
        return $node;
    }//end toDomNode()

    /**
     * Replace value with placeholder and add value to array
     *
     * TODO - determine quotes needed for non-parameterized
     *        non-integer values.
     *
     * @param array $valueArray   The array of values.
     * @param string $placeholder The placeholder.
     *
     * @return string
     */
    public function toParameterized(array &$valueArray, string $placeholder='?'): string
    {
        //in theory null values are to be handled by null object.
        if (! is_null($this->getValue())) {
            if ($this->parameterize) {
                $valueArray[] = $this->__toString();
                return $placeholder;
            } else {
                // TODO - more subtypes need the ()
                switch ($this->subtype) {
                    case 'BIT':
                        return '(' . $this->__toString() . ')';
                        break;
                    default:
                        // integer subtypes do not need () encasing
                        return $this->__toString();
                        break;
                }
            }
        }
        // TODO error ??
        return '';
    }//end toParameterized()

    /**
     * Set the value
     *
     * @param string $value        The value to set.
     * @param null|string $subtype Optional. The subtype to set.
     *
     * @return bool True on success.
     *
     * @throws \InvalidArgumentException
     */
    abstract public function setValue(string $value, $subtype = null): bool;
}//end class

?>