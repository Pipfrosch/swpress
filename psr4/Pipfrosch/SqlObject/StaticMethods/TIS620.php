<?php
declare(strict_types=1);

/**
 * Class for converting to and from TIS-620 / ISO-8859-11 / Windows-874
 *
 * DISCLAIMER: I am not from Thailand and do not speak / read the language.
 *  However I am aware that in UTF-8 each Thai character requires 3 bytes, but in the 8-bit encodings
 *  that exist, each Thai character only requires 1 byte. So if your content is primarily Thai, it
 *  may make more sense to use a Thai 8-bit encoding.
 *
 * TIS-620 is the government sanctioned official character encoding for Thailand.
 * ISO-8859-11 is identical to TI-620 except it adds the non-breaking space character.
 * Windows-874 is identical to ISO-8859-11 except it adds the En-dash, Em-dash, curly-quotes,
 *  (single and double), Bullet, Horizontal Ellipsis, and the Euro.
 *
 * I believe all three work with the MariaDB 'tis620' character encoding but I have not yet tried.
 *
 * This class has dependency on \Pipfrosh\SqObjects\EmojiEntity which requires PHP 7.2+ for the mb_ord()
 * function. Load both classes:
 *
 *    use \Pipfrosch\SqlObjects\EmojiEntity
 *    use \Pipfrosch\SqlObjects\TIS620
 *
 *  NOTE: The EmojiEntity may not properly handle some emojis with skin tone modifiers, that will be fixed.
 *
 * By default, this class uses Windows-874 for Thai.
 * If instead you want strict TIS-620 without support for the Non-Breaking space character, then before
 *  calling this class, your web application should have
 *
 *    define('TIS620_ENCODE_MODE', 'strict');
 *
 * On the other hand if you want ISO-8859-11 (same as TIS-620 but with Non-Breaking Space), then instead:
 *
 *    define('TIS620_ENCODE_MODE', 'iso-8859-11');
 *
 *
 * The static method `encodeTo8bit()` is used to encode a UTF-8 string to a Thai 8-bit encoding.
 *
 *    TIS620::encodeTo8bit($yourUtf8String);
 *
 * When the only argument is a string, it will return false if the string contains unicode characters
 *  not directly supported by the Thai 8-bit encoding.
 *
 * When a second argument exists and is set to `true` it will convert Emojis and common Unicode characters
 *  that are not directly supported by the Thai 8-bit encoding into an XML entity, allowing the string to
 *  be encoded. It will return false if there are still other characters left that are not supported by the
 *  Thai 8-bit encoding.
 *
 *    $newstring = TIS620::encodeTo8bit($yourUtf8String, true);
 *
 * When a third argument exists and is set to 'true` then any unicode characters not supported by the
 *  Thai 8-bit encoding are converted to a ? character.
 *
 *    $newstring = TIS620::encodeTo8bit($yourUtf8String, true, true);
 *
 * The static method `encodeToUTF8()` will convert an 8-bit Thai string to UTF-8.
 *
 *    $newstring = TIS620::encodeToUTF8($your8bitString, true);
 *
 * @package    SqlObject
 * @subpackage StaticMethods
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject\StaticMethods;

/**
 * TIS620 / ISO 8859-11 class
 */
class TIS620
{
    /**
     * These characters are in Windows 874 but are not in ISO-8859-11 / TIS-620
     * so they should be entity encoded when not using Windows 874
     *
     * @var array
     */
    protected static $onlyInWindows874 = array(
        '/\x{2013}/u'    => '&#x2013;', // &ndash;  En Dash
        '/\x{2014}/u'    => '&#x2014;', // &mdash;  Em Dash
        '/\x{2018}/u'    => '&#x2018;', // &lsquo;  Left single curly quote
        '/\x{2019}/u'    => '&#x2019;', // &rsquo;  Right single curly quote
        '/\x{2022}/u'    => '&#x2022;', // &bull;   Bullet
        '/\x{2026}/u'    => '&#x2026;', // &hellip; Horizontal ellipsis
        '/\x{201C}/u'    => '&#x201C;', // &ldquo;  Left double curly quote
        '/\x{201D}/u'    => '&#x201D;', // &rdquo;  Right double curly quote
        '/\x{20AC}/u'    => '&#x20AC;', // &euro;   Euro
    );

    /**
     * Commonly used Unicode characters from HTML 4.1 that are outside Thai range.
     * This should ONLY be applied to UTF-8 input, do not apply this to TIS-620.
     *
     * The reason I am converting to numbered entities is because they need to
     * remain compatible with generic XML.
     *
     * Other unicode characters that are commonly used with Thai can and should
     * be added, please keep alphabetical order and comment them after the comma
     * to note what they are.
     *
     * Emojis are handled differently with different static class.
     *
     * @var array
     */
    protected static $entityConversion = array(
        '/\x{00A1}/u'    => '&#x00A1;', // &iexcl;  Inverted exclamation mark
        '/\x{00A2}/u'    => '&#x00A2;', // &cent;   Cent
        '/\x{00A3}/u'    => '&#x00A3;', // &pound;  Pound
        '/\x{00A4}/u'    => '&#x00A4;', // &curren; Currency
        '/\x{00A5}/u'    => '&#x00A5;', // &yen;    Yen
        '/\x{00A6}/u'    => '&#x00A6;', // &brvbar; Broken vertical bar
        '/\x{00A7}/u'    => '&#x00A7;', // &sect;   Section
        '/\x{00A8}/u'    => '&#x00A8;', // &uml;    Spacing diaeresis
        '/\x{00A9}/u'    => '&#x00A9;', // &copy;   Copyright
        '/\x{00AA}/u'    => '&#x00AA;', // &ordf;   Feminine ordinal indicator
        '/\x{00AB}/u'    => '&#x00AB;', // &laquo;  Opening/Left angle quotation mark
        '/\x{00AC}/u'    => '&#x00AC;', // &not;    Negation
        '/\x{00AD}/u'    => '&#x00AD;', // &shy;    Soft hyphen
        '/\x{00AE}/u'    => '&#x00AE;', // &reg;    Registered trademark
        '/\x{00AF}/u'    => '&#x00AF;', // &macr;   Spacing macron
        '/\x{00B0}/u'    => '&#x00B0;', // &deg;    Degree
        '/\x{00B1}/u'    => '&#x00B1;', // &plusmn; Plus or minus
        '/\x{00B2}/u'    => '&#x00B2;', // &sup2;   Superscript 2
        '/\x{00B3}/u'    => '&#x00B3;', // &sup3;   Superscript 3
        '/\x{00B4}/u'    => '&#x00B4;', // &acute;  Spacing acute
        '/\x{00B5}/u'    => '&#x00B5;', // &micro;  Micro
        '/\x{00B6}/u'    => '&#x00B6;', // &para;   Paragraph
        '/\x{00B8}/u'    => '&#x00B8;', // &cedil;  Spacing cedilla
        '/\x{00B9}/u'    => '&#x00B9;', // &sup1;   Superscript 1
        '/\x{00BA}/u'    => '&#x00BA;', // &ordm;   Masculine ordinal indicator
        '/\x{00BB}/u'    => '&#x00BB;', // &raquo;  Closing/Right angle quotation mark
        '/\x{00BC}/u'    => '&#x00BC;', // &frac14; Fraction 1/4
        '/\x{00BD}/u'    => '&#x00BD;', // &frac12; Fraction 1/2
        '/\x{00BE}/u'    => '&#x00BE;', // &frac34; Fraction 3/4
        '/\x{00BF}/u'    => '&#x00BF;', // &iquest; Inverted question mark
        '/\x{00C0}/u'    => '&#x00C0;', // &Agrave; Capital A with grave accent
        '/\x{00C1}/u'    => '&#x00C1;', // &Aacute; Capital A with acute accent
        '/\x{00C2}/u'    => '&#x00C2;', // &Acirc;  Capital A with circumflex accent
        '/\x{00C3}/u'    => '&#x00C3;', // &Atilde; Capital A with tilde
        '/\x{00C4}/u'    => '&#x00C4;', // &Auml;   Capital A with umlaut
        '/\x{00C5}/u'    => '&#x00C5;', // &Aring;  Capital A with ring
        '/\x{00C6}/u'    => '&#x00C6;', // &AElig;  Capital AE ligature
        '/\x{00C7}/u'    => '&#199;',
        '/\x{00C8}/u'    => '&#200;',
        '/\x{00C9}/u'    => '&#201;',
        '/\x{00CA}/u'    => '&#202;',
        '/\x{00CB}/u'    => '&#203;',
        '/\x{00CC}/u'    => '&#204;',
        '/\x{00CD}/u'    => '&#205;',
        '/\x{00CE}/u'    => '&#206;',
        '/\x{00CF}/u'    => '&#207;',
        '/\x{00D0}/u'    => '&#208;',
        '/\x{00D1}/u'    => '&#209;',
        '/\x{00D2}/u'    => '&#210;',
        '/\x{00D3}/u'    => '&#211;',
        '/\x{00D4}/u'    => '&#212;',
        '/\x{00D5}/u'    => '&#213;',
        '/\x{00D6}/u'    => '&#214;',
        '/\x{00D8}/u'    => '&#216;',
        '/\x{00D9}/u'    => '&#217;',
        '/\x{00DA}/u'    => '&#218;',
        '/\x{00DB}/u'    => '&#219;',
        '/\x{00DC}/u'    => '&#220;',
        '/\x{00DD}/u'    => '&#221;',
        '/\x{00DE}/u'    => '&#222;',
        '/\x{00DF}/u'    => '&#223;',
        '/\x{00E0}/u'    => '&#224;',
        '/\x{00E1}/u'    => '&#225;',
        '/\x{00E2}/u'    => '&#226;',
        '/\x{00E3}/u'    => '&#227;',
        '/\x{00E4}/u'    => '&#228;',
        '/\x{00E5}/u'    => '&#229;',
        '/\x{00E6}/u'    => '&#230;',
        '/\x{00E7}/u'    => '&#231;',
        '/\x{00E8}/u'    => '&#232;',
        '/\x{00E9}/u'    => '&#233;',
        '/\x{00EA}/u'    => '&#234;',
        '/\x{00EB}/u'    => '&#235;',
        '/\x{00EC}/u'    => '&#236;',
        '/\x{00ED}/u'    => '&#237;',
        '/\x{00EE}/u'    => '&#238;',
        '/\x{00EF}/u'    => '&#239;',
        '/\x{00F0}/u'    => '&#240;',
        '/\x{00F1}/u'    => '&#241;',
        '/\x{00F2}/u'    => '&#242;',
        '/\x{00F3}/u'    => '&#243;',
        '/\x{00F4}/u'    => '&#244;',
        '/\x{00F5}/u'    => '&#245;',
        '/\x{00F6}/u'    => '&#246;',
        '/\x{00F8}/u'    => '&#248;',
        '/\x{00F9}/u'    => '&#249;',
        '/\x{00FA}/u'    => '&#250;',
        '/\x{00FB}/u'    => '&#251;',
        '/\x{00FC}/u'    => '&#252;',
        '/\x{00FD}/u'    => '&#253;',
        '/\x{00FE}/u'    => '&#254;',
        '/\x{00FF}/u'    => '&#255;',
        '/\x{0152}/u'    => '&#338;',
        '/\x{0153}/u'    => '&#339;',
        '/\x{0160}/u'    => '&#352;',
        '/\x{0161}/u'    => '&#353;',
        '/\x{0178}/u'    => '&#376;',
        '/\x{0192}/u'    => '&#402;',
        '/\x{02C6}/u'    => '&#710;',
        '/\x{02DC}/u'    => '&#732;',
        '/\x{0391}/u'    => '&#913;',
        '/\x{0392}/u'    => '&#914;',
        '/\x{0393}/u'    => '&#915;',
        '/\x{0394}/u'    => '&#916;',
        '/\x{0395}/u'    => '&#917;',
        '/\x{0396}/u'    => '&#918;',
        '/\x{0397}/u'    => '&#919;',
        '/\x{0398}/u'    => '&#920;',
        '/\x{0399}/u'    => '&#921;',
        '/\x{039A}/u'    => '&#922;',
        '/\x{039B}/u'    => '&#923;',
        '/\x{039C}/u'    => '&#924;',
        '/\x{039D}/u'    => '&#925;',
        '/\x{039E}/u'    => '&#926;',
        '/\x{039F}/u'    => '&#927;',
        '/\x{03A0}/u'    => '&#928;',
        '/\x{03A1}/u'    => '&#929;',
        '/\x{03A3}/u'    => '&#931;',
        '/\x{03A4}/u'    => '&#932;',
        '/\x{03A5}/u'    => '&#933;',
        '/\x{03A6}/u'    => '&#934;',
        '/\x{03A7}/u'    => '&#935;',
        '/\x{03A8}/u'    => '&#936;',
        '/\x{03A9}/u'    => '&#937;',
        '/\x{03B1}/u'    => '&#945;',
        '/\x{03B2}/u'    => '&#946;',
        '/\x{03B3}/u'    => '&#947;',
        '/\x{03B4}/u'    => '&#948;',
        '/\x{03B5}/u'    => '&#949;',
        '/\x{03B6}/u'    => '&#950;',
        '/\x{03B7}/u'    => '&#951;',
        '/\x{03B8}/u'    => '&#952;',
        '/\x{03B9}/u'    => '&#953;',
        '/\x{03BA}/u'    => '&#954;',
        '/\x{03BB}/u'    => '&#955;',
        '/\x{03BC}/u'    => '&#956;',
        '/\x{03BD}/u'    => '&#957;',
        '/\x{03BE}/u'    => '&#958;',
        '/\x{03BF}/u'    => '&#959;',
        '/\x{03C0}/u'    => '&#960;',
        '/\x{03C1}/u'    => '&#961;',
        '/\x{03C2}/u'    => '&#962;',
        '/\x{03C3}/u'    => '&#963;',
        '/\x{03C4}/u'    => '&#964;',
        '/\x{03C5}/u'    => '&#965;',
        '/\x{03C6}/u'    => '&#966;',
        '/\x{03C7}/u'    => '&#967;',
        '/\x{03C8}/u'    => '&#968;',
        '/\x{03C9}/u'    => '&#969;',
        '/\x{03D1}/u'    => '&#977;',
        '/\x{03D2}/u'    => '&#978;',
        '/\x{03D6}/u'    => '&#982;',
        '/\x{2002}/u'    => '&#8194;',
        '/\x{2003}/u'    => '&#8195;',
        '/\x{2009}/u'    => '&#8201;',
        '/\x{200C}/u'    => '&#8204;',
        '/\x{200D}/u'    => '&#8205;',
        '/\x{200E}/u'    => '&#8206;',
        '/\x{200F}/u'    => '&#8207;',
        '/\x{201A}/u'    => '&#8218;',
        '/\x{201E}/u'    => '&#8222;',
        '/\x{2020}/u'    => '&#8224;',
        '/\x{2021}/u'    => '&#8225;',
        '/\x{2030}/u'    => '&#8240;',
        '/\x{2032}/u'    => '&#8242;',
        '/\x{2033}/u'    => '&#8243;',
        '/\x{2039}/u'    => '&#8249;',
        '/\x{203A}/u'    => '&#8250;',
        '/\x{203E}/u'    => '&#8254;',
        '/\x{2122}/u'    => '&#8482;',
        '/\x{2190}/u'    => '&#8592;',
        '/\x{2191}/u'    => '&#8593;',
        '/\x{2192}/u'    => '&#8594;',
        '/\x{2193}/u'    => '&#8595;',
        '/\x{2194}/u'    => '&#8596;',
        '/\x{21B5}/u'    => '&#8629;',
        '/\x{2200}/u'    => '&#8704;',
        '/\x{2202}/u'    => '&#8706;',
        '/\x{2203}/u'    => '&#8707;',
        '/\x{2205}/u'    => '&#8709;',
        '/\x{2207}/u'    => '&#8711;',
        '/\x{2208}/u'    => '&#8712;',
        '/\x{2209}/u'    => '&#8713;',
        '/\x{220B}/u'    => '&#8715;',
        '/\x{220F}/u'    => '&#8719;',
        '/\x{2211}/u'    => '&#8721;',
        '/\x{2212}/u'    => '&#8722;',
        '/\x{2217}/u'    => '&#8727;',
        '/\x{221D}/u'    => '&#8733;',
        '/\x{2220}/u'    => '&#8736;',
        '/\x{2227}/u'    => '&#8743;',
        '/\x{2228}/u'    => '&#8744;',
        '/\x{2229}/u'    => '&#8745;',
        '/\x{222A}/u'    => '&#8746;',
        '/\x{222B}/u'    => '&#8747;',
        '/\x{2234}/u'    => '&#8756;',
        '/\x{223C}/u'    => '&#8764;',
        '/\x{2245}/u'    => '&#8773;',
        '/\x{2248}/u'    => '&#8776;',
        '/\x{2260}/u'    => '&#8800;',
        '/\x{2261}/u'    => '&#8801;',
        '/\x{2264}/u'    => '&#8804;',
        '/\x{2265}/u'    => '&#8805;',
        '/\x{2282}/u'    => '&#8834;',
        '/\x{2283}/u'    => '&#8835;',
        '/\x{2284}/u'    => '&#8836;',
        '/\x{2286}/u'    => '&#8838;',
        '/\x{2287}/u'    => '&#8839;',
        '/\x{2295}/u'    => '&#8853;',
        '/\x{2297}/u'    => '&#8855;',
        '/\x{22A5}/u'    => '&#8869;',
        '/\x{22C5}/u'    => '&#8901;',
        '/\x{2308}/u'    => '&#8968;',
        '/\x{2309}/u'    => '&#8969;',
        '/\x{230A}/u'    => '&#8970;',
        '/\x{230B}/u'    => '&#8971;',
        '/\x{25CA}/u'    => '&#9674;',
        '/\x{2663}/u'    => '&#9827;',
        '/\x{2665}/u'    => '&#9829;',
        '/\x{2666}/u'    => '&#9830;'
    );

    /**
     * Returns the mode
     *
     * @return string
     */
    protected static function returnCharsetMode(): string
    {
        $mode = 'win874';
        if (defined('TIS620_ENCODE_MODE')) {
            $usermode = strtolower(TIS620_ENCODE_MODE);
            if ($usermode === 'strict') {
                $mode = 'strict';
            } elseif($usermode === 'iso-8859-11') {
                $mode = 'withnbsp';
            }
        }
        return $mode;
    }

    /**
     * For testing
     *
     * @return array
     */
    public static function returnEntityConversionArray(): array
    {
        $mode = self::returnCharsetMode();
        switch($mode) {
            case 'strict':
                $arr = array('/\x{00A0}/u' => '&#160;');
                return array_merge($arr, self::$onlyInWindows874, self::$entityConversion);
                break;
            case 'win874':
                return self::$entityConversion;
                break;
            default:
                return array_merge(self::$onlyInWindows874, self::$entityConversion);
        }
    }//end returnEntityConversionArray()

    /**
     * Encodes a valid UTF-8 string to Thai 8-bit
     *
     * @param string $string     The string to convert.
     * @param bool   $entities   Optional. When set to true, unicode characters outside the Thai range that
     *                           are common in HTML documents (such as curly quotes and copyright symbol)
     *                           will be converted to XML safe numbered entities allowing the encoding to
     *                           take place.
     * @param bool   $compensate Optional. When set to true, characters outside of ISO 8859-11 that were
     *                           not converted to entities are converted to entity for a white square.
     *
     * @return bool|string Returns converted string, or false on failure.
     */
    public static function encodeTo8bit(string $string, bool $entities = false, bool $compensate = false)
    {
        // Bug? Should range be 00-7F instead of 20-7E ?? check
        $mode = self::returnCharsetMode();
        switch($mode) {
            case 'strict':
                $ThaiUnicodeRangeCheck = '/[^\x{0000}-\x{007F}\x{0E01}-\x{0E3A}\x{0E3F}-\x{0E5B}]/u';
                $ThaiEightBitRangeCheck = '/[^\x20-\x7E\xA1-\xDA\xDF-\xFB]/';
                break;
            case 'win874':
                $ThaiUnicodeRangeCheck = '/[^\x{0000}-\x{007F}\x{0E01}-\x{0E3A}\x{0E3F}-\x{0E5B}\x{00A0}\x{2013}\x{2014}\x{2018}\x{2019}\x{2022}\x{2026}\x{201C}\x{201D}\x{20AC}]/u';
                $ThaiEightBitRangeCheck = '/[^\x20-\x7E\x80\x85\x91-\x97\xA0-\xDA\xDF-\xFB]/';
                break;
            default:
                $ThaiUnicodeRangeCheck = '/[^\x{0000}-\x{007F}\x{0E01}-\x{0E3A}\x{0E3F}-\x{0E5B}\x{00A0}]/u';
                $ThaiEightBitRangeCheck = '/[^\x20-\x7E\xA0-\xDA\xDF-\xFB]/';
        }
        // do not convert if already in proper proper range
        if (preg_match($ThaiEightBitRangeCheck, $string) === 0) {
            return $string;
        }
        // make sure what we have is UTF-8 and exit if not
        if (! mb_detect_encoding($string, 'UTF-8', true)) {
            return false;
        }
        // make sure all characters within Thai range
        if (preg_match($ThaiUnicodeRangeCheck, $string) !== 0) {
            if ($entities) {
                // Do emoji conversion first
                EmojiEntity::threeByteEncode($string);
                EmojiEntity::twoByteEncode($string);
                // Now common unicode characters that are not emoji
                $arr = self::returnEntityConversionArray();
                $s = array_keys($arr);
                $r = array_values($arr);
                $string = preg_replace($s, $r, $string);
                if (! is_string($string)) {
                    return false;
                }
            }
        }
        // check to see what's left
        if (preg_match($ThaiUnicodeRangeCheck, $string) !== 0) {
            if (! $compensate) {
                return false;
            }
            $string = preg_replace($ThaiUnicodeRangeCheck, '?', $string);
            if (! is_string($string)) {
                return false;
            }
        }
        // TIS-120 and ISO-85591-11 are also valid WIN-874 and we verified covered characters so
        //  just always use WIN-874 as target encoding.
        $converted = iconv("UTF-8", "WINDOWS-874", $string);
        if (is_string($converted)) {
            return $converted;
        }
        return false;
    }//end encodeTo8bit()

    /**
     * Encodes a ISO-8859-11 string to UTF-8.
     *
     * Since all valid TIS-620 and ISO-85591-11 string are valid Win-874 strings, this
     * function always specifies the Windows-874 charset as source charset.
     *
     * @param string $string The string to convert.
     *
     * @return bool|string Returns converted string, or false on failure.
     */
    public static function encodeToUTF8(string $string)
    {
        if (empty($string)) {
            return '';
        }
        $ThaiEightBitRangeCheck = '/[^\x20-\x7E\x80\x85\x91-\x97\xA0-\xDA\xDF-\xFB]/';
        if (preg_match($ThaiEightBitRangeCheck, $string) !== 0) {
            return false;
        }
        $converted = iconv("WINDOWS-874", "UTF-8", $string);
        if (is_string($converted) && ! empty($converted)) {
            return $converted;
        }
        return false;
    }//end encodeToUTF8()
}//end class

?>