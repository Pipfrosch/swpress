<?php
declare(strict_types=1);

/**
 * Class for converting emoji charaters to XML entities.
 *
 * Motivation for this class is to allow Emojis to be used with databases that have
 * a charset that only supports 1 or 2 byte unicode codepoints. If you use a charset
 * that supports 4 byte unicode codepoints (like utf8mb4 with MariaDB) then this class
 * is not needed.
 *
 * It requires PHP 7.2+ for the mb_ord() function which allows for radically better
 * performance than preg_replace to convert emoji characters to entities.
 *
 * A unit test to test every single emoji in Unicode v12.0 is in
 *  tests/SqlObject/EmojiEncodeTest.php
 *
 * For MariaDB, the utf8 charset only supports 2 byte unicode codepoints, you MUST use
 * utf8b4 to insert 3 byte emoji codepoints directly into the database. If your database
 * uses utf8 and but not the utf8mb4 variant, or if you are using a 1 byte character
 * encoding (such as latin1 or tis620) then emojis have to be converted to entities.
 *
 * When the database charset does not support 3 byte unicode codepoints, the
 *   threeByteEncode() static method should be run on the string before insert.
 *
 * That function will convert any 3 (or 4) byte codepoint into XML numbered entities.
 * Since it does not distinguish between Emoji and non-Emoji for 3/4 byte codepoints,
 * it can use mb_ord to get the number for the entity which makes it very fast. It does
 * however have to use preg_replace to check for some multi-character emoji sequences
 * that contain a u200D and sometimes a uFE0F.
 *
 * When the database charset also does not support 2 byte unicode codepoints (e.g.
 * the commonly used latin1 charset) then also run the
 *   twoByteEncode() static method on the string, but due to the way preg_replace
 * works and the fact that some Emojis are compound, run the threeByteEncode() first.
 *
 * For the two-byte function, by default it does not convert the few emojis that are
 * common enough they also have HTML 4.1 named entities. This is because they may be
 * included in some 8-bit charsets. If you want them encoded, pass an argument of
 * `true` to the two-byte function.
 *
 * Both functions check the string first to see if it contains characters that are
 * 3 bytes or 2 bytes respectively before applying the regular expressions to them,
 * as it is a waste of resources if the string does not contain emojis.
 *
 * Both functions output void, they modify the input argument.
 *
 * @package    SqlObject
 * @subpackage StaticMethods
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject\StaticMethods;

/**
 * Emoji to Entity static methods
 */
class EmojiEntity
{
    /**
     * These are characters common enough they have HTML 4.1 named entities
     *  and thus *may* be defined in some 8-bit charsets. I believe only the
     *  copyright and registered are, but trademark may be in some and it is
     *  possible the card suites are common enough to be in some.
     *
     * @var array
     */
    protected static $htmlNamedEntityOverlap = array(
        '00A9', // Copyright
        '00AE', // Registered
        '2122', // Trade mark
        '2660', // Spade suit
        '2663', // Club suit
        '2665', // Heart suit
        '2666', // Diamond suit
    );

    /**
     * Array of two-byte hex codes to convert if found.
     *
     * @var array
     */
    protected static $twoByteHex = array(
        '203C', // Double exclamation mark
        '2049', // Exclamation question mark
        '2139', // Information
        '2194', // Left-Right arrow
        '2195', // Up-Down arrow
        '2196', // Up-left arrow
        '2197', // Up-right arrow
        '2198', // Down-right arrow
        '2199', // Down-left arrow
        '213A', //   FIXME ??? https://unicode-table.com/en/213A/
        '213B', //   FIXME ??? https://unicode-table.com/en/213B/
        '21A9', // Right arrow curving left
        '21AA', // Left arrow curving right
        '231A', // Watch
        '231B', // Hourglass done
        '2328', // Keyboard
        '23C2', //   FIXME ??? https://unicode-table.com/en/23C2/
        '23CF', // Eject button
        '23E9', // Fast-forward button
        '23EA', // Fast-reverse button
        '23EB', // Fast-up button
        '23EC', // Fast-down button
        '23ED', // Next track button
        '23EE', // Last track button
        '23EF', // Play or pause button
        '23F0', // Alarm clock
        '23F1', // Stopwatch
        '23F2', // Timer clock
        '23F3', // Hourglass not done
        '23F8', // Pause button
        '23F9', // Stop button
        '23FA', // Record button
        '24C2', // Circled M
        '25AA', // Black small square
        '25AB', // White small square
        '25B6', // Play button
        '25C0', // Reverse button
        '25FB', // White medium square
        '25FC', // Black medium square
        '25FD', // White medium-small square
        '25FE', // Black medium-small square
        '2600', // Sun
        '2601', // Cloud
        '2602', // Umbrella
        '2603', // Snowman
        '2604', // Comet
        '260E', // Telephone
        '2611', // Check box with check
        '2614', // Umbrella with rain drops
        '2615', // Hot beverage
        '2618', // Shamrock
        '261D', // Index finger pointing up
        '2620', // Skull and crossbones
        '2622', // Radioactive
        '2623', // Biohazard
        '2626', // Orthodox cross
        '262A', // Star and crescent
        '262E', // Peace symbol
        '262F', // Yin yang
        '2638', // Wheel of Dharma
        '2639', // Frowning face
        '263A', // Smiling face
        '2640', // Female sign
        '2642', // Male sign
        '2648', // Aries
        '2649', // Taurus
        '264A', // Gemini
        '264B', // Cancer
        '264C', // Leo
        '264D', // Virgo
        '264E', // Libra
        '264F', // Scorpio
        '2650', // Sagittarius
        '2651', // Capricorn
        '2652', // Aquarius
        '2653', // Pisces
        '265F', // Chess pawn
        '2668', // Hot springs
        '267B', // Recycling symbol
        '267E', // Infinity
        '267F', // Wheelchair symbol
        '2692', // Hammer and pick
        '2693', // Anchor
        '2694', // Crossed swords
        '2695', // Medical symbol
        '2696', // Balance scale
        '2697', // Alembic
        '2699', // Gear
        '269B', // Atom symbol
        '269C', // Fleur-de-lis
        '26A0', // Warning
        '26A1', // High-voltage
        '26AA', // White circle
        '26AB', // Black circle
        '26B0', // Coffin
        '26B1', // Funeral urn
        '26BD', // Soccer ball
        '26BE', // Baseball
        '26C4', // Snowman without snow
        '26C5', // Sun behind cloud
        '26C8', // Cloud with lightening and rain
        '26CE', // Ophiuchus
        '26CF', // Pick
        '26D1', // Rescue worker's helmet
        '26D3', // Chains
        '26D4', // No entry
        '26E9', // Shinto shrine
        '26EA', // Church
        '26F0', // Mountain
        '26F1', // Umbrella on ground
        '26F2', // Fountain
        '26F3', // Flag in hole
        '26F4', // Ferry
        '26F5', // Sailboat
        '26F7', // Skier
        '26F8', // Ice skate
        '26F9', // Person bouncing ball
        '26FA', // Tent
        '26FD', // Fuel pump
        '2702', // Scissors
        '2705', // Check mark button
        '2708', // Airplane
        '2709', // Envelope
        '270A', // Raised fist
        '270B', // Raised hand
        '270C', // Victory hand
        '270D', // Writing hand
        '270F', // Pencil
        '2712', // Black nib
        '2714', // Check mark
        '2716', // Multiplication sign
        '271D', // Latin cross
        '2721', // Star of David
        '2728', // Sparkles
        '2733', // Eight-spoked asterisk
        '2734', // Eight-pointed star
        '2744', // Snowflake
        '2747', // Sparkle
        '274C', // Cross mark
        '274E', // Cross mark button
        '2753', // Question mark
        '2754', // White question mark
        '2755', // White exclamation mark
        '2757', // Exclamation mark
        '2763', // Heart exclamation
        '2764', // Red heart
        '2795', // Plus sign
        '2796', // Minus sign
        '2797', // Division sign
        '27A1', // Right arrow
        '27B0', // Curly loop
        '27BF', // Double curly loop
        '2934', // Right arrow curving up
        '2935', // Right arrow curving down
        '2B05', // Left arrow
        '2B06', // Up arrow
        '2B07', // Down arrow
        '2B1B', // Black large square
        '2B1C', // White large square
        '2B50', // Star
        '2B55', // Hollow red circle
        '3030', // Wavy dash
        '303D', // Part alteration mark
        '3297', // Japanese "congratulations" button
        '3299', // Japanese "secret" button
    );

    /**
     * Special case multi-sequence involving two and three byte codepoints but no u200D modifier.
     *
     * @var array
     */
    protected static $specialMultiSequence = array(
        //  Raised Hand
        '/\x{270B}\x{1F3FB}/u'                 => '&#x270B;&#x1F3FB;',                 // light
        '/\x{270B}\x{1F3FC}/u'                 => '&#x270B;&#x1F3FC;',                 // medium-light
        '/\x{270B}\x{1F3FD}/u'                 => '&#x270B;&#x1F3FD;',                 // medium
        '/\x{270B}\x{1F3FE}/u'                 => '&#x270B;&#x1F3FE;',                 // medium-dark
        '/\x{270B}\x{1F3FF}/u'                 => '&#x270B;&#x1F3FF;',                 // dark
        //  Victory Hand
        '/\x{270C}\x{1F3FB}/u'                 => '&#x270C;&#x1F3FB;',                 // light
        '/\x{270C}\x{1F3FC}/u'                 => '&#x270C;&#x1F3FC;',                 // medium-light
        '/\x{270C}\x{1F3FD}/u'                 => '&#x270C;&#x1F3FD;',                 // medium
        '/\x{270C}\x{1F3FE}/u'                 => '&#x270C;&#x1F3FE;',                 // medium-dark
        '/\x{270C}\x{1F3FF}/u'                 => '&#x270C;&#x1F3FF;',                 // dark
        //  index pointing up
        '/\x{261D}\x{1F3FB}/u'                 => '&#x261D;&#x1F3FB;',                 // light
        '/\x{261D}\x{1F3FC}/u'                 => '&#x261D;&#x1F3FC;',                 // medium-light
        '/\x{261D}\x{1F3FD}/u'                 => '&#x261D;&#x1F3FD;',                 // medium
        '/\x{261D}\x{1F3FE}/u'                 => '&#x261D;&#x1F3FE;',                 // medium-dark
        '/\x{261D}\x{1F3FF}/u'                 => '&#x261D;&#x1F3FF;',                 // dark
        //  raised fist
        '/\x{270A}\x{1F3FB}/u'                 => '&#x270A;&#x1F3FB;',                 // light
        '/\x{270A}\x{1F3FC}/u'                 => '&#x270A;&#x1F3FC;',                 // medium-light
        '/\x{270A}\x{1F3FD}/u'                 => '&#x270A;&#x1F3FD;',                 // medium
        '/\x{270A}\x{1F3FE}/u'                 => '&#x270A;&#x1F3FE;',                 // medium-dark
        '/\x{270A}\x{1F3FF}/u'                 => '&#x270A;&#x1F3FF;',                 // dark
        //  writing hand
        '/\x{270D}\x{1F3FB}/u'                 => '&#x270D;&#x1F3FB;',                 // light
        '/\x{270D}\x{1F3FC}/u'                 => '&#x270D;&#x1F3FC;',                 // medium-light
        '/\x{270D}\x{1F3FD}/u'                 => '&#x270D;&#x1F3FD;',                 // medium
        '/\x{270D}\x{1F3FE}/u'                 => '&#x270D;&#x1F3FE;',                 // medium-dark
        '/\x{270D}\x{1F3FF}/u'                 => '&#x270D;&#x1F3FF;',                 // dark
        // bouncing ball
        '/\x{26F9}\x{1F3FB}/u'                 => '&#x26F9;&#x1F3FB;',                 //light
        '/\x{26F9}\x{1F3FC}/u'                 => '&#x26F9;&#x1F3FC;',                 //medium-light
        '/\x{26F9}\x{1F3FD}/u'                 => '&#x26F9;&#x1F3FD;',                 //medium
        '/\x{26F9}\x{1F3FE}/u'                 => '&#x26F9;&#x1F3FE;',                 //medium-dark
        '/\x{26F9}\x{1F3FF}/u'                 => '&#x26F9;&#x1F3FF;',                 //dark
    );

    /**
     * We do NOT want to mess with u200D or uFE0F characters that are not part of an
     * emoji sequence or we could break non-emoji languages that use them. So multi-
     * sequence emojis need special handling.
     *
     * @var array
     */
    protected static $multiSequence = array(
        // These have a u200D + emoji + u200D
        '/\x{200D}\x{1F91D}\x{200D}/u'                     => '&#x200D;&#x1F91D;&#x200D;',
        // Couples Kissing
        '/\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F48B}\x{200D}/u' => '&#x200D;&#x2764;&#xFE0F;&#x200D;&#x1F48B;&#x200D;',
        // Couples with heart
        '/\x{200D}\x{2764}\x{FE0F}\x{200D}/u' => '&#x200D;&#x2764;&#xFE0F;&#x200D;',
        // person generic - used with skin tones
        '/\x{200D}\x{2695}\x{FE0F}/u'          => '&#x200D;&#x2695;&#xFE0F;',          // medical
        '/\x{200D}\x{2696}\x{FE0F}/u'          => '&#x200D;&#x2696;&#xFE0F;',          // judge
        '/\x{200D}\x{2708}\x{FE0F}/u'          => '&#x200D;&#x2708;&#xFE0F;',          // pilot
        // Generic
        '/\x{FE0F}\x{200D}\x{2640}\x{FE0F}/u'  => '&#xFE0F;&#x200D;&#x2640;&#xFE0F;',  // female
        '/\x{FE0F}\x{200D}\x{2642}\x{FE0F}/u'  => '&#xFE0F;&#x200D;&#x2642;&#xFE0F;',  // male
        '/\x{200D}\x{2640}\x{FE0F}/u'          => '&#x200D;&#x2640;&#xFE0F;',          // female
        '/\x{200D}\x{2642}\x{FE0F}/u'          => '&#x200D;&#x2642;&#xFE0F;',          // male
        '/\x{1F468}\x{200D}/u'                 => '&#x1F468;&#x200D;',                 // male
        '/\x{1F469}\x{200D}/u'                 => '&#x1F469;&#x200D;',                 // female
        '/\x{1F466}\x{200D}/u'                 => '&#x1F466;&#x200D;',                 // boy
        '/\x{1F467}\x{200D}/u'                 => '&#x1F467;&#x200D;',                 // girl
        // Emoji + u200D + Emoji
        '/\x{1F415}\x{200D}\x{1F9BA}/u' => '&#x1F415;&#x200D;&#x1F9BA;', // service dog
        // These have a u200D + emoji + uFE0F so manual in order
        // @codingStandardsIgnoreLine
        '/\x{1F441}\x{FE0F}\x{200D}\x{1F5E8}\x{FE0F}/u' => '&#x1F441;&#xFE0F;&#x200D;&#x1F5E8;&#xFE0F;', // Eye in speech bubble
        // flag
        '/\x{1F3F4}\x{200D}\x{2620}\x{FE0F}/u' => '&#x1F3F4;&#x200D;&#x2620;&#xFE0F;', // Pirate flag
        '/\x{1F3F3}\x{FE0F}\x{200D}/u'         => '&#x1F3F3;&#xFE0F;&#x200D;',         // White flag with two modifiers
        // misc objects 200D+emoji
        '/\x{200D}\x{1F9AF}/u'                 => '&#x200D;&#x1F9AF;',                 // probing cane
        '/\x{200D}\x{1F9BC}/u'                 => '&#x200D;&#x1F9BC;',                 // motorized wheelchair
        // Generic skin tone
        '/\x{1F468}\x{1F3FB}\x{200D}/u'        => '&#x1F468;&#x1F3FB;&#x200D;',        // male light
        '/\x{1F468}\x{1F3FC}\x{200D}/u'        => '&#x1F468;&#x1F3FC;&#x200D;',        // male medium light
        '/\x{1F468}\x{1F3FD}\x{200D}/u'        => '&#x1F468;&#x1F3FD;&#x200D;',        // male medium
        '/\x{1F468}\x{1F3FE}\x{200D}/u'        => '&#x1F468;&#x1F3FE;&#x200D;',        // male medium dark
        '/\x{1F468}\x{1F3FF}\x{200D}/u'        => '&#x1F468;&#x1F3FF;&#x200D;',        // male dark
        '/\x{1F469}\x{1F3FB}\x{200D}/u'        => '&#x1F469;&#x1F3FB;&#x200D;',        // female light
        '/\x{1F469}\x{1F3FC}\x{200D}/u'        => '&#x1F469;&#x1F3FC;&#x200D;',        // female medium light
        '/\x{1F469}\x{1F3FD}\x{200D}/u'        => '&#x1F469;&#x1F3FD;&#x200D;',        // female medium
        '/\x{1F469}\x{1F3FE}\x{200D}/u'        => '&#x1F469;&#x1F3FE;&#x200D;',        // female medium dark
        '/\x{1F469}\x{1F3FF}\x{200D}/u'        => '&#x1F469;&#x1F3FF;&#x200D;',        // female dark
    );

    /**
     * Two Byte Emojis
     *
     * @param string $string The string to encode.
     * @param bool   $named  Optional. Include overlap with HTML 4.1 named entities.
     *                       Defaults to false.
     *
     * @return void
     */
    public static function twoByteEncode(string &$string, $named = false): void
    {
        if (preg_match('/[^\x{00}-\x{FF}]/u', $string) === 0) {
            // no two-byte codepoints
            return;
        }
        $test = preg_replace('/\x{FE0F}\x{20E3}/u', '&#xFE0F;&#x20E3;', $string);
        $test = preg_replace_callback(
            '/[^\x{00}-\x{FF}]/u',
            /**
             * Converts 2 byte emoji to hex entity
             *
             * @param array $m Array of match.
             *
             * @return string The entity.
             */
            function (array $m):string {
                $uni = strtoupper(dechex(mb_ord($m[0], 'utf8')));
                if (in_array($uni, self::$twoByteHex)) {
                    return '&#x' . $uni . ';';
                }
                return $m[0];
            },
            $test
        );
        if ($named) {
            $test = preg_replace_callback(
                '/[^\x{00}-\x{7F}]/u',
                /**
                 * Converts 1 and 2 byte emojis that have HTML 4.1 named entity
                 * equivalents to hex entity
                 *
                 * @param array $m Array of match.
                 *
                 * @return string The entity.
                 */
                function (array $m):string {
                    $uni = strtoupper(dechex(mb_ord($m[0], 'utf8')));
                    $uni = str_pad($uni, 4, '0', STR_PAD_LEFT);
                    if (in_array($uni, self::$htmlNamedEntityOverlap)) {
                        return '&#x' . $uni . ';';
                    }
                    return $m[0];
                },
                $test
            );
        }
        if (is_string($test)) {
            $string = $test;
        }
    }//end twoByteEncode()

    /**
     * Three Byte Emojis
     *
     * @param string $string The string to encode.
     *
     * @return void
     */
    public static function threeByteEncode(string &$string): void
    {
        if (preg_match('/[^\x{0000}-\x{FFFF}]/u', $string) === 0) {
            // no three or four byte characters
            return;
        }
    
        if (preg_match('/\x{200D}/u', $string) !== 0) {
            $arr = array_merge(self::$specialMultiSequence, self::$multiSequence);
        } else {
            $arr = self::$specialMultiSequence;
        }
        $s = array_keys($arr);
        $r = array_values($arr);
        $partial = preg_replace($s, $r, $string);
        $test = preg_replace_callback(
            '/[^\x{0000}-\x{FFFF}]/u',
            /**
             * Converts any > 2byte character to hex entity
             *
             * @param array $m Array of match.
             *
             * @return string The entity.
             */
            function (array $m):string {
                $uni = strtoupper(dechex(mb_ord($m[0], 'utf8')));
                return '&#x' . $uni . ';';
            },
            $partial
        );
        if (is_string($test)) {
            $string = $test;
        }
    }//end threeByteEncode()
}//end class

?>