<?php
declare(strict_types=1);

/**
 * Class primarily for very large arrays that do not need to be updated very often.
 *
 * Most of the raw data in this class will come from the MariaDB knowledge base.
 *  See the license info in the PHPDoc comments for the licenses associated with that
 *  data.
 *
 * @package    SqlObject
 * @subpackage StaticMethods
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject\StaticMethods ;

/**
 * The Constants Identifier Class
 */
class Constants
{
    /**
     * Array of reserved keywords that require quoting if used as identifier
     *
     * Please keep in alphabetic order and note the MariaDB version for additions.
     *
     * @see     https://mariadb.com/kb/en/library/reserved-words/
     * @author  MariaDB <sales-nam@mariadb.com>
     * @license https://creativecommons.org/licenses/by-sa/3.0/ CC-BY-SA
     * @license http://www.gnu.org/copyleft/fdl.html GFDL
     *
     * @var array
     */
    protected static $reservedKeywords = array(
        'ACCESSIBLE',
        'ADD',
        'ALL',
        'ALTER',
        'ANALYZE',
        'AND',
        'AS',
        'ASC',
        'ASENSITIVE',
        'BEFORE',
        'BETWEEN',
        'BIGINT',
        'BINARY',
        'BLOB',
        'BOTH',
        'BY',
        'CALL',
        'CASCADE',
        'CASE',
        'CHANGE',
        'CHAR',
        'CHARACTER',
        'CHECK',
        'COLLATE',
        'COLUMN',
        'CONDITION',
        'CONSTRAINT',
        'CONTINUE',
        'CONVERT',
        'CREATE',
        'CROSS',
        'CURRENT_DATE',
        'CURRENT_TIME',
        'CURRENT_TIMESTAMP',
        'CURRENT_USER',
        'CURSOR',
        'DATABASE',
        'DATABASES',
        'DAY_HOUR',
        'DAY_MICROSECOND',
        'DAY_MINUTE',
        'DAY_SECOND',
        'DEC',
        'DECIMAL',
        'DECLARE',
        'DEFAULT',
        'DELAYED',
        'DELETE',
        'DESC',
        'DESCRIBE',
        'DETERMINISTIC',
        'DISTINCT',
        'DISTINCTROW',
        'DIV',
        'DOUBLE',
        'DROP',
        'DUAL',
        'EACH',
        'ELSE',
        'ELSEIF',
        'ENCLOSED',
        'ESCAPED',
        'EXCEPT',
        'EXISTS',
        'EXIT',
        'EXPLAIN',
        'FALSE',
        'FETCH',
        'FLOAT',
        'FLOAT4',
        'FLOAT8',
        'FOR',
        'FORCE',
        'FOREIGN',
        'FROM',
        'FULLTEXT',
        'GENERAL', // Added in MariaDB 5.5
        'GRANT',
        'GROUP',
        'HAVING',
        'HIGH_PRIORITY',
        'HOUR_MICROSECOND',
        'HOUR_MINUTE',
        'HOUR_SECOND',
        'IF',
        'IGNORE',
        'IGNORE_SERVER_IDS', // Added in MariaDB 5.5
        'IN',
        'INDEX',
        'INFILE',
        'INNER',
        'INOUT',
        'INSENSITIVE',
        'INSERT',
        'INT',
        'INT1',
        'INT2',
        'INT3',
        'INT4',
        'INT8',
        'INTEGER',
        'INTERSECT', // Added in MariaDB 10.3.0
        'INTERVAL',
        'INTO',
        'IS',
        'ITERATE',
        'JOIN',
        'KEY',
        'KEYS',
        'KILL',
        'LEADING',
        'LEAVE',
        'LEFT',
        'LIKE',
        'LIMIT',
        'LINEAR',
        'LINES',
        'LOAD',
        'LOCALTIME',
        'LOCALTIMESTAMP',
        'LOCK',
        'LONG',
        'LONGBLOB',
        'LONGTEXT',
        'LOOP',
        'LOW_PRIORITY',
        'MASTER_HEARTBEAT_PERIOD', // Added in MariaDB 5.5
        'MASTER_SSL_VERIFY_CERT',
        'MATCH',
        'MAXVALUE', // Added in MariaDB 5.5
        'MAXIMUMBLOB',
        'MEDIUMINT',
        'MEDIUMTEXT',
        'MIDDLEINT',
        'MINUTE_MICROSECOND',
        'MINUTE_SECOND',
        'MOD',
        'MODIFIES',
        'NATURAL',
        'NOT',
        'NO_WRITE_TO_BINLOG',
        'NULL',
        'NUMERIC',
        'ON',
        'OPTIMIZE',
        'OPTION',
        'OPTIONALLY',
        'OR',
        'ORDER',
        'OUT',
        'OUTER',
        'OUTFILE',
        'OVER', // Added in MariaDB 10.2.0
        'PARTITION', // Added in MariaDB 10.0
        'PRECISION',
        'PRIMARY',
        'PROCEDURE',
        'PURGE',
        'RANGE',
        'READ',
        'READS',
        'READ_WRITE',
        'REAL',
        'RECURSIVE', // Added in MariaDB 10.2.0
        'REFERENCES',
        'REGEXP',
        'RELEASE',
        'RENAME',
        'REPEAT',
        'REPLACE',
        'REQUIRE',
        'RESIGNAL', // Added in MariaDB 5.5
        'RESTRICT',
        'RETURN',
        'RETURNING', // Added in MariaDB 10.0.5
        'REVOKE',
        'RIGHT',
        'RLIKE',
        'ROWS', // Added in MariaDB 10.2.4
        'SCHEMA',
        'SCHEMAS',
        'SECOND_MICROSECOND',
        'SELECT',
        'SENSITIVE',
        'SEPARATOR',
        'SET',
        'SHOW',
        'SIGNAL', // Added in MariaDB 5.5
        'SLOW', // Added in MariaDB 5.5
        'SMALLINT',
        'SPATIAL',
        'SPECIFIC',
        'SQL',
        'SQLEXCEPTION',
        'SQLSTATE',
        'SQLWARNING',
        'SQL_BIG_RESULT',
        'SQL_CALC_FOUND_ROWS',
        'SQL_SMALL_RESULT',
        'SSL',
        'STARTING',
        'STRAIGHT_JON',
        'TABLE',
        'TERMINATED',
        'THEN',
        'TINYBLOB',
        'TINYINT',
        'TINYTEXT',
        'TO',
        'TRAILING',
        'TRIGGER',
        'TRUE',
        'UNDO',
        'UNION',
        'UNIQUE',
        'UNLOCK',
        'UNSIGNED',
        'UPDATE',
        'USAGE',
        'USE',
        'USING',
        'UTC_DATE',
        'UTC_TIME',
        'UTC_TIMESTAMP',
        'VALUES',
        'VARBINARY',
        'VARCHAR',
        'VARCHARACTER',
        'VARYING',
        'WHEN',
        'WHERE',
        'WHILE',
        'WINDOW', // Added on MariaDB 10.2.0
        'WITH',
        'WRITE',
        'XOR',
        'YEAR_MONTH',
        'ZEROFILL'
    );

    /**
     * Array of keywords that do not need to be quoted when used as identifiers
     *  for historic reasons, but probably should be.
     *
     * @see     https://mariadb.com/kb/en/library/reserved-words/
     * @author  MariaDB <sales-nam@mariadb.com>
     * @license https://creativecommons.org/licenses/by-sa/3.0/ CC-BY-SA
     * @license http://www.gnu.org/copyleft/fdl.html GFDL
     *
     * @var array
     */
    protected static $exceptionsReservedKeywords = array(
        'ACTION',
        'BIT',
        'DATE',
        'ENUM',
        'NO',
        'TEXT',
        'TIME',
        'TIMESTAMP'
    );

    /**
     * Array of reserved keywords from Oracle Mode
     *
     * Please keep in alphabetic order and note the MariaDB version for additions.
     *
     * @see     https://mariadb.com/kb/en/library/reserved-words/
     * @author  MariaDB <sales-nam@mariadb.com>
     * @license https://creativecommons.org/licenses/by-sa/3.0/ CC-BY-SA
     * @license http://www.gnu.org/copyleft/fdl.html GFDL
     *
     * @var array
     */
    protected static $oracleModeReservedKeywords = array(
        'BODY',
        'ELSIF',
        'GOTO',
        'HISTORY', // Restricted in MariaDB <= 10.3.6 only
        'PACKAGE',
        'PERIOD', // Restricted in MariaDB <= 10.3.6 only
        'RAISE',
        'ROWTYPE',
        'SYSTEM', // Restricted in MariaDB <= 10.3.6 only
        'SYSTEM_TIME', // Restricted in MariaDB <= 10.3.6 only
        'VERSIONING', // Restricted in MariaDB <= 10.3.6 only
        'WITHOUT' // Restricted in MariaDB <= 10.3.6 only
    );

    /**
     * Array of supported character sets in MariaDB with default collation
     *
     * Please keep in alphabetic order and note the MariaDB version for additions.
     *
     * @see     https://mariadb.com/kb/en/library/supported-character-sets-and-collations/
     * @author  MariaDB <sales-nam@mariadb.com>
     * @license https://creativecommons.org/licenses/by-sa/3.0/ CC-BY-SA
     * @license http://www.gnu.org/copyleft/fdl.html GFDL
     *
     * @var array
     */
    protected static $validCharsetToDefaultCollation = array(
        'ascii'    => 'ascii_general_ci',
        'armscii8' => 'armscii8_general_ci',
        'big5'     => 'big5_chinese_ci',
        'binary'   => 'binary',
        'cp850'    => 'cp850_general_ci',
        'cp852'    => 'cp852_general_ci',
        'cp866'    => 'cp866_general_ci',
        'cp932'    => 'cp932_japanese_ci',
        'cp1250'   => 'cp1250_general_ci',
        'cp1251'   => 'cp1251_general_ci',
        'cp1256'   => 'cp1256_general_ci',
        'cp1257'   => 'cp1257_general_ci',
        'dec8'     => 'dec8_swedish_ci',
        'cp850'    => 'cp850_general_ci',
        'euckr'    => 'euckr_korean_ci',
        'eucjpms'  => 'eucjpms_japanese_ci',
        'gb2312'   => 'gb2312_chinese_ci',
        'gbk'      => 'gbk_chinese_ci',
        'geostd8'  => 'geostd8_general_ci',
        'greek'    => 'greek_general_ci',
        'hebrew'   => 'hebrew_general_ci',
        'hp8'      => 'hp8_english_ci',
        'keybcs2'  => 'keybcs2_general_ci',
        'koi8r'    => 'koi8r_general_ci',
        'koi8u'    => 'koi8u_general_ci',
        'latin1'   => 'latin1_swedish_ci',
        'latin2'   => 'latin2_general_ci',
        'latin5'   => 'latin5_turkish_ci',
        'latin7'   => '	latin7_general_ci',
        'macce'    => 'macce_general_ci',
        'macroman' => 'macroman_general_ci',
        'sjis'     => 'sjis_japanese_ci',
        'swe7'     => 'swe7_swedish_ci',
        'tis620'   => 'tis620_thai_ci',
        'ucs2'     => 'ucs2_general_ci',
        'ujis'     => 'ujis_japanese_ci',
        'utf8'     => 'utf8_general_ci',
        'utf8mb4'  => 'utf8mb4_general_ci',
        'utf16'    => 'utf16_general_ci',
        'utf16le'  => 'utf16le_general_ci',
        'utf32'    => 'utf32_general_ci',
    );

    /**
     * Charset to PHP mb Encoding
     *
     * This needs work
     *
     * @var array
     */
    protected $charsetToMbEncoding = array(
        'ascii'    => 'ASCII',
        'armscii8' => 'ArmSCII-8',
        'big5'     => 'BIG-5',
        'binary'   => null,
        'cp850'    => 'CP850',
        'cp852'    => null, // see https://en.wikipedia.org/wiki/Code_page_852
        'cp866'    => 'CP866',
        'cp932'    => 'CP932',
        'cp1250'   => null, // see https://en.wikipedia.org/wiki/Windows-1250
        'cp1251'   => 'Windows-1251',
        'cp1256'   => null, // see https://en.wikipedia.org/wiki/Windows-1256
        'cp1257'   => null, // see https://en.wikipedia.org/wiki/Windows-1257
        'dec8'     => null, // see https://en.wikipedia.org/wiki/Multinational_Character_Set
        'cp850'    => 'CP850',
        'euckr'    => 'EUC-KR',
        'eucjpms'  => 'eucJP-win',
        'gb2312'   => 'EUC-CN',
        'gbk'      => 'GB18030', //?? this may not be correct
        'geostd8'  => null, // see https://tools.ietf.org/html/draft-giasher-geostd8-00
        'greek'    => 'ISO-8859-7', // see https://dev.mysql.com/doc/refman/5.5/en/charset-se-me-sets.html
        'hebrew'   => 'ISO-8859-8', // see https://dev.mysql.com/doc/refman/5.5/en/charset-se-me-sets.html
        'hp8'      => '',
        'keybcs2'  => null, // see https://en.wikipedia.org/wiki/Kamenick%C3%BD_encoding
        'koi8r'    => 'KOI8-R',
        'koi8u'    => 'KOI8-U',
        'latin1'   => 'Windows-1252', //see https://dev.mysql.com/doc/refman/8.0/en/charset-we-sets.html
        'latin2'   => 'ISO-8859-2', // see https://en.wikipedia.org/wiki/ISO/IEC_8859-2
        'latin5'   => 'ISO-8859-9', // see https://en.wikipedia.org/wiki/ISO/IEC_8859-9
        'latin7'   => 'ISO-8859-13', // see https://en.wikipedia.org/wiki/ISO/IEC_8859-13
        'macce'    => null, // see https://en.wikipedia.org/wiki/Mac_OS_Central_European_encoding
        'macroman' => null, // see https://en.wikipedia.org/wiki/Mac_OS_Roman
        'sjis'     => 'SJIS',
        'swe7'     => null, // 7-bit swedish
        'tis620'   => null, //but see https://www.mr2t.com/php-convert-utf-8-to-tis-620-and-tis-620-to-utf-8/
        'ucs2'     => 'UCS-2',
        'ujis'     => 'EUC-JP',
        'utf8'     => 'UTF-8',
        'utf8mb4'  => 'UTF-8',
        'utf16'    => 'UTF-16',
        'utf16le'  => 'UTF-16LE',
        'utf32'    => 'UTF-32',
    );

    /**
     * Array of supported numeric data types in MariaDB
     *
     * Please keep in alphabetic order and note the MariaDB version for additions.
     *
     * @see     https://mariadb.com/kb/en/library/data-types-numeric-data-types/
     * @author  MariaDB <sales-nam@mariadb.com>
     * @license https://creativecommons.org/licenses/by-sa/3.0/ CC-BY-SA
     * @license http://www.gnu.org/copyleft/fdl.html GFDL
     *
     * @var array
     */
    protected $numericDataTypes = array(
        'BIGINT',
        'BIT',
        'BOOLEAN',
        'DECIMAL', // synonyms: DEC, NUMERIC, FIXED
        'DOUBLE', // synonyms: REAL, DOUBLE PRECISION
        'FLOAT',
        'INT', // synonyms: INTEGER
        'MEDIUMINT',
        'SMALLINT',
        'TINYINT',
    );

    /**
     * Array of supported temporal data types in MariaDB
     *
     * Please keep in alphabetic order and note the MariaDB version for additions.
     *
     * @see     https://mariadb.com/kb/en/library/date-and-time-data-types/
     * @author  MariaDB <sales-nam@mariadb.com>
     * @license https://creativecommons.org/licenses/by-sa/3.0/ CC-BY-SA
     * @license http://www.gnu.org/copyleft/fdl.html GFDL
     *
     * @var array
     */
    protected $temporalDataTypes = array(
        'DATE',
        'DATETIME',
        'TIME',
        'TIMESTAMP',
        'YEAR'
    );

    /**
     * Array of supported string data types in MariaDB
     *
     * Please keep in alphabetic order and note the MariaDB version for additions.
     *
     * @see     https://mariadb.com/kb/en/library/date-and-time-data-types/
     * @author  MariaDB <sales-nam@mariadb.com>
     * @license https://creativecommons.org/licenses/by-sa/3.0/ CC-BY-SA
     * @license http://www.gnu.org/copyleft/fdl.html GFDL
     *
     * @var array
     */
    protected $stringDataTypes = array(
        'BINARY', //synonyms: CHAR BYTE
        'BLOB',
        'CHAR',
        'ENUM',
        'LONGBLOB',
        'LONGTEXT', //alias: JSON
        'MEDIUMTEXT',
        'SET',
        'TEXT',
        'TINYBLOB',
        'TINYTEXT',
        'VARBINARY',
        'VARCHAR',
    );

    /**
     * Synonym array
     *
     * @var array
     */
    protected $datatypeSynonym = array(
        'DEC'              => 'DECIMAL',
        'NUMERIC'          => 'DECIMAL',
        'FIXED'            => 'DECIMAL',
        'REAL'             => 'DOUBLE',
        'DOUBLE PRECISION' => 'DOUBLE',
        'INTEGER'          => 'INTEGER',
        'CHAR BYTE'        => 'BINARY',
        'JSON'             => 'LONGTEXT'
    );

    /**
     * Data types where zerotype is applicable
     *
     * @var array
     */
    protected $zerodateDataTypes = array(
        'DATE',
        'DATETIME',
        'YEAR'
    );

    /**
     * Data types where signed is applicable
     *
     * @var array
     */
    protected $signedDataTypes = array(
        'BIGINT',
        'DECIMAL',
        'DOUBLE',
        'FLOAT',
        'INT',
        'MEDIUMINT',
        'SMALLINT',
        'TINYINT'
    );

    /**
     * Data types where charset is applicable
     *
     * @var array
     */
    protected $charsetDataTypes = array(
        'CHAR',
        'ENUM',
        'LONGTEXT',
        'MEDIUMTEXT',
        'SET',
        'TEXT',
        'TINYTEXT',
        'VARCHAR'
    );

    /**
     * Return the reserved keyword array
     *
     * @param bool $exception Include the keywords that technically do not require quoting.
     * @param bool $oracle    Include the keywords that are only reserved in Oracle mode.
     *
     * @return array
     */
    public static function returnReservedKeywords(bool $exception = true, bool $oracle = true): array
    {
        //$return = self::$reservedKeywords;
        if ($exception && $oracle) {
            return array_merge(
                self::$reservedKeywords,
                self::$exceptionsReservedKeywords,
                self::$oracleModeReservedKeywords
            );
        }
        if ($exception) {
            return array_merge(
                self::$reservedKeywords,
                self::$exceptionsReservedKeywords
            );
        }
        if ($oracle) {
            return array_merge(
                self::$reservedKeywords,
                self::$oracleModeReservedKeywords
            );
        }
        return self::$reservedKeywords;
    }//end returnReservedKeywords()

    /**
     * Returns default collation or null if charset is not valid
     *
     * @param string $charset The charset.
     *
     * @return null|string The default collation or null if charset not valid.
     */
    public static function getDefaultCollation(string $charset)
    {
        if (isset(self::$validCharsetToDefaultCollation[$charset])) {
            return self::$validCharsetToDefaultCollation[$charset];
        }
        return null;
    }//end getDefaultCollation()

    /**
     * Returns all valid collations for a given charset or null
     *
     * @param string $charset The charset.
     *
     * @return null|array Array of valid charset or null if invalid charset.
     *
     * @see https://mariadb.com/kb/en/library/supported-character-sets-and-collations/
     */
    public static function getCollationArray(string $charset)
    {
        switch ($charset) {
            case 'armscii8':
                $arr = array(
                    'armscii8_general_ci',
                    'armscii8_bin',
                    'armscii8_general_nopad_ci',
                    'armscii8_nopad_bin'
                );
                break;
            case 'ascii':
                $arr = array(
                    'ascii_general_ci',
                    'ascii_bin',
                    'ascii_general_nopad_ci',
                    'ascii_nopad_bin'
                );
                break;
            case 'big5':
                $arr = array(
                    'big5_chinese_ci',
                    'big5_bin',
                    'big5_chinese_nopad_ci',
                    'big5_nopad_bin'
                );
                break;
            case 'binary':
                $arr = array(
                    'binary',
                );
                break;
            case 'cp850':
                $arr = array(
                    'cp850_general_ci',
                    'cp850_bin',
                    'cp850_general_nopad_ci',
                    'cp850_nopad_bin'
                );
                break;
            case 'cp852':
                $arr = array(
                    'cp852_general_ci',
                    'cp852_bin',
                    'cp852_general_nopad_ci',
                    'cp852_nopad_bin'
                );
                break;
            case 'cp866':
                $arr = array(
                    'cp866_general_ci',
                    'cp866_bin',
                    'cp866_general_nopad_ci',
                    'cp866_nopad_bin'
                );
                break;
            case 'cp932':
                $arr = array(
                    'cp932_japanese_ci',
                    'cp932_bin',
                    'cp932_japanese_nopad_ci',
                    'cp932_nopad_bin'
                );
                break;
            case 'cp1250':
                $arr = array(
                    'cp1250_general_ci',
                    'cp1250_czech_cs',
                    'cp1250_croatian_ci',
                    'cp1250_bin',
                    'cp1250_polish_ci',
                    'cp1250_general_nopad_ci',
                    'cp1250_nopad_bin'
                );
                break;
            case 'cp1251':
                $arr = array(
                    'cp1251_bulgarian_ci',
                    'cp1251_ukrainian_ci',
                    'cp1251_bin',
                    'cp1251_general_ci',
                    'cp1251_general_cs',
                    'cp1251_nopad_bin',
                    'cp1251_general_nopad_ci'
                );
                break;
            case 'cp1256':
                $arr = array(
                    'cp1256_general_ci',
                    'cp1256_bin',
                    'cp1256_general_nopad_ci',
                    'cp1256_nopad_bin'
                );
                break;
            case 'cp1257':
                $arr = array(
                    'cp1257_lithuanian_ci',
                    'cp1257_bin',
                    'cp1257_general_ci',
                    'cp1257_nopad_bin',
                    'cp1257_general_nopad_ci'
                );
                break;
            case 'dec8':
                $arr = array(
                    'dec8_swedish_ci',
                    'dec8_bin',
                    'dec8_swedish_nopad_ci',
                    'dec8_nopad_bin'
                );
                break;
            case 'eucjpms':
                $arr = array(
                    'eucjpms_japanese_ci',
                    'eucjpms_bin',
                    'eucjpms_japanese_nopad_ci',
                    'eucjpms_nopad_bin'
                );
                break;
            case 'euckr':
                $arr = array(
                    'euckr_korean_ci',
                    'euckr_bin',
                    'euckr_korean_nopad_ci',
                    'euckr_nopad_bin'
                );
                break;
            case 'gb2312':
                $arr = array(
                    'gb2312_chinese_ci',
                    'gb2312_bin',
                    'gb2312_chinese_nopad_ci',
                    'gb2312_nopad_bin'
                );
                break;
            case 'gbk':
                $arr = array(
                    'gbk_chinese_ci',
                    'gbk_bin',
                    'gbk_chinese_nopad_ci',
                    'gbk_nopad_bin'
                );
                break;
            case 'geostd8':
                $arr = array(
                    'geostd8_general_ci',
                    'geostd8_bin',
                    'geostd8_general_nopad_ci',
                    'geostd8_nopad_bin'
                );
                break;
            case 'greek':
                $arr = array(
                    'greek_general_ci',
                    'greek_bin',
                    'greek_general_nopad_ci',
                    'greek_nopad_bin'
                );
                break;
            case 'hebrew':
                $arr = array(
                    'hebrew_general_ci',
                    'hebrew_bin',
                    'hebrew_general_nopad_ci',
                    'hebrew_nopad_bin'
                );
                break;
            case 'hp8':
                $arr = array(
                    'hp8_english_ci',
                    'hp8_bin',
                    'hp8_english_nopad_ci',
                    'hp8_nopad_bin'
                );
                break;
            case 'keybcs2':
                $arr = array(
                    'keybcs2_general_ci',
                    'keybcs2_bin',
                    'keybcs2_general_nopad_ci',
                    'keybcs2_nopad_bin'
                );
                break;
            case 'koi8r':
                $arr = array(
                    'koi8r_general_ci',
                    'koi8r_bin',
                    'koi8r_general_nopad_ci',
                    'koi8r_nopad_bin'
                );
                break;
            case 'koi8u':
                $arr = array(
                    'koi8u_general_ci',
                    'koi8u_bin',
                    'koi8u_general_nopad_ci',
                    'koi8u_nopad_bin'
                );
                break;
            case 'latin1':
                $arr = array(
                    'latin1_german1_ci',
                    'latin1_swedish_ci',
                    'latin1_danish_ci',
                    'latin1_german2_ci',
                    'latin1_bin',
                    'latin1_general_ci',
                    'latin1_general_cs',
                    'latin1_spanish_ci',
                    'latin1_swedish_nopad_ci',
                    'latin1_nopad_bin'
                );
                break;
            case 'latin2':
                $arr = array(
                    'latin2_czech_cs',
                    'latin2_general_ci',
                    'latin2_hungarian_ci',
                    'latin2_croatian_ci',
                    'latin2_bin',
                    'latin2_general_nopad_ci',
                    'latin2_nopad_bin'
                );
                break;
            case 'latin5':
                $arr = array(
                    'latin5_turkish_ci',
                    'latin5_bin',
                    'latin5_turkish_nopad_ci',
                    'latin5_nopad_bin'
                );
                break;
            case 'latin7':
                $arr = array(
                    'latin7_estonian_cs',
                    'latin7_general_ci',
                    'latin7_general_cs',
                    'latin7_bin',
                    'latin7_general_nopad_ci',
                    'latin7_nopad_bin'
                );
                break;
            case 'macce':
                $arr = array(
                    'macce_general_ci',
                    'macce_bin',
                    'macce_general_nopad_ci',
                    'macce_nopad_bin'
                );
                break;
            case 'macroman':
                $arr = array(
                    'macroman_general_ci',
                    'macroman_bin',
                    'macroman_general_nopad_ci',
                    'macroman_nopad_bin'
                );
                break;
            case 'sjis':
                $arr = array(
                    'sjis_japanese_ci',
                    'sjis_bin',
                    'sjis_japanese_nopad_ci',
                    'sjis_nopad_bin'
                );
                break;
            case 'swe7':
                $arr = array(
                    'swe7_swedish_ci',
                    'swe7_bin',
                    'swe7_swedish_nopad_ci',
                    'swe7_nopad_bin'
                );
                break;
            case 'tis620':
                $arr = array(
                    'tis620_thai_ci',
                    'tis620_bin',
                    'tis620_thai_nopad_ci',
                    'tis620_nopad_bin'
                );
                break;
            case 'ucs2':
                $arr = array(
                    'ucs2_general_ci',
                    'ucs2_bin',
                    'ucs2_unicode_ci',
                    'ucs2_icelandic_ci',
                    'ucs2_latvian_ci',
                    'ucs2_romanian_ci',
                    'ucs2_slovenian_ci',
                    'ucs2_polish_ci',
                    'ucs2_estonian_ci',
                    'ucs2_spanish_ci',
                    'ucs2_swedish_ci',
                    'ucs2_turkish_ci',
                    'ucs2_czech_ci',
                    'ucs2_danish_ci',
                    'ucs2_lithuanian_ci',
                    'ucs2_slovak_ci',
                    'ucs2_spanish2_ci',
                    'ucs2_roman_ci',
                    'ucs2_persian_ci',
                    'ucs2_esperanto_ci',
                    'ucs2_hungarian_ci',
                    'ucs2_sinhala_ci',
                    'ucs2_german2_ci',
                    'ucs2_croatian_mysql561_ci',
                    'ucs2_unicode_520_ci',
                    'ucs2_vietnamese_ci',
                    'ucs2_general_mysql500_ci',
                    'ucs2_croatian_ci',
                    'ucs2_myanmar_ci',
                    'ucs2_thai_520_w2',
                    'ucs2_general_nopad_ci',
                    'ucs2_nopad_bin',
                    'ucs2_unicode_nopad_ci',
                    'ucs2_unicode_520_nopad_ci'
                );
                break;
            case 'utf8':
                $arr = array(
                    'utf8_general_ci',
                    'utf8_bin',
                    'utf8_unicode_ci',
                    'utf8_icelandic_ci',
                    'utf8_latvian_ci',
                    'utf8_romanian_ci',
                    'utf8_slovenian_ci',
                    'utf8_polish_ci',
                    'utf8_estonian_ci',
                    'utf8_spanish_ci',
                    'utf8_swedish_ci',
                    'utf8_turkish_ci',
                    'utf8_czech_ci',
                    'utf8_danish_ci',
                    'utf8_lithuanian_ci',
                    'utf8_slovak_ci',
                    'utf8_spanish2_ci',
                    'utf8_roman_ci',
                    'utf8_persian_ci',
                    'utf8_esperanto_ci',
                    'utf8_hungarian_ci',
                    'utf8_sinhala_ci',
                    'utf8_german2_ci',
                    'utf8_croatian_mysql561_ci',
                    'utf8_unicode_520_ci',
                    'utf8_vietnamese_ci',
                    'utf8_general_mysql500_ci',
                    'utf8_croatian_ci',
                    'utf8_myanmar_ci',
                    'utf8_thai_520_w2',
                    'utf8_general_nopad_ci',
                    'utf8_nopad_bin',
                    'utf8_unicode_nopad_ci',
                    'utf8_unicode_520_nopad_ci'
                );
                break;
            case 'utf8mb4':
                $arr = array(
                    'utf8mb4_general_ci',
                    'utf8mb4_bin',
                    'utf8mb4_unicode_ci',
                    'utf8mb4_icelandic_ci',
                    'utf8mb4_latvian_ci',
                    'utf8mb4_romanian_ci',
                    'utf8mb4_slovenian_ci',
                    'utf8mb4_polish_ci',
                    'utf8mb4_estonian_ci',
                    'utf8mb4_spanish_ci',
                    'utf8mb4_swedish_ci',
                    'utf8mb4_turkish_ci',
                    'utf8mb4_czech_ci',
                    'utf8mb4_danish_ci',
                    'utf8mb4_lithuanian_ci',
                    'utf8mb4_slovak_ci',
                    'utf8mb4_spanish2_ci',
                    'utf8mb4_roman_ci',
                    'utf8mb4_persian_ci',
                    'utf8mb4_esperanto_ci',
                    'utf8mb4_hungarian_ci',
                    'utf8mb4_sinhala_ci',
                    'utf8mb4_german2_ci',
                    'utf8mb4_croatian_mysql561_ci',
                    'utf8mb4_unicode_520_ci',
                    'utf8mb4_vietnamese_ci',
                    'utf8mb4_croatian_ci',
                    'utf8mb4_myanmar_ci',
                    'utf8mb4_thai_520_w2',
                    'utf8mb4_general_nopad_ci',
                    'utf8mb4_nopad_bin',
                    'utf8mb4_unicode_nopad_ci',
                    'utf8mb4_unicode_520_nopad_ci'
                );
                break;
            case 'utf16':
                $arr = array(
                    'utf16_general_ci',
                    'utf16_bin',
                    'utf16_unicode_ci',
                    'utf16_icelandic_ci',
                    'utf16_latvian_ci',
                    'utf16_romanian_ci',
                    'utf16_slovenian_ci',
                    'utf16_polish_ci',
                    'utf16_estonian_ci',
                    'utf16_spanish_ci',
                    'utf16_swedish_ci',
                    'utf16_turkish_ci',
                    'utf16_czech_ci',
                    'utf16_danish_ci',
                    'utf16_lithuanian_ci',
                    'utf16_slovak_ci',
                    'utf16_spanish2_ci',
                    'utf16_roman_ci',
                    'utf16_persian_ci',
                    'utf16_esperanto_ci',
                    'utf16_hungarian_ci',
                    'utf16_sinhala_ci',
                    'utf16_german2_ci',
                    'utf16_croatian_mysql561_ci',
                    'utf16_unicode_520_ci',
                    'utf16_vietnamese_ci',
                    'utf16_croatian_ci',
                    'utf16_myanmar_ci',
                    'utf16_thai_520_w2',
                    'utf16_general_nopad_ci',
                    'utf16_nopad_bin',
                    'utf16_unicode_nopad_ci',
                    'utf16_unicode_520_nopad_ci'
                );
                break;
            case 'utf16le':
                $arr = array(
                    'utf16le_general_ci',
                    'utf16le_bin',
                    'utf16le_general_nopad_ci',
                    'utf16le_nopad_bin',
                );
                break;
            case 'utf32':
                $arr = array(
                    'utf32_general_ci',
                    'utf32_bin',
                    'utf32_unicode_ci',
                    'utf32_icelandic_ci',
                    'utf32_latvian_ci',
                    'utf32_romanian_ci',
                    'utf32_slovenian_ci',
                    'utf32_polish_ci',
                    'utf32_estonian_ci',
                    'utf32_spanish_ci',
                    'utf32_swedish_ci',
                    'utf32_turkish_ci',
                    'utf32_czech_ci',
                    'utf32_danish_ci',
                    'utf32_lithuanian_ci',
                    'utf32_slovak_ci',
                    'utf32_spanish2_ci',
                    'utf32_roman_ci',
                    'utf32_persian_ci',
                    'utf32_esperanto_ci',
                    'utf32_hungarian_ci',
                    'utf32_sinhala_ci',
                    'utf32_german2_ci',
                    'utf32_croatian_mysql561_ci',
                    'utf32_unicode_520_ci',
                    'utf32_vietnamese_ci',
                    'utf32_croatian_ci',
                    'utf32_myanmar_ci',
                    'utf32_thai_520_w2',
                    'utf32_general_nopad_ci',
                    'utf32_nopad_bin',
                    'utf32_unicode_nopad_ci',
                    'utf32_unicode_520_nopad_ci'
                );
                break;
            case 'ujis':
                $arr = array(
                    'ujis_japanese_ci',
                    'ujis_bin',
                    'ujis_japanese_nopad_ci',
                    'ujis_nopad_bin'
                );
                break;
            default:
                $collation = self::getDefaultCollation($charset);
                if (is_string($collation)) {
                    $arr = array($collation);
                } else {
                    $arr = null;
                }
                break;
        }
        return $arr;
    }//end getCollationArray()

    /**
     * Validate data type
     *
     * @param string $datatype The datatype to validate.
     *
     * @return bool True if valid
     */
    public static function validateDataType(string $datatype): bool
    {
        if (in_array($datatype, self::$numericDataTypes)) {
            return true;
        }
        if (in_array($datatype, self::$temporalDataTypes)) {
            return true;
        }
        if (in_array($datatype, self::$stringDataTypes)) {
            return true;
        }
        return false;
    }//end validateDataType()

    /**
     * Return data type from synonym or false if invalid
     *
     * @param string $datatype The data type.
     *
     * @return bool|string
     */
    public static function normalizeDataType(string $datatype)
    {
        $datatype = trim(strtoupper($datatype));
        if (in_array($datatype, self::$datatypeSynonym)) {
            $datatype = self::$datatypeSynonym[$datatype];
        }
        if (self::validateDataType($datatype)) {
            return $datatype;
        }
        return false;
    }//end normalizeDataType()

    /**
     * Determine whether or not zerodate is appropriate for specified
     * data type.
     *
     * @param string $datatype The datatype to check.
     *
     * @return bool
     */
    public static function zerodateCapable(string $datatype): bool
    {
        if (in_array($datatype, self::$zerodateDataTypes)) {
            return true;
        }
        return false;
    }//end zerodateCapable()

    /**
     * Determine whether or not signed is appropriate for specified
     * data type.
     *
     * @param string $datatype The datatype to check.
     *
     * @return bool
     */
    public static function signedCapable(string $datatype): bool
    {
        if (in_array($datatype, self::$signedDataTypes)) {
            return true;
        }
        return false;
    }//end signedCapable()

    /**
     * Determine whether or not charset is appropriate for specified
     * data type.
     *
     * @param string $datatype The datatype to check.
     *
     * @return bool
     */
    public static function charsetCapable(string $datatype): bool
    {
        if (in_array($datatype, self::$charsetDataTypes)) {
            return true;
        }
        return false;
    }//end charsetCapable()
}//end class

?>