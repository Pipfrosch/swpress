<?php
declare(strict_types=1);

/**
 * Class for Colulmn identifiers
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The Column Identifier Class
 */
class ColumnIdentifier extends AbstractIdentifier
{
    /**
     * @var string
     */
    protected $subtype = 'COLUMN';

    /**
     * Identifier object for a table, only needed when
     *  using fully qualified name
     *
     * @var null|\Pipfrosch\SqlObject\TableIdentifier
     */
    protected $tableIdentifier = null;

    /**
     * The data type for the column
     *
     * @var null|string
     */
    protected $datatype = null;

    /**
     * Whether or not null vulues are allowed for the column
     *
     * @var bool
     */
    protected $nullok = true;

    /**
     * Only had meaning for a few temporal types.
     *
     * @var null|bool
     */
    protected $zerodate = null;

    /**
     * Only has meaning for some numeric types.
     *
     * @var null|bool
     */
    protected $signed = null;

    /**
     * Only has meaning for some text types.
     *
     * @var null|string
     */
    protected $charset = null;

    /**
     * Only has meaning for some text types.
     *
     * @var null|string
     */
    protected $collation = null;

    /**
     * Add table identifier
     *
     * @param \Pipfrosch\SqlObject\TableIdentifier $tableIdentifier The Table Identifier.
     *
     * @return void
     */
    public function setTableIdentifier(\Pipfrosch\SqlObject\TableIdentifier $tableIdentifier): void
    {
        $test = $tableIdentifier->getValue();
        if (! is_null($test)) {
            $this->tableIdentifier = $tableIdentifier;
        }
    }

    /**
     * Set the data type for the column
     *
     * @param string $string The datatype to set.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function setDataType(string $string): void
    {
        $datatype = StaticMethods\Constants::normalizeDataType($string);
        if (! is_string($datatype)) {
            throw Exceptions\InvalidIdentifierArgument::invalidDataType($string);
        }
        $this->datatype = $datatype;
    }

    /**
     * Set the nullok parameter
     *
     * @param bool $nullok Whether or not column allows null values.
     *
     * @return void
     */
    public function setNullOkay(bool $nullok): void
    {
        $this->nullok = $nullok;
    }//end setNullOkay()

    /**
     * Get the nullok parameter
     *
     * @return bool
     */
    public function getNullOkay(): bool
    {
        return $this->nullok;
    }//end getNullOkay()

    /**
     * Set the zerodate parameter
     *
     * @param bool $zerodate Whether or not column allows zerodate values.
     *
     * @return void
     */
    public function setZeroDate(bool $zerodate): void
    {
        if (is_null($this->datatype) || ! StaticMethods\Constants::zerodateCapable($this->datatype)) {
            return;
        }
        $this->zerodate = $zerodate;
    }//end setZeroDate()

    /**
     * Get the zerodate parameter
     *
     * @return null|bool
     */
    public function getZeroDate()
    {
        return $this->zerodate;
    }//end getZeroDate()

    /**
     * Set the signed parameter
     *
     * @param bool $signed Whether or not column uses signed values.
     *
     * @return void
     */
    public function setSigned(bool $signed): void
    {
        if (is_null($this->datatype) || ! StaticMethods\Constants::signedCapable($this->datatype)) {
            return;
        }
        $this->signed = $signed;
    }//end setSigned()

    /**
     * Get the signed parameter
     *
     * @return null|bool
     */
    public function getSigned()
    {
        return $this->signed;
    }//end getSigned()

    /**
     * Set the charset parameter
     *
     * @param string      $charset   The charset for the column.
     * @param null|string $collation The collation to use. Null uses default for charset.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function setCharset($charset, $collation = null): void
    {
        if (is_null($this->datatype) || ! StaticMethods\Constants::charsetCapable($this->datatype)) {
            return;
        }
        $charset = trim(strtolower($charset));
        $defaultCollation = StaticMethods\Constants::getDefaultCollation($charset);
        if (! is_string($defaultCollation)) {
            throw Exceptions\InvalidIdentifierArgument::invalidCharset($charset);
        }
        if (is_string($collation)) {
            $collation = trim(strtolower($collation));
            $arr = StaticMethods\Constants::getCollationArray($charset);
            // $arr won't be null but make static analysis tool happy.
            if (is_array($arr)) {
                if (! in_array($collation, $arr)) {
                    throw Exceptions\InvalidIdentifierArgument::invalidCollation($collation, $charset);
                }
                $this->charset = $charset;
                $this->collation = $collation;
            }
            return;
        }
        $this->charset = $charset;
        $this->collation = $defaultCollation;
    }//end setCharset()

    /**
     * Set the collation parameter
     *
     * @param null|string $collation The collation to use. Null uses default for charset.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function setCollation($collation = null): void
    {
        if (is_null($this->datatype) || ! StaticMethods\Constants::charsetCapable($this->datatype)) {
            return;
        }
        if (! is_string($this->charset)) {
            throw Exceptions\InvalidIdentifierArgument::charsetNotDefined();
        }
        if (is_null($collation)) {
            $this->collation = StaticMethods\Constants::getDefaultCollation($this->charset);
            return;
        }
        $collation = trim(strtolower($collation));
        $arr = StaticMethods\Constants::getCollationArray($this->charset);
        // $arr won't be null but make static analysis tool happy.
        if (is_array($arr)) {
            if (! in_array($collation, $arr)) {
                throw Exceptions\InvalidIdentifierArgument::invalidCollation($collation, $this->charset);
            }
            $this->collation = $collation;
        }
    }//end setCollation()

    /**
     * Get the charset parameter
     *
     * @return null|string
     */
    public function getCharset()
    {
        return $this->charset;
    }//end getCharset()

    /**
     * Get the collation
     *
     * @return null|string
     */
    public function getCollation()
    {
        return $this->collation;
    }//end getCollation()

    /**
     * To String has to override what is in abstract class
     *
     * @return string
     */
    public function __toString()
    {
        $return = '';
        if (is_null($this->value)) {
            return '';
        }
        if (! is_null($this->tableIdentifier)) {
            $table = $this->tableIdentifier->__toString();
            if (strlen($table) > 0) {
                $return = $table . '.';
            }
        }
        if (! $this->requiresQuotes) {
            $return .= $this->value;
        } else {
            if ($this->ANSIquotes) {
                $return .= '"' . preg_replace('/"/', "", $this->value) . '"';
            } else {
                $return .= '`' . preg_replace('/`/', '``', $this->value) . '`';
            }
        }
        // final check to see if result is reserved
        $test = strtoupper($return);
        $keywords = StaticMethods\Constants::returnReservedKeywords();
        if (in_array($test, $keywords)) {
            if ($this->ANSIquotes) {
                return '"' . $return . '"';
            } else {
                return '`' . $return . '`';
            }
        }
        return $return;
    }//end __toString()

    /**
     * Return DOMNode
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode|\DOMComment
     */
    public function toDomNode(\DOMDocument $dom)
    {
        if (is_null($this->value)) {
            $node = $dom->createComment('Invalid Identifier');
            return $node;
        }        
        $node = $dom->createElement('identifier', $this->__toString());
        /**
         * @psalm-suppress PossiblyNullArgument
         */
        $node->setAttribute('type', $this->subtype);
        if ($this->nullok) {
            $node->setAttribute('nullable', 'null ok');
        } else {
            $node->setAttribute('nullable', 'not null');
        }
        if (is_string($this->charset)) {
            $node->setAttribute('charset', $this->charset);
            if (is_string($this->collation)) {
                $node->setAttribute('collation', $this->collation);
            }
        }
        if (is_bool($this->zerodate) && $this->zerodate) {
            $node->setAttribute('zerodate', 'zerodate ok');
        }
        if (is_bool($this->signed)) {
            if ($this->signed) {
                $node->setAttribute('signed', 'signed');
            } else {
                $node->setAttribute('unsigned', 'unsigned');
            }
        }
        return $node;
    }

    /**
     * The constructor
     *
     * @param string $value      The identifier string.
     * @param bool   $ANSIquotes Whether or not to use ANSI quotes.
     *
     */
    public function __construct(string $value, bool $ANSIquotes = false)
    {
        $this->ANSIquotes = $ANSIquotes;
        $this->stripQuotes($value);
        $this->validateIdentifier($value);
    }//end __construct()
}//end class
