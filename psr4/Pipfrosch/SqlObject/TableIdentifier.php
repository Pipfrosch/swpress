<?php
declare(strict_types=1);

/**
 * Class for Table identifiers
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The Table Identifier Class
 */
class TableIdentifier extends AbstractIdentifier
{
    /**
     * @var string
     */
    protected $subtype = 'TABLE';

    /**
     * Identifier object for a database, only needed when
     *  using fully qualified name
     *
     * @var null|\Pipfrosch\SqlObject\DatabaseIdentifier
     */
    protected $databaseIdentifier = null;

    /**
     * Only has meaning for some text types.
     *
     * @var null|string
     */
    protected $charset = null;

    /**
     * Only has meaning for some text types.
     *
     * @var null|string
     */
    protected $collation = null;

    /**
     * Add database identifier
     *
     * @param \Pipfrosch\SqlObject\DatabaseIdentifier $databaseIdentifier The Database Identifier.
     *
     * @return void
     */
    public function setDatabaseIdentifier(\Pipfrosch\SqlObject\DatabaseIdentifier $databaseIdentifier): void
    {
        $test = $databaseIdentifier->getValue();
        if (! is_null($test)) {
            $this->databaseIdentifier = $databaseIdentifier;
        }
    }

    /**
     * Set the charset parameter
     *
     * @param string      $charset   The charset for the column.
     * @param null|string $collation The collation to use. Null uses default for charset.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function setCharset($charset, $collation = null): void
    {
        if (is_null($this->datatype) || ! StaticMethods\Constants::charsetCapable($this->datatype)) {
            return;
        }
        $charset = trim(strtolower($charset));
        $defaultCollation = StaticMethods\Constants::getDefaultCollation($charset);
        if (! is_string($defaultCollation)) {
            throw Exceptions\InvalidIdentifierArgument::invalidCharset($charset);
        }
        if (is_string($collation)) {
            $collation = trim(strtolower($collation));
            $arr = StaticMethods\Constants::getCollationArray($charset);
            // $arr won't be null but make static analysis tool happy.
            if (is_array($arr)) {
                if (! in_array($collation, $arr)) {
                    throw Exceptions\InvalidIdentifierArgument::invalidCollation($collation, $charset);
                }
                $this->charset = $charset;
                $this->collation = $collation;
            }
            return;
        }
        $this->charset = $charset;
        $this->collation = $defaultCollation;
    }//end setCharset()

    /**
     * Set the collation parameter
     *
     * @param null|string $collation The collation to use. Null uses default for charset.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function setCollation($collation = null): void
    {
        if (is_null($this->datatype) || ! StaticMethods\Constants::charsetCapable($this->datatype)) {
            return;
        }
        if (! is_string($this->charset)) {
            throw Exceptions\InvalidIdentifierArgument::charsetNotDefined();
        }
        if (is_null($collation)) {
            $this->collation = StaticMethods\Constants::getDefaultCollation($this->charset);
            return;
        }
        $collation = trim(strtolower($collation));
        $arr = StaticMethods\Constants::getCollationArray($this->charset);
        // $arr won't be null but make static analysis tool happy.
        if (is_array($arr)) {
            if (! in_array($collation, $arr)) {
                throw Exceptions\InvalidIdentifierArgument::invalidCollation($collation, $this->charset);
            }
            $this->collation = $collation;
        }
    }//end setCollation()

    /**
     * Get the charset parameter
     *
     * @return null|string
     */
    public function getCharset()
    {
        return $this->charset;
    }//end getCharset()

    /**
     * Get the collation
     *
     * @return null|string
     */
    public function getCollation()
    {
        return $this->collation;
    }//end getCollation()

    /**
     * To String has to override what is in abstract class
     *
     * @return string
     */
    public function __toString()
    {
        $return = '';
        if (is_null($this->value)) {
            return '';
        }
        if (! is_null($this->databaseIdentifier)) {
            $database = $this->databaseIdentifier->__toString();
            if (strlen($database) > 0) {
                $return = $database . '.';
            }
        }
        if (! $this->requiresQuotes) {
            $return .= $this->value;
        } else {
            if ($this->ANSIquotes) {
                $return .= '"' . preg_replace('/"/', "", $this->value) . '"';
            } else {
                $return .= '`' . preg_replace('/`/', '``', $this->value) . '`';
            }
        }
        // final check to see if result is reserved
        $test = strtoupper($return);
        $keywords = StaticMethods\Constants::returnReservedKeywords();
        if (in_array($test, $keywords)) {
            if ($this->ANSIquotes) {
                return '"' . $return . '"';
            } else {
                return '`' . $return . '`';
            }
        }
        return $return;
    }//end __toString()

    /**
     * The constructor
     *
     * @param string $value      The identifier string.
     * @param bool   $ANSIquotes Whether or not to use ANSI quotes
     *
     */
    public function __construct(string $value, bool $ANSIquotes = false)
    {
        $this->ANSIquotes = $ANSIquotes;
        $this->stripQuotes($value);
        $this->validateIdentifier($value);
    }//end __construct()
}//end class

?>