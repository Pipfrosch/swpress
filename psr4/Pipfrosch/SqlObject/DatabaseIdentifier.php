<?php
declare(strict_types=1);

/**
 * Class for Database identifiers
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The Database Identifier Class
 */
class DatabaseIdentifier extends AbstractIdentifier
{
    /**
     * @var string
     */
    protected $subtype = 'DATABASE';

    /**
     * The constructor
     *
     * @param string $value      The identifier string.
     * @param bool   $ANSIquotes Whether or not to use ANSI quotes
     *
     */
    public function __construct(string $value, bool $ANSIquotes = false)
    {
        $this->ANSIquotes = $ANSIquotes;
        $this->stripQuotes($value);
        $this->validateIdentifier($value);
    }//end __construct()
}//end class

?>