<?php
declare(strict_types=1);

/**
 * Abstract class for building QUERY objects
 *
 * NOT ready to use.
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * Abstract class for building QUERY objects
 */
abstract class AbstractQueryObject
{
    /**
     * Defines the query type
     *
     * @var string
     */
    protected $queryType = 'NOT DEFINED';

    /**
     * Defines which clauses are allowed
     * in a particular query type.
     *
     * @var array
     */
    protected $clauseTypes = array();

    /* The various clause types */

    /**
     * FROM clause
     *
     * @todo object class does not yet exist.
     *
     * @var null|\Pipfrosch\SqlObject\FromClause
     */
    protected $fromClause = null;

    /**
     * INTO clause
     *
     * @todo object class does not yet exist.
     *
     * @var null|\Pipfrosch\SqlObject\IntoClause
     */
    protected $intoClause = null;

    /**
     * LIMIT clause
     *
     * @var null|\Pipfrosch\SqlObject\LimitClause
     */
    protected $limitClause = null;

    /**
     * ORDER BY clause
     *
     * @todo Object class does not yet exist.
     *
     * @var null|\Pipfrosch\SqlObject\OrderByClause
     */
    protected $orderByClause = null;

    /**
     * SELECT clause
     *
     * @todo object class does not yet exist.
     *
     * @var null|\Pipfrosch\SqlObject\SelectClause
     */
    protected $selectClause = null;

    /**
     * WHERE clause
     *
     * @todo Object class does not yet exist.
     *
     * @var null|\Pipfrosch\SqlObject\WhereClause
     */
    protected $whereClause = null;

    /* FROM CLAUSE */

    /**
     * Set the FROM clause.
     *
     * @param \Pipfrosch\SqlObject\FromClause $fromClause The FROM clause.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function addFromClause(\Pipfrosch\SqlObject\FromClause $fromClause): void
    {
        if (! in_array('FROM', $this->clauseTypes)) {
            throw Exceptions\QueryException::invalidClauseType($this->queryType, 'FROM');
        }
        $this->fromClause = $fromClause;
    }//end addFromClause()

    /**
     * Remove the FROM clause.
     *
     * @return void
     */
    public function delFromClause(): void
    {
        $this->fromClause = null;
    }//end delFromClause()

    /* INTO CLAUSE */

    /**
     * Set the INTO clause.
     *
     * @param \Pipfrosch\SqlObject\IntoClause $intoClause The INTO clause.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function addIntoClause(\Pipfrosch\SqlObject\IntoClause $intoClause): void
    {
        if (! in_array('INTO', $this->clauseTypes)) {
            throw Exceptions\QueryException::invalidClauseType($this->queryType, 'INTO');
        }
        $this->intoClause = $intoClause;
    }//end addIntoClause()


    /**
     * Remove the INTO clause.
     *
     * @return void
     */
    public function delIntoClause(): void
    {
        $this->intoClause = null;
    }//end delIntoClause()

    /* LIMIT CLAUSE */

    /**
     * Set the LIMIT clause.
     *
     * @param \Pipfrosch\SqlObject\LimitClause $limitClause The LIMIT clause.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function addLimitClause(\Pipfrosch\SqlObject\LimitClause $limitClause): void
    {
        if (! in_array('LIMIT', $this->clauseTypes)) {
            throw Exceptions\QueryException::invalidClauseType($this->queryType, 'LIMIT');
        }
        $this->limitClause = $limitClause;
    }//end addLimitClause()

    /**
     * Remove the LIMIT clause.
     *
     * @return void
     */
    public function delLimitClause(): void
    {
        $this->limitClause = null;
    }//end delLimitClause()

    /* ORDER BY CLAUSE */

    /**
     * Set the ORDER BY clause.
     *
     * @param \Pipfrosch\SqlObject\OrderByClaise $orderByClause The ORDER BY clause.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function addOrderByClause(\Pipfrosch\SqlObject\OrderByClaise $orderByClause): void
    {
        if (! in_array('ORDER BY', $this->clauseTypes)) {
            throw Exceptions\QueryException::invalidClauseType($this->queryType, 'ORDER BY');
        }
        $this->orderByClause = $orderByClause;
    }//end addOrderByClause(

    /**
     * Remove the ORDER BY clause.
     *
     * @return void
     */
    public function delOrderByClause(): void
    {
        $this->orderByClause = null;
    }//end delOrderByClause()

    /* SELECT CLAUSE */

    /**
     * Set the SELECT clause.
     *
     * @param \Pipfrosch\SqlObject\SelectClause $selectClause The SELECT clause.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function addSelectClause(\Pipfrosch\SqlObject\SelectClause $selectClause): void
    {
        if (! in_array('SELECT', $this->clauseTypes)) {
            throw Exceptions\QueryException::invalidClauseType($this->queryType, 'SELECT');
        }
        $this->fromClause = $fromClause;
    }//end addSelectClause()

    /**
     * Remove the SELECT clause.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function delSelectClause(): void
    {
        $requiredQueries = array(
            'SELECT'
        );
        if (in_array($this->queryType, $requiredQueries)) {
            throw Exceptions\QueryException::requiredClause($this->queryType, 'SELECT');
        }
        $this->selectClause = null;
    }//end delSelectClause()

    /* WHERE CLAUSE */

    /**
     * Set the WHERE clause.
     *
     * @param \Pipfrosch\SqlObject\WhereClause $whereClause The WHERE clause.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function addWhereClause(\Pipfrosch\SqlObject\WhereClause $whereClause): void
    {
        if (! isset($this->whereClause)) {
            throw Exceptions\QueryException::invalidClauseType($this->queryType, 'FROM');
        }
        $this->whereClause = $whereClause;
    }//end addWhereClause()

    /**
     * Remove the WHERE clause.
     *
     * @return void
     */
    public function delWhereClause(): void
    {
        $this->whereClause = null;
    }//end delWhereClause()

    /* ABSTRACT FUNCTIONS */

    /**
     * Export query to PDO compatible ->query() string
     *
     * @return string
     */
    abstract public function __toString(): string;

    /**
     * Export query to DOMNode object
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode
     */
    abstract public function toDomNode(\DOMDocument $dom): \DOMNode;

    /**
     * Export query to PDO compatible prepared statement and value
     *
     * @return \stdClass an object with query and value array
     */
    abstract public function toParameterized(): \stdClass;
}//end class

?>