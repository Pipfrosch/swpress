<?php
declare(strict_types=1);

/**
 * Invalid Numeric Argument exceptions
 *
 * @package    SqlObject
 * @subpackage Exceptions
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject\Exceptions;

/**
 * Invalid Numeric Argument
 */
class InvalidNumericArgument extends \InvalidArgumentException
{
    /**
     * Filter special html characters.
     *
     * @param string $input The string to filter.
     *
     * @return string The filtered string.
     */
    protected static function specialCharacterFilter(string $input): string
    {
        $s = array();
        $r = array();
        $s[] = '/&/';
        $r[] = '&amp;';
        $s[] = '/</';
        $r[] = '&lt;';
        $s[] = '/>/';
        $r[] = '&gt;';
        return preg_replace($s, $r, $input);
    }//end specialCharacterFilter()

    /**
     * Expecting string representation of an integer
     *
     * @param string $arg The error argument.
     *
     * @return \InvalidArgumentException
     */
    public static function expectingIntegerString(string $arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'Expecting string representation of an integer, but received <code>%s</code>',
            $arg
        ));
    }//end expectingIntegerString()

    /**
     * Expecting string representation of a boolean
     *
     * @param string $arg The error argument.
     *
     * @return \InvalidArgumentException
     */
    public static function expectingBooleanString(string $arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'Expecting a string value of <code>0</code> or <code>1</code> but received <code>%s</code>.',
            $arg
        ));
    }//end expectingBooleanString()

    /**
     * Expecting string of integer within range
     *
     * @param string $arg     The error argument.
     * @param string $subtype The subtype argument.
     * @param bool   $signed  Whether signed int or not.
     *
     * @return \InvalidArgumentException
     */
    public static function integerStringOutOfRange(string $arg, string $subtype, bool $signed)
    {
        $arg = self::specialCharacterFilter($arg);
        $subtype = self::specialCharacterFilter($subtype);
        $stringSigned = 'unsigned';
        if ($signed) {
            $stringSigned = 'signed';
        }
        return new self(sprintf(
            'Expecting a string representation of an integer within the valid range for a <code>MariaDB %s %s</code>, received <code>%s</code>.',
            $stringSigned,
            $subtype,
            $arg
        ));
    }//end integerStringOutOfRange()

    /**
     * Expecting integer within range
     *
     * @param int    $arg     The error argument.
     * @param string $subtype The subtype argument.
     * @param bool   $signed  Whether signed int or not.
     *
     * @return \InvalidArgumentException
     */
    public static function integerOutOfRange(int $arg, string $subtype, bool $signed)
    {
        $argString = (string) $arg;
        $subtype = self::specialCharacterFilter($subtype);
        $stringSigned = 'unsigned';
        if ($signed) {
            $stringSigned = 'signed';
        }
        return new self(sprintf(
            'Expecting an integer within the valid range for a <code>MariaDB %s %s</code>, received <code>%s</code>.',
            $stringSigned,
            $subtype,
            $argString
        ));
    }//end integerOutOfRange()

    /**
     * Expecting string or integer within range
     *
     * @param string $arg     The error argument.
     * @param string $subtype The subtype argument.
     * @param bool   $signed  Whether signed int or not.
     *
     * @return \InvalidArgumentException
     */
    public static function integerValueOutOfRange(string $arg, string $subtype, bool $signed)
    {
        $arg = self::specialCharacterFilter($arg);
        $subtype = self::specialCharacterFilter($subtype);
        $stringSigned = 'unsigned';
        if ($signed) {
            $stringSigned = 'signed';
        }
        return new self(sprintf(
            'Expecting a string or an integer within the valid range for a <code>MariaDB %s %s</code>, received <code>%s</code>.',
            $stringSigned,
            $subtype,
            $arg
        ));
    }//end integerValueOutOfRange()

    /**
     * Expecting non-negative integer
     *
     * @param int $arg The error argument.
     *
     * @return \InvalidArgumentException
     */
    public static function negativeInteger(int $arg)
    {
        $str = (string) $arg;
        return new self(sprintf(
            'Expecting a non-negative integer, but <code>%s</code> was supplied.',
            $str
        ));
    }//end negativeInteger()

    /**
     * Integer too large
     *
     * @param int $arg  The error argument.
     * @param int $size The bit size of argument.
     * @param int $limit The bit limit.
     *
     * @return \InvalidArgumentException
     */
    public static function integerTooLarge(int $arg, int $size, int $limit)
    {
        $str = (string) $arg;
        $ssize = (string) $size;
        $slimit = (string) $limit;
        return new self(sprintf(
            'Expecting an unsigned integer within <code>%s</code> bits, but <code>%s (%s bits)</code> was supplied.',
            $slimit,
            $str,
            $ssize
        ));
    }//end integerTooLarge()

    /**
     * Octal too large
     *
     * @param string $arg   The error argument.
     * @param int    $size  The bit size of argument.
     * @param int    $limit The bit limit.
     *
     * @return \InvalidArgumentException
     */
    public static function octalTooLarge(string $arg, int $size, int $limit)
    {
        $str = self::specialCharacterFilter($arg);
        $ssize = (string) $size;
        $slimit = (string) $limit;
        return new self(sprintf(
            'Expecting an octal argument within <code>%s</code> bits, but <code>%s (%s bits)</code> was supplied.',
            $slimit,
            $str,
            $ssize
        ));
    }//end octalTooLarge()

    /**
     * Hex too large
     *
     * @param string $arg  The error argument.
     * @param int    $size The bit size of argument.
     * @param int    $limit The bit limit.
     *
     * @return \InvalidArgumentException
     */
    public static function hexTooLarge(string $arg, int $size, int $limit)
    {
        $str = self::specialCharacterFilter($arg);
        $ssize = (string) $size;
        $slimit = (string) $limit;
        return new self(sprintf(
            'Expecting a hexadecimal argument within <code>%s</code> bits, but <code>%s (%s bits)</code> was supplied.',
            $slimit,
            $str,
            $ssize
        ));
    }//end hexTooLarge()

    /**
     * BIT string large
     *
     * @param string $arg  The error argument.
     * @param int    $size The bit size of argument.
     * @param int    $limit The bit limit.
     *
     * @return \InvalidArgumentException
     */
    public static function bitStringTooLarge(string $arg, int $size, int $limit)
    {
        $str = self::specialCharacterFilter($arg);
        $ssize = (string) $size;
        $slimit = (string) $limit;
        return new self(sprintf(
            'Expecting a BIT string argument within <code>%s</code> bits, but <code>%s (%s bits)</code> was supplied.',
            $slimit,
            $str,
            $ssize
        ));
    }//end bitStringTooLarge()

    /**
     * Invalid Octal
     *
     * @param string $arg The error argument.
     *
     * @return \InvalidArgumentException
     */
    public static function expectingOctalArgument(string $arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'Expecting a valid octal string, but received <code>%s</code>.',
            $arg
        ));
    }//end expectingOctalArgument()

    /**
     * Invalid Hex
     *
     * @param string $arg The error argument.
     *
     * @return \InvalidArgumentException
     */
    public static function expectingHexArgument(string $arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'Expecting a valid hex string, but received <code>%s</code>.',
            $arg
        ));
    }//end expectingHexArgument()

    /**
     * Invalid bit size
     *
     * @param int $arg The invalid argument.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidBitSize(int $arg)
    {
        $str = (string) $arg;
        return new self(sprintf(
            'The <code>BIT</code> numeric expects must be between <code>1</code> and <code>64</code> inclusive, but <code>%s</code> was supplied.',
            $str
        ));
    }//end invalidBitSize()

    /**
     * Invalid BIT argument
     *
     * @param string $arg The invalid argument.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidBitString(string $arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'Expecting a BIT argument but received <code>%s</code>.',
            $arg
        ));
    }//end invalidBitString()

    /**
     * Invalid bit size for existing value
     *
     * @param int    $arg   The invalid argument.
     * @param string $value The existing value.
     *
     * @return \InvalidArgumentException
     */
    public static function existingBitValueTooLarge(int $arg, string $value)
    {
        $str = (string) $arg;
        $value = self::specialCharacterFilter($value);
        return new self(sprintf(
            'You are attempting to set the bit size to <code>%s</code> but that is too small for the existing value <code>%s</code>.',
            $str,
            $value
        ));
    }//end existingBitValueTooLarge()
}//end class

?>