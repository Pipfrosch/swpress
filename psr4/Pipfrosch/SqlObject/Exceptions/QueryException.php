<?php
declare(strict_types=1);

/**
 * Invalid Query exceptions
 *
 * @package    SqlObject
 * @subpackage Exceptions
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject\Exceptions;

/**
 * Invalid Subtype exceptions
 */
class QueryException extends \InvalidArgumentException
{
    /**
     * Filter special html characters.
     *
     * @param string $input The string to filter.
     *
     * @return string The filtered string.
     */
    protected static function specialCharacterFilter(string $input): string
    {
        $s = array();
        $r = array();
        $s[] = '/&/';
        $r[] = '&amp;';
        $s[] = '/</';
        $r[] = '&lt;';
        $s[] = '/>/';
        $r[] = '&gt;';
        return preg_replace($s, $r, $input);
    }//end specialCharacterFilter()

    /**
     * Invalid clause for query type
     *
     * @param string $type   The query type.
     * @param string $clause The clause type.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidClauseType(string $type, string $clause)
    {
        $type = self::specialCharacterFilter($type);
        $clause = self::specialCharacterFilter($clause);
        return new self(sprintf(
            'The <code>%s</code> clause type is not applicable within the <code>%s</code> query type.',
            $clause,
            $type
        ));
    }//end invalidClauseType()

    /**
     * Clause required by query type
     *
     * @param string $type   The query type.
     * @param string $clause The clause type.
     *
     * @return \InvalidArgumentException
     */
    public static function requiredClause(string $type, string $clause)
    {
        $type = self::specialCharacterFilter($type);
        $clause = self::specialCharacterFilter($clause);
        return new self(sprintf(
            'The <code>%s</code> clause type is REQUIRED by the <code>%s</code> query type.',
            $clause,
            $type
        ));
    }//end requiredClause()
}//end class

?>