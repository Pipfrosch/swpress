<?php
declare(strict_types=1);

/**
 * Invalid Temporal Argument exceptions
 *
 * @package    SqlObject
 * @subpackage Exceptions
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject\Exceptions;

/**
 * Invalid Temporal Argument
 */
class InvalidTemporalArgument extends \InvalidArgumentException
{
    /**
     * Filter special html characters.
     *
     * @param string $input The string to filter.
     *
     * @return string The filtered string.
     */
    protected static function specialCharacterFilter(string $input): string
    {
        $s = array();
        $r = array();
        $s[] = '/&/';
        $r[] = '&amp;';
        $s[] = '/</';
        $r[] = '&lt;';
        $s[] = '/>/';
        $r[] = '&gt;';
        return preg_replace($s, $r, $input);
    }//end specialCharacterFilter()

    /**
     * Invalid string value
     *
     * @param string $arg          The invalid string argument.
     * @param bool   $microseconds Whether or not microseconds are allowed.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidTemporalString(string $arg, bool $microseconds = true)
    {
        $arg = self::specialCharacterFilter($arg);
        if ($microseconds) {
            $format = 'YYYY-MM-DD HH:MM:SS[.ssssss]';
        } else {
            $format = 'YYYY-MM-DD HH:MM:SS';
        }
        return new self(sprintf(
            'Expecting <code>%s</code> style argument, receiced <code>%s</code>.',
            $format,
            $arg
        ));
    }//end invalidTemporalString()

    /**
     * Invalid time portion
     *
     * @param string $arg The invalid time portion of string.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidTimeString(string $arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'The string <code>%s</code> is not a valid <code>HH:MM:SS</code> time string.',
            $arg
        ));
    }//end invalidTimeString()

    /**
     * Invalid date portion
     *
     * @param string $arg The invalid date portion of string.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidDateString(string $arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'The string <code>%s</code> is not a valid <code>YYYY-MM-DD</code> date string.',
            $arg
        ));
    }//end invalidTimeString()

    /**
     * Invalid Time
     *
     * @param string $arg The invalid time.
     *
     * @return \InvalidArgumentException
     */
    public static function bogusTime($arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'The string <code>%s</code> is not a valid time.',
            $arg
        ));
    }//end bogusTime()

    /**
     * Invalid Date
     *
     * @param string $arg The invalid date.
     *
     * @return \InvalidArgumentException
     */
    public static function bogusDate($arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'The string <code>%s</code> is not a valid date.',
            $arg
        ));
    }//end bogusDate()

    /**
     * Invalid Year
     *
     * @param string $arg The invalid year.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidYear($arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'MariaDB requires a year argument between <code>1000</code> and <code>9999</code> inclusive, but <code>%s</code> was provided.',
            $arg
        ));
    }//end invalidYear()

    /**
     * Argument does not match subtype
     *
     * @param string $subtype The subtype.
     * @param string $missing The missing part.
     *
     * @return \InvalidArgumentException
     */
    public static function missingComponentForSubtype(string $subtype, string $missing)
    {
        $subtype = self::specialCharacterFilter($subtype);
        $missing = self::specialCharacterFilter($missing);
        return new self(sprintf(
            'The <code>%s</code> was specified, but the argument is missing a <code>%s</code> component.',
            $subtype,
            $missing
        ));
    }//end missingComponentForSubtype()

    /**
     * Invalid Timezone
     *
     * @param string $arg The invalid timezone.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidTimeZone(string $arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'The specified time zone <code>%s</code> does not appear to be valid.',
            $arg
        ));
    }//end invalidTimeZone()

    /**
     * Non-32 bit timestamp
     *
     * @param int $arg The invalid timestamp.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidTimestampBytes(int $arg)
    {
        $str = (string) $arg;
        return new self(sprintf(
            'The MariaDB TIMESTAMP type requires a <code>32-bit</code> (<code>4 byte</code>) <code>signed integer</code>, received <code>%s</code>.',
            $str
        ));
    }//end invalidTimestampBytes()

    /**
     * Reserved timestamp
     *
     * @return \InvalidArgumentException
     */
    public static function epochTimestamp()
    {
        return new self(
            'The TIMESTAMP value corresponding with the UNIX epoch is reserved in MariaDB and can not be used.'
        );
    }//end epochTimestamp()
}//end class

?>