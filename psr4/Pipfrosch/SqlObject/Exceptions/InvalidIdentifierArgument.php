<?php
declare(strict_types=1);

/**
 * Invalid Identifier Argument exceptions
 *
 * @package    SqlObject
 * @subpackage Exceptions
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject\Exceptions;

/**
 * Invalid Identifier Argument
 */
class InvalidIdentifierArgument extends \InvalidArgumentException
{
    /**
     * Filter special html characters.
     *
     * @param string $input The string to filter.
     *
     * @return string The filtered string.
     */
    protected static function specialCharacterFilter(string $input): string
    {
        $s = array();
        $r = array();
        $s[] = '/&/';
        $r[] = '&amp;';
        $s[] = '/</';
        $r[] = '&lt;';
        $s[] = '/>/';
        $r[] = '&gt;';
        return preg_replace($s, $r, $input);
    }//end specialCharacterFilter()

    /**
     * Invalid Charset
     *
     * @param string $arg The invalid charset.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidCharset(string $arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'The provided charset <code>%s</code> is not a valid MariaDB charset.',
            $arg
        ));
    }//end invalidCharset()

    /**
     * Invalid Collation
     *
     * @param string $arg     The invalid collation.
     * @param string $charset The charset.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidCollation(string $arg, string $charset)
    {
        $arg = self::specialCharacterFilter($arg);
        $charset = self::specialCharacterFilter($charset);
        return new self(sprintf(
            'The provided collation <code>%s</code> is not a valid collation for the <code>%s</code> charset.',
            $arg,
            $charset
        ));
    }//end invalidCollation()

    /**
     * Charset not set
     *
     * @return \InvalidArgumentException
     */
    public static function charsetNotDefined()
    {
        return new self('The <code>charset</code> must be defined before setting the collation.');
    }//end charsetNotDefined()

    /**
     * Datatype not valid
     *
     * @param arg The invalid argument.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidDataType(string $arg)
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'The provided data type <string>%s</string> is not a valid MariaDB data type.',
            $arg
        ));
    }//end invalidDataType()
}//end class