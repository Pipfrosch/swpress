<?php
declare(strict_types=1);

/**
 * Invalid Subtype exceptions
 *
 * @package    SqlObject
 * @subpackage Exceptions
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject\Exceptions;

/**
 * Invalid Subtype exceptions
 */
class InvalidSubtype extends \InvalidArgumentException
{
    /**
     * Filter special html characters.
     *
     * @param string $input The string to filter.
     *
     * @return string The filtered string.
     */
    protected static function specialCharacterFilter(string $input): string
    {
        $s = array();
        $r = array();
        $s[] = '/&/';
        $r[] = '&amp;';
        $s[] = '/</';
        $r[] = '&lt;';
        $s[] = '/>/';
        $r[] = '&gt;';
        return preg_replace($s, $r, $input);
    }//end specialCharacterFilter()

    /**
     * Generic exception message.
     *
     * @param string $arg  The invalid subtype.
     * @param string $type The larger type.
     *
     * @return \InvalidArgumentException
     */
    public static function genericInvalidSubtype(string $arg, string $type = '')
    {
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'The specified subtype <code>%s</code> is not a valid MariaDB%s subtype.',
            $arg,
            $type
        ));
    }//end genericInvalidSubtype()

    /**
     * Exception for invalid integer subtype argument.
     *
     * @param string $arg The invalid subtype.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidIntegerSubtype(string $arg)
    {
        return self::genericInvalidSubtype($arg, ' <code> integer</code>');
    }//end

    /**
     * Exception for invalid temporal subtype argument
     *
     * @param string $arg The invalid subtype.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidTemporalSubtype(string $arg)
    {
        return self::genericInvalidSubtype($arg, '<code> time or date</code>');
    }//end invalidTemporalSubtype()
}//end class

?>