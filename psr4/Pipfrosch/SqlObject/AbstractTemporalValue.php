<?php
declare(strict_types=1);

/**
 * Temporal data value
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The TemporalValue object for MariaDB date and time types
 */
abstract class AbstractTemporalValue implements DataValueInterface
{
    /**
     * @var string
     */
    protected $type = 'TEMPORAL';

    /**
     * Should be upper case. Set by constructor.
     *
     * @var string
     */
    protected $subtype;

    /**
     * @var bool
     */
    protected $parameterize = true;

    /**
     * Must be instantiated by the constructor
     *
     * @var \DateTimeZone
     */
    protected $dateTimeZone;

    /**
     * Array of value subtypes other than null
     *
     * @see https://mariadb.com/kb/en/library/date-and-time-data-types/
     */
    protected $validSubtype = array(
        'DATE',
        'TIME',
        'DATETIME',
        'YEAR'
    );

    /**
     * @param null|string
     */
    protected $year = null;

    /**
     * @param null|string
     */
    protected $monthday = null;

    /**
     * @param null|string
     */
    protected $time = null;

    /**
     * @param string
     */
    protected $microseconds = null;

    /**
     * @var bool
     */
    protected $zerodate = true;

    /**
     * Validate the subtype. This should be called by setSubType()
     *
     * @param null|string $subtype The subType to set.
     *
     * @return string|bool|null
     */
    protected function validateSubType($subtype)
    {
        if (is_null($subtype)) {
            return null;
        }
        $subtype = strtoupper(trim($subtype));
        if (isset($this->subtypeSynonyms[$subtype])) {
            return $this->subtypeSynonyms[$subtype];
        }
        if (! in_array($subtype, $this->validSubtype)) {
            return false;
        }
        return $subtype;
    }//end validateSubType()

    /**
     * Return the type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }//end getType()

    /**
     * Return the subtype
     *
     * @return null|string
     */
    public function getSubtype()
    {
        return $this->subtype;
    }//end getSubtype()

    /**
     * Whether or not to parameterize the value when created a prepared
     * statement with the value.
     *
     * @return bool True if it should be parameterized
     */
    public function getParametized(): bool
    {
        return $this->parameterize;
    }//end getParametized()

    /**
     * Set whether or not to paramatize the value when creating a prepared
     * statement.
     *
     * @param bool $parameterize True if set to parameterize.
     *
     * @return void
     */
    public function setParametize(bool $parameterize): void
    {
        $this->parameterize = $parameterize;
    }//end setParametize()

    /**
     * Set the value from a PHP DateTime object
     *
     * @param \DateTime $dateTime The DateTime object.
     *
     * @return void
     */
    public function valueFromDateTime(\DateTime $dateTime): void
    {
        $epoch = intval($dateTime->format('U'), 10);
        $this->valueFromEpoch($epoch);
    }//end valueFromDateTime()

    /**
     * Generate a value string for the withWhatever functions
     *
     * @return null|string
     */
    protected function internalDateTime()
    {
        if ($this->subtype === 'TIMESTAMP') {
            return null;
        }
        $value = null;
        if (is_string($this->year) && is_string($this->monthday)) {
            $value = $this->year . $this->monthday;
        }
        if (is_string($this->time)) {
            if (is_string($value)) {
                $value .= ' ' . $this->time;
            } else {
                $value = $this->time;
            }
            if (is_string($this->microseconds)) {
                $value .= $this->microseconds;
            }
        }
        if (! is_string($value) && is_string($this->year)) {
            $value = $this->year;
        }
        return $value;
    }//end internalDateTime()

    /**
     * Return the value as a DateTime object
     *
     * @return null|\DateTime
     */
    public function getDateTimeObject()
    {
        if ($this->subtype === 'TIMESTAMP') {
            $value = $this->getValue();
        } else {
            $value = $this->internalDateTime();
        }    
        if (! is_string($value)) {
            return null;
        }
        $dt = new \DateTime($value, $this->dateTimeZone);
        return $dt;
    }//end getDateTimeObject()

    /**
     * Get the timezone identifier
     *
     * @param bool $abbr When true, it gets abbreviated timezone.
     *
     * @return string
     */
    public function getTimeZone(bool $abbr = false): string
    {
        $dt = new \DateTime();
        $dt->setTimeZone($this->dateTimeZone);
        if ($abbr) {
            return $dt->format('T');
        }
        return $dt->format('e');
    }//end getTimeZone()

    /**
     * Replace value with placeholder and add value to array
     *
     * TODO - determine quotes needed for dates
     *
     * @param array  $valueArray   The array of values.
     * @param string $placeholder  The placeholder.
     *
     * @return string
     */
    public function toParameterized(array &$valueArray, string $placeholder='?'): string
    {
        if (! $this->parameterize) {
            // TODO - this is where quotes may be needed
            return $this->__toString();
        }
        // TODO when null is intended for date value a NULL object value
        //  should be used.
        if (is_null($this->getValue())) {
            return '';
        }
        $valueArray[] = $this->__toString();
        return $placeholder;
    }//end toParameterized()

    /**
     * Return the value
     *
     * @return null|string
     */
    abstract public function getValue();

    /**
     * Set the value. Must throw exception if value does not match type
     * or subtype if specified
     *
     * @param string      $value   The value as a string.
     * @param null|string $subtype The data subtype.
     *
     * @return bool True on success, should throw exception on failure.
     *
     * @throws \InvalidArgumentException
     */
    abstract public function setValue(string $value, $subtype = null): bool;

    /**
     * Set the value from a UNIX timestamp (seconds from epoch)
     *
     * @param null|int $timestamp The UNIX timestamp to convert. If null, uses current.
     *
     * @return void
     */
    abstract public function valueFromEpoch($timestamp = null): void;

    /**
     * Should return the value as a string - but return the appropriate
     * string for SQL if null.
     *
     * @return string
     */
    abstract public function __toString(): string;

    /**
     * Export the value to a DOMNode
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode|\DOMComment
     */
    abstract public function toDomNode(\DOMDocument $dom);
}//end class

?>