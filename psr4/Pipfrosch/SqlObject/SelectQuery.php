<?php
declare(strict_types=1);

/**
 * The SELECT query object
 *
 * NOT ready to use.
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/* eventually will implement a query interface */

class SelectQuery extends AbstractQueryObject
{
    /**
     * Defines the query type
     *
     * @var string
     */
    protected $queryType = 'SELECT';

    /**
     * Clause types for SELECT query
     *
     * @var array
     */
    protected $clauseTypes = array(
        'SELECT',
        'INTO',
        'FROM',
        'WHERE',
        'ORDER BY',
        'LIMIT'
    );
     
    /* Some of these methods will be moved to an abstract class */
    
    /**
     * Export to string. Does not parameterize the query, mostly for debug.
     *
     * @return string
     */
    public function __toString(): string
    {
        $arr = array();
        /**
         * Never null, set in constructor and can not be removed.
         *
         * @psalm-suppress PossiblyNullReference
         */
        $arr[] = $this->selectClause->__toString();
        if (! is_null($this->intoClause)) {
            $arr[] = $this->intoClause->__toString();
        }
        if (! is_null($this->fromClause)) {
            $arr[] = $this->fromClause->__toString();
        }
        // does where clause require from? check
        if (! is_null($this->whereClause)) {
            $arr[] = $this->whereClause->__toString();
        }
        if (! is_null($this->orderByClause)) {
            $arr[] = $this->orderByClause->__toString();
        }
        if (! is_null($this->limitClause)) {
            $arr[] = $this->limitClause->__toString();
        }
        $string = implode(' ', $arr);
        return $string;
    }//end __toString()

    /**
     * Export query to DOMNode object
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode
     */
    public function toDomNode(\DOMDocument $dom): \DOMNode
    {
        $node = $dom->createElement('query');
        $node->setAttribute('type', 'SELECT');
        $node->setAttribute('engine', 'MariaDB');
        /**
         * Never null, set in constructor and can not be removed.
         *
         * @psalm-suppress PossiblyNullReference
         */
        $selectNode = $this->selectClause->toDomNode($dom);
        $node->appendChild($selectNode);
        if (! is_null($this->intoClause)) {
            $intoNode = $this->intoClause->toDomNode($dom);
            $node->appendChild($intoNode);
        }
        if (! is_null($this->fromClause)) {
            $fromNode = $this->fromClause->toDomNode($dom);
            $node->appendChild($fromNode);
        }
        if (! is_null($this->whereClause)) {
            $whereNode = $this->whereClause->toDomNode($dom);
            $node->appendChild($whereNode);
        }
        if (! is_null($this->orderByClause)) {
            $orderByNode = $this->orderByClause->toDomNode($dom);
            $node->appendChild($orderByNode);
        }
        if (! is_null($this->limitClause)) {
            $limitNode = $this->limitClause->toDomNode($dom);
            $node->appendChild($limitNode);
        }
        return $node;
    }//end toDomNode()

    /**
     * Query to PDO prepared statement
     *
     * @return \stdClass
     */
    public function toParameterized(): \stdClass
    {
        $values = array();
        $arr = array();
        /**
         * Never null, set in constructor and can not be removed.
         *
         * @psalm-suppress PossiblyNullReference
         */
        $arr[] = $this->selectClause->toParameterized($values);
        if (! is_null($this->intoClause)) {
            $arr[] = $this->intoClause->toParameterized($values);
        }
        if (! is_null($this->fromClause)) {
            $arr[] = $this->fromClause->toParameterized($values);
        }
        if (! is_null($this->whereClause)) {
            $arr[] = $this->whereClause->toParameterized($values);
        }
        if (! is_null($this->orderByClause)) {
            $arr[] = $this->orderByClause->toParameterized($values);
        }
        if (! is_null($this->limitClause)) {
            // This clause does not ever get parameterized.
            $arr[] = $this->limitClause->__toString();
        }
        $return = new \stdClass();
        $return->parameterized = implode(' ', $arr);
        $return->values = $values;
        return $return;
    }//end toParameterized()

    /**
     * The constructor.
     *
     * @param \Pipfrosch\SqlObject\SelectClause $selectClause The select clause.
     */
    public function __construct(\Pipfrosch\SqlObject\SelectClause $selectClause)
    {
        $this->selectClause = $selectClause;
    }//end __construct()
}//end of class