SQL Objects
===========

This is a collection of classes intended to make communication with a database
cleaner, taking into consideration that database types do not always jive with
PHP types.

Initially the classes are being developed for the purposes of constructing
prepared statements that allow validation of the values when the column type
is known, but eventually it will also include parsing classes and replace the
functionallity of the PHPSQLParser set of classes.

Unit tests are in the `tests/SqlObject` directory of the main `swpress` project.

Basically the concept is that an SQL query will be an object that contains
various clause objects.

A `SELECT` query object will at a minimum contain a `SELECT` clause object, and
likely will also have a `FROM` and `WHERE` clause object and often will have a
`ORDER BY` and sometimes a `LIMIT` clause object.

The various clause objects will sometimes have additional clause objects,
sometimes even a query object, and often value objects.

From the query object, a method to create the query the string will exist,
resulting in a string that can be fed to the PDO `prepare()` method and an
array of values that can then be fed to the prepared statement `execute()`
method, resulting in carrying out the query.
