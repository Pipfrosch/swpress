<?php
declare(strict_types=1);

/**
 * The TimeStamp object for MariaDB temporal types.
 *
 * WARNING: MariaDB does not store any timezone information with temporal data types. It is my recommondation
 *  that you always use UTC with a server but sometimes with temporal data types, UTC is not the appropriate
 *  timezone for efficient queries. For example, you may want to know trends on what hours during the day are
 *  most likely to result in sales of products on the sales floor, from stores in a wide variety of different
 *  timezones.
 *
 *  When the temporal values being stored are NOT in UTC, I recommend your database tables have a column where
 *  you can store the time zone used for the data in rows that include temporal column data.
 *
 *  With this class, you can set the timezone when instantiating the class or after instantiating the class,
 *  but note that changing the timezone after data has been added to the object does NOT result in converting
 *  the data to the new timezone. Also note that data added as a string through the setValue() method does not
 *  take timezone into consideration. The set timezone is only taken into consideration when adding data through
 *  either the valueFromEpoch() method or the valueFromDateTime() method. The former takes a UNIX timestamp as
 *  the argument (seconds since January 1 1970 00:00:00 UTC) and the latter takes a PHP \DateTime object as the
 *  argument.
 *
 *  If you need to convert to an existing instance of this class with values to a different time zone, the way
 *  to accomplish that task is to use the getDateTimeObject() method, which returns a \DateTime object (or null)
 *  and then you can change the timezome and use the valueFromDateTime() with that object, resulting in converting
 *  it to the new timezone.
 *
 * WARNING: When importing from a \DateTime object, the timezone in that object is only used for the purpose of
 *  converting the date and time to what is set in the instance of this class importing it, which defaults to
 *  whatever your PHP application is using as the timezone. If you need the data in instances of this object to
 *  be in a specific timezone, make sure to instantiate this class with the precise timezone you intend the data
 *  strings to be in when used in a MariaDB database.
 *
 * Again I emphasize that for best practices, MariaDB will be configuted to use UTC and your PHP scripts will be
 *  configured to use UTC and when you need to store data in another timezone as often is legitimately the case,
 *  design your database with a column where you can store the timezone for the temporal data.
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The DateTime object for MariaDB temporal types.
 */
class DateTimeValue extends AbstractTemporalValue implements DataValueInterface
{
    /**
     * @param array
     */
    protected $zoneList;

    /**
     * Set the timezone
     *
     * @param string $timezone The timezone to set.
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     */
    public function setTimeZone(string $timezone): bool
    {
        if (in_array($timezone, $this->zoneList)) {
            $this->dateTimeZone = new \DateTimeZone($timezone);
            return true;
        }
        throw Exceptions\InvalidTemporalArgument::invalidTimeZone($timezone);
    }//end setTimeZone()

    /**
     * Set the subtype. If no date/time properties are set and the
     * subtype is valid it always works. If ant date/time properties
     * are set then setting the subtype only works of the result
     * will of getValue will be valid for the subtype.
     *
     * @param string $subtype The subtype to set.
     *
     * @return bool True on success.
     *
     * @throws \InvalidArgumentException
     */
    public function setSubtype(string $subtype): bool
    {
        $origSubtype = $subtype;
        $subtype = $this->validateSubType($subtype);
        if (! is_string($subtype)) {
            throw Exceptions\InvalidSubtype::invalidTemporalSubtype($origSubtype);
        }
        if (is_null($this->year)
        && is_null($this->monthday)
        && is_null($this->time)
        && is_null($this->microseconds)) {
            $this->subtype = $subtype;
            return true;
        }
        switch ($subtype) {
            case 'DATE':
            case 'DATETIME':
            case 'YEAR':
                if (is_null($this->year)) {
                    throw Exceptions\InvalidTemporalArgument::missingComponentForSubtype($subtype, 'year');
                }
                if ($subtype === 'YEAR') {
                    break;
                }
                if (is_null($this->monthday)) {
                    throw Exceptions\InvalidTemporalArgument::missingComponentForSubtype($subtype, 'month and day');
                }
                if ($subtype === 'DATE') {
                    break;
                }
                //no break
            default:
                if (is_null($this->time)) {
                    throw Exceptions\InvalidTemporalArgument::missingComponentForSubtype($subtype, 'time');
                }
        }
        $this->subtype = $subtype;
        return true;
    }//end setSubtype()

    /**
     * Set the value from conforming string.
     *
     * @param string      $value   The value as a string.
     * @param string|null $subtype The subtype associated with the value.
     *
     * @return bool True on success, should throw exception on failure.
     *
     * @throws \InvalidArgumentException
     */
    public function setValue(string $value, $subtype = null): bool
    {
        $string = trim($value);
        if (is_null($subtype)) {
            $subtype = $this->subtype;
        }
        $origSubtype = $subtype;
        $subtype = $this->validateSubType($subtype);
        if (! is_string($subtype)) {
            throw Exceptions\InvalidSubtype::invalidTemporalSubtype($value);
        }
        $microseconds = null;
        //attempt to get get microseconds first
        $pos = strrpos($string, '.');
        if (is_int($pos)) {
            $microseconds = substr($string, $pos);
            if (preg_match('/^\.[0-9]{1,6}$/', $microseconds) === 0) {
                throw Exceptions\InvalidTemporalArgument::invalidTemporalString($value);
            }
            $string = substr($string, 0, $pos);
        }
        //attempt to get the time portion
        $time = null;
        $pos = strpos($string, ':');
        if (is_int($pos)) {
            $pos = $pos - 2;
            if ($pos < 0) {
                throw Exceptions\InvalidTemporalArgument::invalidTemporalString($value);
            }
            $time = substr($string, $pos);
            if (preg_match('/^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]$/', $time) !== 1) {
                throw Exceptions\InvalidTemporalArgument::invalidTimeString($time);
            }
            $hour = intval(substr($time, 0, 2), 10);
            $minutes = intval(substr($time, 3, 2), 10);
            $seconds = intval(substr($time, 6), 10);
            if ($hour > 23 || $minutes > 59 || $seconds > 59) {
                throw Exceptions\InvalidTemporalArgument::bogusTime($time);
            }
            $string = trim(substr($string, 0, $pos));
        }
        //attempt to parse the date portion
        $year = null;
        $monthday = null;
        $len = strlen($string);
        switch ($len) {
            case 0:
                break;
            case 4:
                if (is_string($time) || is_string($microseconds)) {
                    // year only but has time
                    throw Exceptions\InvalidTemporalArgument::invalidTemporalString($string);
                }
                if (intval($string, 10) >= 1000) {
                    $year = $string;
                } else {
                    throw Exceptions\InvalidTemporalArgument::invalidYear($string);
                }
                break;
            case 10:
                if (preg_match('/^[1-9][0-9]{3,3}-[0-1][0-9]-[0-3][0-9]$/', $string) !== 1) {
                    throw Exceptions\InvalidTemporalArgument::invalidDateString($string);
                }
                $intyear = intval(substr($string, 0, 4), 10);
                $intmonth = intval(substr($string, 5, 2), 10);
                $intday = intval(substr($string, 8), 10);
                if (! checkdate($intmonth, $intday, $intyear)) {
                    throw Exceptions\InvalidTemporalArgument::bogusDate($string);
                }
                $year = substr($string, 0, 4);
                $monthday = substr($string, 4);
                break;
            default:
                throw Exceptions\InvalidTemporalArgument::invalidTemporalString($string);
                break;
        }
        // now validate against subtype
        switch ($subtype) {
            case 'TIME':
                if (! is_string($time)) {
                    throw Exceptions\InvalidTemporalArgument::missingComponentForSubtype('TIME', 'time');
                }
                break;
            case 'DATE':
                if (! is_string($year)) {
                    throw Exceptions\InvalidTemporalArgument::missingComponentForSubtype('DATE', 'date');
                }
                if (! is_string($monthday)) {
                    throw Exceptions\InvalidTemporalArgument::missingComponentForSubtype('DATE', 'month/day');
                }
                break;
            case 'DATETIME':
                if (! is_string($year)) {
                    throw Exceptions\InvalidTemporalArgument::missingComponentForSubtype('DATETIME', 'date');
                }
                if (! is_string($monthday)) {
                    throw Exceptions\InvalidTemporalArgument::missingComponentForSubtype('DATETIME', 'month/day');
                }
                if (! is_string($time)) {
                    throw Exceptions\InvalidTemporalArgument::missingComponentForSubtype('DATETIME', 'time');
                }
                break;
            default:
                if (! is_string($year)) {
                    throw Exceptions\InvalidTemporalArgument::missingComponentForSubtype('YEAR', 'year');
                }
        }
        // set the class properties
        $this->year = $year;
        $this->monthday = $monthday;
        $this->time = $time;
        $this->microseconds = $microseconds;
        $this->subtype = $subtype;
        return true;
    }//end setValue()

    /**
     * Set the value from a UNIX timestamp (seconds from epoch)
     *
     * @param null|int $timestamp The UNIX timestamp to convert. If null, uses current.
     *
     * @return void
     */
    public function valueFromEpoch($timestamp = null): void
    {
        if (! is_int($timestamp)) {
            $timestamp = time();
        }
        //$defaultTimeZone = date_default_timezone_get();
        //$systz = new \DateTimeZone($defaultTimeZone);
        $dt = new \DateTime();
        $dt->setTimeZone($this->dateTimeZone);
        $dt->setTimestamp($timestamp);
        //set the property values
        $this->year = $dt->format('Y');
        $this->monthday = $dt->format('-m-d');
        $this->time = $dt->format('H:i:s');
        $this->microseconds = null;
    }//end valueFromEpoch()

    /**
     * Returns the value appropriate to the type
     *
     * @return string|null
     */
    public function getValue()
    {
        $invalid = false;
        $return = '';
        switch ($this->subtype) {
            case 'YEAR':
            case 'DATE':
            case 'DATETIME':
                if (is_string($this->year)) {
                    $return .= $this->year;
                } else {
                    $invalid = true;
                    break;
                }
                if ($this->subtype === 'YEAR') {
                    break;
                }
                if (is_string($this->monthday)) {
                    $return .= $this->monthday;
                    if ($this->subtype === 'DATE') {
                        break;
                    } else {
                        $return .= ' ';
                    }
                } else {
                    $invalid = true;
                    break;
                }
                // no break
            case 'TIME':
                if (is_string($this->time)) {
                    $return .= $this->time;
                } else {
                    $invalid = true;
                    break;
                }
                if (is_string($this->microseconds)) {
                    $return .= $this->microseconds;
                }
                break;
            default:
                break;
        }
        if ($invalid) {
            return null;
        }
        return $return;
    }//end getValue()

    /**
     * Returns value as a string but returns zero values
     *  if value is null.
     *
     * @return string
     */
    public function __toString(): string
    {
        $return = $this->getValue();
        if (! is_string($return)) {
            $return = '';
        }
        if (empty($return) && $this->zerodate) {
            switch ($this->subtype) {
                case 'YEAR':
                    $return = '0000';
                    break;
                case 'DATE':
                    $return = '0000-00-00';
                    break;
                case 'DATETIME':
                    $return = '0000-00-00 00:00:00';
                    break;
                default:
                    break;
            }
        }
        return $return;
    }//end __toString()

    /**
     * Return value as DOMNode
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode|\DOMComment
     */
    public function toDomNode(\DOMDocument $dom)
    {
        $usezerodate = false;
        $value = $this->getValue();
        if (is_null($value)) {
            $value = '';
            if ($this->zerodate) {
                switch ($this->subtype) {
                    case 'YEAR':
                        $value = '0000';
                        $usezerodate = true;
                        break;
                    case 'DATE':
                        $value = '0000-00-0000';
                        $usezerodate = true;
                        break;
                    case 'DATETIME':
                        $value = '0000-00-0000 00:00:00';
                        $usezerodate = true;
                        break;
                    default:
                        break;
                }
            }
        }
        if (empty($value)) {
            $node = $dom->createComment('Invalid ' . $this->subtype . ' value');
            return $node;
        }
        $node = $dom->createElement('value', $value);
        $node->setAttribute('type', $this->type);
        $node->setAttribute('subtype', $this->subtype);
        if ($usezerodate) {
            $node->setAttribute('zerodate', 'zerodate');
        } else {
            $node->setAttribute('tz', $this->getTimeZone(true));
        }
        return $node;
    }//end toDomNode()

    /**
     * Spawn a YEAR version of the object.
     *
     * @param string|null $value The value to use.
     *
     * @return static
     */
    public function withYear($value = null)
    {
        $return = new static();
        $return->setTimeZone($this->getTimeZone());
        $return->setSubtype('YEAR');
        $return->setZeroDate($this->zerodate);
        if (is_null($value)) {
            $value = $this->internalDateTime();
        }
        if (is_string($value)) {
            $return->setValue($value);
        }
        return $return;
    }//end withYear()

    /**
     * Spawn a DATE version of the object.
     *
     * @param string|null $value The value to use.
     *
     * @return static
     */
    public function withDate($value = null)
    {
        $return = new static();
        $return->setTimeZone($this->getTimeZone());
        $return->setSubtype('DATE');
        $return->setZeroDate($this->zerodate);
        if (is_null($value)) {
            $value = $this->internalDateTime();
        }
        if (is_string($value)) {
            $return->setValue($value);
        }
        return $return;
    }//end withDate()

    /**
     * Spawn a TIME version of the object.
     *
     * @param string|null $value The value to use.
     *
     * @return static
     */
    public function withTime($value = null)
    {
        $return = new static();
        $return->setTimeZone($this->getTimeZone());
        $return->setSubtype('TIME');
        $return->setZeroDate($this->zerodate);
        if (is_null($value)) {
            $value = $this->internalDateTime();
        }
        if (is_string($value)) {
            $return->setValue($value);
        }
        return $return;
    }//end withTime()

    /**
     * Spawn a DATETIME version of the object.
     *
     * @param string|null $value The value to use.
     *
     * @return static
     */
    public function withDateTime($value = null)
    {
        $return = new static();
        $return->setTimeZone($this->getTimeZone());
        $return->setSubtype('DATETIME');
        $return->setZeroDate($this->zerodate);
        if (is_null($value)) {
            $value = $this->internalDateTime();
        }
        if (is_string($value)) {
            $return->setValue($value);
        }
        return $return;
    }//end withDateTime()

    /**
     * Set whether or not zerodate is allowed.
     *
     * @param bool $zerodate True if allowed.
     *
     * @return void
     */
    public function setZeroDate(bool $zerodate): void
    {
        $this->zerodate = $zerodate;
    }//end setZeroDate()

    /**
     * The constructor.
     *
     * @param null|string $tzi      The time zone identifier.
     * @param null|bool   $zerodate Where or not to allow zerodate.
     */
    public function __construct($tzi = null, $zerodate = null)
    {
        $this->subtype = 'DATETIME';
        $this->zoneList = timezone_identifiers_list();
        if (is_null($tzi)) {
            $tzi = date_default_timezone_get();
        }
        $zoneList = timezone_identifiers_list();
        if (! in_array($tzi, $zoneList)) {
            $tzi = 'UTC';
        }
        $this->dateTimeZone = new \DateTimeZone($tzi);
        if (is_bool($zerodate)) {
            $this->zerodate = $zerodate;
        }
    }//end __construct()
}//end class