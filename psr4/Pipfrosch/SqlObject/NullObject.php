<?php
declare(strict_types=1);

/**
 * Null Value Object
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The IntegerValue object for MariaDB numeric types
 */
class NullValue implements DataValueInterface
{
    /**
     * @var bool
     */
    protected $parametize = false;

    /**
     * Get the type.
     * Must return the type. Should be upper case ASCII.
     *
     * @return string
     */
    public function getType()
    {
        return 'NULL';
    }//end getType()

    /**
     * Get the subtype.
     * Must return null or string. When string, should be upper case ASCII.
     *
     * @return null|string
     */
    public function getSubtype()
    {
        return null;
    }//end getSubtype()

    /**
     * Get the value. Must return null or string, even for numeric types.
     * Should not return quoted value.
     *
     * @return null|string
     */
    public function getValue()
    {
        return null;
    }//end getValue()

    /**
     * Set the value. Must throw exception if value does not match type
     * or subtype if specified
     *
     * @param string      $value   The value as a string.
     * @param null|string $subtype The data subtype.
     *
     * @return bool True on success, should throw exception on failure.
     *
     * @throws \InvalidArgumentException
     */
    public function setValue(string $value, $subtype = null): bool
    {
        return false;
    }//end setValue()

    /**
     * Whether or not to parametize the value when created a prepared
     * statement with the value.
     *
     * @return bool True if it should be parametized
     */
    public function getParametized(): bool
    {
        return $this->parametize;
    }//end getParametized()

    /**
     * Set whether or not to paramatize the value when creating a prepared
     * statement.
     *
     * @param bool $parametize True if set to parametize.
     *
     * @return void
     */
    public function setParametize(bool $parametize): void
    {
        $this->parametize = $parametize;
    }//end setParametize()

    /**
     * Return a DOMDocument Node
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode
     */
    public function toDomNode(\DOMDocument $dom): \DOMNode
    {
        $node = $dom->createElement('value');
        $node->setAttribute('type', 'null');
        return $node;
    }//end toDomNode()

    /**
     * Should return the value as a string - equivalent to getValue()
     * but return empty string if null except if type is NULL.
     *
     * @return string
     */
    public function __toString()
    {
        return 'NULL';
    }//end __toString()

    /**
     * Replace value with placeholder and add value to array
     *
     * TODO - determine how NULL is handled in parameterized queries.
     *
     * @param array $valueArray   The array of values.
     * @param string $placeholder The placeholder.
     *
     * @return string
     */
    public function toParameterized(array &$valueArray, string $placeholder='?'): string
    {
        //TODO investigate how NULL is handled
        return '';
    }//end toParameterized()

}//end class

?>