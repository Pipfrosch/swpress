<?php
/**
 * Interface for data values that are either constant or can be inserted into a database column.
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The DataValue Interface
 */
interface DataValueInterface
{
    /**
     * Get the type.
     * Must return the type. Should be upper case ASCII.
     *
     * @return string
     */
    public function getType();

    /**
     * Get the subtype.
     * Must return null or string. When string, should be upper case ASCII.
     *
     * @return null|string
     */
    public function getSubtype();

    /**
     * Get the value. Must return null or string, even for numeric types.
     * Should not return quoted value.
     *
     * @return null|string
     */
    public function getValue();

    /**
     * Set the value. Must throw exception if value does not match type
     * or subtype if specified
     *
     * @param string      $value   The value as a string.
     * @param null|string $subtype The data subtype.
     *
     * @return bool True on success, should throw exception on failure.
     *
     * @throws \InvalidArgumentException
     */
    public function setValue(string $value, $subtype = null): bool;

    /**
     * Whether or not to parametize the value when created a prepared
     * statement with the value.
     *
     * @return bool True if it should be parametized
     */
    public function getParametized(): bool;

    /**
     * Set whether or not to paramatize the value when creating a prepared
     * statement.
     *
     * @param bool $parametize True if set to parametize.
     *
     * @return void
     */
    public function setParametize(bool $parametize): void;

    /**
     * Should return the value as a string - equivalent to getValue()
     * but return NULL if null.
     *
     * @return string
     */
    public function __toString();

    /**
     * Export the value to a DOMNode
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode|\DOMComment
     */
    public function toDomNode(\DOMDocument $dom);

    /**
     * Replace value with placeholder and add value to array
     *
     * TODO - determine quotes needed for non-parameterized
     *        non-integer values.
     *
     * @param array  $valueArray   The array of values.
     * @param string $placeholder  The placeholder.
     *
     * @return string
     */
    public function toParameterized(array &$valueArray, string $placeholder='?'): string;
}//end interface

?>