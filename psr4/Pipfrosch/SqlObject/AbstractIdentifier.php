<?php
declare(strict_types=1);

/**
 * Abstract class for identifiers
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The Identifier Interface
 */
abstract class AbstractIdentifier
{
    /**
     * List of identifier types
     *
     * @var array
     */
    protected $validSubtype = array(
        'DATABASE',
        'TABLE',
        'COLUMN',
        'INDEX',
        'ALIAS',
        'VIEW',
        'TRIGGER',
        'EVENT',
        'VARIABLE',
        'PARTITION',
        'TABLESPACE',
        'SAVEPOINT',
        'LABEL',
        'USER',
        'ROLE',
        'STORED_ROUTINE'
    );

    /**
     * Array of encodings to try
     *
     * @var array
     */
    protected $mbencodings = array(
        'ASCII',
        'UTF-8',
        'eucjp-win',
        'ISO-8859-1',
        'ISO-8859-2',
        'ISO-8859-5',
        'ISO-8859-7',
    );

    /**
     * The subtype
     *
     * @var null|string
     */
    protected $subtype = null;

    /**
     * The identifier value - should be set by constructor.
     *
     * @var null|string
     */
    protected $value = null;

    /**
     * Whether or not to use ANSI_QUOTES
     *
     * Set by constructor if true
     *
     * @var bool
     */
    protected $ANSIquotes = false;

    /**
     * Whether or not the identifier must be
     * quoted
     *
     * @var bool
     */
    protected $requiresQuotes = false;

    /**
     * Returns maximum length
     *
     * @return int
     */
    public function maximumLength(): int
    {
        switch ($this->subtype) {
            case 'user':
                return 80;
                break;
            case 'role':
                return 128;
                break;
            case 'label':
                return 16;
                break;
            case 'alias':
                return 256;
                break;
            default:
                return 64;
                break;
        }
    }

    /**
     * Strip existing quotes from the identifier.
     *
     * @param string $identifier
     *
     * @return void
     */
    public function stripQuotes(string &$identifier): void
    {
        $strip = false;
        $len = strlen($identifier);
        if ($len > 2) {
            $first = substr($identifier, 0, 1);
            $last = substr($identifier, -1);
            if ($first === $last) {
                if ($first === '`') {
                    $strip = true;
                } elseif($first === '"' && $this->ANSIquotes) {
                    $strip = true;
                }
            }
        }
        if ($strip) {
            $identifier = substr($identifier,1,($len -2));
        }
    }//end stripQuotes()

    /**
     * validate a legal identifier and also determine if it MUST
     * be quoted.
     *
     * @param string $identifier
     *
     * @return bool
     *
     * @see https://mariadb.com/kb/en/library/identifier-names/
     */
    public function validateIdentifier(string &$identifier): bool
    {
        $maxlen = $this->maximumLength();
        if (strlen($identifier) > $maxlen) {
            return false;
        }
        // character set
        if (function_exists('mb_detect_encoding')) {
            if (! mb_detect_encoding($identifier, 'ASCII', true) && ! mb_detect_encoding($identifier, 'UTF-8', true)) {
                //try to detect encoding and convert
                $encoding = mb_detect_encoding($identifier, $this->mbencodings);
                if (is_string($encoding)) {
                    $identifier = mb_convert_encoding($identifier, 'UTF-8', $encoding);
                }
            }
        }
        // detect ascii null byte
        if (preg_match('/\x00/', $identifier) > 0) {
            return false;
        }
        // detect extended unicode range
        if (preg_match('/[^\x{0000}-\x{FFFF}]/u', $identifier) > 0) {
            return false;
        }
        $requiresQuotes = false;
        $last = substr($identifier, -1);
        if ($last === ' ') {
            if (in_array($this->subtype, array('DATABASE', 'COLUMN', 'TABLE'))) {
                return false;
            } else {
                $requiresQuotes = true;
            }
        }
        if (ctype_digit($identifier)) {
            $requiresQuotes = true;
        }
        if (preg_match('/^[0-9]+e/', $identifier)) {
            $requiresQuotes = true;
        }
        if (preg_match('/[^0-9a-zA-Z$_\x{0080}-\x{FFFF}]/u', $identifier) > 0) {
            $requiresQuotes = true;
        }
        // after all checks have passed
        $this->requiresQuotes = $requiresQuotes;
        $this->value = $identifier;
        return true;
    }

    /**
     * Return the type
     *
     * @return string
     */
    public function getType()
    {
        return 'identifier';
    }//end getType()

    /**
     * Return the value
     *
     * @return null|string
     */
    public function getValue()
    {
        return $this->value;
    }//end getValue()

    /**
     * Return a string. Override for column and table.
     *
     * @return string
     */
    public function __toString()
    {
        if (! is_string($this->value)) {
            return '';
        }
        if (! $this->requiresQuotes) {
            $return = $this->value;
        } elseif ($this->ANSIquotes) {
            $return = '"' . preg_replace('/"/', "", $this->value) . '"';
        } else {
            $return = '`' . preg_replace('/`/', '``', $this->value) . '`';
        }
        // final check to see if result is reserved
        $test = strtoupper($return);
        $keywords = StaticMethods\Constants::returnReservedKeywords();
        if (in_array($test, $keywords)) {
            if ($this->ANSIquotes) {
                return '"' . $return . '"';
            } else {
                return '`' . $return . '`';
            }
        }
        return $return;
    }

    /**
     * Return DOMNode
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode|\DOMComment
     */
    public function toDomNode(\DOMDocument $dom)
    {
        if (is_null($this->value)) {
            $node = $dom->createComment('Invalid Identifier');
            return $node;
        }        
        $node = $dom->createElement('identifier', $this->__toString());
        /**
         * @psalm-suppress PossiblyNullArgument
         */
        $node->setAttribute('type', $this->subtype);
        if ($this->ANSIquotes) {
            $node->setAttribute('quote', 'ANSI');
        }
        return $node;
    }
}//end class

?>