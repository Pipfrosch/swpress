<?php
declare(strict_types=1);

/**
 * The LIMIT clause
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SqlObject;

/**
 * The Limit Clause
 */
class LimitClause implements QueryClauseInterface
{
    /**
     * @null|\Pipfrosch\SqlObject\IntegerValue
     */
    protected $offset = null;

    /**
     * @null|\Pipfrosch\SqlObject\IntegerValue
     */
    protected $rowcount = null;

    /**
     * @null|\Pipfrosch\SqlObject\IntegerValue
     */
    protected $rowsexamined = null;

    /**
     * Set the offset
     *
     * @param int  $offset     The offset to set.
     * @param bool $parametize Whether or not to parametize the offset.
     *
     * @return bool True on success, otherwise false
     */
    public function setOffset(int $offset, bool $parametize = false): bool
    {
        if ($offset < 0) {
            return false;
        }
        $intValue = new IntegerValue();
        try {
            $offsetObject = $intValue->withInt($offset, false);
        } catch (\InvalidArgumentException $e) {
            return false;
        }
        $offsetObject->setParametize($parametize);
        $this->offset = $offsetObject;
        return true;
    }//end setOffset()

    /**
     * Get the offset
     *
     * @return null|string The offset as a string.
     */
    public function getOffset()
    {
        if (is_null($this->offset)) {
            return null;
        }
        return $this->offset->__toString();
    }//end getOffset()

    /**
     * Set the row count
     *
     * @param int  $rowcount   The rowcount to set.
     * @param bool $parametize Whether or not to parametize the rowcount.
     *
     * @return bool True on success, otherwise false
     */
    public function setRowcount(int $rowcount, bool $parametize = false): bool
    {
        if ($rowcount < 0) {
            return false;
        }
        $intValue = new IntegerValue();
        try {
            $rowcountObject = $intValue->withInt($rowcount, false);
        } catch (\InvalidArgumentException $e) {
            return false;
        }
        $rowcountObject->setParametize($parametize);
        $this->rowcount = $rowcountObject;
        return true;
    }//end setRowcount()

    /**
     * Get the rowcount
     *
     * @return null|string The rowcount as a string.
     */
    public function getRowcount()
    {
        if (is_null($this->rowcount)) {
            return null;
        }
        return $this->rowcount->__toString();
    }//end getRowcount()

    /**
     * Set the ROWS EXAMINED
     *
     * @param int  $rowsexamined   The rowcount to set.
     * @param bool $parametize     Whether or not to parametize the rowsexamined.
     *
     * @return bool True on success, otherwise false
     */
    public function setRowsExamined(int $rowsexamined, bool $parametize = false): bool
    {
        if ($rowsexamined < 0) {
            return false;
        }
        $intValue = new IntegerValue();
        try {
            $rowsExaminedObject = $intValue->withInt($rowsexamined, false);
        } catch (\InvalidArgumentException $e) {
            return false;
        }
        $rowsExaminedObject->setParametize($parametize);
        $this->rowsexamined = $rowsExaminedObject;
        return true;
    }//end setRowsExamined()

    /**
     * Get the rowsexamined
     *
     * @return null|string The rows examined as a string.
     */
    public function getRowsExamined()
    {
        if (is_null($this->rowsexamined)) {
            return null;
        }
        return $this->rowsexamined->__toString();
    }//end getRowsExamined()

    /**
     * Get the type
     *
     * @return string
     */
    public function getType(): string
    {
        return 'LIMIT';
    }//end getType()

    /**
     * Return clause as a string
     *
     * @return string
     */
    public function __toString(): string
    {
        if (is_null($this->rowcount) && is_null($this->rowsexamined)) {
            //Not a valid LIMIT clause
            return '';
        }
        if (! is_null($this->offset) && is_null($this->rowcount)) {
            //Not a valid LIMIT clause
            return '';
        }
        $return = 'LIMIT ';
        if (! is_null($this->offset)) {
            $return = $return . $this->offset->__toString() . ',';
        }
        if (! is_null($this->rowcount)) {
            $return .= $this->rowcount->__toString();
        }
        if (! is_null($this->rowsexamined)) {
            $return = $return . ' ROWS EXAMINED ' . $this->rowsexamined->__toString();
        }
        $return = preg_replace('/\s+/', ' ', $return);
        //should never happen but...
        if (! is_string($return)) {
            return '';
        }
        return $return;
    }//end __toString()

    /**
     * Return clause as a DOMNode
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode|\DOMComment
     */
    public function toDomNode(\DOMDocument $dom)
    {
        $valid = true;
        if (is_null($this->rowcount) && is_null($this->rowsexamined)) {
            //Not a valid LIMIT clause
            $valid = false;
        }
        if (! is_null($this->offset) && is_null($this->rowcount)) {
            //Not a valid LIMIT clause
            $valid = false;
        }
        if (! $valid) {
            $node = $dom->createComment('Invalid LIMIT clause');
            return $node;
        }
        $node = $dom->createElement('clause');
        $node->setAttribute('type', 'LIMIT');
        if (! is_null($this->offset)) {
            $child = $dom->createElement('offset');
            $node->appendChild($child);
            $offsetNode = $this->offset->toDomNode($dom);
            $child->appendChild($offsetNode);
        }
        if (! is_null($this->rowcount)) {
            $child = $dom->createElement('rowcount');
            $node->appendChild($child);
            $rowcountNode = $this->rowcount->toDomNode($dom);
            $child->appendChild($rowcountNode);
        }
        if (! is_null($this->rowsexamined)) {
            $child = $dom->createElement('rowsexamined');
            $node->appendChild($child);
            $rowsexaminedNode = $this->rowsexamined->toDomNode($dom);
            $child->appendChild($rowsexaminedNode);
        }
        return $node;
    }//end toDomNode()
}//end class

?>