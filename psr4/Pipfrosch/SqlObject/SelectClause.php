<?php
declare(strict_types=1);

/**
 * The SELECT clause
 *
 * WORK IN PROGRESS
 *
 * @package SqlObject
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 *
 * @see https://mariadb.com/kb/en/library/select/
 * @see https://mariadb.com/kb/en/library/optimizer-hints/
 */

namespace Pipfrosch\SqlObject;

/**
 * The SELECT Clause
 */
class SelectClause implements QueryClauseInterface
{
    /**
     * array of allowed modifiers for SELECT
     *
     * @var array
     */
    protected $allowedModifiers = array(
        'ALL',
        'DISTINCT',
        'DISTINCTROW'
    );

    /**
     * The modifier
     *
     * @var null|string
     */
    protected $modifier = null;

    /**
     * HIGH PRIORTY Optimizer Hint
     *
     * @see https://mariadb.com/kb/en/high_priority-and-low_priority-clauses/
     *
     * @var bool
     */
    protected $highPriority = false;

    /**
     * STRAIGHT_JOIN Optimizer Hint
     *
     * Only applicable with JOIN
     *
     * @see https://mariadb.com/kb/en/library/join-syntax/
     *
     * @var bool
     */
    protected $straightJoin = false;

    /* add small_result big_result buffer_result here */

    /**
     * SQL_CACHE / SQL_NO_CACHE Optimizer Hint
     *
     * @see https://mariadb.com/kb/en/library/query-cache/
     *
     * @nvar null|bool When null, neither optimizer value is specified. When
     *                 true, SQL_CACHE is specified. When false, SQL_NO_CACHE.
     */
    protected $queryCache = null;

    /**
     * SQL_CALC_FOUND_ROWS Optimizer Hint
     *
     * When a LIMIT clause is used, this counts how many matches there would have
     * been without the LIMIT clause so it can be selected in the next query with
     * the FOUND_ROWS() select.
     *
     * @see https://mariadb.com/kb/en/library/found_rows/
     *
     * @var bool
     */
    protected $sqlCalcFoundRows = false;

    /* use/force/ignore index ??? */

    /**
     * Set the modifier argument.
     *
     * @param null|string $modifier
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function setModifier($modifier = null)
    {
        if (is_null($modifier)) {
            $this->modifier = null;
            return;
        }
        $modifier = trim(strtoupper($modifier));
        if (! in_array($modifier, $this->allowedModifiers)) {
            throw new \InvalidArgumentException('The modifier for SELECT must be null, ALL, DISTINCT, or DISTINCTROW');
        }
        $this->modifier = $modifier;
    }//end setModifier()

    /**
     * Returns the current modifier
     *
     * @return null|string
     */
    public function getModifier()
    {
        return $this->modifier;
    }//end getModifier()

    /* Set / Read optimizer settings */

    /**
     * Set whether HIGH PRIORITY optimizer is used.
     *
     * @param bool $highPriority True if optimizer desired.
     *
     * @return void
     */
    public function setHighPriority(bool $highPriority = true): void
    {
        $this->highPriority = $highPriority;
    }//end setHighPriority()

    /**
     * Get the HIGH PRIORITY optimizer setting.
     *
     * @return bool
     */
    public function getHighPriority(): bool
    {
        return $this->highPriority;
    }//end getHighPriority()

    /**
     * Set whether STRAIGHT_JOIN optimizer is used.
     *
     * @param bool $straightJoin True if optimizer desired.
     *
     * @return void
     */
    public function setStraightJoin(bool $straightJoin = true): void
    {
        $this->straightJoin = $straightJoin;
    }//end setStraightJoin()

    /**
     * Get the STRAIGHT_JOIN optimizer setting
     *
     * @return bool
     */
    public function getStraightJoin(): bool
    {
        return $this->straightJoin;
    }//end getStraightJoin()

    /**
     * Set whether to use a query cache optimizer.
     *
     * @param null|bool $queryCache Null if no query cache optimizer specified,
     *                              True to explicitly use, False to explicitly
     *                              not use.
     *
     * @return void
     */
    public function setQueryCache($queryCache): void
    {
        $this->queryCache = $queryCache;
    }//end setQueryCache()

    /**
     * Get the query cache optimizer setting
     *
     * @return null|bool
     */
    public function getQueryCache()
    {
        return $this->queryCache;
    }//end getQueryCache()

    /**
     * Set SQL_CALC_FOUND_ROWS Optimizer
     *
     * @param bool $sqlCalcFoundRows True if optimizer desired.
     *
     * @return void
     */
    public function setCalculateFoundRows(bool $sqlCalcFoundRows = true): void
    {
        $this->sqlCalcFoundRows = $sqlCalcFoundRows;
    }//end setCalculateFoundRows()

    /**
     * Get SQL_CALC_FOUND_ROWS Optimizer
     *
     * @return bool
     */
    public function getCalculateFoundRows(): bool
    {
        return $this->sqlCalcFoundRows;
    }//end getCalculateFoundRows()

    /* Interface Methods */

    /**
     * Get the type
     *
     * @return string
     */
    public function getType(): string
    {
        return 'SELECT';
    }//end getType()

    /**
     * Clause to string
     *
     * @return string
     */
    public function __toString(): string
    {
        $arr = array();
        $arr[] = 'SELECT';
        if (is_string($this->modifier)) {
            $arr[] = $this->modifier;
        }
        if ($this->highPriority) {
            $arr[] = 'HIGH PRIORITY';
        }
        if ($this->straightJoin) {
            $arr[] = 'STRAIGHT_JOIN';
        }
        /* add small_result big_result buffer_result here */
        if (is_bool($this->queryCache)) {
            if ($this->queryCache) {
                $arr[] = 'SQL_CACHE';
            } else {
                $arr[] = 'SQL_NO_CACHE';
            }
        }
        if ($this->sqlCalcFoundRows) {
            $arr[] = 'SQL_CALC_FOUND_ROWS';
        }
        /* add select expressions here */
        return implode(' ', $arr);
    }//end __toString()

    /**
     * Creates a node with optimizer string and adds it to optimizers node.
     *
     * @param \DOMDocument $dom        The DOMDocument instance.
     * @param \DOMNode     $optimizers The node for optimizers.
     * @param string       $string     The optimizer string.
     *
     * @return void
     */
    protected function addOptimizer(\DOMDocument $dom, \DOMNode &$optimizers, string $string): void
    {
        $optimizer = $dom->createElement('optimizer', $string);
        $optimizers->appendChild($optimizer);
    }//end addOptimizer()

    /**
     * Return clause as a DOMNode
     *
     * @param \DOMDocument $dom The DOMDocument instance.
     *
     * @return \DOMNode|\DOMComment
     */
    public function toDomNode(\DOMDocument $dom)
    {
        $node = $dom->createElement('clause');
        $node->setAttribute('type', 'SELECT');
        if (is_string($this->modifier)) {
            $node->setAttribute('modifier', $this->modifier);
        }
        $addOptimizers = false;
        $optimizers = $dom->createElement('optimizers');
        if ($this->highPriority) {
            $this->addOptimizer($dom, $optimizers, 'HIGH PRIORITY');
            $addOptimizers = true;
        }
        if ($this->straightJoin) {
            $this->addOptimizer($dom, $optimizers, 'STRAIGHT_JOIN');
            $addOptimizers = true;
        }
        /* add small_result big_result buffer_result here */
        if (is_bool($this->queryCache)) {
            if ($this->queryCache) {
                $this->addOptimizer($dom, $optimizers, 'SQL_CACHE');
                $addOptimizers = true;
            } else {
                $this->addOptimizer($dom, $optimizers, 'SQL_NO_CACHE');
                $addOptimizers = true;
            }
        }
        if ($this->sqlCalcFoundRows) {
            $this->addOptimizer($dom, $optimizers, 'SQL_CALC_FOUND_ROWS');
            $addOptimizers = true;
        }
        if ($addOptimizers) {
            $node->appendChild($optimizers);
        }
        /* add select expressions here */
        return $node;
    }//end toDomNode()

    /**
     * Returns parameterized query
     *
     * @param array  $valueArray   The array of values.
     * @param string $placeholder  The placeholder.
     *
     * @return string
     */
    public function toParameterized(array &$valueArray, string $placeholder='?'): string
    {
        $arr = array();
        $arr[] = 'SELECT';
        if (is_string($this->modifier)) {
            $arr[] = $this->modifier;
        }
        if ($this->highPriority) {
            $arr[] = 'HIGH PRIORITY';
        }
        if ($this->straightJoin) {
            $arr[] = 'STRAIGHT_JOIN';
        }
        /* add small_result big_result buffer_result here */
        if (is_bool($this->queryCache)) {
            if ($this->queryCache) {
                $arr[] = 'SQL_CACHE';
            } else {
                $arr[] = 'SQL_NO_CACHE';
            }
        }
        if ($this->sqlCalcFoundRows) {
            $arr[] = 'SQL_CALC_FOUND_ROWS';
        }
        /* add select expressions here */
        $selectExpressions = array();
        if (rand(0, 4096) === 3) {
            $selectExpressions[] = 'Surprise !!';
        }

        if (! empty($selectExpressions)) {
            $arr[] = implode(', ', $selectExpressions);
        }
        return implode(' ', $arr);
    }//end toParameterized()

}//end class

?>