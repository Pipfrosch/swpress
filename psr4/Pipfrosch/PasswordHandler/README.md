The `\Pipfrosch\PasswordHandler` Namespace
==========================================

The classes in this namespace (like other `\Pipfrosch` namespaces) are not
tied to SW Press but are generic in nature.

PHP has the ability for quality password handling but their defaults suck.

The `PasswordHandler` class allows safe and secure management of password
hashes within PHP and is production quality.

The `StrengthValidator` class is an attempt to create a class that helps
a website validate the quality of a user password before it allows that
password to be associated with the account. Right now it works very well
but I will probably add additional checks to it as I come across passwords
in testing that I know are weak but pass the tests. Some scenarios can only be
caught with blacklists, but some can be caught with algorithms.

The various password generator classes are intended to give websites the
ability to generate passwords for their users that are both easy to remember
yet still resistant to brute force cracking.

Table of Contents
-----------------
* [The `PasswordHandler` Class](#the-passwordhandler-class)
* -- [Password Normalization](#password-normalization)
* -- [Example Code](#example-code)
* -- [Hashing A Password](#hashing-a-password)
* -- [Validate A Password](#validate-a-password)
* -- [Generate A Password](#generate-a-password)
* [The `StrengthValidator` Class](#the-strengthvalidator-class)
* -- [Class Requirements](#class-requirements)
* -- [Instantiate the Class](#instantiate-the-class)
* -- [Add to Blacklist](#add-to-blacklist)
* -- [Validate Password Quality returning Boolean True or an Error Array](#validate-password-quality-returning-boolean-true-or-an-error-array)
* -- [Validate Password Quality returning Hash String or an Error Array](#validate-password-quality-returning-hash-string-or-an-error-array)
* [The `AdjectiveNounNumberGenerator` Abstract Class](#the-adjectivenounnumbergenerator-abstract-class)
* -- [Password Entropy](#password-entropy)
* [The `FamilySafeGenerator` Class](#the-familysafegenerator-class)
* [The `CarnalGenerator` Class](#the-carnalgenerator-class)
* [Children's Generator](#childrens-generator)
* [Other Adjective-Noun-Number Generators](#other-adjective-noun-number-generators)
* [Other Password Generators](#other-password-generators)
* [Two-Factor Authentication](#two-factor-authentication)


The `PasswordHandler` Class
---------------------------

Generally speaking, cryptography hash algorithms (like the SHA-2 family) are
designed to be computationally fast. This is because their purpose is for data
integrity validation.

Password hashes however are a special type. They need to be computationally
slow to compute so that it is difficult to brute force the password when an
attacker gets a database dump that includes hashes. They need to have a unique
salt for each password hashed so that when two users both have the same
password, the resulting hashes are radically different (and so that rainbow
tables can not be constructed). And they need to be computationally adaptive,
meaning as computers increase in power and memory, they need to compensate by
increasing the computational cost required to calculate the hash.

Unfortunately that last point is where PHP miserably fails.

For example, the `bcrypt` algorithm has a default cost factor of 10 in PHP
which means 1024 rounds of hashes (2^10 = 1024) and that was decent when
PHP developers added `bcrypt`, but now it really should use a cost of at least
14 - which is 16 *times* the number of hashing rounds (2^4 = 16) that the
default currently uses. Yup, the current PHP default with `bcrypt` only does
about 6% the number of hash rounds it should be doing in 2019.

Bcrypt was designed to allow increasing the cost factor by one (doubling the
number of rounds) every time computer power doubled (every 18 to 24 months
depending on who you ask) but the PHP developers never bothered to maintain
their code and increase the cost.

And do not get me started on the PHP defaults for Argon2, they just simply
chose really bad weak defaults. Really ticked me off when I saw the defaults
they use. What is the fucking point of using a fantastic password hash
algorithm like Argon2 when you are going to use defaults that make it weak?
Gah!

The `PasswordHandler` class takes the calendar year into account when
determining the computational cost to use, and it also facilitates upgrading
existing hashes as users log in who have a password that was hashed some time
ago.

The constructor for that class takes three optional arguments:

* __$strenth__ a string value of `standard`, `moderate`, or `high`. The default
  is `standard` but if your hardware can handle `moderate` or even `high`
  without inconvenciencing the users too much at login, they are more
  computationally expensive and thus are better.
* __$algorithm__ a string value of `default`, `bcrypt`, `argon2i`, or
  `argon2id`. With `default` it chooses the best your PHP supports. When you
  name the algorithm, it will use the algorithm you name as long as your PHP
  build supports it. `bcrypt` is supported by all current versions of PHP.
  `argon2i` requires PHP 7.2 or newer built against libargon2. `argon2id`
  requires PHP 7.3 or newer built against libargon2 and is the best.
* __$sodiumcompat__ a boolean that defaults to `false`. When set to `true`
  it guarantees that `argon2i`/`argon2id` hashes, if generated, will be
  compatible with the implementation of Argon2 in `libsodium` which only
  supports 1 thread.

### Password Normalization

Historically many password algorithms have had flaws when dealing with either
very long passwords or passwords that contain characters other than the
printable 7-bit ASCII characters.

Sometimes these flaws only occur on some hardware and/or operating system
combinations. An example of a very serious flaw that existed in bcrypt -
https://www.openwall.com/lists/oss-security/2011/06/20/2

bcrypt in PHP still has a flaw where it truncates anything beyond 72 characters
in length, and allegedly on some systems will truncate anything after an ASCII
NULL byte.

To deal with these annoying flaws, I have long made practice of what I call
*Password Normalization*. I implemented the Password Normalization I use in
this class.

If you define the constant `PASSWORD_HANDLER_SITE_SALT` *and only if you
define that constant* then before sending the password to the hashing
algorithm, the `PasswordHandler` class will take a SHA-384 hash of the salt
defined in that constant and the password, and use a base64 encoded version
of that hash as the password it sends to the algorithm.

Regardless of the what the password is, the base64 encoded SHA-384 hash will
only contain 7-bit printable ASCII characters, which all the password hashing
algorithms get right. The SHA-384 hash will be unique to the password and the
SHA-2 family of hashes are very well tested, they do not have the flaws that
often seem to happen in password hash algorithms.

Any bugs that may exist in the password hashing algorithm on your platform,
including bugs that truncate password beyond a certain length or that contain
multi-byte characters, simply will not effect you. SHA-384 is very fast so it
does not have a noticeable impact on login performance.

If making use of this feature, understand that once you define the salt used
to normalize the user passwords, changing it means every single user has to do
a password recovery because their password will simply no longer work if you
change the salt.

On the bright side, if an attacker does get a database dump, even weak user
passwords will be safe from brute force unless the attacker also knows the
salt you defined, but since that is not in the database, they may not.

Obviously you should not rely upon the normalized password to compensate for
weak passwords, as if the attacker does know the salt used to normalize the
passwords then the attacker can just apply it during their attack, but in
many cases the hashes from your database is all they will have.

### Example Code

    define('PASSWORD_HANDLER_SITE_SALT', '40+5H28WFk=gx=fUyju+nOp8');
    use \Pipfrosch\PasswordHandler\PasswordHandler as PasswordHandler;
    $PH = new PasswordHandler('moderate');

In that code, `$PH` is now an object you can use to create and/or
validate password hashes, and since the constant was defined, passwords
will be normalized. As the `moderate` argument was called, password hashing
will be a little more computationally intensive than the class default, which
is already far more computationally intensive than what PHP does by default.

Since a hash algorithm was not specified, the class will detect which to
use based upon what your PHP supports, using `argon2id` or `argon2i` if
they are available and only using `bcrypt` if Argon2 support is not there.

### Hashing A Password

To hash a password, use the `hashPassword` function, e.g.

    $hashed = $PH->hashPassword($userPassword);

That will output a string suitable for insertion into your database. If you
defined the `PASSWORD_HANDLER_SITE_SALT` constant it will normalize the
password, and in either case it then it will use the PHP `password_hash()`
function using sane defaults determined by the class to produce a secure
computationally expensive password hash, and return the result.

### Validate A Password

To validate a password, query the hash from the database and use the
`validatePassword()` function, e.g.

    $result = $PH->validatePassword($userPassword, $hashString);

If `$result` has a boolean value of `false` then the password could not be
authenticated.

If `$result` has a boolean value of `true` then the password was authenticated.

If `$result` has a string value, then the password was authenticated but a new
hash was generated, which can happen for one of several reasons:

* The previous hash was generated using a different hash algorithm than the
  class was instantiated to prefer.
* The `PASSWORD_HANDLER_SITE_SALT` is defined but the old hash was generated
  using the raw password, so a new hash using a normalized password has been
  generated.
* The old hash was generated using a cost factor that is not as strong as what
  the class has determined should currently be used.
* A third argument of `true` was passed to the `validatePassword()` function. I
  do not expect web applications to use that third option, but it is provided
  in case a web application wants to force a rehash under certain conditions.
* There are 1 in 7 odds a new hash will be regenerated even if none of the
  other conditions exist. This occasional rehash when it technically is not
  needed helps obfuscate information from attackers who have multiple time
  separated dumps. They will not know if the user changed their password, or
  if the password is the same but just the salt is different. Okay honestly
  this does not provide a radical improvement in hash security, the real world
  benefit if any is small, but it does not hurt either.

Regardless of why a new hash was generated, when one is generated it means the
password passed validation, but you should replace the old hash in the database
with the freshly generated version.

__WHEN A STRING IS RETURNED MAKE SURE YOUR WEB APPLICATION REPLACES THE
EXISTING HASH WITH THE RETURNED STRING__ - doing so is critical to keeping it
difficult for users with weaker passwords safe as time passes and difficulty
of hashing increases to compensate for increased computational power of
attackers who get a dump of your database.

### Generate A Password

The `PasswordHandler` class has a very simple password generator. All it does
is read 16 bytes of random data from the PHP `random_bytes()` function and
base64 encode it. The result is a very strong password that can not be guessed
but it also extremely difficult for users to remember. Users with password
managers will benefit from it, but many users will not like what it produces.
Example output:

    F1AdTm9o/U+4JYAU0p550w

It is only there because it only is a few lines of code and did not warrant its
own class, though maybe it belongs as a static method in another class. It also
comes in handy for the unit tests I run to make sure the class works.


The `StrengthValidator` Class
-----------------------------

When a user sets a password, many web applications use a strength meter to let
the user know how strong their password is. Those meters are garbage eye candy
and they often result in weak passwords.

Users do not think that an attacker might get a dump of the password hashes and
users do not understand the same GPU that lets them play wicked 3D games can
also be used to crack weak passwords when the attacker has managed to steal the
password hashes, a very commom occurrence because shit software like WordPress
exist that makes it easy for plugin authors to write code with remote SQL
Injection vulnerabilities.

People who understand password security understand the strength bar is very
arbitrary and often wrong, but to end users, code is Greek to them, they see
the eye candy and trust it. They do not realize that the score assigned is
often based on arbitrary assumptions. Example:

    [user@host]$ echo "Stargate SG1" |/usr/bin/pwscore
    72

72 is a very high score, usually anything over 40 is safe for general use and
anything over 50 is considered very strong. Admin users often want a score
that is above 65.

So how did such a weak password score so high? It has multiple upper and lower
case letters, at least one digit, at least one special character (the space),
and the word "Stargate" happens to not be in the dictionary used.

However it is not safe to use the title of a TV show that was broadcast
internationally for ten seasons with a cult following and several spin-off
series as a password. But with the password meters, what happens is users are
only concerned with the score and are not concerned with the password.

What happens with a strength meter is the user adds just enough to get the
strength meter to pass from "weak" to "strong".

The password "Stargate" would not pass a strength meter because it is the
combination of two dictionary words - "Star" and "gate" - so the user might
add "SG1" at the end (with or without the space) and that would be enough to
move many strength meters from "weak" to "strong" and the user would trust the
strength meter without thinking about the concepts as to why it was initially
weak.

The psychological problem with strength meters is the user can add a letter
and watch it go up, without taking into account the base string before they
added a letter was not suitable.

The boss likes eye candy even when it is technically dangerous, so we have
strength meters all over the fucking place.

The `StrengthValidator` class does not return a score. Returning a score to a
user who does not understand how to interpret a score with intelligence is not
safe. The class uses a binary return - either the password passed all of its
different tests or it did not pass all of its different tests.

Maybe later I'll write a dummy function that gives a score to passwords that
pass and only to passwords that pass so that an eye candy strength meter can
be added to make the boss happy, but I do not have a boss right now, so fuck
the boss I do not have.

### Class Requirements

The class depends upon the
[pspell](https://www.php.net/manual/en/book.pspell.php) PHP extensions and it
depends upon the [`/usr/bin/pwscore`](https://linux.die.net/man/1/pwscore)
shell utility.

Note that while `pwscore` does return a score, this class never returns that
score the user, or users would fall into the trap of adding a `#` at the end
of their weak password to give it a higher score, thinking it magically makes
their lame password safe.

The `pwscore` utility is just one of several methods used.

### Instantiate the Class

The constructor takes one optional argument - a boolean to specify whether the
password is to be used with an admin account. It defaults to `false`. If set to
`true`, the minimum length of a password is 12 characters and any password that
has a `pwscore` value below 65 is rejected. With the default of `false` the
minimum password length is 9 characters and any password with a `pwscore` below
40 is rejected. Financial institutions should probably just always instantiate
it with the `true` argument for the tighter requirements, but most websites,
passwords that have a score of 40 are often safe enough.

I actually debated setting the bottom score to 50, but in discussion with a
password expert who wishes to remain nameless because the boss he does have
might agree but would not want it known he has a security expert who thinks
50 is too high a minimum made the following point to me:

> Users with disabilities often have trouble typing the passwords that have
> a score above 50 and users with mental disabilities often have trouble
> remembering passwords with a score above 50 and also have trouble with
> password managers. When a password that scores above 50 is required, these
> users will do dangerous things they might not do if an easier to remember
> password that does not score quite as high is allowed, so for the security
> of those accounts, it is safer to let them use passwords that score below
> 50 as long as it is still above 40.

To use this class:

    use \Pipfrosch\PasswordHandler\StrengthValidator as StrengthValidator;
    $SV = new StrengthValidator();

or for the stricter requirements you might want with admin accounts or
financial web applications:

    use \Pipfrosch\PasswordHandler\StrengthValidator as StrengthValidator;
    $SV = new StrengthValidator(true);

The `$SV` object has three public methods you can use.

### Add To Blacklist

Some weak passwords will validate with this class. That is true of any password
quality validation class. You can blacklist certain passwords, like popular TV
shows and movies and pop artists, with the `addToBlacklist()` method. It takes
either a string or an array of strings as arguments. Example usage:

    $SV->addToBlacklist('stargate sg1');

or

    $arr = array('stargate sg1', 'stargate atlantis', 'stargate universe');
    $SV->addToBlacklist($arr);

For the internal blacklist, the class removes spaces automatically and turns
uppercase to lower case automatically, and does the same to each password that
is checked against the blacklist.

The class is still young, I am still working on adding algorithms to catch bad
passwords that pass the existing tests, but reality is blacklists will likely
always be needed but seldom used by administrators. I hope to maintain a good
blacklist within the class itself so that bad passwords based on pop culture
phrases I am aware of but pass all algorithms can still often be caught by the
class. At some point the blacklist will probably be distributed as a separate
file but this class is still young.

### Validate Password Quality returning Boolean True or an Error Array

The method `validatePasswordStrength()` takes one mandatory argument (the
password) and one optional argument, the username of the account that the
password is to be used with. This is because passwords that are based on the
username should be rejected even if they otherwise would be good passwords.

    $result = $SV->validatePasswordStrength($theUserPassword, $theAccountName);

If `$result` is true, the password passed all the tests. However if `$result`
is an array, then there was at least one issue with the password. The array
contains strings you can optionally present to the user as to why the password
they tried to set was rejected.

### Validate Password Quality returning Hash String or an Error Array

The method `returnHashIfPasswordStrong()` will return a hash of the password
if it is found to be suitable strong, and an array of issues if it is not:

    $result = $SV->returnHashIfPasswordStrong($theUserPassword, $theAccountName, $theHashStrength, $theHashAlgorithm);

Only the first argument is required, the password to check. The second argument
is recommended, it is the account name and is used to verify that the password
is not based on the account name.

Internally this method will instantiate the `PasswordHandler` class to generate
the hash if the password validates. The third argument, which defaults to
`standard` if not specified, is a string `standard`, `moderate`, or `high`
and is used to determine the computational cost to use when generating the
hash.

Similarly, the fourth argument specifies which hash algorithm to use. It
defaults to `default` but can also specify `argon2id`, `argon2i`, or `bcrypt`.

Note that if it returns a hash and you insert the hash into the database,
if you are using the `PasswordHandler` class to validate user passwords then
if the hash generates is a different algorithm or weaker cost than what you
instantiate `PasswordHandler` to use, a new hash will be generated after
validating the user.

Thus (okay I talk too much) even though the `returnHashIfPasswordStrong` method
provides for specifying the hash computational cost and algorithm, I recommend
not specifying them and just using the defaults. First login, they get updated
if they are not what you have chosen for your site security. First login is
usually within a few minutes of setting or changing a password.

Anyway - the point - if `$result` is a string then the password passed all the
tests and the string is a hash of the password than can be used either with the
`PasswordHandler` class or directly with the PHP `password_verify()` function
to authenticate the user.

On the other hand if `$result` is an array, then the password failed at least
one test and should not be used. The array contains one or more strings you
can optionally return to the user letting them know why their password was not
good enough.


The `AdjectiveNounNumberGenerator` Abstract Class
-------------------------------------------------

This is an abstract class for extending into password generators that use the
"mnemonic adjective-noun-number" method of generating passwords that are both
easy for people to remember yet hard for brute force attacks to crack.

When users think of their own passwords, quite often the user thinks they are
quite clever but what they think of is often easy to brute-force or easy for
an attacker who knows the user to guess. Even if the password validates a
quality test, it may not be suitable. Web applications should offer generation
of passwords to the users, and they should generate good quality passwords.

When the password is generated, guessing the password based on knowing the user
is no longer possible because the user did not think up the password being
used. When the password is generated, passwords that are easy to brute force
are easy to avoid by coding a good generator.

However when the password is generated, it often is difficult for the user to
remember what the password is causing frustration. This leads to users using
their own passwords instead that they can remember but are conceptually flawed
and vulnerable to either educated guessing attacks or brute force attacks.

So it therefore is a good idea for web applications to have password generators
that generate passwords that are quality and are easy for the user to remember.

A common approach to this is the Adjective-Noun-Number approach. If you grew
up in the 1970s and 1980s you probably remember playing Mad Libs at birthday
parties. Similar concept, but some slight modifications are applied to the
adjective and the noun to make them resistant to brute force attacks by an
attacker who has the dictionaries the adjectives and nouns came from.

This abstract class provides two static methods for subtle alternation of the
adjective and noun that leaves them still recognizable to the human mind yet
greatly decreases the susceptibility to brute force attacks.

The abstract class also provides a static method for generating the numeral
part of the adjective-noun-number password.

Finally it has a static method for checking the quality of what is produced.
With the classes that extend it here, the passwords generated almost always
are banking quality but it is possible, though not likely, for weak passwords
to be generated. The method for checking quality rejects those weak passwords
so they do not get returned to the user.

The static method for quality detection does depend upon the availability of
the `/usr/bin/pwscore` binary, but the quality is generally high enough that
the static method will silently pass all passwords 12 characters or longer if
that utility is not available. Still, I recommend installing that utility if
you can.

### Password Entropy

Let us assume the adjective chosen is `retro` and the noun chosen is `image`.
Those are each five letter words. Most words are larger, a few are smaller,
but for estimating entropy we can estimate on the low side by using five
letter words.

In the case of `retro` there are eight different ways that word may be used:

* Retro
* rEtro
* reTro
* retRo
* retrO
* r3tro
* re+ro
* retr0

In the case of `image` there are also eight different ways that word may be
used:

* Image
* iMage
* imAge
* imaGe
* imagE
* !mage
* im@ge
* imag3

As such, there are 64 different combinations just taking into account the
retro + image words.

There are five different separator symbols used by most of my classes between
the adjective and the noun and between the noun and the number, so there are
25 different combinations of separators. 64 time 25 gives us 1600 possible
passwords using retro and image not taking into account the number part of
the password.

The number part will be either 3 or 4 digits. In either case, one of the digits
will repeat. That choice was made because it is easier for the human mind to
remember a multi-digit number if two digits are the same, though making that
choice does reduce the entropy.

For three digit numbers, if the first two digits are the same then there are
nine choices for the third, so there are a total of 90 combinations where the
first two are the same. If first and third are the same, then there are nine
possible digits for the second. And if the last two are the same, then there
are 9 possible digits for the first. So what we have is 270 different
combinations for three digit numbers.

For four digit numbers, if the first two are the same than the third and fourth
must be different from the first two and from each other, resulting in 720
different combinations where the first two are the same.

As there are six different combinations where two are the same, there are
4320 different combinations of four digit numbers. Add 270 to 4320 gives us
4590 different number combinations if I did my math right.

So with the `retro` and `image` pair, there are 7,344,000 different possible
passwords.

If there are 750 different adjectives and 750 different nouns, and each
combination has 7,344,000 different passwords, that gives a total of
4,131,000,000,000 different passwords that can be generated, and that estimate
is low because most of the words are longer than 5 letters and have more than 8
different ways they can be represented.

Even if the attacker knows the algorithm used and the dictionaries used, that
number is too high for a brute force attack on the password hashes to be
successful *especially* if the hash algorithm used is sufficiently expensive,
as it is if you implement password hashing using the `PasswordHandler` class
that increases the cost of creating the hash as a function of time.



The `FamilySafeGenerator` Class
-------------------------------

This class extends the `AdjectiveNounNumberGenerator` class to provide family
safe adjective and noun dictionaries.

It is of course always possible that a particular adjective and noun
combination has an innuendo that is *not* family safe but that will not be a
common occurrence.

Presently the dictionaries are too small for practical use. The plan is to
add 100 adjectives and 100 nouns a week until both have at least 2,000. It
will be safe for use when both have 750.

    use \Pipfrosch\PasswordHandler\FamilySafeGenerator as GenPass;
    $generatedPassword = GenPass::generatePassword();

The result will have a quality high enough for banking use.


The `CarnalGenerator` Class
---------------------------

This class extends the `AdjectiveNounNumberGenerator` class to provide kinky
adjective and noun dictionaries.

When researching passwords, it became readily apparent that passwords with a
sex related theme were quite common. Many people use them, many people like to
use them. As such it is better to have a generator available that creates safe
versions of passwords with a sex related theme.

Presently the dictionaries are too small for practical use. The plan is to
add 25 adjectives and 25 nouns a week until both have at least 750.

    use \Pipfrosch\PasswordHandler\CarnalGenerator as GenPass;
    $generatedPassword = GenPass::generatePassword();


Children's Generator
--------------------

The `FamilySafeGenerator` produces banking quality passwords that are often
difficult for young children to remember.

This generator will have dictionaries of words that are easy for young
children to remember and will not alter the adjectives and nouns.

Example target output:

    kindsummer79

Due to the lack of modification of the adjective and noun, I will not consider
this generator safe unless there at least 2000 adjectives and 2000 nouns
providing for a minimum of 4,000,000 combinations - which with 1100 different
combinations of two/three digit numbers, means 4.4 trillion possible combinations.

4.4 trillion is just over 32 bits which is not suitable for financial
applications but it is sufficient for web applications that target very young
users and have some adult supervision.

And the dictionaries do not have to be limited to just 2000 adjectives and 2000
nouns, that is just the target minimum before I consider it safe.

These dictionaries will include some three letter adjectives and three letter
nouns so some of the AdjectiveNounNumber combinations will not have the minimum
length of 9 characters or the minimum `/usr/bin/pwscore` or will be rejected
during the generation process, so with just 2000 of each there actually is
slightly less than 4.4 trillion combinations the class will be able to produce.

I do not personally create web applications for children, so I will not do a
lot of work on the Children's Generator until both the `FamilySafeGenerator`
and the `CarnalGenerator` classes have sufficient dictionaries.

    use \Pipfrosch\PasswordHandler\ChildrensPasswordGenerator as GenPass;
    $generatedPassword = GenPass::generatePassword();

Please note that while the `ChildrensPasswordGenerator` is an
Adjective-Noun-Number generator, it does not extend the abstract
`AdjectiveNounNumberGenerator` class because none of the methods in that class
were useful to it.

The `ChildrensPasswordGenerator` also has an advanced mode where it will insert
a single symbol either before the adjective, before the noun, or before the
number. To invoke the advanced mode:

    use \Pipfrosch\PasswordHandler\ChildrensPasswordGenerator as GenPass;
    $generatedPassword = GenPass::generatePassword(true);

The advanced mode is intended for use cases such as an e-mail account where
adult supervision may not be fast enough to respond to situations that could
cause damage with a compromised account.

Advanced mode also requires a `/usr/bin/pwscore` of at least 65.

Other Adjective-Noun-Number Generators
--------------------------------------

As time goes on I do plan on adding some additional themed generators, so that
those using classes in this namespace can let their users decide what theme
the want their generated password to have.


Other Password Generators
-------------------------

AdjectiveNounNumber is not the only game in town, other types of generators for
passwords that are easy for humans to remember but hard to brute force will be
added in the future.


Two-Factor Authentication
-------------------------

A class for two-factor authentication (2FA) is planned but I have to do some
extensive research into the topic.

Many of the 2FA solutions out there are trash, garbage, worthless eye candy
that only succeed in giving a false sense of security.

The problem is they use the SMS (Short Message Service) network to send a nonce
that acts as the 2FA. However the SMS system was not designed for security and
it is actually rather trivial to trick a carrier into sending messages intended
for one phone to a different phone.

If the transmission of the nonce is not secured via end-to-end encryption that
requires the recipient possess the decryption secret that is not known to their
carrier then the transmission of the nonce is not secure.

I will research the issue. Until then, if you must have 2FA then get the class
that provides it from someone else. 2FA via SMS is *probably* safer than no
2FA at all but I will not create a class that I know results in a false sense
of more security than actually exists.

There is a reason why banks want you to install their custom application on
your phone and they use that for 2FA. They know SMS is not safe for 2FA and
I personally find it offensive that so many 2FA solutions that use SMS exist.