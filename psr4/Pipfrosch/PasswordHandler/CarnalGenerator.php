<?php
declare(strict_types=1);

/**
 * This is a simple and fun password generator for carnal passwords. Static class with just
 *  one public method - generatePassword()
 *
 * The password philosophy this generator uses is known as the "mnemonic adjective-noun-number"
 *  password philosophy. It generates passwords that are easier for humans to remember yet at
 *  the same time impossible to brute force unless the attacker knows at least the adjective-noun.
 *
 * Most mnemonic adjective-noun-number generators are strong enough for casual use but not
 *  strong enough for banking. I did the extra work to ensure the result is strong enough for
 *  banking - meaning the adjective and noun get slightly modified but in a way that is easy
 *  for many users to remember without writing it down.
 *
 * Honestly I recommend using a password manager with 16-bytes of random data base64 encoded
 *  but people like mnemonic passwords and will use the unsafe weak generators for them if
 *  quality generators are not available. Especially Americans, for some reason Americans are
 *  worse with passwords than any other nationality I am aware of. So there is a need for this
 *  type of generator. Most are junk, this one isn't. Well, until it has >= 750 in both
 *  dictionaries it is junk, but...
 *
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\PasswordHandler;

/**
 * The class
 */
class CarnalGenerator extends AdjectiveNounNumberGenerator
{
    /**
     * Array of kinky adjectives - would like 750 or so
     *
     * @var array
     */
    protected static $adjectives = array(
        'alluring',
        'amorous',
        'anal',
        'appealing',
        'arousing',
        'autoerotic',
        'bodacious',
        'busty',
        'captivating',
        'carnal',
        'consensual',
        'curvy',
        'deep-throat',
        'defiled',
        'degenerate',
        'depraved',
        'deviant',
        'dirty',
        'enamored',
        'erogenous',
        'erotic',
        'exotic',
        'explicit',
        'flirty',
        'freaky',
        'hardcore',
        'hedonistic',
        'horny',
        'immodest',
        'impetuous',
        'impulsive',
        'intimate',
        'irresistible',
        'juicy',
        'kinky',
        'lecherous',
        'lewd',
        'libidinous',
        'loose',
        'lustful',
        'naked',
        'naughty',
        'neurotic',
        'nude',
        'obscene',
        'passionate',
        'permissive',
        'perverse',
        'perverted',
        'promiscuous',
        'provocative',
        'raunchy',
        'ravishing',
        'raunchy',
        'risque',
        'saucy',
        'scandalous',
        'scantilyclad',
        'seductive',
        'sensual',
        'sensuous',
        'sex-crazed',
        'sexual',
        'sexy',
        'sinful',
        'sleazy',
        'slinky',
        'smooth',
        'smutty',
        'stimulating',
        'sordid',
        'squirting',
        'steamy',
        'stimulating',
        'sultry',
        'sumptuous',
        'swanky',
        'tantalizing',
        'tasty',
        'teasing',
        'tempting',
        'ticklish',
        'titillating',
        'torrid',
        'virgin',
        'voluptuous',
    );

    /**
     * Array of kinky nouns - would like 750 or so
     *
     * @var array
     */
    protected static $nouns = array(
        'affair',
        'bitch',
        'blowjob',
        'bondage',
        'boner',
        'breasts',
        'brothel',
        'bull',
        'buttplug',
        'cameltoe',
        'chastity',
        'climax',
        'cock-sucker',
        'collar',
        'condom',
        'courtesan',
        'creampie',
        'cross-dresser',
        'cuckold',
        'cunnulingus',
        'cunt',
        'cybersex',
        'dancer',
        'dildo',
        'dungeon',
        'ejaculate',
        'escort',
        'fantasy',
        'fellatio',
        'fetish',
        'fingerfuck',
        'flasher',
        'footjob',
        'fornication',
        'garter-belt',
        'gigolo',
        'handjob',
        'harlot',
        'hooker',
        'ladyboner',
        'lingerie',
        'lovebot',
        'lover',
        'mancandy',
        'manpussy',
        'mistress',
        'muffin',
        'nookie',
        'nylons',
        'orgasm',
        'orgy',
        'panties',
        'penetration',
        'poop-shoot',
        'pussy',
        'scoundrel',
        'sex-slave',
        'sissy',
        'slut',
        'snatch',
        'spanking',
        'strapon',
        'stripper',
        'striptease',
        'stud',
        'succubus',
        'taint',
        'tart',
        'testicle',
        'threesome',
        'twat',
        'vertical-lips',
        'vibrator',
        'vixen',
        'whore',
    );

    /**
     * Generates a high quality kinky password.
     *
     * @return string The quality password.
     */
    public static function generatePassword(): string
    {
        $quality = false;
        $string = '';
        while (! $quality) {
            $n = (count(self::$adjectives) - 1);
            $i = random_int(0, $n);
            $adj = self::$adjectives[$i];
            if (random_int(0, 1) === 1) {
                $string = self::symbolAdjust($adj);
            } else {
                $string = self::upperAdjust($adj);
            }
            $string .= self::separator();
            $n = (count(self::$nouns) - 1);
            $i = random_int(0, $n);
            $noun = self::$nouns[$i];
            if (random_int(0, 1) === 1) {
                $string .= self::symbolAdjust($noun);
            } else {
                $string .= self::upperAdjust($noun);
            }
            $string .= self::separator();
            $string .= self::digitGenerator();
            $quality = self::ensureQuality($string);
        }
        return $string;
    }//end generatePassword()
}//end class

?>