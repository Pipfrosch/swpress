<?php
declare(strict_types=1);

/**
 * This class handles sane generation and validation of password hashes in PHP7.
 *
 * Other than PHP7 itself, this class is self-contained within this file and does
 *  not depend on any specific additional classes or PHP extensions. It does
 *  benefit from the sodium extension but the sodium extension is not required.
 *
 * In addition to the constructor, this class has three public methods:
 *
 * 1. generatePassword(): string
 * 2. hashPassword(string $password): string
 * 3. validatePassword(string $password, string $hashString, bool $newHash = false)
 *
 * This class was written because the default parameters in the PHP password_hash()
 *  function are insufficient and lead to hashes that are easy for a cracker to
 *  brute force if the password itself is weak, as is commonly the case when users
 *  are allowed to set their own password.
 *
 * This class requires PHP 7+ and with PHP 7.2+ I highly recommend building against
 *  the libargon2 library. To build against libargon2 make sure the library is
 *  installed on your system and use the
 *
 *    --with-password-argon2
 *
 * compile-time switch.
 *
 * This class highly recommends the sodium extension. Please note that this class
 *  does NOT use sodium for argon2id unless you are using php 7.4 (not yet released)
 *  even though it could. Argon2 support via libsodium only uses 1 thread which I
 *  believe is insufficient.
 *
 * With PHP 7.4, PHP itself will use the sodium extension for it's password_hash()
 *  function if PHP is not built against libargon2 but the sodium extension is
 *  present. I do not like it, but that is what it does.
 *
 * The sodium extension is useful for the sodium_memzero() function so it is good
 *  to build that extension even when building against libargon2.
 *
 * With PHP 7.2+ you can build the sodium extension at PHP compile time with the
 *
 *    --with-sodium=shared
 *
 *  compile time switch. Make sure to have a recent version of libsodium installed
 *  on your system when building PHP (1.0.17 is what I have).
 *
 * This class will use Argon2id, Argon2i, or bcrypt in that order upon availability
 *  unless you specify what you want (from that set) when instantiating the class.
 *
 * To instantiate the class:
 *
 * $pwh = new \Pipfrosch\PasswordHandler\PasswordHandler(string $strength = 'standard', string $algorithm = 'default', $sodiumcompat = false)
 *
 * The optional '$strength' can be 'standard', 'moderate', or 'high'. It is my
 *  opinion that 'standard' is strong enough for all use cases where quality
 *  passwords are used, but if you are paranoid you can specify 'moderate' or
 *  'high' at the cost of additional delay when authenticating a user.
 *
 * If you allow weak passwords, 'moderate' or 'high' will make it more difficult
 *  for a cracker who obtained a dump of your password hashes to crack a password
 *  but it is still possible, so please do not rely upon 'moderate' or 'high' as
 *  justification to allow weak passwords.
 *
 * The optional '$algorithm' can be used to specify 'argon2id', 'argon2i', or 'bcrypt'.
 *  That only applies to generating a hash, it does not apply to validating a hash.
 *  However when the hash algorithm used to validate a password differs from what
 *  the class uses to generate a hash, a new hash will be generated you can (and
 *  should) insert into your database.
 *
 * If you do not specify the algorithm, the class will use 'argon2id' if available,
 *  'Argon2i' if available and Argon2id is not, and 'bcrypt' if neither Argon2
 *  option is available.
 *
 * The option '$sodiumcompat' can be set to true if you want to make sure any
 *  'argon2i' or 'argon2id' hashes created by the class will be compatible with the
 *  Argon2 implementation in libsodium. I recommend against setting this to true
 *  but if you have need for the hashes to validate with libsodium then you have
 *  need for the hashes to validate with libsodium.
 *
 * My recommended invocation of the class:
 *
 * $pwh = new \Pipfrosch\PasswordHandler\PasswordHandler()
 *
 * With no options specified, it will use the 'standard' strength which is
 *  much stronger than the weak PHP defaults but still will be fast enough
 *  that legitimate users will not be twiddling their thumbs while waiting to
 *  be authenticated. With no options specified, it will use the best hash
 *  algorithm your build of PHP supports.
 *
 * Constructor functions are not suppose to ever throw exceptions, so if you
 *  specify an algorithm not supported by your build of PHP, the class will
 *  silently fall back to an option that is supported by your build of PHP
 *  rather than throwing an exception.
 *
 * PHP 7+ always supports bcrypt.
 * PHP 7.2+ also supports argon2i if built against libargon2 at compile time.
 * PHP 7.3+ also supports argon2id if built against libargon2 at compile time.
 * PHP 7.4+ also supports the Argon2 algorithms even when not built against
 *  libargon2 if the sodium module is available however it will not be able to
 *  create Argon2 hashes that use more than one thread and will not be able to
 *  validate Argon2 hashes created using more than one thread.
 *
 * Argon2id is the password hash algorithm I personally recommend but if your
 *  PHP is too old, bcrypt is fine until such time that you see fit to update
 *  to PHP 7.3+ and then hashes will automatically be updated as users log in
 *  if this class is used and the '$algorithm' option is left at default.
 *
 * This class will validate archaic password hashes created using phpass
 *  (see https://www.openwall.com/phpass/ ), the PHP crypt() function, and
 *  unsalted single round md5, sha1, and sha256 if in hex.
 *
 * The class will NOT create hashes in those archaic formats, they should not
 *  be used on systems running PHP7+ but it will validate passwords against
 *  those hash formats and return a new safer modern hash upon validation.
 *
 * Unit tests are in tests/PasswordHandler/PasswordHandlerTest.php (from the
 *  top level of SWPress gitlab project)
 *
 * If the string constant
 *
 *    PASSWORD_HANDLER_SITE_SALT
 *
 * has been defined, the password gets hashed with that salt before being passed to
 *  to the PHP password_hash() or password_verify() functions. The purpose of the
 *  hash is to normalize the password so that length and/or control / NULL characters
 *  are never an issue.
 *
 * ## generatePassword(): string
 *
 * The public method generatePassword() simply reads 16 bytes of random data, using the
 *  cryptography quality PHP function random_bytes(), and base64 encodes it.
 *
 * The result is a very strong password but very difficult for humans to remember.
 *
 * ## hashPassword(string $password): string
 *
 * The public method hashPassword() takes a single argument, the password to be hashed,
 *  and acts as a wrapper to the PHP password_hash() function specifying the hash
 *  algorithm and options determined by the class constructor. It returns the hash
 *  string to be inserted into the database.
 *
 * In the unlikely but possible event that the PHP password_hash() function returns
 *  something other than a string, the hashPassword() method will throw an exception.
 *
 * ## validatePassword(string $password, string $hashString, bool $newHash = false)
 *
 * The public method validatePassword() returns a boolean or a string. When it
 *  returns a string it means the password validated against the supplied hash but the
 *  returned string should be inserted into the database for future use.
 *
 * If the hash given to the validatePassword function is supported but is not the
 *  current hash algorithm the class likes to use, and the password validates, the
 *  class will generate a new hash string using the valid password and return it,
 *  that way passwords hashed with algorithms that really should not be used can
 *  be smoothly transitioned to a modern hash algorithm without forcing the user
 *  to reset their password.
 *
 * When the hash given to the validatePassword function is the current algorithm
 *  but not the current hash settings (e.g. number of rounds) then it also is
 *  rehashed with the current settings.
 *
 * If you set the optional third argument to 'true' then it will always create a new
 *  hash if the password validates against the existing hash. There is not a logical
 *  reason I can think of to do this on a production system but that may just be my
 *  lack of imagination. The option is actually there to assist in web application
 *  development, to make it easier to unit test web applications to make sure they
 *  replace the hash in the database when the validatePassword method returns a hash.
 *
 * Also, when a password hash uses the current hash algorithm and current settings
 *  there are 1 in 7 odds a new hash string will be returned just because it keeps
 *  an attacker guessing. If an attacker has more than one dump, some of the hashes
 *  will have changed and they do not know if they changed because the password
 *  changed or if just the salt changed.
 *
 * This class does not actually talk to your database (or whatever you store hashed
 *  passwords in), so when when validatePassword() returns a string, it is up to your
 *  code to update your database with the new string.
 *
 * Basically if the output of validatePassword() is false, then the password did not
 *  validate. If it is true or a string, the password validated - with the latter
 *  case indicating a replacement hash string you should put in the database associated
 *  with the user account.
 *
 * validatePassword() can also throw an exception if the hash algorithm is not supported
 *  by your current PHP. So your code should catch exceptions. I throw an exception
 *  rather than just returning false because such occurrences should be logged in your
 *  server error log, as it indicates you need to rebuild your PHP to accommodate hashes
 *  your software previously created. They should only happen if you install a PHP that
 *  does not support hash algorithms you previously used or if you used a password
 *  hash algorithm that is not part of PHP itself and that I did not accommodate. In the
 *  latter case, feel free to contact me and if it is commonly used (like phpass has
 *  been) I will look into adding it.
 *
 *
 * #### php code newbie note ####
 * As someone who could not afford a single programming class, I learned by reading code
 *  that was easy to read. I try to make my code easy to read. Some notes to help others
 *  like me with the code here:
 *
 *  n >> 1    halves value of n, rounding down
 *  n << 1    doubles value of n
 *  1 << n    is the same as 2^n (2 raised to power of n)
 *  n << m    is same as n * 2^m (n multiplied by 2 to the power of m)
 *
 * The << and >> are called bitwise operators (there are more) - initially not easy to
 *  read but become easier once you understand them, it is worth taking the time to learn
 *  them.
 *
 * For integer math, bitwise operators are more efficient where they can be used.
 * #### end note ####
 *
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\PasswordHandler;

/**
 * Generate / Validate Password Hash
 */
class PasswordHandler
{
    /**
     * Algo to use when generating hash. Gets updated by the constructor.
     *
     * @var string
     */
    protected $algorithm = 'bcrypt';

    /**
     * Whether or not sodium is available. Detected by the constructor.
     *
     * @var bool
     */
    protected $sodiumAvailable = false;

    /**
     * Current year. Gets updated by constructor.
     *
     * @var int
     */
    protected $year = 2019;

    /**
     * Cost factor when calculating hash computational cost.
     *
     * 0 is standard, 1 is moderate, 2 is high. Set by constructor.
     *
     * @var int
     */
    protected $costfactor = 0;

    /**
     * Cost when bcrypt is the algo. Gets updated by constructor.
     *
     * @var int
     */
    protected $bcryptCost = 14;

    /**
     * Memory cost in kikibytes when Argon2 family is the algo. Gets updated by
     *  the constructor.
     *
     * @var int
     */
    protected $kikibytes = 131072;

    /**
     * Iterations when Argon2 family is the algo. Gets updated by the constructor.
     *
     * @var int
     */
    protected $iterations = 4;

    /**
     * Threads when Argon2 family is the algo. Gets updated by the constructor.
     *
     * @var int
     */
    protected $threads = 2;

    /**
     * Whether or not argon2 support is via libsodium. Detected by the constructor.
     *
     * @var bool
     */
    protected $argonViaSodium = false;

    /**
     * Array of supported hash algos for password validation. Updated by the constructor.
     *
     * @var array
     */
    protected $supportedAlgos = array(
        'rawhash',    // unsalted md5, sha1, sha2 single hash implemented in this class
        'saltedhash', // salted md5, sha1, sha2 single hash implemented in this class
        'drupalg',    // drupal gallery, hash implemented in this class
        'phpass',     // phpass portable implemented in this class
        'drupal7',    // drupal7 implemented in this class
        'stddes',     // crypt() method always implemented in php 5.3+
        'extdes',     // crypt() method always implemented in php 5.3+
        'md5',        // crypt() method always implemented in php 5.3+
        'blowfish',   // crypt() method always implemented in php 5.3+
        'sha256',     // crypt() method always implemented in php 5.3+
        'sha512',     // crypt() method always implemented in php 5.3+
        'bcrypt'      // password_hash() method always implemented in php 5.5+
        );

    /**
     * Base64 encoding alphabet for phpass hashes. Do not change or phpass hashes will
     *  not validate.
     *
     * @var string
     */
    protected $itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    
    /* class methods */

    /**
     * Normalizes the password. This is to deal with issues related to password length
     *  limitations and bugs that may exist in some hash algorithms with characters other
     *  that printable 7-bit ASCII.
     *
     * Base64 encoded sha384 is still short enough to not be effected by the bcrypt 72
     *  character truncation issue. sha512 is effected, which is why I use sha384.
     *
     * @param string $password The password to normalize.
     *
     * @return string The normalized password
     */
    protected function normalizePassword(string $password): string
    {
        if (! defined('PASSWORD_HANDLER_SITE_SALT')) {
            // this function is only called if salt defined but...
            return $password;
        }
        $raw = hash('sha384', PASSWORD_HANDLER_SITE_SALT . $password, true);
        if ($this->sodiumAvailable) {
            sodium_memzero($password);
        }
        return base64_encode($raw);
    }//end normalizePassword()

    /**
     * Calculates the bcrypt cost based on year and seed. Every other year the
     *  bcrypt cost increases by one (doubling the rounds required)
     *
     * @return void
     */
    protected function calculateBcryptCost(): void
    {
        $cost = $this->costfactor + (($this->year - 1991) >> 1);
        if ($cost > 31) {
            $this->bcryptCost = 31;
            return;
        }
        if ($cost > $this->bcryptCost) {
            $this->bcryptCost = $cost;
        }
    }//end calculateBcryptCost()

    /**
     * Calculates the Argon2 kikibytes to use, doubling every three years
     *
     * @return void
     */
    protected function calculateKikiBytes(): void
    {
        $costfactor = $this->costfactor;
        if ($this->argonViaSodium) {
            //compensate for lack of more than one thread by doubling the
            // kikibytes
            $costfactor++;
        }
        $exp = $costfactor + intdiv($this->year, 3) - 666;
        $kikibytes = (1 << $exp) << 10;
        if ($kikibytes > $this->kikibytes) {
            $this->kikibytes = $kikibytes;
        }
    }//end calculateKikiBytes()

    /**
     * Calculates the Argon2 iterations value to use, adding one every two years
     *
     * @return void
     */
    protected function calculateIterations(): void
    {
        $iterations = ($this->costfactor << 1) + (($this->year - 2011) >> 1);
        if ($iterations > $this->iterations) {
            $this->iterations = $iterations;
        }
    }//end calculateIterations()

    /**
     * Calculate the Argon2 threads to use, always using 1 if by libsodium
     *
     * @return void
     */
    protected function calculateThreads(): void
    {
        $costfactor = $this->costfactor;
        if ($this->argonViaSodium) {
            $this->threads = 1;
            return;
        }
        $costfactor++;
        $this->threads = $costfactor << 1;
    }//end calculateThreads()

    /**
     * Detects whether or not an Argon2 hash needs to be regenerated
     *
     * @param string $input The cost factors.
     *
     * @return bool True if it needs to be regenerated, otherwise false.
     *
     * @throws \InvalidArgumentException If argon2 support is via sodium then
     *                                   hashes with >1 thread can't be validated.
     */
    protected function testArgonCost(string $input): bool
    {
        $arr = explode(',', $input);
        if (count($arr) !== 3) {
            return true;
        }
        $threads = intval(substr($arr[2], 2), 10);
        if ($this->argonViaSodium) {
            if ($threads !== 1) {
                if (! defined('PASSWORD_ARGON2_PROVIDER') || (PASSWORD_ARGON2_PROVIDER !== 'sodium')) {
                    // force a rehash
                    return true;
                } else {
                    // @codingStandardsIgnoreLine
                    throw new \InvalidArgumentException('This argon2 hash requires PHP linked against libargon2. You must rebuild your PHP install.');
                }
            }
        }
        if ($threads < $this->threads) {
            return true;
        }
        $memcost = intval(substr($arr[0], 2), 10);
        if ($memcost < $this->kikibytes) {
            return true;
        }
        $timecost = intval(substr($arr[1], 2), 10);
        if ($timecost < $this->iterations) {
            return true;
        }
        return false;
    }//end testArgonCost()

    /* phpass compatibility protected methods */

    /**
     * From phpass 0.5 - which is public domain. Modified by Alice Wonder. My mods
     *  were purely stylistic and to (in my opinion) aid in readability.
     *
     * Encodes the input in base64 using the phpass alphabet.
     *
     * This function makes heavy use of bitwise operators.
     *  The 0x3f is hex for decimal 63 - the highest value in a singe base64 digit.
     *
     * @author Solar Designer <solar@openwall.com>
     * @author Alice Wonder <paypal@domblogger.net>
     *
     * @see https://www.openwall.com/phpass/
     * @see https://www.php.net/manual/en/language.operators.bitwise.php
     *
     * @param string $input The raw input.
     * @param int    $count The count.
     *
     * @return string The custom base64 encoded string
     */
    protected function phpassEncode64(string $input, int $count): string
    {
        $output = '';
        $i = 0;
        while ($i < $count) {
            $value = ord($input[$i++]);
            $output .= $this->itoa64[$value & 0x3f];
            if ($i < $count) {
                $value |= ord($input[$i]) << 8;
            }
            $output .= $this->itoa64[($value >> 6) & 0x3f];
            if ($i++ >= $count) {
                break;
            }
            if ($i < $count) {
                $value |= ord($input[$i]) << 16;
            }
            $output .= $this->itoa64[($value >> 12) & 0x3f];
            if ($i++ >= $count) {
                break;
            }
            $output .= $this->itoa64[($value >> 18) & 0x3f];
        }
        return $output;
    }//end phpassEncode64()

    /**
     * From phpass 0.5 - which is public domain. Modified by Alice Wonder. My mods
     *  were purely stylistic and to (in my opinion) aid in readability.
     *
     * Generates a phpass portable hash for use in validating password against existing
     *  hash.
     *
     * @author Solar Designer <solar@openwall.com>
     * @author Alice Wonder <paypal@domblogger.net>
     *
     * @see https://www.openwall.com/phpass/
     *
     * @param string $password The password.
     * @param string $setting  The hash string (which includes the setting).
     *
     * @return string The resulting hash
     */
    protected function phpassCryptPrivate(string $password, string $setting): string
    {
        // guarantees a hash mismatch unless malformed setting starts with *0
        $output = '*0';
        if (substr($setting, 0, 2) === $output) {
            // guarantees a hash mismatch if malformed $setting starts with *0
            $output = '*1';
        }
        $id = substr($setting, 0, 3);
        # phpass uses "$P$", phpBB3 uses "$H$" for the same thing
        if (! in_array($id, array('$P$', '$H$'))) {
            if ($this->sodiumAvailable) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $output;
        }
        $count_log2 = strpos($this->itoa64, $setting[3]);
        if (! is_int($count_log2)) {
            if ($this->sodiumAvailable) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $output;
        }
        if ($count_log2 < 7 || $count_log2 > 30) {
            if ($this->sodiumAvailable) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $output;
        }
        $count = 1 << $count_log2;
        $salt = substr($setting, 4, 8);
        if (strlen($salt) !== 8) {
            if ($this->sodiumAvailable) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $output;
        }
        /*
         * phpass was forced to use md5 because at the time it was the only cryptographic
         * hash algorithm that was widely supported in every build of PHP
         */
        $hash = md5($salt . $password, true);
        while ($count > 0) {
            $hash = md5($hash . $password, true);
            $count--;
        }
        // now generate the real output
        $output = substr($setting, 0, 12);
        $output .= $this->phpassEncode64($hash, 16);
        if ($this->sodiumAvailable) {
            sodium_memzero($password);
        }
        return $output;
    }//end phpassCryptPrivate

    /**
     * Generates Drupal 7 password hash
     *
     * Modeled after phpassCryptPrivate() as Drupal 7 hash seems to be modeled after phpass.
     *
     * @see https://api.drupal.org/api/drupal/includes%21password.inc/function/_password_crypt/7.x
     *
     * @param string $password The password to validate.
     * @param string $setting  The password hash, which contains the actual settings.
     *
     * @return array Array of hashes. Drupal 7 allows for multiple hash algorithms however it
     *               also allows truncating hash so there is no way to determine which hash
     *               algorithm was used, so we have to test against as many as make sense.
     *               It appears that SHA-512 is used by default but the API indicates others
     *               can be used.
     */
    protected function drupalSevenPasswordHash($password, $setting): array
    {
        $return = array();
        $algos = array('sha512');
        $outputLength = strlen($setting);
        if ($outputLength < 12) {
            if ($this->sodiumAvailable) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $return;
        }
        if ($outputLength <= 55) {
            $algos[] = 'sha256';
        }
        if ($outputLength <= 76) {
            $algos[] = 'sha384';
        }
        if ($outputLength <= 39) {
            $algos[] = 'sha1';
        }
        if ($outputLength <= 34) {
            $algos[] = 'md5';
        }
        $setting = substr($setting, 0, 12);
        $id = substr($setting, 0, 3);
        # drupal 7 uses $s$
        if ($id !== '$S$') {
            if ($this->sodiumAvailable) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $return;
        }
        $salt = substr($setting, 4, 8);
        $count_log2 = strpos($this->itoa64, $setting[3]);
        if (! is_int($count_log2)) {
            if ($this->sodiumAvailable) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $return;
        }
        $count = 1 << $count_log2;
        foreach ($algos as $algo) {
            $hash = hash($algo, $salt . $password, true);
            while ($count > 0) {
                $hash = hash($algo, $hash . $password, true);
                $count--;
            }
            $len = strlen($hash);
            $encoded = $setting . $this->phpassEncode64($hash, $len);
            if (strlen($encoded) >= $outputLength) {
                $return[] = substr($encoded, 0, $outputLength);
            }
        }
        if ($this->sodiumAvailable) {
            sodium_memzero($password);
        }
        return $return;
    }//end drupalSevenPasswordHash()

    /**
     * Allows validating passwords using some archaic hash methods not supported by the
     * native php7 password_verify() function.
     *
     * @see https://www.openwall.com/phpass/
     *
     * @param string $password   The password being validated.
     * @param string $hashString The password hash string.
     *
     * @return bool True if valid, otherwise returns false.
     */
    protected function compatPasswordValidate(string $password, string $hashString): bool
    {
        if (ctype_xdigit($hashString)) {
            $n = strlen($hashString);
            switch ($n) {
                case 32:
                    $testHash = md5($password);
                    break;
                case 40:
                    $testHash = hash('sha1', $password, false);
                    break;
                default:
                    $testHash = hash('sha256', $password, false);
                    break;
            }
            $return = false;
            if (hash_equals($hashString, $testHash)) {
                $return = true;
            }
            if ($this->sodiumAvailable) {
                sodium_memzero($password);
            }
            return $return;
        }
        if (substr_count($hashString, ':') > 0) {
            $pos = strpos($hashString, ':');
            if (is_int($pos) && $pos >= 32) {
                $hash = substr($hashString, 0, $pos);
                if (ctype_xdigit($hash)) {
                    $pos++;
                    $salt = substr($hashString, $pos);
                    $n = strlen($hash);
                    switch ($n) {
                        case 32:
                            $testHash = md5($password . $salt);
                            break;
                        case 40:
                            $testHash = hash('sha1', $password . $salt, false);
                            break;
                        default:
                            $testHash = hash('sha256', $password . $salt, false);
                            break;
                    }
                    $return = false;
                    if (hash_equals($hash, $testHash)) {
                        $return = true;
                    }
                    if ($this->sodiumAvailable) {
                        sodium_memzero($password);
                    }
                    return $return;
                }
            }
        }
        if (substr($hashString, 0, 3) === '$P$' || substr($hashString, 0, 3) === '$H$') {
            $testHash = $this->phpassCryptPrivate($password, $hashString);
            $return = false;
            if ($testHash === $hashString) {
                $return = true;
            }
            if ($this->sodiumAvailable) {
                sodium_memzero($password);
            }
            return $return;
        }
        if (substr($hashString, 0, 3) === '$S$') {
            $return = false;
            $arr = $this->drupalSevenPasswordHash($password, $hashString);
            foreach ($arr as $testHash) {
                if ($testHash === $hashString) {
                    $return = true;
                    break;
                }
            }
            if ($this->sodiumAvailable) {
                sodium_memzero($password);
            }
            return $return;
        }
        //drupal gallery
        $test = substr($hashString, -32);
        if (ctype_xdigit($test)) {
            $n = strlen($hashString) - 32;
            if ($n > 0) {
                $salt = substr($hashString, 0, $n);
                $testHash = md5($salt . $password);
                $return = false;
                if (hash_equals($test, $testHash)) {
                    $return = true;
                }
                if ($this->sodiumAvailable) {
                    sodium_memzero($password);
                }
                return $return;
            }
        }
        // return false
        if ($this->sodiumAvailable) {
            sodium_memzero($password);
        }
        return false;
    }//end compatPasswordValidate

    /* public methods */

    /**
     * Very simple 16-byte password generator.
     *
     * @return string The generated password.
     */
    public function generatePassword(): string
    {
        $raw = random_bytes(16);
        $password = rtrim(base64_encode($raw), '=');
        if ($this->sodiumAvailable) {
            sodium_memzero($raw);
        }
        return $password;
    }//end generatePassword()

    /**
     * Create a hash from a password.
     *
     * @param string $password The password to be hashed.
     *
     * @return string The hashed password
     *
     * @throws \InvalidArgumentException
     */
    public function hashPassword(string $password): string
    {
        switch ($this->algorithm) {
            case 'argon2i':
                $passAlgo = PASSWORD_ARGON2I;
                $options = array(
                    'memory_cost' => $this->kikibytes,
                    'time_cost'   => $this->iterations,
                    'threads'     => $this->threads
                );
                break;
            case 'argon2id':
                $passAlgo = PASSWORD_ARGON2ID;
                $options = array(
                    'memory_cost' => $this->kikibytes,
                    'time_cost'   => $this->iterations,
                    'threads'     => $this->threads
                );
                break;
            default:
                $passAlgo = PASSWORD_BCRYPT;
                $options = array(
                    'cost' => $this->bcryptCost
                );
                break;
        }
        if (defined('PASSWORD_HANDLER_SITE_SALT')) {
            $password = $this->normalizePassword($password);
        }
        $hash = password_hash($password, $passAlgo, $options);
        if ($this->sodiumAvailable) {
            sodium_memzero($password);
        }
        if (! is_string($hash)) {
            throw new \InvalidArgumentException('Something is broken, password hash could not be generated.');
        }
        return $hash;
    }//end hashPassword()

    /**
     * Validates a password against a hash. Returns false on
     *  failure, and either true or a new hash on success.
     *
     * @param string $password   The password to validate.
     * @param string $hashString The hash string.
     * @param bool   $newHash    Optional. When set to true, a new hash from a validated
     *                           password will be generated. When set to the default value
     *                           of false, there are 1 in 7 odds a new hash will be generated
     *                           but it will always be generated when this method detects a
     *                           new hash is needed due to insufficient cost parameters in the
     *                           existing hash.
     *
     * @return bool|string
     *
     * @throws \InvalidArgumentException
     */
    public function validatePassword(string $password, string $hashString, bool $newHash = false)
    {
        $algoIdentifier = 'invalid';
        $tmp = explode('$', $hashString);
        if ((! isset($tmp[1])) || (substr($hashString, 0, 1) !== '$')) {
            if (strlen($hashString) === 13) {
                $algoIdentifier = 'stddes';
            } elseif ((strlen($hashString) === 20) && (substr($hashString, 0, 1) === '_')) {
                $algoIdentifier = 'extdes';
            } elseif (ctype_xdigit($hashString)) {
                $algoIdentifier = 'raw';
            } elseif (substr_count($hashString, ':') > 0) {
                $algoIdentifier = 'salted';
            }
            // special case for drupal gallery
            if ($algoIdentifier === 'invalid') {
                $test = substr($hashString, -32);
                if (ctype_xdigit($test)) {
                    $algoIdentifier = 'drupalg';
                }
            }
        } else {
            $algoIdentifier = $tmp[1];
        }
        switch ($algoIdentifier) {
            case 'argon2id':
                $hashAlgo = 'argon2id';
                if (isset($tmp[3])) {
                    if ($this->testArgonCost($tmp[3])) {
                        $newHash = true;
                    }
                }
                break;
            case 'argon2i':
                $hashAlgo = 'argon2i';
                if (isset($tmp[3])) {
                    if ($this->testArgonCost($tmp[3])) {
                        $newHash = true;
                    }
                }
                break;
            case '2y':
                $hashAlgo = 'bcrypt';
                if (isset($tmp[2])) {
                    $currentCost = intval($tmp[2]);
                    if ($currentCost < $this->bcryptCost) {
                        $newHash = true;
                    }
                }
                break;
            case '1':
                $hashAlgo = 'md5';
                break;
            case '2a':
            case '2x':
                $hashAlgo = 'blowfish';
                break;
            case '5':
                $hashAlgo = 'sha256';
                break;
            case '6':
                $hashAlgo = 'sha512';
                break;
            case 'stddes':
                $hashAlgo = 'stddes';
                break;
            case 'extdes':
                $hashAlgo = 'extdes';
                break;
            case 'raw':
                $hashAlgo = 'rawhash';
                break;
            case 'salted':
                $hashAlgo = 'saltedhash';
                break;
            case 'drupalg':
                $hashAlgo = 'drupalg';
                break;
            case 'P':
            case 'H':
                $hashAlgo = 'phpass';
                break;
            case 'S':
                $hashAlgo = 'drupal7';
                break;
            default:
                throw new \InvalidArgumentException('Unsupported password hash algorithm');
        }
        if (! in_array($hashAlgo, $this->supportedAlgos)) {
            throw new \InvalidArgumentException('Unsupported password hash algorithm');
        }
        if ($hashAlgo !== $this->algorithm) {
            $newHash = true;
        } elseif (! $newHash) {
            // one in 7 odds to regenerate hash
            $n = random_int(1, 7);
            if ($n === 4) {
                $newHash = true;
            }
        }
        switch ($hashAlgo) {
            case 'phpass':
            case 'drupal7':
            case 'rawhash':
            case 'saltedhash':
            case 'drupalg':
                if (! $this->compatPasswordValidate($password, $hashString)) {
                    if ($this->sodiumAvailable) {
                        sodium_memzero($password);
                    }
                    return false;
                }
                break;
            default:
                if (defined('PASSWORD_HANDLER_SITE_SALT')) {
                    $normalized = $this->normalizePassword($password);
                    // validate the hash
                    if (! password_verify($normalized, $hashString)) {
                        // try non-normalized version, hash may predate implementation of
                        //  normalized passwords
                        $newHash = true;
                        if (! password_verify($password, $hashString)) {
                            if ($this->sodiumAvailable) {
                                sodium_memzero($normalized);
                                sodium_memzero($password);
                            }
                            return false;
                        }
                    }
                // This elseif is for systems that do not normalize the password
                } elseif (! password_verify($password, $hashString)) {
                    if ($this->sodiumAvailable) {
                        sodium_memzero($password);
                    }
                    return false;
                }
                break;
        }
        // only gets this far if password validates
        if ($newHash) {
            $returnHash = $this->hashPassword($password);
        }
        if ($this->sodiumAvailable) {
            sodium_memzero($password);
            if (isset($normalized)) {
                sodium_memzero($normalized);
            }
        }
        if (isset($returnHash)) {
            return $returnHash;
        }
        return true;
    }//end validatePassword()

    /**
     * The constructor
     *
     * @param string $strength     Optional. You can specify 'standard', 'moderate', or 'high'.
     *                             Defaults to 'standard'.
     * @param string $algorithm    Optional. You can specify 'bcrypt', 'argon2i', or 'argon2id'. Any
     *                             other string and it detects which to use. If the version of PHP
     *                             does not support the choice, it detects.
     * @param bool   $sodiumcompat Optional. Defaults to false. When true, even if PHP is compiled
     *                             against libargon2 it will only use 1 thread for argon2 hashes to
     *                             retain hash compatibility with the sodium extension.
     */
    public function __construct(string $strength = 'standard', string $algorithm = 'default', bool $sodiumcompat = false)
    {
        if (defined('PASSWORD_ARGON2_PROVIDER') && (PASSWORD_ARGON2_PROVIDER === 'sodium')) {
            // PHP 7.4+ compiled w/o libargon2 but with sodium extension available
            $this->argonViaSodium = true;
        } elseif ($sodiumcompat) {
            $this->argonViaSodium = true; // a white lie...
        }
        $year = intval(date('Y'));
        if ($year > $this->year) {
            $this->year = $year;
        }
        switch ($strength) {
            case 'moderate':
                $this->costfactor = 1;
                break;
            case 'high':
                $this->costfactor = 2;
                break;
            default:
                break;
        }
        if (defined('PASSWORD_ARGON2I')) {
            $this->supportedAlgos[] = 'argon2i';
            if (PHP_VERSION_ID >= 70300) {
                $this->supportedAlgos[] = 'argon2id';
            }
        }
        // check for sodium
        if (extension_loaded('sodium')) {
            $this->sodiumAvailable = true;
        }
        switch ($algorithm) {
            case 'bcrypt':
                $this->calculateBcryptCost();
                break;
            case 'argon2i':
                if (defined('PASSWORD_ARGON2I')) {
                    $this->calculateKikiBytes();
                    $this->calculateIterations();
                    $this->calculateThreads();
                    $this->algorithm = 'argon2i';
                } else {
                    $this->calculateBcryptCost();
                }
                break;
            default:
                // for specified argon2id we still have to detect
                if (defined('PASSWORD_ARGON2I')) {
                    $this->calculateKikiBytes();
                    $this->calculateIterations();
                    $this->calculateThreads();
                    if (PHP_VERSION_ID >= 70300) {
                        $this->algorithm = 'argon2id';
                    } else {
                        $this->algorithm = 'argon2i';
                    }
                } else {
                    $this->calculateBcryptCost();
                }
                break;
        }
    }//end __construct()
}//end class

?>