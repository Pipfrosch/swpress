<?php
declare(strict_types=1);

/**
 * Ajax script for password generators.
 * Modify to your needs, Line 59 specifically.
 *
 * What it does, it eats a query specifying the type of password to generate
 * and returns it in a simple JSON object.
 *
 * Optional $_POST['type'] to specify the type of password to generate:
 *
 * familysafe:
 *    returns an AdjectiveNounNumber password theoretically of banking quality
 *    from the family safe dictionaries.
 * carnal:
 *    return an AdjectiveNounNumber password theoretically of banking quality
 *    from the kinky dictionaries.
 * childrens:
 *    returns an AdjectiveNounNumber password that probably is not banking
 *    quality from the children's dictionaries.
 * default:
 *    returns a base64 encoded representation of 16 random bytes. A quality
 *    pRNG is used to generate the random bytes. This is a very high quality
 *    password but is difficult for humans to remember.
 *
 * Additionally, if the $_POST['advanced'] is set, then if the generator used
 *    is the children's generator, is returns a modified version that also does
 *    include one non alpha-numeric character, making it a better choice for
 *    use cases like e-mail accounts. The value of the $_POST['advanced']
 *    variable does not matter, only whether or not it is set. Also, this
 *    variable has no meaning for the other generators.
 *
 *
 * Intended use - your web page for password generation does an AJAX call to
 * this script and this script returns a generated password that your web
 * page then puts into the form for the user to enter a password.
 *
 * Note that at this time, most generators have dictionaries too small to be
 * considered safe.
 *
 * This ajax response script not yet tested.
 *
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

// only return passwords over https
if (! isset($_SERVER['HTTPS'])) {
    exit;
}
if (empty($_SERVER['HTTPS'])) {
    exit;
}

// modify me to your filesystem
$PasswordHandlerDir = '/home/alice/gitlab/swpress/psr4/Pipfrosch/PasswordHandler/';

$ANNabstractClass = $PasswordHandlerDir . 'AdjectiveNounNumberGenerator.php';
$FamilySafe = $PasswordHandlerDir . 'FamilySafeGenerator.php';
$ChildrensGenerator = $PasswordHandlerDir . 'ChildrensPasswordGenerator.php';
$CarnalGenerator = $PasswordHandlerDir . 'CarnalGenerator.php';

// This AJAX script will still produce a secure output even if the needed classes
//  are not loaded, so don't die badly if they are not available
if (file_exists($ChildrensGenerator)) {
    require_once($ChildrensGenerator);
}
if (file_exists($ANNabstractClass)) {
    require_once($ANNabstractClass);
    if (file_exists($CarnalGenerator)) {
        require_once($CarnalGenerator);
    }
    if (file_exists($FamilySafe)) {
        require_once($FamilySafe);
    }
}

/**
 * Generates a very secure 16-byte random password
 *
 * @return string
 */
function fallbackGenerator(): string
{
    $raw = random_bytes(16);
    $pass = rtrim(base64_encode($raw), '=');
    if (function_exists('sodium_memzero')) {
        sodium_memzero($raw);
    }
    return $pass;
}//end fallbackGenerator()


$type='default';
$advanced = false; // used for children's generator
if (isset($_POST['type'])) {
    $type = strtolower($_POST['type']);
    $type = preg_replace('/[^a-z]/', '', $type);
}
if (isset($_POST['advanced'])) {
    $advanced = true; // only has meaning for children's generator
}

$password = null;
$message = '(Demonstration Only. Dictionaries Too Small. Do Not Use.)';
switch ($type) {
    case 'familysafe':
        if (class_exists('\Pipfrosch\PasswordHandler\FamilySafeGenerator')) {
            $password = \Pipfrosch\PasswordHandler\FamilySafeGenerator::generatePassword();
        }
        break;
    case 'carnal':
        if (class_exists('\Pipfrosch\PasswordHandler\CarnalGenerator')) {
            $password = \Pipfrosch\PasswordHandler\CarnalGenerator::generatePassword();
        }
        break;
    case 'childrens':
        if (class_exists('\Pipfrosch\PasswordHandler\ChildrensPasswordGenerator')) {
            $password = \Pipfrosch\PasswordHandler\ChildrensPasswordGenerator::generatePassword($advanced);
        }
        break;
}
if (empty($password)) {
    $password = fallbackGenerator();
    $message = "(High Quality <abbr title='pseudo-Random Number Generator'>pRNG</abbr>. Safe to use.)";
}

$return = array();
$return['password'] = $password;
$return['message'] = $message;
$return['timestamp'] = time();

header("Content-Type: application/json");
print json_encode($return);
if (function_exists('sodium_memzero')) {
    sodium_memzero($password);
}
exit;
?>