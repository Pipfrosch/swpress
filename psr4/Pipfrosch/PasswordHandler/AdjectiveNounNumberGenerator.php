<?php
declare(strict_types=1);

/**
 * This is a simple and fun password generator. Static class with just
 *  one public method - generatePassword()
 *
 * Extend the class to redefine the $adjectives and $nouns array to suit your theme.
 *
 * It primarily serves the purpose of demonstrating how easy-ish to remember passwords
 *  can be generated.
 *
 * The password philosophy this generator uses is known as the "mnemonic adjective-noun-number"
 *  password philosophy. It generates passwords that are easier for humans to remember yet at
 *  the same time impossible to brute force unless the attacker knows at least the adjective-noun.
 *
 * Most mnemonic adjective-noun-number generators are strong enough for casual use but not
 *  strong enough for banking. I did the extra work to ensure the result is strong enough for
 *  banking - meaning the adjective and noun get slightly modified but in a way that is easy
 *  for many users to remember without writing it down.
 *
 * Long term plan is to have a dozen or so different adjective-noun-number generators each
 *  with a different theme that users can pick from, as well as some other types of generators
 *  too. The extended classes should have at least 750 adjectives and and at least 750 nouns as
 *  that means half a million adjective-noun combinations each of which also has a number string
 *  as well as the adjective and noun both being modified from the dictionary form.
 *
 * Honestly I recommend using a password manager with 16-bytes of random data base64 encoded
 *  but people like mnemonic passwords and will use the unsafe weak generators for them if
 *  quality generators are not available. Especially Americans, for some reason Americans are
 *  worse with passwords than any other nationality I am aware of. So there is a need for this
 *  type of generator. Most are junk, this one isn't. Well, until it has >= 750 in both
 *  dictionaries it is, but...
 *
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\PasswordHandler;

/**
 * The class
 */
abstract class AdjectiveNounNumberGenerator
{
    /**
     * Characters to turn into a symbol
     *
     * @var array
     */
    protected static $symbolMap = array(
        'a' => '@',
        'e' => '3',
        'i' => '!',
        'l' => '1',
        'o' => '0',
        's' => '5',
        't' => '+',
    );

    /**
     * Takes a string and turns one random letter in the string
     *  to either a symbol or upper case.
     *
     * @param string $input The input string.
     *
     * @return string The modified string
     */
    protected static function symbolAdjust(string $input): string
    {
        $n = strlen($input);
        if ($n === 0) {
            return 'fail';
        }
        $found = false;
        $pos = 0;
        $substitute = '';
        $i = 0;
        while (! $found) {
            $pos = random_int(0, ($n - 1));
            $char = substr($input, $pos, 1);
            if (isset(self::$symbolMap[$char])) {
                $substitute = self::$symbolMap[$char];
                $found = true;
            } else {
                $i++;
            }
            if ($i === $n) {
                $substitute = strtoupper($char);
                $found = true;
            }
        }
        if ($pos === 0) {
            $return = $substitute;
        } else {
            $return = substr($input, 0, $pos) . $substitute;
        }
        $remainder = ($n - $pos);
        if ($remainder > 0) {
            $return .= substr($input, ($pos + 1));
        }
        return $return;
    }//end symbolAdjust()

    /**
     * Takes a string and turns one random letter in the string
     *  to upper case.
     *
     * @param string $input The input string.
     *
     * @return string The modified string
     */
    protected static function upperAdjust(string $input): string
    {
        $n = strlen($input);
        if ($n === 0) {
            return 'fail';
        }
        $pos = random_int(0, ($n - 1));
        $char = substr($input, $pos, 1);
        $substitute = strtoupper($char);
        if ($pos === 0) {
            $return = $substitute;
        } else {
            $return = substr($input, 0, $pos) . $substitute;
        }
        $remainder = ($n - $pos);
        if ($remainder > 0) {
            $return .= substr($input, ($pos + 1));
        }
        return $return;
    }//end upperAdjust()

    /**
     * Generate the digits at the end of the password.
     *  Will be either three or four digits in length and will
     *  have either two or three unique numbers (one number will
     *  be re-used)
     *
     * @return string The digits.
     */
    protected static function digitGenerator(): string
    {
        $length = random_int(3, 4);
        $arr = array();
        $arr[] = random_int(0, 9);
        $found = false;
        while (! $found) {
            $test = random_int(0, 9);
            if (! in_array($test, $arr)) {
                $arr[] = $test;
                if ((count($arr) + 1) === $length) {
                    $found = true;
                }
            }
        }
        // $arr now has either 2 or 3 different numbers in it
        $m = random_int(0, ($length - 2));
        $string = (string) $arr[$m];
        foreach ($arr as $x) {
            $string .= (string) $x;
        }
        return str_shuffle($string);
    }//end digitGenerator()

    /**
     * Add a separator character. Having a separator
     *  between Adjective and Noun and between Noun
     *  and Number both makes the password easier to
     *  remember and increases the entropy, by a factor
     *  of 25 in this case.
     *
     * @return string
     */
    protected static function separator(): string
    {
        $n = random_int(0, 4);
        switch ($n) {
            case 0:
                return ':';
                break;
            case 1:
                return '-';
                break;
            case 2:
                return '|';
                break;
            case 3:
                return '_';
                break;
        }
        return '#';
    }//end separator()

    /**
     * Most generated passwords with this class have a quality of 100 but a few
     *  are below 70 (yet still seem to be above 60)
     *
     * This function rejects any with a quality below 85.
     *
     * pwscore is a utility developed by Red Hat for Red Hat Enterprise Linux
     *  but I believe most Linux distributions now include it. I
     *  have not seen it on *bsd systems but it probably builds
     *  on them, I don't do a lot on *bsd.
     *
     * According to pwscore documentation, anything above 50
     *  is fairly strong. Requiring > 85 is pedantic but since
     *  vast majority score the maximum 100 and most that do not
     *  are still above 90, it is both okay to be pedantic and
     *  okay to just silently return true if the pwscore utility
     *  is not present.
     *
     * @param string $password The password to validate quality of.
     *
     * @return bool True if quality >= 85, otherwise false.
     */
    protected static function ensureQuality($password): bool
    {
        if (strlen($password) < 12) {
            return false;
        }
        $pwscore = '/usr/bin/pwscore';
        // graceful exit if not present
        if (! file_exists($pwscore)) {
            return true;
        }
        setlocale(LC_ALL, 'en_US.utf-8');
        $password = escapeshellarg($password);
        $shellcommand = 'echo ' . $password . ' |' . $pwscore . ' 2>&1';
        exec($shellcommand, $out, $ret);
        if ($ret !== 0) {
            return false;
        }
        if (! is_numeric($out[0])) {
            return false;
        }
        $score = intval($out[0]);
        if ($score < 85) {
            return false;
        }
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
        }
        return true;
    }//end ensureQuality()

    /**
     * The public function classes extending this one must have.
     *
     * @return string
     */
    abstract public static function generatePassword();
}//end class

?>