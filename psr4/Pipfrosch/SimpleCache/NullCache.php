<?php
declare(strict_types=1);

/**
 * A dummy implementation for when the server is not set up for either Redis or APCu
 *
 * @package SimpleCache
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\SimpleCache;

/**
 * A dummy implementation with same public methods, parameters, and return types as a proper
 * PSR-16 implementation would have (except the exceptions are never thrown).
 */
class NullCache implements \Psr\SimpleCache\CacheInterface
{
    /**
     * Always returns the default.
     *
     * @param string $key     The unique key of this item in the cache.
     * @param mixed  $default Default value to return if the key does not exist.
     *
     * @return mixed The value of the item from the cache, or $default in case of cache miss.
     */
    public function get($key, $default = null)
    {
        return $default;
    }//end get()

    /**
     * Always returns false as there is no actual cache engine.
     *
     * @param string                 $key   The key of the item to store.
     * @param mixed                  $value The value of the item to store, must be serializable.
     * @param null|int|\DateInterval $ttl   Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     */
    public function set($key, $value, $ttl = null)
    {
        return false;
    }//end set()

    /**
     * Always returns true because there is no actual cache engine.
     *
     * @param string $key The unique cache key of the item to delete.
     *
     * @return bool True if the item was successfully removed. False if there was an error.
     */
    public function delete($key)
    {
        return true;
    }//end delete()

    /**
     * Always return true because there is no actual cache engine.
     *
     * @return bool True on success and false on failure.
     */
    public function clear()
    {
        return true;
    }//end clear()

    /**
     * Always returns default because there is no actual cache engine.
     *
     * @param iterable $keys    A list of keys that can obtained in a single operation.
     * @param mixed    $default Default value to return for keys that do not exist.
     *
     * @return iterable A list of key => value pairs. Cache keys that do not exist or are
     *                  stale will have $default as value.
     */
    public function getMultiple($keys, $default = null)
    {
        return $default;
    }//end getMultiple()

    /**
     * Always returns false because there is no actual cache engine.
     *
     * @param iterable               $values A list of key => value pairs for a multiple-set operation.
     * @param null|int|\DateInterval $ttl    Optional. The TTL value of this item. If no value is sent and
     *                                       the driver supports TTL then the library may set a default value
     *                                       for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     */
    public function setMultiple($values, $ttl = null)
    {
        return false;
    }//end setMultiple()

    /**
     * Always returns true because there is no actual cache engine.
     *
     * @param iterable $keys A list of string-based keys to be deleted.
     *
     * @return bool True if the items were successfully removed. False if there was an error.
     */
    public function deleteMultiple($keys)
    {
        return true;
    }//end deleteMultiple()

    /**
     * Always returns false because there is no cache engine.
     *
     * @param string $key The cache item key.
     *
     * @return bool
     */
    public function has($key)
    {
        return false;
    }//end has()

    /**
     * The constructor.
     */
    public function __construct()
    {
    }//end __construct()
}//end class

?>