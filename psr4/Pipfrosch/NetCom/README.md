NetCom README
=============

These are classes in an attempt to implement:

* [PSR-7](https://www.php-fig.org/psr/psr-7/)
* Possibly [PSR-15](https://www.php-fig.org/psr/psr-15/)
* Possibly [PSR-17](https://www.php-fig.org/psr/psr-17/)
* Possibly [PSR-18](https://www.php-fig.org/psr/psr-18/)

as a replacement for the `\Requests` and `\SWPress\HTTP` classes currently used
by the SWPress fork of WordPress.

However these classes will be generic in nature and useful in other contexts as
well, which is why they are not being put within the `\SWPress` namespace.

When complete, the `\Requests` and `\SWPress\HTTP` classes will likely remain
for some time to make it easier to port plugins and themes written for
WordPress.

These classes are not ready for use.


PSR-7 Classes
-------------

### The `\Pipfrosch\NetCom\URI` Class

Implements `\Psr\Http\Message\UriInterface` but still needs some work and unit
tests.

Has the following two helper classes:

1. `\Pipfrosch\NetCom\UrlEncode` -- Static methods for intelligently applying
   percent encoding to components of a URI.
2. `\Pipfrosch\NetCom\ValidatePathByScheme` -- Static methods for validating
   the `path` component of a URI based upon the `scheme` component. Needs a lot
   of work.
3. `\Pipfrosch\NetCom\UriInvalidArgumentException` -- Static methods for
   throwing `\InvalidArgumentException` exceptions that includes useful
   information about the reason for the exception. Still needs work.
4. `\Pipfrosch\NetCom\NormalizeISBN` -- Static methods for normalizing an ISBN
   number.

#### Additional Features

When presented with a `host` sub-component of the `authority` component that
contains non-ASCII characters or with a `path` component that looks like an
e-mail address where the hostname contains non-ASCII characters, the class will
convert then to IDNA ASCII. To convert them back to unicode with UTF-8 encoding
just use the `URI->setUnicode(boolean)` function with an argument of `true` and
those components of the URI will be returned in unicode with UTF-8 encoding.
Likewise the argument of `false` sets it to the default where they are instead
displayed with so-called ‘punycode’ ASCII (prefix with `xn--`).

When the `User Info` sub-component of the `authority` component contains a
password, by default this class will not return the password. If you really
need the password returned, use the `URI->setShowPassword(bool)` function with
an argument of `true` and the class will return a URI that includes the
password if present. Likewise you can return it to the default state by using
an argument of `false`.

A valid URI always has a scheme, but the PSR-7 interface being implemented does
not allow requiring a scheme or attempting to guess the scheme when not
provided. If you use this class to parse a non-conforming URI that lacks a
scheme, you can attempt to guess the scheme with the `URI->guessScheme()`
method. If a scheme is already defined, it will return the scheme. Otherwise it
will attempt to guess the scheme based upon the port (if set) and the path (if
set) and if it can guess a scheme that is valid with the port and path, it will
return the guess. If it can not guess, it returns false. The function does not
set the scheme it guesses, you can do that with the `URI->setScheme(string)`
function using the guess as the supplied string argument.

#### Optional Dependency

If the [PEAR](https://pear.php.net/) class
[`\Net_IPv6`](https://pear.php.net/package/Net_IPv6) is either loaded or
available for autoload, then `\Pipfrosch\NetCom\URI` will compact IPv6 address
to their shortest form when an IPv6 address is the `host` sub-component of the
`authority` component.
