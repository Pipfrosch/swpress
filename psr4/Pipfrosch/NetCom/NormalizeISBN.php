<?php
declare(strict_types=1);

/**
 * This class provides static methods for normalizing ISBN numbers.
 *
 * Note that for some registration groups, this class does not (yet) know how to separate
 * the registrant element from the publication element so for those registration groups,
 * there will be no hyphen between those two groups in the normalized output.
 *
 * Currently it only understands how to separate for 978 prefix (and 10 digit) with a
 * registration group of 0 or 1 - which are the vast majority of what is sold within the
 * United States.
 *
 * Unit tests in file tests/NetCom/NormalizeISBNTest.php
 *
 * @package    Netcom
 * @subpackage PSR-7
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\NetCom;

/**
 * Static methods for ISBN normalizing.
 */
class NormalizeISBN
{

    /**
     * Calculate checksum bit for 10 digit ISBN
     *
     * @param string $isbn The ISBN to create check digit for.
     *
     * @return string The check digit
     */
    public static function checksum10digit(string $isbn): string
    {
        $sum = 0;
        $reverse = strrev($isbn);
        for ($i=1; $i<10; $i++) {
            $digit = intval(substr($reverse, $i, 1), 10);
            $sum = $sum + (($i + 1) * $digit);
        }
        $remainder = ($sum % 11);
        if ($remainder === 0) {
            return '0';
        } elseif ($remainder === 1) {
            return 'X';
        } else {
            return (string) (11 - $remainder);
        }
    }//end checksum10digit()

    /**
     * Calculate checksum bit for 13 digit ISBN
     *
     * @param string $isbn   The last 10 digits of ISBN to create check digit for.
     * @param string $prefix The three digit prefix. Defaults to '978'.
     *
     * @return string The check digit
     */
    public static function checksum13digit(string $isbn, string $prefix = '978'): string
    {
        $sum = 0;
        $isbn = $prefix . $isbn;
        for ($i=0; $i<12; $i++) {
            $digit = intval(substr($isbn, $i, 1), 10);
            if ($i % 2 === 0) {
                $sum = $sum + $digit;
            } else {
                $sum = $sum + ($digit * 3);
            }
        }
        $remainder = ($sum % 10);
        if ($remainder === 0) {
            return '0';
        } else {
            return (string) (10 - $remainder);
        }
    }//end checksum13digit()

    /**
     * Calculates the checksum digit when you do not know what it is. Mostly for debugging.
     *
     * @param string $string  Input string to retrieve correct checksum digit for.
     *
     * @return bool|string The calculated checksum digit, or false if it can not be calculated.
     */
    public static function calculateChecksum(string $string)
    {
        $string = preg_replace(array('/-/', '/\s+/', '/x/'), array('', '', 'X'), $string);
        if (strlen($string) === 9) {
            $string .= 'X';
            if (preg_match('/^[0-9]{9,9}[0-9X]$/', $string) !== 1 && preg_match('/^[0-9]{12,12}[0-9X]$/', $string) !== 1) {
                return false;
            }
        }
        if (strlen($string) === 12) {
            $string .= 'X';
            if (preg_match('/^[0-9]{13,13}[0-9X]$/', $string) !== 1 && preg_match('/^[0-9]{12,12}[0-9X]$/', $string) !== 1) {
                return false;
            }
        }
        $l = strlen($string);
        switch ($l) {
            case 10:
                return self::checksum10digit($string);
                break;
            case 13:
                $prefix = substr($string, 0, 3);
                $string = substr($string, 3, 10);
                return self::checksum13digit($string, $prefix);
        }
        return false;
    }//end calculateChecksum()

    /**
     * Normalize an ISBN number.
     *
     * @param string $string  Input string to normalize.
     * @param bool   $convert Optional. Convert ISBN 10 to ISBN 13 when normalizing. Defaults to False.
     *
     * @return string|bool The normalized string or false on failure.
     */
    public static function normalizeISBN(string $string, bool $convert = false)
    {
        $string = strtoupper($string);
        $string = preg_replace('/[^0-9X]/', '', $string);
        $return = '';
        $thirteen = true;
        $prefix = '978';
        if (strlen($string) === 10) {
            $thirteen = false;
        } elseif (strlen($string) === 13) {
            $prefix = substr($string, 0, 3);
            $string = substr($string, 3, 10);
            $return = $prefix . '-';
        } else {
            return false;
        }
        if (preg_match('/[0-9]{9,9}[0-9X]$/', $string) !== 1) {
            return false;
        } elseif (! $thirteen) {
            $checksum = substr($string, 9, 1);
            if (! $convert) {
                $calculated = self::checksum10digit($string);
                if ($checksum !== $calculated) {
                    return false;
                }
            } else {
                $return = '978-';
                // verify 10 digit is valid
                $calculated = self::checksum10digit($string);
                if ($calculated !== $checksum) {
                    return false;
                }
                $checkThirteen = self::checksum13digit($string);
                $string = substr($string, 0, 9) . $checkThirteen;
            }
        } else {
             $checksum = substr($string, 9, 1);
             $checkThirteen = self::checksum13digit($string, $prefix);
            if ($checksum !== $checkThirteen) {
                return false;
            }
        }
        switch ($prefix) {
            case '978':
                $firstDigit = substr($string, 0, 1);
                switch ($firstDigit) {
                    case '0':
                        $return .= '0-';
                        $found = false;
                        if (preg_match('/^0[0-1]/', $string) === 1) {
                            //2+6 digit
                            $return .= substr($string, 1, 2) . '-';
                            $return .= substr($string, 3, 6) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^0228/', $string) === 1) {
                            //4+4 digit
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^0369/', $string) === 1) {
                            //4+4 digit
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^0655/', $string) === 1) {
                            //4+4 digit
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^0639[0-7]/', $string) === 1) {
                            //4+4 digit
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^0639/', $string) === 1) {
                            //7+2 digit
                            $return .= substr($string, 1, 7) . '-';
                            $return .= substr($string, 8, 2) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^0648/', $string) === 1) {
                            //7+2 digit
                            $return .= substr($string, 1, 7) . '-';
                            $return .= substr($string, 8, 2) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^07/', $string) === 1) {
                            //4+4 digit
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^08[0-4]/', $string) === 1) {
                            //4+4 digit
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        // now safe to do the 3-5 match
                        if (! $found && preg_match('/^0[2-6]/', $string) === 1) {
                            //3+5 digit
                            $return .= substr($string, 1, 3) . '-';
                            $return .= substr($string, 4, 5) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^08[5-9]/', $string) === 1) {
                            //5+3 digit
                            $return .= substr($string, 1, 5) . '-';
                            $return .= substr($string, 6, 3) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^09[0-4]/', $string) === 1) {
                            //6+2 digit
                            $return .= substr($string, 1, 6) . '-';
                            $return .= substr($string, 7, 2) . '-';
                            $found = true;
                        }
                        if (! $found) {
                            //7+1
                            $return .= substr($string, 1, 7) . '-';
                            $return .= substr($string, 8, 1) . '-';
                        }
                        break;
                    case 1:
                        $return .= '1-';
                        $found = false;
                        if (preg_match('/^10[1-8]/', $string) === 1) {
                            //2+6 digit
                            $return .= substr($string, 1, 2) . '-';
                            $return .= substr($string, 3, 6) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^100/', $string)) {
                            //3+5
                            $return .= substr($string, 1, 3) . '-';
                            $return .= substr($string, 4, 5) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^109/', $string)) {
                            //4+4
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^179/', $string)) {
                            //4+4
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^139[8-9]/', $string)) {
                            //4+4
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^1[1-3]/', $string)) {
                            //3+5
                            $return .= substr($string, 1, 3) . '-';
                            $return .= substr($string, 4, 5) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^15[5-9]/', $string)) {
                            //5+3
                            $return .= substr($string, 1, 5) . '-';
                            $return .= substr($string, 6, 3) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^1[4-5]/', $string)) {
                            //4+4
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^16/', $string)) {
                            //5+3
                            $return .= substr($string, 1, 5) . '-';
                            $return .= substr($string, 6, 3) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^173[2-9]/', $string)) {
                            //7+1
                            $return .= substr($string, 1, 7) . '-';
                            $return .= substr($string, 8, 1) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^1775[0-3]/', $string)) {
                            //7+1
                            $return .= substr($string, 1, 7) . '-';
                            $return .= substr($string, 8, 1) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^17[04]/', $string)) {
                            //5+3
                            $return .= substr($string, 1, 5) . '-';
                            $return .= substr($string, 6, 3) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^171[0-6]/', $string)) {
                            //5+3
                            $return .= substr($string, 1, 5) . '-';
                            $return .= substr($string, 6, 3) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^17/', $string)) {
                            //4+4
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^1999/', $string)) {
                            //7+1
                            $return .= substr($string, 1, 7) . '-';
                            $return .= substr($string, 8, 1) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^18[0-5]/', $string)) {
                            //5-3
                            $return .= substr($string, 1, 5) . '-';
                            $return .= substr($string, 6, 3) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^186[0-8]/', $string)) {
                            //5-3
                            $return .= substr($string, 1, 5) . '-';
                            $return .= substr($string, 6, 3) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^1869[0-7]/', $string)) {
                            //5-3
                            $return .= substr($string, 1, 5) . '-';
                            $return .= substr($string, 6, 3) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^18/', $string)) {
                            //6-2
                            $return .= substr($string, 1, 6) . '-';
                            $return .= substr($string, 7, 2) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^197[3-7]/', $string)) {
                            //4-4
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^198[0-6]/', $string)) {
                            //4-4
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^1987[0-7]/', $string)) {
                            //4-4
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^1916[0-4]/', $string)) {
                            //7-1
                            $return .= substr($string, 1, 7) . '-';
                            $return .= substr($string, 8, 1) . '-';
                            $found = true;
                        }
                        if (! $found && preg_match('/^191650[0-5]/', $string)) {
                            //7-1
                            $return .= substr($string, 1, 7) . '-';
                            $return .= substr($string, 8, 1) . '-';
                            $found = true;
                        }
                        if (! $found) {
                            //6-2
                            $return .= substr($string, 1, 6) . '-';
                            $return .= substr($string, 7, 2) . '-';
                            $found = true;
                        }
                        break;
                    case '2':
                        $return .= '2-';
                        $found = false;
                        if (preg_match('/^2226/', $string)) {
                            //3-5
                            $return .= substr($string, 1, 3) . '-';
                            $return .= substr($string, 4, 5) . '-';
                            $found = true;
                        }
                        if (! $found) {
                            $return .= $firstDigit . '-';
                            $return .= substr($string, 1, 8) . '-';
                        }
                        break;
                    case '3':
                        $return .= '3-';
                        $found = false;
                        if (preg_match('/^37965/', $string)) {
                            //4-4
                            $return .= substr($string, 1, 4) . '-';
                            $return .= substr($string, 5, 4) . '-';
                            $found = true;
                        }
                        if (! $found) {
                            $return .= $firstDigit . '-';
                            $return .= substr($string, 1, 8) . '-';
                        }
                        break;
                    case '4':
                        $return .= '4-';
                        $found = false;
                        if (preg_match('/^419/', $string)) {
                            //2-6
                            $return .= substr($string, 1, 2) . '-';
                            $return .= substr($string, 3, 6) . '-';
                            $found = true;
                        }
                        if (! $found) {
                            $return .= $firstDigit . '-';
                            $return .= substr($string, 1, 8) . '-';
                        }
                        break;
                    case '5':
                        $return .= '5-';
                        $found = false;
                        if (preg_match('/^585270/', $string)) {
                            //5-3
                            $return .= substr($string, 1, 5) . '-';
                            $return .= substr($string, 6, 3) . '-';
                            $found = true;
                        }
                        if (! $found) {
                            $return .= $firstDigit . '-';
                            $return .= substr($string, 1, 8) . '-';
                        }
                        break;
                    case '7':
                        $return .= '7-';
                        $found = false;
                        if (preg_match('/^7301/', $string)) {
                            //3-5
                            $return .= substr($string, 1, 3) . '-';
                            $return .= substr($string, 4, 5) . '-';
                            $found = true;
                        }
                        if (! $found) {
                            $return .= $firstDigit . '-';
                            $return .= substr($string, 1, 8) . '-';
                        }
                        break;
                    case '6':
                        $firstTwo = substr($string, 0, 2);
                        if ($firstTwo === '65') {
                            // Brazil
                            $return .= substr($string, 0, 2) . '-';
                            $return .= substr($string, 2, 7) . '-';
                        } elseif (preg_match('/^6[3-4]/', $string) === 1) {
                            // unallocated
                            return false;
                        } elseif (preg_match('/^62[5-9]/', $string) === 1) {
                            // unallocated
                            return false;
                        } else {
                            $return .= substr($string, 0, 3) . '-';
                            $return .= substr($string, 3, 6) . '-';
                        }
                        break;
                    case '8':
                        $return .= substr($string, 0, 2) . '-';
                        $return .= substr($string, 2, 7) . '-';
                        break;
                    case '9':
                        $firstTwo = substr($string, 0, 2);
                        switch ($firstTwo) {
                            case '96':
                                $firstThree = substr($string, 0, 3);
                                $return .= $firstThree . '-';
                                switch ($firstThree) {
                                    case '965':
                                        $found = false;
                                        if (preg_match('/^965222/', $string) === 1) {
                                            //3+3
                                            $return .= substr($string, 3, 3) . '-';
                                            $return .= substr($string, 6, 3) . '-';
                                            $found = true;
                                        }
                                        if (! $found && preg_match('/^965376/', $string) === 1) {
                                            //3+3
                                            $return .= substr($string, 3, 3) . '-';
                                            $return .= substr($string, 6, 3) . '-';
                                            $found = true;
                                        }
                                        break;
                                    default:
                                        $return .= substr($string, 3, 6) . '-';
                                        break;
                                }
                                break;
                            case '90':
                            case '91':
                            case '92':
                            case '93':
                            case '94':
                                $return .= $firstTwo . '-';
                                $return .= substr($string, 2, 7) . '-';
                                break;
                            case '99':
                                if (substr($string, 0, 3) === '999') {
                                    $return .= substr($string, 0, 5) . '-';
                                    $return .= substr($string, 5, 4) . '-';
                                } else {
                                    $return .= substr($string, 0, 4) . '-';
                                    $return .= substr($string, 4, 5) . '-';
                                }
                                break;
                            default:
                                $return .= substr($string, 0, 3) . '-';
                                $return .= substr($string, 3, 6) . '-';
                                break;
                        }
                        break;
                }
                break;
            case '979':
                $return .= substr($string, 0, 2) . '-';
                $return .= substr($string, 2, 7) . '-';
                break;
            default:
                return false;
        // Now calculate the checksum or verify the checksum and add it
        }
        $return .= substr($string, 9, 1);
        return $return;
    }//end normalizeISBN()

    /**
     * Normalize an ISBN number without hyphens
     *
     * @param string $string  Input string to normalize.
     * @param bool   $convert Optional. Convert ISBN 10 to ISBN 13 when normalizing. Defaults to False.
     *
     * @return string|bool The normalized string or false on failure.
     */
    public static function nohyphenISBN(string $string, bool $convert = false)
    {
        $normalized = self::normalizeISBN($string, $convert);
        if (is_string($normalized)) {
            return preg_replace('/-/', '', $normalized);
        }
        return false;
    }//end nohyphenISBN()
}//end class

?>