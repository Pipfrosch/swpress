<?php
declare(strict_types=1);

/**
 * This class provides static methods for RFC 3986 percent encoding of URI characters.
 *
 * Unit tests in file tests/NetCom/UrlEncodeTest.php
 *
 * @package    Netcom
 * @subpackage PSR-7
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 *
 * @see https://tools.ietf.org/html/rfc3986
 */

namespace Pipfrosch\NetCom;

/**
 * Static methods for URL encoding.
 */
class UrlEncode
{
    /**
     * Array of characters to always url encode
     */
    protected static $encodeMe = array(
        "/%/"     => "%25",
        "/ /"     => "%20",
        "/\{/"    => "%7B",
        "/\}/"    => "%7D",
        "/\\\\/"  => "%5C",
        "/\^/"    => "%5E",
        "/`/"     => "%60",
        "/#/"     => "%23",
        "/\!/"    => "%21",
        "/\\$/"   => "%24",
        "/\'/"    => "%27",
        "/\(/"    => "%28",
        "/\)/"    => "%29",
        "/\*/"    => "%2A",
        "/;/"     => "%3B",
        "/=/"     => "%3D", //fixme
        "/\&/"    => "%26", //fixme
        "/\</"    => "%3C",
        "/\>/"    => "%3E",
    );

    /**
     * Use regular expressions to replace unsafe characters with percent encoded.
     *
     * @param array  $array  Characters in addition to always encode.
     * @param string $string The string to encode.
     *
     * @return string The encoded string.
     */
    protected static function makeSafe($array, $string): string
    {
        mb_internal_encoding('UTF-8');
        $string = rawurldecode($string);
        $dirty = array_merge(self::$encodeMe, $array);
        $s = array();
        $r = array();
        foreach ($dirty as $key => $value) {
            $s[] = $key;
            $r[] = $value;
        }
        $return = preg_replace($s, $r, $string);
        // ASCII control characters
        $return = preg_replace_callback(
            '/[\x00-\x1F\x7F]/',
            /**
             * Anonymous function for callback
             *
             * @param array Array of match.
             *
             * @return string The percent encoded replacement.
             */
            function (array $m):string {
                return rawurlencode($m[0]);
            },
            $return
        );
        //now non-ascii characters
        $return = preg_replace_callback(
            '/[^\x00-\x7F]/',
            /**
             * Anonymous function for callback
             *
             * @param array Array of match.
             *
             * @return string The percent encoded replacement.
             */
            function (array $m):string {
                return rawurlencode($m[0]);
            },
            $return
        );
        return $return;
    }//end makeSafe()

    /**
     * Provide URL Encoding for the fragment string.
     *
     * @param string $input The URI fragment to encode.
     *
     * @return string The encoded fragment.
     */
    public static function fragmentEncode(string $input): string
    {
        $array = array(
            "/:/"  => "%3A",
            "/,/"  => "%2C",
            "/@/"  => "%40",
            "/\+/" => "%2B",
            "/\[/" => "%5B",
            "/\]/" => "%5D",
            "/\|/" => "%7C",
        );
        return self::makeSafe($array, $input);
    }//end fragmentEncode()

    /**
     * Provide URL Encoding for the query string. Do not run this on the query
     * string itself, but on the key and value, to avoid & and = being encoded.
     *
     * @param string $input The URI query to encode.
     *
     * @return string The encoded query string.
     */
    public static function queryEncode(string $input): string
    {
        $array = array(
            "/@/" => "%40",
        );
        $arr = explode("&", $input);
        $j = count($arr);
        for ($i=0; $i<$j; $i++) {
            $barr = explode("=", $arr[$i]);
            $jj = count($barr);
            for ($ii=0; $ii<$jj; $ii++) {
                $barr[$ii] = self::makeSafe($array, $barr[$ii]);
            }
            $arr[$i] = implode('=', $barr);
        }
        return implode('&', $arr);
    }//end queryEncode()

    /**
     * Provide URL Encoding for the path string.
     *
     * @param string $input The URI path to encode.
     *
     * @return string The encoded path.
     */
    public static function pathEncode(string $input): string
    {
        $array = array(
            "/\?/" => "%3F",
            "/\[/" => "%5B",
            "/\]/" => "%5D",
            "/\|/" => "%7C",
        );
        return self::makeSafe($array, $input);
    }//end pathEncode()
}//end class

?>