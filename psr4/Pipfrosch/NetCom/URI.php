<?php
declare(strict_types=1);

/**
 * This class implements RFC 3986 URI.
 *
 * Work in progress.
 *
 * Unit tests in file tests/NetCom/URITest.php
 *
 * @package    Netcom
 * @subpackage PSR-7
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 *
 * @see https://tools.ietf.org/html/rfc3986
 * @see https://www.iana.org/assignments/uri-schemes/uri-schemes.xhtml
 */

namespace Pipfrosch\NetCom;

/**
 * A class that implements the UriInterface from PSR-7
 */
class URI implements \Psr\Http\Message\UriInterface
{
    /**
     * For internationalized domain names.
     *
     * @const VARIANT The IDNA variant to use.
     */
    const VARIANT = INTL_IDNA_VARIANT_UTS46;

    /**
     * A place to store strings for exceptions.
     *
     * @var array
     */
    protected $throwArray = array();

    /**
     * When set to true, will return unicode encoded in UTF8 where applicable
     *
     * @var bool
     */
    protected $unicode = false;

    /**
     * When set to true, passwords in the URI will be shown.
     *
     * @var bool
     */
    protected $showPassword = false;

    /**
     * @var string
     */
    protected $scheme = '';

    /**
     * @var string
     */
    protected $username = '';

    /**
     * @var string
     */
    protected $userpass = '';

    /**
     * @var string
     */
    protected $host = '';

    /**
     * @var null|int
     */
    protected $port = null;

    /**
     * If it looks like an e-mail address type of path, set to true so the domain part
     * can optionally be returned as a unicode domain encoded in UTF-8.
     *
     * @var bool
     */
    protected $emailTypePath = false;

    /**
     * @var string
     */
    protected $path = '';

    /**
     * @var string
     */
    protected $query = '';

    /**
     * @var string
     */
    protected $fragment = '';

    /**
     * Port to scheme array.
     *
     * @see https://gist.githubusercontent.com/mahmoud/2fe281a8daaff26cfe9c15d2c5bf5c8b/raw/0f2490ea69dd4cb96400920d6fd8692205917eb6/scheme_port_map.json
     *
     * @var array
     */
    protected $registeredPorts = array(
        'acap'     =>  674,
        'afap'     =>  548,
        'dict'     => 2628,
        'dns'      =>   53,
        'ftp'      =>   21,
        'gopher'   =>   70,
        'http'     =>   80,
        'https'    =>  443,
        'imap'     =>  143,
        'imaps'    =>  993,
        'ipp'      =>  631,
        'ipps'     =>  631,
        'irc'      =>  194,
        'ircs'     => 6697,
        'ldap'     =>  389,
        'ldaps'    =>  636,
        'mms'      => 1755,
        'msrp'     => 2855,
        'mtqp'     => 1038,
        'nntp'     =>  119,
        'pop'      =>  110,
        'pops'     =>  995,
        'prospero' => 1525,
        'redis'    => 6379,
        'rsync'    =>  873,
        'rtsp'     =>  554,
        'rtsps'    =>  332,
        'rtspu'    => 5005,
        'sftp'     =>   22,
        'smb'      =>   45,
        'snmp'     =>  161,
        'ssh'      =>   22,
        'svn'      => 3690,
        'telnet'   =>   23,
        'ventrilo' => 3784,
        'vnc'      => 5900,
        'wais'     =>  210,
        'ws'       =>   80,
        'wss'      =>  443,
    );

    /**
     * Allowed Protocol List
     *
     * @var array
     */
    protected static $allowedProtocolList = array(
        'http',
        'https',
        'ftp',
        'ftps',
        'mailto',
        'news',
        'irc',
        'gopher',
        'nntp',
        'feed',
        'telnet',
        'mms',
        'rtsp',
        'svn',
        'tel',
        'fax',
        'xmpp',
        'webcal',
        'urn',
    );

    /** Protected Methods */

    /**
     * Extract the fragment from a URI.
     *
     * @param string $URI The URI to extract the fragment from.
     *
     * @return void
     */
    protected function extractFragment(string &$URI): void
    {
        $pos = strrpos($URI, "#");
        if (is_int($pos)) {
            $pos++;
            $fragment = substr($URI, $pos);
            $this->setFragment($fragment);
            $pos--;
            $URI = substr($URI, 0, $pos);
        }
    }//end extractFragment()

    /**
     * Validate a query.
     *
     * For Key=Value queries, a key may not be re-used. There is no specification I know of
     * on how to handle key re-use and different programs handle it in different ways, so it
     * simply should not be done.
     *
     * For Key=Value queries, this validator requires that the key does not contain any
     * unicode characters outside the ASCII range and will not change after URI encoding.
     *
     * @param string      $query  The query to validate.
     * @param string|null $scheme The scheme to use. Defaults to null (uses class property).
     *
     * @return bool|string The valid query normalized, or false if not legal.
     */
    protected function validateQuery(string $query, $scheme = null)
    {
        if (empty($query)) {
            return $query;
        }
        if (empty($scheme)) {
            $scheme = $this->scheme;
        }
        $scheme = strtolower($scheme);
        if (empty($scheme) || $scheme === 'urn') {
            // FIXME TODO - proper value of non KEY=Value queries
            return $query;
        }
        $allowDuplicateKeys = false;
        if (in_array($scheme, array('http', 'https'))) {
            $allowDuplicateKeys = true;
        }
        $keyArray = array();
        $arr = explode('&', $query);
        $j = count($arr);
        for ($i=0; $i<$j; $i++) {
            $barr = explode('=', $arr[$i]);
            $jjj = count($barr);
            if ($jjj > 2) {
                return false;
            } elseif($jjj === 1 || $barr[1] === '') {
                $barr[1] = $barr[0];
            }
            //make sure key does not have non-ascii
            if (! mb_detect_encoding($barr[0], 'ASCII', true)) {
                return false;
            }
            //make sure key is not altered by percent encoding
            $key = UrlEncode::queryEncode($barr[0]);
            if ($key !== $barr[0]) {
                return false;
            }
            //make sure key is not already used.
            if (! $allowDuplicateKeys) {
                if (in_array($key, $keyArray)) {
                    return false;
                }
                $keyArray[] = $key;
            }
            $arr[$i] = implode('=', $barr);
        }
        return implode('&', $arr);
    }//end validateQuery()

    /**
     * Extract the query from a URI that already has fragment removed.
     *
     * @param string $URI The URI to extract the query from.
     *
     * @return bool True on success, False on failure.
     */
    protected function extractQuery(string &$URI): bool
    {
        $pos = strrpos($URI, '?');
        if (is_int($pos)) {
            $pos++;
            $query = substr($URI, $pos);
            $this->throwArray = array();
            $this->throwArray[] = $query;
            $res = $this->setQuery($query);
            $pos--;
            $URI = substr($URI, 0, $pos);
        }
        if (isset($res) && ($res === false)) {
            return false;
        }
        return true;
    }//end extractQuery()


    /** Scheme Specific Functions */

    /**
     * Removes properties not appropriate for specified scheme.
     * May not be complete, also need to check relevant RFCs.
     *
     * @return void
     */
    protected function nukePropertiesByScheme(): void
    {
        switch ($this->scheme) {
            // known URI schemes w/ no authority or fragment
            case 'bitcoin':
            case 'mailto':
                $this->username = '';
                $this->userpass = '';
                $this->host = '';
                $this->port = null;
                $this->fragment = '';
                break;
            // known uri schemes with no authority
            case 'urn':
                $this->username = '';
                $this->userpass = '';
                $this->host = '';
                $this->port = null;
                break;
        }
    }//end nukePropertiesByScheme()

    /**
     * Validate a scheme.
     *
     * @param string $scheme The scheme to validate.
     *
     * @return null|string The valid scheme normalized, or null if not legal.
     */
    protected function validateScheme(string $scheme)
    {
        if (empty($scheme)) {
            // an empty scheme is valid
            return '';
        }
        $scheme = strtolower($scheme);
        if (preg_match('/^[a-z][a-z0-9\+\.\-]+$/', $scheme)) {
            return $scheme;
        }
        return null;
    }//end validateScheme()

    /**
     * Extracts the scheme from a URI.
     *
     * @param string $URI   The URI to extract the scheme from.
     *
     * @return bool Returns true on success, false on failure.
     */
    protected function extractScheme(string &$URI)
    {
        $scheme = null;
        $length = strpos($URI, ':');
        if ($length === false) {
            if (! is_null($this->validateScheme($URI))) {
                $scheme = $URI;
                $URI = '';
            } else {
                $scheme = '';
            }
        } else {
            $scheme = substr($URI, 0, $length);
            $start = $length + 1;
            $URI = substr($URI, $start);
        }
        $scheme = $this->validateScheme($scheme);
        $this->throwArray = array();
        $this->throwArray[] = $scheme;
        if (! is_null($scheme)) {
            // path is not yet set, so can not check scheme matches path.
            if ($this->setScheme($scheme) !== false) {
                return true;
            }
        }
        return false;
    }//end extractScheme()

    /**
     * Sets the port property inferred from the scheme.
     *
     * @param string $scheme The scheme to infer port from.
     *
     * @return void
     */
    protected function setPortByScheme(string $scheme): void
    {
        $setPort = false;
        if (empty($this->port) || empty($this->scheme)) {
            $setPort = true;
        }
        if (! empty($this->scheme)) {
            if (isset($this->registeredPorts[$this->scheme])) {
                $official = $this->registeredPorts[$this->scheme];
                if ($official === $this->port) {
                    $setPort = true;
                }
            }
        }
        if ($setPort) {
            if (isset($this->registeredPorts[$scheme])) {
                $this->port = $this->registeredPorts[$scheme];
            } else {
                $this->port = null;
            }
        }
    }//end setPortByScheme()

    /**
     * Loads additional default ports from key => value array.
     *
     * @param array $dict A Key => Value array of schemes => ports. The value should be a
     *                    positive integer or null. If null, any existing scheme that matches
     *                    the Key will have the default port removed from the registeredPorts
     *                    array.
     *
     * @return void
     */
    public function loadDefaultSchemeToPorts(array $dict): void
    {
        $removeKeys = array();
        foreach ($dict as $key => $value) {
            $scheme = $this->validateScheme($key);
            if (! empty($scheme)) {
                if (empty($value)) {
                    //remove key from array
                    $removeKeys[$scheme] = true;
                } elseif (is_int($value) && $value > 0) {
                    $this->registeredPorts[$scheme] = $value;
                }
            }
        }
        if (count($removeKeys) > 0) {
            $this->registeredPorts = array_diff_key($this->registeredPorts, $removeKeys);
        }
    }//end loadDefaultSchemeToPorts()

    /** Authority Specific Functions */

    /**
     * Takes a host as input, normalizes it, and sets the host property.
     *
     * @param string $host The host to normalize and set.
     *
     * @return string|bool The normalized host if success, False if not a valid host.
     */
    protected function normalizeHost(string $host)
    {
        // empty is valid
        if (empty($host)) {
            return '';
        }
        $host = trim($host);
        $host = ltrim($host, '[');
        $host = rtrim($host, ']');
        // IPv4 test
        if (filter_var($host, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            return $host;
        }
        // IPv6 test
        if (filter_var($host, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
            if (class_exists('\Net_IPv6', true)) {
                /**
                 * Optional PEAR package, composer can not install for
                 * debug so to make vimeo/psalm happy...
                 *
                 * @psalm-suppress UndefinedClass
                 */
                $host = \Net_IPv6::compress($host, true);
            }
            return '[' . strtolower($host) . ']';
        }
        // we have a hostname, probably...
        if (! mb_detect_encoding($host, 'ASCII', true)) {
            // attempt to convert
            if (function_exists('idn_to_ascii')) {
                $host = idn_to_ascii($host, 0, self::VARIANT);
                if ($host === false) {
                    //conversion failed
                    return false;
                }
            } else {
                // nothing we can do
                return false;
            }
        }
        // if still here we have an ASCII string.
        $host = strtolower($host);
        if (filter_var($host, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME)) {
            //make sure not an invalid IPv4 address
            if (preg_match('/[a-z]/', $host) === 0) {
                return false;
            }
            return $host;
        }
        return false;
    }//end normalizeHost()

    /**
     * Return internationalized variant of a hostname.
     *
     * @return string|bool The hostname in unicode with UTF-8 encoding, or false if the
     *                     hostname can not be converted.
     */
    protected function unicodeHostname()
    {
        if (substr($this->host, 0, 4) !== 'xn--') {
            return $this->host;
        }
        // if still here we have an IDNA ASCII host
        if (function_exists('idn_to_utf8')) {
            $utf8 = idn_to_utf8($this->host, 0, self::VARIANT);
            if (is_string($utf8)) {
                return $utf8;
            }
        }
        return false;
    }//end unicodeHostname()

    /**
     * Extract and parse the authority from a URI that already has scheme extracted.
     *
     * @param string $URI   The URI to extract the authority from.
     * @param bool   $throw Whether or not to throw exception on invalid hostname.
     *
     * @return void
     *
     * @throws \InvalidArgumentException for invalid authority unless second parameter false.
     */
    protected function extractAuthority(string &$URI, bool $throw): void
    {
        if (substr($URI, 0, 2) === '//') {
            $URI = substr($URI, 2);
        } else {
            return;
        }
        $pos = strpos($URI, '/');
        if ($pos === false) {
            $authority = $URI;
            $URI = '';
        } else {
            $authority = substr($URI, 0, $pos);
            $URI = substr($URI, $pos);
        }
        // now get the user info from the authority
        $pos = strpos($authority, '@');
        if (is_int($pos)) {
            $userinfo = substr($authority, 0, $pos);
            $pos++;
            $authority = substr($authority, $pos);
            // now separate user from password.
            $pos = strpos($userinfo, ':');
            if (is_int($pos)) {
                $username = rawurldecode(substr($userinfo, 0, $pos));
                $this->username = rawurlencode($username);
                $pos++;
                $userpass = rawurldecode(substr($userinfo, $pos));
                $this->userpass = rawurlencode($userpass);
            } else {
                $username = rawurldecode($userinfo);
                $this->username = rawurlencode($username);
            }
        }
        // now get the host part - note the strrpos gets LAST occurrence.
        $pos = strrpos($authority, ':');
        if (is_int($pos)) {
            $host = substr($authority, 0, $pos);
            $pos++;
            $port = substr($authority, $pos);
            $nhost = $this->normalizeHost($host);
            if (is_string($nhost)) {
                $this->host = $nhost;
                if (is_numeric($port)) {
                    $port = intval($port, 10);
                    if ($port > 0) {
                        $this->port = $port;
                    }
                } elseif (! is_bool($port)) {
                    throw Exceptions\UriInvalidArgumentException::invalidUriPort($port);
                }
            } elseif ($throw) {
                throw Exceptions\UriInvalidArgumentException::invalidUriHost($host);
            }
        } else {
            $host = $this->normalizeHost($authority);
            if (is_string($host)) {
                $this->host = $host;
            } elseif ($throw) {
                throw Exceptions\UriInvalidArgumentException::invalidUriHost($authority);
            }
        }
    }//end extractAuthority()

    /**
     * Takes unicode UTF-8 domain names into account for e-mail address
     * type domain names.
     *
     * @param string $path The path.
     *
     * @return string The path adjusted if necessary.
     */
    protected function adjustPathForEmail(string $path): string
    {
        $multi = explode(',', $path);
        $j = count($multi);
        for ($i=0; $i<$j; $i++) {
            $arr = explode('@', $multi[$i]);
            if (count($arr) !== 2) {
                return $path;
            }
            if (! mb_detect_encoding($arr[1], 'ASCII', true)) {
                // attempt to covert
                if (function_exists('idn_to_ascii')) {
                    $arr[1] = idn_to_ascii($arr[1], 0, self::VARIANT);
                }
            }
            $testEmail = trim($arr[0] . '@' . $arr[1]);
            if (! filter_var($testEmail, FILTER_VALIDATE_EMAIL)) {
                return $path;
            }
            $multi[$i] = $testEmail;
        }
        $this->emailTypePath = true;
        return implode(',', $multi);
    }//end adjustPathForEmail()

    /**
     * When the path is for e-mail addresses and unicode is enabled,
     * this function converts punycode domains in the e-mail address(es) to
     * unicode encoded in UTF-8
     *
     * @return string
     */
    protected function adjustPathFromEmail(): string
    {
        $multi = explode(',', $this->path);
        $j = count($multi);
        for ($i=0; $i<$j; $i++) {
            $arr = explode('@', $multi[$i]);
            if (count($arr) !== 2) {
                //should never happen but...
                return $this->path;
            }
            if (substr($arr[1], 0, 4) === 'xn--') {
                if (function_exists('idn_to_utf8')) {
                    $utf8 = idn_to_utf8($arr[1], 0, self::VARIANT);
                    if (is_string($utf8)) {
                        $arr[1] = $utf8;
                    }
                }
            }
            $multi[$i] = implode('@', $arr);
        }
        return implode(',', $multi);
    }//end adjustPathFromEmail()

    /**
     * Normalize URN paths that we know how to normalize.
     *
     * @param string $path The URN path to normalize.
     *
     * @return string The normalized path.
     */
    protected function normalizeUrnPath(string $path): string
    {
        $arr = explode(':', $path);
        if (count($arr) < 2) {
            // nothing we can do
            return $path;
        }
        // lowercase the namespace
        $arr[0] = trim(strtolower($arr[0]));
        switch ($arr[0]) {
            case 'ietf':
                $j = count($arr);
                for ($i=1; $i<$j; $i++) {
                    $arr[$i] = trim(strtolower($arr[$i]));
                }
                break;
            case 'isbn':
                $test = NormalizeISBN::normalizeISBN($arr[1]);
                if (is_string($test)) {
                    $arr[1] = $test;
                }
                break;
            case 'uuid':
                $str = strtolower($arr[1]);
                $str = preg_replace('/-/', '', $str);
                if (strlen($str) === 32) {
                    $arr[1] = substr($str, 0, 8) . '-';
                    $arr[1] .= substr($str, 8, 4) . '-';
                    $arr[1] .= substr($str, 12, 4) . '-';
                    $arr[1] .= substr($str, 16, 4) . '-';
                    $arr[1] .= substr($str, 20, 12);
                }
                break;
        }
        return implode(':', $arr);
    }//end normalizeUrnPath()

    /**
     * Process the path
     *
     * @param string $path The path to process.
     *
     * @return bool True if valid path, false otherwise.
     */
    protected function processPath(string $path)
    {
        if (empty($path)) {
            return true;
        }
        return $this->setPath($path);
    }//end processPath()


    /**
     * Non PSR-7 "setCamel" Public Functions.
     * These may not be available in other implementations of the interface.
     *
     * None of these "setCamel" functions will throw exceptions but same may return
     * false to indicate a failure.
     */

    /**
     * Manual setting of the scheme. When manually setting the scheme, the class will
     *  automatically set if the existing port or scheme is empty or if the existing
     *  port matches the existing scheme. However it is recommended that you manually
     *  set the port (either to the corresponding integer or to null) as well, even if
     *  you are using the default port. The function returns the port after setting
     *  the scheme and port, so you can verify it is what you are expecting.
     *
     * @param string $scheme The scheme to set.
     *
     * @return int|null|bool The current port after setting (integer or null) on
     *                       success, false on invalid scheme.
     */
    public function setScheme(string $scheme)
    {
        $scheme = $this->validateScheme($scheme);
        if (is_null($scheme)) {
            return false;
        }
        $this->setPortByScheme($scheme);
        $this->scheme = $scheme;
        $this->nukePropertiesByScheme();
        return $this->port;
    }//end setScheme()

    /**
     * Sets the user info
     *
     * @param string      $username The username to set.
     * @param string|null $userpass Optional. The password to set. Defaults to null.
     *
     * @return void
     */
    public function setUserInfo(string $username, $userpass = null): void
    {
        $username = rawurldecode(trim($username));
        $this->username = rawurlencode($username);
        if (empty($userpass)) {
            $userpass = '';
        }
        $userpass = rawurldecode(trim($userpass));
        $this->userpass = rawurlencode($userpass);
    }//end setUserInfo()

    /**
     * Set the host
     *
     * @param string $host The host to set.
     *
     * @return bool True on valid host, otherwise false.
     */
    public function setHost(string $host): bool
    {
        $host = $this->normalizeHost($host);
        if (is_string($host)) {
            $this->host = $host;
            return true;
        }
        return false;
    }//end setHost()

    /**
     * Explicitly set the port
     *
     * @psalm-suppress RedundantConditionGivenDocblockType
     *
     * @param int|null $port The port to set. Defaults to null.
     *
     * @return bool True on success, false on failure.
     */
    public function setPort($port = null): bool
    {
        if (is_null($port)) {
            $this->port = null;
            return true;
        }
        if (is_int($port)) {
            if ($port > 0) {
                $this->port = $port;
                return true;
            }
        }
        return false;
    }//end setPort()

    /**
     * Explicitly set the path.
     *
     * @param string $path The path to set.
     *
     * @return bool True on success, otherwise false.
     */
    public function setPath(string $path): bool
    {
        $this->emailTypePath = false;
        if ($this->scheme === 'urn') {
            $path = $this->normalizeUrnPath($path);
        } else {
            $path = $this->adjustPathForEmail($path);
        }
        if (ValidatePathByScheme::validatePath($this->scheme, $path)) {
            $path = UrlEncode::pathEncode($path);
            $this->path = $path;
            return true;
        }
        return false;
    }//end setPath()

    /**
     * Explicitly set the query.
     *
     * @param string $query The query to set.
     *
     * @return bool True on success, otherwise false.
     */
    public function setQuery(string $query): bool
    {
        $query = trim($query);
        if (empty($query)) {
            $this->query = '';
            return true;
        }
        // FIXME - VALIDATE QUERY
        $validquery = $this->validateQuery($query);
        if (is_string($validquery)) {
            $query = $validquery;
        } else {
            return false;
        }
        $query = UrlEncode::queryEncode($query);
        $this->query = $query;
        return true;
    }//end setQuery()

    /**
     * Explicitly set the query via GetQueryBuilder object
     * HTTP/HTTPS scheme only.
     *
     * @param GetQueryBuilder $object The GetQueryBuilderObject.
     *
     * @return bool True on success, otherwise false.
     */
    public function setGetQuery(GetQueryBuilder $object): bool
    {
        if ($this->scheme !== 'https' && $this->scheme !== 'http') {
            return false;
        }
        $this->query = $object->__toString();
        return true;
    }//end setGetQuery()

    /**
     * Explicitly set the fragment.
     *
     * @param string $fragment The fragment to set.
     *
     * @return void
     */
    public function setFragment(string $fragment): void
    {
        $fragment = trim($fragment);
        $this->fragment = UrlEncode::fragmentEncode($fragment);
    }//end setFragment()

    /**
    * Set unicode property.
    *
    * @param bool $unicode Whether or not to return Unicode encoded in UTF-8 where applicable.
    *
    * @return void
    */
    public function setUnicode(bool $unicode): void
    {
        $this->unicode = $unicode;
    }//end setUnicode()

    /**
     * Allow the password to be shown.
     *
     * @param bool $showPassword Whether or not to output password part of authority.
     *
     * @return void
     */
    public function setShowPassword(bool $showPassword): void
    {
        $this->showPassword = $showPassword;
    }//end setShowPassword()

    /** End of Non PSR-7 "setCamel" Public Functions */


    /** PSR-7 UriInterface Public Functions */

    /**
     * Return an RFC 3986 scheme.
     *
     * @see https://tools.ietf.org/html/rfc3986#section-3.1
     *
     * @return string The scheme, lower case. Empty string if no scheme.
     */
    public function getScheme(): string
    {
        return $this->scheme;
    }//end getScheme()

    /**
     * Return an RFC 3986 authority.
     *
     * @see https://tools.ietf.org/html/rfc3986#section-3.2
     *
     * @return string The authority, lower case. Empty string if no authority.
     */
    public function getAuthority(): string
    {
        if (empty($this->host) && empty($this->username)) {
            return '';
        }
        $authority = '';
        if (! empty($this->username)) {
            $authority .= $this->username;
            if ($this->showPassword && (! empty($this->userpass))) {
                $authority .= ':' . $this->userpass;
            }
            if (! empty($this->host)) {
                $authority .= '@';
            }
        }
        // host port
        if ($this->unicode) {
            $host = $this->unicodeHostname();
            if (is_string($host)) {
                $authority .= $host;
            } else {
                $authority .= $this->host;
            }
        } else {
            $authority .= $this->host;
        }
        // port part
        if ((strlen($authority) > 2) && (! empty($this->port))) {
            $addPort = false;
            if (empty($this->scheme)) {
                $addPort = true;
            } else {
                if (isset($this->registeredPorts[$this->scheme])) {
                    $standard = $this->registeredPorts[$this->scheme];
                    if ($standard !== $this->port) {
                        $addPort = true;
                    }
                } else {
                    $addPort = true;
                }
            }
            if ($addPort) {
                $authority .= ':' . (string) $this->port;
            }
        }
        return $authority;
    }//end getAuthority()

    /**
     * Return an RFC 3968 User Info component.
     *
     * Note that by default, a password is not included. Use setShowPassword(bool $showPassword)
     * first if you need a password included.
     *
     * @return string The user info in username[:userpass]
     */
    public function getUserInfo(): string
    {
        if (empty($this->username)) {
            return '';
        }
        $userinfo = $this->username;
        if ($this->showPassword && (! empty($this->userpass))) {
            $userinfo .= ':' . $this->userpass;
        }
        return $userinfo;
    }//end getUserInfo()

    /**
     * Return an RFC 3986 host.
     *
     * @see https://tools.ietf.org/html/rfc3986#section-3.2.2
     *
     * @return string The host, lower case. Empty string if no host.
     */
    public function getHost(): string
    {
        if ($this->unicode) {
            $host = $this->unicodeHostname();
            if (is_string($host)) {
                return $host;
            }
        }
        return $this->host;
    }//end getHost()

    /**
     * Return an RFC 3986 port.
     *
     * @see https://tools.ietf.org/html/rfc3986#section-3.2.3
     *
     * @return int|null The port number, or null if port is not set or if matches scheme.
     */
    public function getPort()
    {
        if (! empty($this->scheme) && isset($this->registeredPorts[$this->scheme])) {
            $standard = $this->registeredPorts[$this->scheme];
            if ($standard === $this->port) {
                return null;
            }
        }
        return $this->port;
    }//end getPort()

    /**
     * Return an RFC 3986 path.
     *
     * @return string The path, or an empty string if there is not a path.
     */
    public function getPath(): string
    {
        if ($this->unicode && $this->emailTypePath) {
            return $this->adjustPathFromEmail();
        }
        return $this->path;
    }//end getPath()

    /**
     * Return an RFC 3986 query string
     *
     * @return string The query string, or empty if there is not one.
     */
    public function getQuery(): string
    {
        return $this->query;
    }//end getQuery()

    /**
     * Return an RFC 3986 fragment string
     *
     * @return string The fragment, or empty if there is not one.
     */
    public function getFragment(): string
    {
        return $this->fragment;
    }//end getFragment()

    /**
     * Return an instance with the specified scheme.
     *
     * @param string $scheme The scheme for the instance.
     *
     * @return static A new instance with the specified scheme.
     *
     * @throws \InvalidArgumentException for invalid scheme.
     */
    public function withScheme($scheme)
    {
        $scheme = trim($scheme);
        $this->throwArray = array();
        $this->throwArray[] = $scheme;
        if (! $this->validateScheme($scheme)) {
            throw Exceptions\UriInvalidArgumentException::invalidUriScheme($this->throwArray);
        }
        if (! ValidatePathByScheme::validatePath($scheme, $this->path)) {
            $this->throwArray[] = $this->path;
            throw Exceptions\UriInvalidArgumentException::invalidUriScheme($this->throwArray);
        }
        if (! is_string($this->validateQuery($this->query, $scheme))) {
            //FIXME send a different exception stating scheme no workie with query
            throw Exceptions\UriInvalidArgumentException::schemeDoesNotMatchQueryString($scheme, $this->query);
        }
        $this->throwArray = array();
        $return = new static();

        $return->setUnicode($this->unicode);
        $return->setUserInfo($this->username, $this->userpass);
        $return->setHost($this->host);
        $return->setPort($this->port);
        $return->setPath($this->path);
        $return->setQuery($this->query);
        $return->setFragment($this->fragment);
        $return->setScheme($scheme);
        return $return;
    }//end withScheme()

    /**
     * Return an instance with the specified User Info.
     *
     * @param string      $user     The username to set.
     * @param string|null $password Optional. The password to set.
     *
     * @return static A new instance with the specified user info.
     */
    public function withUserInfo($user, $password = null)
    {
        if (empty($password)) {
            if (substr_count($user, ':') > 0) {
                $arr = explode(':', $user);
                $user = $arr[0];
                $password = $arr[1];
            }
        }
        $return = new static();
        $return->setUnicode($this->unicode);
        $return->setUserInfo($user, $password);
        $return->setHost($this->host);
        $return->setPort($this->port);
        $return->setPath($this->path);
        $return->setQuery($this->query);
        $return->setFragment($this->fragment);
        $return->setScheme($this->scheme);
        return $return;
    }//end withUserInfo()

    /**
     * Return an instance with the specified host.
     *
     * @param string $host The host to set.
     *
     * @return static A new instance with the specified host.
     *
     * @throws \InvalidArgumentException for invalid host.
     */
    public function withHost($host)
    {
        $normalizedHost = $this->normalizeHost($host);
        if (! is_string($normalizedHost)) {
            throw Exceptions\UriInvalidArgumentException::invalidUriHost($host);
        }
        $return = new static();

        $return->setUnicode($this->unicode);
        $return->setUserInfo($this->username, $this->userpass);
        $return->setHost($normalizedHost);
        $return->setPort($this->port);
        $return->setPath($this->path);
        $return->setQuery($this->query);
        $return->setFragment($this->fragment);
        $return->setScheme($this->scheme);
        return $return;
    }//end withHost()

    /**
     * Return an instance with the specified port.
     *
     * @param null|int $port The port to set.
     *
     * @return static A new instance with the specified host.
     *
     * @throws \InvalidArgumentException for invalid host.
     */
    public function withPort($port)
    {
        if (! empty($port)) {
            if ($port < 1) {
                throw Exceptions\UriInvalidArgumentException::invalidUriPort($port);
            }
        }
        $return = new static();

        $return->setUnicode($this->unicode);
        $return->setUserInfo($this->username, $this->userpass);
        $return->setHost($this->host);
        $return->setScheme($this->scheme);
        $return->setPath($this->path);
        $return->setQuery($this->query);
        $return->setFragment($this->fragment);
        $return->setPort($port);
        return $return;
    }//end withPort()

    /**
     * Returns an instance with the specified path.
     *
     * @param string $path The path to set.
     *
     * @return static A new instance with the specified path.
     *
     * @throws \InvalidArgumentException for invalid path.
     */
    public function withPath($path)
    {
        $path = trim($path);
        if ($this->scheme === 'urn') {
            $path = preg_replace('/^urn:/i', '', $path);
        }
        $this->throwArray = array();
        $this->throwArray[] = $path;
        if (! empty($this->scheme)) {
            $this->throwArray[] = $this->scheme;
        }
        if ($this->scheme === 'urn') {
            $path = $this->normalizeUrnPath($path);
        } else {
            $path = $this->adjustPathForEmail($path);
        }
        if (! ValidatePathByScheme::validatePath($this->scheme, $path)) {
            throw Exceptions\UriInvalidArgumentException::invalidUriPath($this->throwArray);
        }
        $this->throwArray = array();
        $return = new static();

        $return->setUnicode($this->unicode);
        $return->setUserInfo($this->username, $this->userpass);
        $return->setHost($this->host);
        $return->setPort($this->port);
        $return->setPath($path);
        $return->setQuery($this->query);
        $return->setFragment($this->fragment);
        $return->setScheme($this->scheme);
        return $return;
    }//end withPath()

    /**
     * Returns an instance with the specified query.
     *
     * An empty query string value is equivalent to removing the query string.
     *
     * @param string $query The query string to use with the new instance.
     *
     * @return static A new instance with the specified query string.
     *
     * @throws \InvalidArgumentException for invalid query strings.
     */
    public function withQuery($query)
    {
        $query = trim($query);
        $validquery = $this->validateQuery($query);
        if (is_string($validquery)) {
            $query = $validquery;
        } else {
            throw Exceptions\UriInvalidArgumentException::invalidUriQuery($query, $this->scheme);
        }
        $return = new static();

        $return->setUnicode($this->unicode);
        $return->setUserInfo($this->username, $this->userpass);
        $return->setHost($this->host);
        $return->setPort($this->port);
        $return->setPath($this->path);
        $return->setQuery($query);
        $return->setFragment($this->fragment);
        $return->setScheme($this->scheme);
        return $return;
    }//end withQuery()

   /**
    * Returns an instance with the specified fragment.
    *
    * An empty query string value is equivalent to removing the fragment.
    *
    * @param string $fragment The fragment string to use with the new instance.
    *
    * @return static A new instance with the specified fragment string.
    */
    public function withFragment($fragment)
    {
        $return = new static();

        $return->setUnicode($this->unicode);
        $return->setUserInfo($this->username, $this->userpass);
        $return->setHost($this->host);
        $return->setPort($this->port);
        $return->setPath($this->path);
        $return->setQuery($this->query);
        $return->setFragment($fragment);
        $return->setScheme($this->scheme);
        return $return;
    }//end withFragment()

    /**
     * Serialize the URI instance as a string.
     *
     * @return string
     */
    public function __toString()
    {
        if (empty($this->scheme)) {
            $return = '';
        } else {
            $return = $this->scheme . ':';
            $this->nukePropertiesByScheme();
        }
        $authority = $this->getAuthority();
        if (! empty($authority)) {
            $return .= '//';
        } elseif ($this->scheme === "file") {
            // file is a special case where localhost is often represented by an empty authority
            $return .= '//';
        }
        $return .= $authority;

        // fixme for separator sanity
        $path = $this->getPath();
        if (! empty($path)) {
            // check if rootless
            if (substr($path, 0, 1) !== "/") {
                if (! empty($authority)) {
                    $return .= '/';
                } elseif ($this->scheme === 'file') {
                    // empty authority on file means localhost is authority.
                    $return .= '/';
                }
            } else {
                // path has least one slash
                if (empty($authority)) {
                    $path = preg_replace('/^\/+/', '/', $path);
                }
            }
        }
        $return .= $path;
        if (! empty($this->query)) {
            $return .= '?' . $this->query;
        }
        if (! empty($this->fragment)) {
            $return .= '#' . $this->fragment;
        }
        return $return;
    }//end __toString()


    /** End of PSR-7 UriInterface Public Functions */


    /**
     * Returns an instance with the specified GetQueryBuilder object.
     * HTTP/HTTPS scheme only.
     *
     * @param GetQueryBuilder $object The GetQueryBuilderObject.
     *
     * @return static A new instance with the specified query string.
     *
     * @throws \InvalidArgumentException for invalid query strings.
     */
    public function withGetQuery(GetQueryBuilder $object)
    {
        if ($this->scheme !== 'https' && $this->scheme !== 'http') {
            throw Exceptions\UriInvalidArgumentException::getQueryObjectWrongScheme($this->scheme);
        }
        $return = new static();

        $return->setUnicode($this->unicode);
        $return->setUserInfo($this->username, $this->userpass);
        $return->setHost($this->host);
        $return->setPort($this->port);
        $return->setPath($this->path);
        $return->setFragment($this->fragment);
        $return->setScheme($this->scheme);
        $return->setGetQuery($object);
        return $return;
    }//end withGetQuery()

    /**
     * Attempts to guess the scheme.
     *
     * @return string|bool The guess, or false if it could not guess a scheme.
     */
    public function guessScheme()
    {
        if (! empty($this->scheme)) {
            return $this->scheme;
        }
        if (empty($this->path)) {
            return false;
        }
        if ($this->emailTypePath) {
            return 'mailto';
        }
        // guess by port
        if (! empty($this->port)) {
            foreach ($this->registeredPorts as $key => $value) {
                if ($value === $this->port) {
                    if (ValidatePathByScheme::validatePath($key, $this->path)) {
                        return $key;
                    }
                }
            }
        }
        // do not include https, http, file, ftp, others with /path/to/whatever type paths
        $schemes = array('urn');
        // now make sure https is last to test this way
        $schemes[] = 'https';
        foreach ($schemes as $scheme) {
            if (ValidatePathByScheme::validatePath($scheme, $this->path)) {
                if (! empty($this->port) && $this->port !== 443) {
                    if ($scheme === 'https') {
                        $scheme = 'http';
                    }
                }
                return $scheme;
            }
        }
        return false;
    }//end guessScheme()

    /**
     * Parses a URI
     *
     * @param string $URI    The URI to parse into its components.
     * @param bool   $throw  Throw an exception on invalid input. Defaults to true.
     *
     * @return void
     *
     * @throws \InvalidArgumentException for invalid uri unless second parameter false.
     */
    public function parseURI(string $URI, bool $throw = true): void
    {
        // clear the existing properties
        $this->scheme = '';
        $this->username = '';
        $this->userpass = '';
        $this->host = '';
        $this->port = null;
        $this->path = '';
        $this->query = '';
        $this->fragment = '';
        $this->emailTypePath = false;
        if (! $this->extractScheme($URI) && $throw) {
            throw Exceptions\UriInvalidArgumentException::invalidUriScheme($this->throwArray);
        }
        $this->extractFragment($URI);
        if (! $this->extractQuery($URI) && $throw) {
            $scheme = $this->scheme;
            /**
             * False positive
             *
             * @psalm-suppress RedundantCondition
             */
            if (empty($scheme)) {
                $scheme = null;
            }
            $query = '';
            if (isset($this->throwArray[0]) && ! empty($this->throwArray[0])) {
                $query = $this->throwArray[0];
            }
            throw Exceptions\UriInvalidArgumentException::invalidUriQuery($query, $scheme);
        }
        $this->extractAuthority($URI, $throw);
        // what's left, if anything should be the path
        if (! $this->processPath($URI) && $throw) {
            $this->throwArray = array();
            $this->throwArray[] = $URI;
            if (! empty($this->scheme)) {
                $this->throwArray[] = $this->scheme;
            }
            throw Exceptions\UriInvalidArgumentException::invalidUriPath($this->throwArray);
        }
    }//end parseURI()

    /**
     * The constructor. Instantiates the class. No arguments.
     */
    public function __construct()
    {
    }//end __construct()
}//end class

?>