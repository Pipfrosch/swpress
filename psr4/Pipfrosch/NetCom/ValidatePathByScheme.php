<?php
declare(strict_types=1);

/**
 * This class provides static method for validating the path component of a RFC 3986 URI
 * according to the relevant scheme.
 *
 * @package    Netcom
 * @subpackage PSR-7
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 *
 * @see https://tools.ietf.org/html/rfc3986
 */

namespace Pipfrosch\NetCom;

/**
 * Static methods for path validation.
 */
class ValidatePathByScheme
{
    /**
     * Array of schemes to validator types
     *
     * @var array
     */
    protected static $schemeArray = array(
        'bitcoin' => 'bitcoin',
        'file'    => 'browserPath',
        'ftp'     => 'browserPath',
        'http'    => 'browserPath',
        'https'   => 'browserPath',
        'mailto'  => 'email',
        'urn'     => 'urn',
    );
    
    /**
     * Checks for generic path violations.
     * Currently always returns true.
     *
     * @param string $path The path to validate.
     *
     * @return true Always returns true.
     */
    protected static function genericPath(string $path): bool
    {
        return true;
    }//end genericPath()

    /**
     * Validate a path for bitcoin scheme. Please note this does NOT
     * validate the address, that is a TODO that will probably use an
     * external class if available.
     *
     * @param string $path The path to validate.
     *
     * @return bool True if valid, otherwise false.
     *
     * @see https://github.com/bitcoin/bips/blob/master/bip-0021.mediawiki
     */
    protected static function bitcoinPath(string $path): bool
    {
        // Bech32 not supported.
        $t = preg_match('/^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/', $path);
        if ((! is_int($t)) || ($t === 0)) {
            return false;
        }
        // TODO allow calling external class to validate address matches internal checksum
        return true;
    }//end bitcoinPath()

    /**
     * Validate a path for email address style path.
     *  Assumes unicode UTF8 already converted to ASCII
     *
     * @param string $path The path to validate.
     *
     * @return bool True if valid, otherwise false.
     */
    protected static function emailPath(string $path): bool
    {
        // multiple are allowed, delimited by a ,
        $multiArray = explode(',', $path);
        foreach ($multiArray as $address) {
            if (! filter_var($address, FILTER_VALIDATE_EMAIL)) {
                return false;
            }
        }
        return true;
    }//end emailPath()

    /**
     * Validate a path for typical browser protocols
     *
     * @param string $path The path to validate.
     *
     * @return bool True if valid, otherwise false.
     */
    

    /**
     * URN scheme methods.
     */

    /**
     * The IETF namespace
     *
     * @param string $NSS The NSS to validate.
     *
     * @return bool True if valid, otherwise false.
     */
    protected static function urnIetf(string $NSS): bool
    {
        $pos = strpos($NSS, ':');
        if (! is_int($pos)) {
            return false;
        }
        $subns = strtolower(substr($NSS, 0, $pos));
        $pos++;
        $remaining = substr($NSS, $pos);
        switch ($subns) {
            case 'rfc':
            case 'fyi':
            case 'std':
            case 'bcp':
                $rs = preg_match('/^[0-9]+$/', $remaining);
                if (is_int($rs) && $rs === 1) {
                    return true;
                }
                return false;
                break;
            case 'id':
            case 'mtg':
                $rs = preg_match('/^[0-9A-Z\-]+$/i', $remaining);
                if (is_int($rs) && $rs === 1) {
                    return true;
                }
                return false;
                break;
            case 'params':
                //$arr = explode(':', $remaining);
                // FIXME FUCME FIXME understand the hierarchy name rules
                if (! mb_detect_encoding($remaining, 'ASCII', true)) {
                    return false;
                }
                return true;
                break;
            default:
                return false;
        }
    }//end urnIetf()

    /**
     * The ISBN namespace
     *
     * @param string $NSS The NSS to validate.
     *
     * @return bool True if valid, otherwise false.
     */
    protected static function urnIsbn(string $NSS): bool
    {
        $isbnType = null;
        $NSS = preg_replace('/-/', '', $NSS);
        //test for 13 digit
        $t = preg_match('/^[0-9]{13,13}$/', $NSS);
        if (is_int($t) && $t === 1) {
            $isbnType = 13;
        } else {
            $t = preg_match('/^[0-9]{9,9}[0-9X]/', $NSS);
            if (is_int($t) && $t === 1) {
                $isbnType = 10;
            }
        }
        
        switch ($isbnType) {
            case 13:
                $validEAN = array('978', '979');
                $EAN = substr($NSS, 0, 3);
                if (! in_array($EAN, $validEAN)) {
                    return false;
                }
                //$reverse = strrev($NSS);
                $sum = 0;
                for ($i=0; $i<13; $i++) {
                    $digit = intval(substr($NSS, $i, 1), 10);
                    if ($i % 2 === 0) {
                        $sum = $sum + $digit;
                    } else {
                        $sum = $sum + ($digit * 3);
                    }
                }
                if ($sum % 10 === 0) {
                    return true;
                }
                break;
            case 10:
                $reverse = strrev($NSS);
                $FIRST = substr($reverse, 0, 1);
                if ($FIRST === "X") {
                    //there is no checksum
                    return true;
                }
                $sum = 0;
                for ($i=0; $i<10; $i++) {
                    $digit = intval(substr($reverse, $i, 1), 10);
                    $sum = $sum + (($i + 1) * $digit);
                }
                if ($sum % 11 === 0) {
                    return true;
                }
                break;
        }
        return false;
    }//end urnIsbn()


    /**
     * The UUID namespace
     *
     * @param string $NSS The NSS to validate.
     *
     * @return bool True if valid, otherwise false.
     */
    protected static function urnUuid(string $NSS): bool
    {
        $NSS = strtolower($NSS);
        $t = preg_match('/^[0-9a-f]{8,8}-[0-9a-f]{4,4}-[0-9a-f]{4,4}-[0-9a-f]{4,4}-[0-9a-f]{12,12}$/', $NSS);
        if (is_int($t) && $t === 1) {
            return true;
        }
        return false;
    }//end urnUuid()

    
    /**
     * Validate a URN path
     *
     * @param string $path The path to validate.
     *
     * @return bool True if valid, otherwise false.
     */
    protected static function urnPath(string $path): bool
    {
        //NID
        $pos = strpos($path, ':');
        if (! is_int($pos)) {
            return false;
        }
        $NID = strtolower(substr($path, 0, $pos));
        $pos++;
        $NSS = substr($path, $pos);
        // NID specific vaildation of the NSS
        switch ($NID) {
            case 'ietf':
                return self::urnIetf($NSS);
                break;
            case 'isbn':
                return self::urnIsbn($NSS);
                break;
            case 'uuid':
                return self::urnUuid($NSS);
                break;
            default:
                if (substr($NID, 0, 4) === "urn-") {
                    //make sure informal NID is valid
                    $match = preg_match('/^urn\-[1-9][0-9]*$/', $NID);
                    if (is_int($match) && $match === 1) {
                        return true;
                    }
                    return false;
                }
                return true;
        }
    }//end urnPath()


    
    /**
     * Determines whether or not this class can validate the specified scheme
     *
     * @param string $scheme The scheme being used.
     *
     * @return bool True if the class can validate paths for the scheme, otherwise
     *              false.
     */
    public static function checkCapability($scheme)
    {
        $scheme = trim($scheme);
        $scheme = strtolower($scheme);
        if (! isset(self::$schemeArray[$scheme])) {
            return false;
        }
        return true;
    }//end checkCapability()

    
    /**
     * The public function to validate a path by scheme.
     *
     * @param string $scheme The scheme being used.
     * @param string $path   The path to validate.
     *
     * @return bool True if valid, otherwise false. Always returns true if path is
     *              an empty string.
     */
    public static function validatePath($scheme, $path): bool
    {
        $path = trim($path);
        if (empty($path)) {
            return true;
        }
        $scheme = trim($scheme);
        $scheme = strtolower($scheme);
        if (! isset(self::$schemeArray[$scheme])) {
            //nothing we can do
            $typeOfScheme = 'generic';
        } else {
            $typeOfScheme = self::$schemeArray[$scheme];
        }
        switch ($typeOfScheme) {
            case 'bitcoin':
                return self::bitcoinPath($path);
                break;
            case 'email':
                return self::emailPath($path);
                break;
            case 'urn':
                return self::urnPath($path);
                break;
            default:
                return self::genericPath($path);
        }
    }//end validatePath()
}//end class

?>