<?php
declare(strict_types=1);

/**
 * Invalid Argument Exceptions
 *
 * @package Netcom
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\NetCom\Exceptions;

/**
 * Throws a \InvalidArgumentException exception.
 */
class UriInvalidArgumentException extends \InvalidArgumentException
{
    /**
     * Array of RFC numbers for scheme path definitions.
     *
     * @var array
     */
    protected static $schemePathDocumentation = array(
        'bitcoin'  => 'bip:21',
        'mailto'   => 'rfc:6068',
        'urn'      => 'rfc:8141',
        'urn:ietf' => 'rfc:6924',
        'urn:isbn' => 'iso:2108_2017',
        'urn:iso'  => 'rfc:5141',
        'urn:uuid' => 'rfc:4122'
    );

    /**
     * Filter special html characters.
     *
     * @param string $input The string to filter.
     *
     * @return string The filtered string.
     */
    protected static function specialCharacterFilter(string $input): string
    {
        $s = array();
        $r = array();
        $s[] = '/&/';
        $r[] = '&amp;';
        $s[] = '/</';
        $r[] = '&lt;';
        $s[] = '/>/';
        $r[] = '&gt;';
        return preg_replace($s, $r, $input);
    }//end specialCharacterFilter()

    /**
     * Creates a BIP reference.
     *
     * Unfortunately there is not a URN for BIP. While at this time there is only one
     * bitcoin related scheme, it is possible in the future there may be others that are
     * also described in a BIP document.
     *
     * @param int $bip The BIP number w/o padding.
     *
     * @return string The BIP string.
     */
    protected static function makeBIP(int $bip): string
    {
        $bip = (string) $bip;
        $bip = str_pad($bip, 4, "0", STR_PAD_LEFT);
        return "BIP " . $bip;
    }//end makeBIP()

    /**
     * Creates an ISO reference.
     *
     * At this time I do not yet understand how to generate an ISO URN
     * that is correct.
     *
     * @param string $iso The ISO reference as a string.
     *
     * @return string
     */
    protected static function makeISO(string $iso): string
    {
        $iso = preg_replace('/_/', ':', $iso);
        return "ISO " . $iso;
    }//end makeISO()

    /**
     * Creates a valid URN for an RFC
     *
     * @param int $rfc The RFC number.
     *
     * @return string The URN.
     */
    protected static function makeRFC(int $rfc): string
    {
        return 'urn:ietf:rfc:' . (string) $rfc;
    }//end makeRFC()

    /**
     * Exception message for an invalid URI scheme.
     *
     * @param string|array $error arguments.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidUriScheme($error)
    {
        $arr = array();
        $continue = true;
        if (is_string($error)) {
            $arr[] = $error;
            $continue = false;
        } elseif (empty($error)) {
            $continue = false;
        }
        if ($continue) {
            if (is_string($error[0])) {
                $arr[] = $error[0];
            } else {
                $continue = false;
            }
        }
        if ($continue && isset($error[1])) {
            if (is_string($error[1])) {
                $arr[] = $error[1];
            } else {
                $continue = false;
            }
        }
        if ($continue) {
            $docscheme = $arr[0];
            if ($docscheme === 'urn') {
                $testpath = $arr[1];
                $pos = strpos($testpath, ':');
                if (is_int($pos)) {
                    $testdoc = $docscheme . ':' . substr($testpath, 0, $pos);
                    if (isset(self::$schemePathDocumentation[$testdoc])) {
                        $docscheme = $testdoc;
                    }
                }
            }
            if (isset(self::$schemePathDocumentation[$docscheme])) {
                $arr[] =  self::$schemePathDocumentation[$docscheme];
            }
        }
        // whew
        $count = count($arr);
        switch ($count) {
            case 1:
                $scheme = (string) $arr[0];
                $scheme = self::specialCharacterFilter($scheme);
                $rfc = self::makeRFC(3986);
                return new self(sprintf(
                    'The specified scheme <code>%s</code> does not conform to <code>%s<code> section 3.1.',
                    $scheme,
                    $rfc
                ));
                break;
            case 2:
                $scheme = (string) $arr[0];
                $path = (string) $arr[1];
                $scheme = self::specialCharacterFilter($scheme);
                $path = self::specialCharacterFilter($path);
                return new self(sprintf(
                    'The specified scheme <code>%s</code> is not valid with the specified path <code>%s</code>.',
                    $scheme,
                    $path
                ));
                break;
            case 3:
                $scheme = (string) $arr[0];
                $path = (string) $arr[1];
                $docArray = explode(':', $arr[2]);
                list($doctype, $reference) = $docArray;
                $scheme = self::specialCharacterFilter($scheme);
                $path = self::specialCharacterFilter($path);

                switch ($doctype) {
                    case 'bip':
                        $arg = intval($reference, 10);
                        $uri = self::makeBIP($arg);
                        break;
                    case 'iso':
                        $arg = (string) $reference;
                        $uri = self::makeISO($arg);
                        break;
                    case 'rfc':
                        $arg = intval($reference, 10);
                        $uri = self::makeRFC($arg);
                        break;
                    default:
                        $uri = self::makeRFC(3986);
                }
                return new self(sprintf(
                    'The specified scheme <code>%s</code> is not valid with the specified path <code>%s</code>, see <code>%s</code>.',
                    $scheme,
                    $path,
                    $uri
                ));
                break;
            default:
                $rfc = self::makeRFC(3986);
                return new self(sprintf(
                    'The specified scheme is not valid. See <code>%s</code> section 3.1.',
                    $rfc
                ));
        }
    }//end invalidUriScheme()

    /**
     * Exception message for an invalid URI host.
     *
     * @param string $error argument.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidUriHost(string $error)
    {
        $host = self::specialCharacterFilter($error);
        $rfc = self::makeRFC(3986);
        return new self(sprintf(
            'The specified host <code>%s</code> is not a valid URI host. See <code>%s</code> section 3.2.2.',
            $host,
            $rfc
        ));
    }//end invalidUriHost()

    /**
     * Exception message for an invalid URI port.
     *
     * @param int|string $error argument.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidUriPort($error)
    {
        $arg = (string) $error;
        $arg = self::specialCharacterFilter($arg);
        return new self(sprintf(
            'The specified port <code>%s</code> is not a valid URI port. It should be an decimal integer greater than 0.',
            $arg
        ));
    }//end invalidUriPort()

    /**
     * Exception message for an invalid URI path.
     *
     * @param string|array $error arguments.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidUriPath($error)
    {
        $arr = array();
        $continue = true;
        if (is_string($error)) {
            $arr[] = $error;
            $continue = false;
        } elseif (empty($error)) {
            $continue = false;
        }
        if ($continue) {
            if (is_string($error[0])) {
                $arr[] = $error[0];
            } else {
                $continue = false;
            }
        }
        if ($continue && isset($error[1])) {
            if (is_string($error[1])) {
                $arr[] = $error[1];
            } else {
                $continue = false;
            }
        }
        if ($continue) {
            $docscheme = $arr[1];
            if ($docscheme === 'urn') {
                $testpath = $arr[0];
                $pos = strpos($testpath, ':');
                if (is_int($pos)) {
                    $testdoc = $docscheme . ':' . substr($testpath, 0, $pos);
                    if (isset(self::$schemePathDocumentation[$testdoc])) {
                        $docscheme = $testdoc;
                    }
                }
            }
            if (isset(self::$schemePathDocumentation[$docscheme])) {
                $arr[] = self::$schemePathDocumentation[$docscheme];
            }
        }
        // whew
        $count = count($arr);
        switch ($count) {
            case 1:
                $path = (string) $arr[0];
                $path = self::specialCharacterFilter($path);
                $rfc = self::makeRFC(3986);
                return new self(sprintf(
                    'The specified path <code>%s</code> does not conform to <code>%s<code> section 3.1.',
                    $path,
                    $rfc
                ));
                break;
            case 2:
                $path = (string) $arr[0];
                $scheme = (string) $arr[1];
                $path = self::specialCharacterFilter($path);
                $scheme = self::specialCharacterFilter($scheme);
                return new self(sprintf(
                    'The specified path <code>%s</code> is not valid with the specified scheme <code>%s</code>.',
                    $path,
                    $scheme
                ));
                break;
            case 3:
                $path = (string) $arr[0];
                $scheme = (string) $arr[1];
                $docArray = explode(':', $arr[2]);
                list($doctype, $reference) = $docArray;
                $path = self::specialCharacterFilter($path);
                $scheme = self::specialCharacterFilter($scheme);

                switch ($doctype) {
                    case 'bip':
                        $arg = intval($reference, 10);
                        $uri = self::makeBIP($arg);
                        break;
                    case 'iso':
                        $arg = (string) $reference;
                        $uri = self::makeISO($arg);
                        break;
                    case 'rfc':
                        $arg = intval($reference, 10);
                        $uri = self::makeRFC($arg);
                        break;
                    default:
                        $uri = self::makeRFC(3986);
                }
                return new self(sprintf(
                    'The specified path <code>%s</code> is not valid with the specified scheme <code>%s</code>, see <code>%s</code>.',
                    $path,
                    $scheme,
                    $uri
                ));
                break;
            default:
                $rfc = self::makeRFC(3986);
                return new self(sprintf(
                    'The specified path is not valid. See <code>%s</code> section 3.1.',
                    $rfc
                ));
        }
    }//end invalidUriPath()

    /**
     * Exception for an invalid query.
     *
     * @param string      $error  The provided query string.
     * @param null|string $scheme The provided scheme, or null.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidUriQuery(string $error, $scheme = null)
    {
        $error = self::specialCharacterFilter($error);
        $queryRules = 'unknown';
        if (! empty($scheme)) {
            $scheme = self::specialCharacterFilter($scheme);
        }
        if (! empty($scheme)) {
            $scheme = self::specialCharacterFilter($scheme);
            return new self(sprintf(
                'The specified query string <code>%s</code> is not a valid query string for the <code>%s</code> scheme.',
                $error,
                $scheme
            ));
        } else {
            return new self(sprintf(
                'The specified query string <code>%s</code> is not a valid query string.',
                $error
            ));
        }
    }//end invalidUriQuery()

    /**
     * Exception for new scheme does not match existing query.
     *
     * @param string $scheme The provided scheme.
     * @param string $query  The provided query.
     *
     * @return \InvalidArgumentException
     */
    public static function schemeDoesNotMatchQueryString(string $scheme, string $query)
    {
        $scheme = self::specialCharacterFilter($scheme);
        $query = self::specialCharacterFilter($query);
        return new self(sprintf(
            'The specified scheme <code>%s</code> is not valid with the existing query <code>%s</code>.',
            $scheme,
            $query
        ));
    }//end schemeDoesNotMatchQueryString()

    /**
     * Exception for GetQueryObject with unsupported scheme.
     *
     * @param string $error The provided scheme.
     *
     * @return \InvalidArgumentException
     */
    public static function getQueryObjectWrongScheme(string $error)
    {
        $error = self::specialCharacterFilter($error);
        return new self(sprintf(
            'A GetQueryBuilder object can not be used with the <code>%s</code> scheme.',
            $error
        ));
    }//end getQueryObjectWrongScheme()
}//end class

?>