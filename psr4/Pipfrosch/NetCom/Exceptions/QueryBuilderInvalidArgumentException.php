<?php
declare(strict_types=1);

/**
 * Invalid Argument Exceptions
 *
 * @package Netcom
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace Pipfrosch\NetCom\Exceptions;

/**
 * Throws a \InvalidArgumentException exception.
 */
class QueryBuilderInvalidArgumentException extends \InvalidArgumentException
{

    /**
     * Filter special html characters.
     *
     * @param string $input The string to filter.
     *
     * @return string The filtered string.
     */
    protected static function specialCharacterFilter(string $input): string
    {
        $s = array();
        $r = array();
        $s[] = '/&/';
        $r[] = '&amp;';
        $s[] = '/</';
        $r[] = '&lt;';
        $s[] = '/>/';
        $r[] = '&gt;';
        return preg_replace($s, $r, $input);
    }//end specialCharacterFilter()

    /**
     * Exception message for an invalid mode.
     *
     * @param string $error The invalid argument.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidMode(string $error)
    {
        $error = self::specialCharacterFilter($error);
        $modes = 'FIRST, LAST, ARRAY, OPTIONARRAY, and EXCEPTION';
        return new self(sprintf(
            'The specified mode <code>%s</code> is not a valid mode. The valid modes are <code>%s</code>.',
            $error,
            $modes
        ));
    }//end invalidMode()

    /**
     * Exception message when a key is empty.
     *
     * @return \InvalidArgumentException
     */
    public static function emptyKey()
    {
        return new self('A <code>GET</code> parameter key may not be empty.');
    }//end emptyKey()

    /**
     * Exception message when a key in not a valid key.
     *
     * @param string $error The invalid argument.
     *
     * @return \InvalidArgumentException
     */
    public static function invalidKey(string $error)
    {
        $error = self::specialCharacterFilter($error);
        return new self(sprintf(
            'The specified key <code>%s</code> is not a valid key for GET parameters.',
            $error
        ));
    }//end invalidKey()

    /**
     * Exception message for duplicate key.
     *
     * @param string $error The invalid argument.
     *
     * @return \InvalidArgumentException
     */
    public static function duplicateKey($error)
    {
        $error = self::specialCharacterFilter($error);
        return new self(sprintf(
            'The specified key <code>%s</code> already has a value assigned to it.',
            $error
        ));
    }//end duplicateKey()
}//end class

?>