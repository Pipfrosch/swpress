<?php
declare(strict_types=1);

/**
 * This class creates GET queries for HTTP/HTTPS URIs
 *
 * Unit tests in file tests/NetCom/GetQueryBuilderTest.php
 *
 * @package    Netcom
 * @subpackage PSR-7
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 *
 * @see https://tools.ietf.org/html/rfc3986
 * @see https://www.iana.org/assignments/uri-schemes/uri-schemes.xhtml
 */

namespace Pipfrosch\NetCom;

/**
 * A class to build GET queries
 */
class GetQueryBuilder
{
    /**
     * @var string The mode. Must be one of 'FIRST', 'LAST', 'ARRAY', 'OPTIONARRAY', 'EXCEPTION'.
     */
    protected $mode = 'OPTIONARRAY';

    /**
     * @var string The separator. For HTTP queries this really should be & but for compatibility
     *             with the `_http_build_query` function of WordPress it can be altered.
     */
    protected $sep = '&';

    /**
     * @var bool Whether or not to use a + instead of %20 to encode space character.
     */
    protected $plusencode = false;

    /**
     * @var bool Serialize arrays with single key and brackets for values.
     */
    protected $bracketSerialize = false;

    /**
     * @var array Array of key=value pairs
     */
    protected $parameters = array();

    /**
     * Deserialize a value
     *
     * @param string $value The value to deserialize.
     *
     * @return string|array
     */
    protected function deserialize($value)
    {
        if (preg_match('/^\[[^\[\]]*\]$/', $value) === 1) {
            $value = ltrim($value, '[');
            $value = rtrim($value, ']');
            return explode(',', $value);
        }
        return $value;
    }//end deserialize()

    /**
     * Verify the GET key will not be altered by URI encoding.
     *
     * @param string $key The key to check.
     *
     * @return bool True if valid key, otherwise false.
     */
    protected function validateGetKey(string $key):bool
    {
        $test = UrlEncode::queryEncode($key);
        if ($test !== $key) {
            return false;
        }
        $test = rawurldecode($key);
        if ($test !== $key) {
            return false;
        }
        // replace these with substr_match
        if (substr_count($key, '=') > 0) {
            return false;
        }
        if (substr_count($key, '&') > 0) {
            return false;
        }
        if (preg_match('/\[[^\[\]]*\]$/', $key) === 1) {
            if (substr_count($key, '[') > 1) {
                return false;
            }
            if (substr_count($key, ']') > 1) {
                return false;
            }
        } else {
            if (substr_count($key, '[') > 0) {
                return false;
            }
            if (substr_count($key, ']') > 0) {
                return false;
            }
        }
        if (substr_count($key, '?') > 0) {
            return false;
        }
        return true;
    }//end validateGetKey()

    /**
     * Takes an array of Key=Value pairs to use as GET parameters.
     *
     * @param array $pairs Array of Key=Value pairs.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function queryFromArray(array $pairs): void
    {
        foreach ($pairs as $key => $value) {
            if (is_string($value)) {
                $value = $this->deserialize($value);
            }
            if ($this->plusencode && is_string($value)) {
                $value = preg_replace('/%20/', ' ', $value);
            }
            if (empty($key)) {
                throw Exceptions\QueryBuilderInvalidArgumentException::emptyKey();
            }
            if (! $this->validateGetKey($key)) {
                throw Exceptions\QueryBuilderInvalidArgumentException::invalidKey($key);
            }
            if ($value === '') {
                $value = $key;
            }
            if (! isset($this->parameters[$key])) {
                $this->parameters[$key] = $value;
            } else {
                switch ($this->mode) {
                    case 'FIRST':
                        break;
                    case 'LAST':
                        $this->parameters[$key] = $value;
                        break;
                    case 'ARRAY':
                        if (! is_array($this->parameters[$key])) {
                            $foo = array($this->parameters[$key]);
                            $this->parameters[$key] = $foo;
                        }
                        if (is_array($value)) {
                            foreach ($value as $part) {
                                $this->parameters[$key][] = $part;
                            }
                        } else {
                            $this->parameters[$key][] = $value;
                        }
                        break;
                    case 'OPTIONARRAY':
                        if (is_array($this->parameters[$key])) {
                            if (is_array($value)) {
                                foreach ($value as $part) {
                                    $this->parameters[$key][] = $part;
                                }
                            } else {
                                $this->parameters[$key][] = $value;
                            }
                        }
                        break;
                    default:
                        throw Exceptions\QueryBuilderInvalidArgumentException::duplicateKey($key);
                        break;
                }
            }
        }
    }//end queryFromArray()

    /**
     * Add a key to the key=value store.
     *
     * @param string       $key   The key to add.
     * @param array|string $value The value to add.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function addKeyVal(string $key, $value): void
    {
        if (empty($key)) {
            throw Exceptions\QueryBuilderInvalidArgumentException::emptyKey();
        } else {
            if ($value === '') {
                $value = $key;
            }
            $arr = array($key => $value);
            $this->queryFromArray($arr);
        }
    }//end addKeyVal()

    /**
     * Removes a key from the key=value store.
     *
     * @param string $key The key to remove.
     *
     * @return void
     */
    public function removeKey(string $key): void
    {
        if (empty($key) || (! isset($this->parameters[$key]))) {
            return;
        }
        $delete = array($key => true);
        $arr = array_diff_key($this->parameters, $delete);
        $this->parameters = $arr;
    }//end removeKey()

    /**
     * Sets the mode for duplicate keys. There are five legal modes:
     *   FIRST: ------- The first key takes priority, additional key=value pairs with the same key
     *                  will be ignored.
     *   LAST: -------- The last key takes priority, prior key=value pairs with the same key will
     *                  be replaced.
     *   ARRAY: ------- When there is more than one key=value pair with the same key, they value
     *                  will be an array of the values.
     *   OPTIONARRAY -- When there is more than one key=value pair with the same key AND the first
     *                  had an array for the value, then additional values will be added to the
     *                  array. Otherwise additional values are ignored.
     *   EXCEPTION ---- Throw an exception when an attempt is made to add a key=value pair for a
     *                  key that already exists.
     *
     * @param string $mode The mode to set. Default mode is OPTIONARRAY.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function setMode(string $mode = 'OPTIONARRAY'): void
    {
        $validModes = array('FIRST','LAST','ARRAY','OPTIONARRAY','EXCEPTION');
        $mode = trim(strtoupper($mode));
        if (in_array($mode, $validModes)) {
            $this->mode = $mode;
            return;
        }
        throw Exceptions\QueryBuilderInvalidArgumentException::invalidMode($mode);
    }//end setMode()

    /**
     * If you use this, you are doing it wrong. I can not guarantee it will still be here in future
     * versions, this method only exists for compatibility with the WordPress `_http_build_query()`
     * function.
     *
     * @param null|string $arg_separator The separator between key=value pairs.
     *
     * @return void
     */
    public function setSeparator($arg_separator = null): void
    {
        if (is_null($arg_separator)) {
            $this->sep = ini_get('arg_separator.output');
            return;
        }
        if (! empty($arg_separator)) {
            $this->sep = $arg_separator;
        }
    }//end setSeparator()

    /**
     * Returns the parameters as a serialized query string.
     *
     * @return string The GET query.
     */
    public function __toString():string
    {
        $arr = array();
        $queryString = '';
        foreach ($this->parameters as $key => $value) {
            if (is_array($value)) {
                if ($this->bracketSerialize && (! empty($value))) {
                    $string = '[' . implode(',', $value) . ']';
                    if ($this->plusencode) {
                        $string = preg_replace('/\s/', '+', $string);
                    }
                    $string = UrlEncode::queryEncode($string);
                    $arr[] = $key . '=' . $string;
                } else {
                    foreach ($value as $string) {
                        $string = (string) $string;
                        if ($this->plusencode) {
                            $string = preg_replace('/\s/', '+', $string);
                        }
                        $string = UrlEncode::queryEncode($string);
                        $arr[] = $key . '=' . $string;
                    }
                }
            } else {
                $string = (string) $value;
                if ($this->plusencode) {
                    $string = preg_replace('/\s/', '+', $string);
                }
                $string = UrlEncode::queryEncode($string);
                $arr[] = $key . '=' . $string;
            }
        }
        asort($arr, SORT_STRING);
        $newArray = array_unique($arr);
        return implode($this->sep, $newArray);
    }//end __toString()

    /**
     * Wrapper to __toString() function
     *
     * @return string The GET query.
     */
    public function getQueryString():string
    {
        return $this->__toString();
    }//end getQueryString()

    /**
     * The constructor function.
     *
     * @see https://www.w3.org/TR/html401/interact/forms.html#h-17.13.4.1 for plusencode. The
     *   URI specification says to use %20 but for HTTP and HTTPS many use the W3C spec instead
     *   of the URI RFC even though an HTTP/HTTPS URL is a URI.
     *
     * @param bool $plusencode       Whether or not to use a + instead of %20 to encode spaces.
     *                               Defaults to false.
     * @param bool $bracketSerialize Serialize multiple values for single key by putting the
     *                               values in comma delimited brackets. Defaults to false.
     */
    public function __construct(bool $plusencode = false, bool $bracketSerialize = false)
    {
        $this->plusencode = $plusencode;
        $this->bracketSerialize = $bracketSerialize;
    }//end __construct()
}//end class

?>