<?php
declare(strict_types=1);

/**
 * A wrapper class for the PSR-16 cache implementation. This wrapper catches the exceptions
 * that PSR-16 implementations are required to throw in certain circumstances and logs them
 * to the error log, returning the default return in those cases, so using this wrapper does
 * not require any exception handling code.
 *
 * @package SWPress
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace SWPress\Core;

/**
 * A wrapper class for the PSR-16 cache implementation.
 */
class CacheWrapper
{
    /**
     * Object for the actual cache engine
     *
     * @var \Psr\SimpleCache\CacheInterface
     */
    protected $engine;

    /**
     * Wrapper for get
     *
     * @param string $key     The unique key of this item in the cache.
     * @param mixed  $default Default value to return if the key does not exist.
     *
     * @return mixed The value of the item from the cache, or $default in case of cache miss.
     */
    public function get(string $key, $default = null)
    {
        try {
            $return = $this->engine->get($key, $default);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return $default;
        }
        return $return;
    }//end get()

    /**
     * Wrapper for set
     *
     * @param string                 $key   The key of the item to store.
     * @param mixed                  $value The value of the item to store, must be serializable.
     * @param null|int|\DateInterval $ttl   Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     */
    public function set(string $key, $value, $ttl = null): bool
    {
        try {
            $return = $this->engine->set($key, $value, $ttl);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
        return $return;
    }//end set()

    /**
     * Wrapper for delete()
     *
     * @param string $key The unique cache key of the item to delete.
     *
     * @return bool True if the item was successfully removed. False if there was an error.
     */
    public function delete(string $key): bool
    {
        try {
            $return = $this->engine->delete($key);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
        return $return;
    }//end delete()

    /**
     * Wrapper for clear()
     *
     * @return bool True on success and false on failure.
     */
    public function clear(): bool
    {
        return $this->engine->clear();
    }//end clear()

    /**
     * Wrapper for getMultiple()
     *
     * @param iterable $keys    A list of keys that can obtained in a single operation.
     * @param mixed    $default Default value to return for keys that do not exist.
     *
     * @return iterable A list of key => value pairs. Cache keys that do not exist or are
     *                  stale will have $default as value.
     */
    public function getMultiple($keys, $default = null)
    {
        try {
            $return = $this->engine->getMultiple($keys, $default);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return $default;
        }
        return $return;
    }//end getMultiple()

    /**
     * Wrapper for setMultiple()
     *
     * @param iterable               $values A list of key => value pairs for a multiple-set operation.
     * @param null|int|\DateInterval $ttl    Optional. The TTL value of this item. If no value is sent and
     *                                       the driver supports TTL then the library may set a default value
     *                                       for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     */
    public function setMultiple(iterable $values, $ttl = null): bool
    {
        try {
            $return = $this->engine->setMultiple($values, $ttl);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
        return $return;
    }//end setMultiple()

    /**
     * Wrapper for deleteMultiple
     *
     * @param iterable $keys A list of string-based keys to be deleted.
     *
     * @return bool True if the items were successfully removed. False if there was an error.
     */
    public function deleteMultiple(iterable $keys): bool
    {
        try {
            $return = $this->engine->deleteMultiple($keys);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
        return $return;
    }//end deleteMultiple()

    /**
     * Wrapper for has
     *
     * @param string $key The cache item key.
     *
     * @return bool Whether or not the key has a value cached.
     */
    public function has(string $key): bool
    {
        try {
            $return = $this->engine->has($key);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
        return $return;
    }//end has()

    /**
     * Class constructor. Determines the cache engine to use and sets it.
     */
    public function __construct()
    {
        $cryptokey = null;
        if (defined('SWP_CRYPTOKEY') && is_string(SWP_CRYPTOKEY)) {
            //verify it is a 32-bit key
            if (ctype_xdigit(SWP_CRYPTOKEY)) {
                $len = strlen(SWP_CRYPTOKEY);
                if ($len === 64) {
                    $cryptokey = SWP_CRYPTOKEY;
                }
            }
        }
        if (defined('SWP_CACHE_PREFIX') && is_string(SWP_CACHE_PREFIX) && strlen(SWP_CACHE_PREFIX) > 3) {
            $webappPrefix = SWP_CACHE_PREFIX;
        } else {
            $webappPrefix = 'SWPRESS';
            if (defined('SWP_TABLE_PREFIX') && is_string(SWP_TABLE_PREFIX)) {
                $webappPrefix .= SWP_TABLE_PREFIX;
            }
            if (defined('DB_NAME') && is_string(DB_NAME)) {
                $webappPrefix .= DB_NAME;
            }
        }
        $webappPrefix = strtoupper($webappPrefix);
        $webappPrefix = preg_replace('/[^A-Z0-9]/', '', $webappPrefix);
        // the salt
        $salt = null;
        if (defined('SWP_CACHE_SALT') && is_string(CACHE_SALT)) {
            $salt = SWP_CACHE_SALT;
        } elseif (defined('NONCE_SALT') && is_string(NONCE_SALT)) {
            if (defined('AUTH_SALT') && is_string(AUTH_SALT)) {
                $salt = md5(AUTH_SALT . NONCE_SALT);
            } else {
                // should never happen but...
                $salt = hash('sha256', AUTH_SALT);
            }
        }
        // check for redis support
        if (defined('SWP_CACHE_ENGINE') && SWP_CACHE_ENGINE === 'REDIS') {
            if (class_exists('\Redis')) {
                // define the redis connection
                $port = 6379;
                if (defined('REDIS_PORT')) {
                    $port = intval(REDIS_PORT, 10);
                }
                $host = 'localhost';
                if (defined('REDIS_HOST') && is_string(REDIS_HOST)) {
                    $host = REDIS_HOST;
                }
                $pass = null;
                if (defined('REDIS_PASS') && is_string(REDIS_PASS)) {
                    $pass = REDIS_PASS;
                }
                $connected = true;
                $authenticated = true;
                $redis = new \Redis();
                try {
                    $redis->connect($host, $port);
                } catch (\Exception $e) {
                    $connected = false;
                    error_log($e->getMessage());
                }
                if ($connected && ! is_null($pass)) {
                    $authenticated = $redis->auth($pass);
                }
                if ($connected && $authenticated) {
                    if (! empty($cryptokey)) {
                        $this->engine = new \Pipfrosch\SimpleCache\SimpleCacheRedisSodium($redis, $cryptokey, $webappPrefix, $salt, true);
                    } else {
                        $this->engine = new \Pipfrosch\SimpleCache\SimpleCacheRedis($redis, $webappPrefix, $salt, true);
                    }
                    return;
                }
            }
        }
        // still here? check for APCu support and use it if applicable
        $test = ini_get('apc.enable_cli');
        $test = (int)$test;
        if ($test === 1) {
            if (! empty($cryptokey)) {
                $this->engine = new \Pipfrosch\SimpleCache\SimpleCacheAPCuSodium($cryptokey, $webappPrefix, $salt, true);
            } else {
                $this->engine = new \Pipfrosch\SimpleCache\SimpleCacheAPCu($webappPrefix, $salt, true);
            }
            return;
        }
        // use the null engine
        $this->engine = new \Pipfrosch\SimpleCache\NullCache();
    }//end __construct()
}//end class

?>