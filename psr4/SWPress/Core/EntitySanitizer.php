<?php
declare(strict_types=1);

/**
 * Misc static methods for dealing with XML/HTML entities
 *
 * @package SWPress
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

namespace SWPress\Core;

/**
 * This class contains static methods for working with XML entities
 */
class EntitySanitizer
{
    /**
     * HTML 4.01 (and earlier) named entities to numbered equivalents
     *
     * @see https://www.freeformatter.com/html-entities.html
     *
     * @var array
     */
    public static $HtmlEntityMap = array(
        '&quot;'     =>   '&#34;',
        '&amp;'      =>   '&#38;',
        '&apos;'     =>   '&#39;',
        '&lt;'       =>   '&#60;',
        '&gt;'       =>   '&#62;',
        '&Agrave;'   =>  '&#192;',
        '&Aacute;'   =>  '&#193;',
        '&Acirc;'    =>  '&#194;',
        '&Atilde;'   =>  '&#195;',
        '&Auml;'     =>  '&#196;',
        '&Aring;'    =>  '&#197;',
        '&AElig;'    =>  '&#198;',
        '&Ccedil;'   =>  '&#199;',
        '&Egrave;'   =>  '&#200;',
        '&Eacute;'   =>  '&#201;',
        '&Ecirc;'    =>  '&#202;',
        '&Euml;'     =>  '&#203;',
        '&Igrave;'   =>  '&#204;',
        '&Iacute;'   =>  '&#205;',
        '&Icirc;'    =>  '&#206;',
        '&Iuml;'     =>  '&#207;',
        '&ETH;'      =>  '&#208;',
        '&Ntilde;'   =>  '&#209;',
        '&Ograve;'   =>  '&#210;',
        '&Oacute;'   =>  '&#211;',
        '&Ocirc;'    =>  '&#212;',
        '&Otilde;'   =>  '&#213;',
        '&Ouml;'     =>  '&#214;',
        '&Oslash;'   =>  '&#216;',
        '&Ugrave;'   =>  '&#217;',
        '&Uacute;'   =>  '&#218;',
        '&Ucirc;'    =>  '&#219;',
        '&Uuml;'     =>  '&#220;',
        '&Yacute;'   =>  '&#221;',
        '&THORN;'    =>  '&#222;',
        '&szlig;'    =>  '&#223;',
        '&agrave;'   =>  '&#224;',
        '&aacute;'   =>  '&#225;',
        '&acirc;'    =>  '&#226;',
        '&atilde;'   =>  '&#227;',
        '&auml;'     =>  '&#228;',
        '&aring;'    =>  '&#229;',
        '&aelig;'    =>  '&#230;',
        '&ccedil;'   =>  '&#231;',
        '&egrave;'   =>  '&#232;',
        '&eacute;'   =>  '&#233;',
        '&ecirc;'    =>  '&#234;',
        '&euml;'     =>  '&#235;',
        '&igrave;'   =>  '&#236;',
        '&iacute;'   =>  '&#237;',
        '&icirc;'    =>  '&#238;',
        '&iuml;'     =>  '&#239;',
        '&eth;'      =>  '&#240;',
        '&ntilde;'   =>  '&#241;',
        '&ograve;'   =>  '&#242;',
        '&oacute;'   =>  '&#243;',
        '&ocirc;'    =>  '&#244;',
        '&otilde;'   =>  '&#245;',
        '&ouml;'     =>  '&#246;',
        '&oslash;'   =>  '&#248;',
        '&ugrave;'   =>  '&#249;',
        '&uacute;'   =>  '&#250;',
        '&ucirc;'    =>  '&#251;',
        '&uuml;'     =>  '&#252;',
        '&yacute;'   =>  '&#253;',
        '&thorn;'    =>  '&#254;',
        '&yuml;'     =>  '&#255;',
        '&nbsp;'     =>  '&#160;',
        '&iexcl;'    =>  '&#161;',
        '&cent;'     =>  '&#162;',
        '&pound;'    =>  '&#163;',
        '&curren;'   =>  '&#164;',
        '&yen;'      =>  '&#165;',
        '&brvbar;'   =>  '&#166;',
        '&sect;'     =>  '&#167;',
        '&uml;'      =>  '&#168;',
        '&copy;'     =>  '&#169;',
        '&ordf;'     =>  '&#170;',
        '&laquo;'    =>  '&#171;',
        '&not;'      =>  '&#172;',
        '&shy;'      =>  '&#173;',
        '&reg;'      =>  '&#174;',
        '&macr;'     =>  '&#175;',
        '&deg;'      =>  '&#176;',
        '&plusmn;'   =>  '&#177;',
        '&sup2;'     =>  '&#178;',
        '&sup3;'     =>  '&#179;',
        '&acute;'    =>  '&#180;',
        '&micro;'    =>  '&#181;',
        '&para;'     =>  '&#182;',
        '&cedil;'    =>  '&#184;',
        '&sup1;'     =>  '&#185;',
        '&ordm;'     =>  '&#186;',
        '&raquo;'    =>  '&#187;',
        '&frac14;'   =>  '&#188;',
        '&frac12;'   =>  '&#189;',
        '&frac34;'   =>  '&#190;',
        '&iquest;'   =>  '&#191;',
        '&times;'    =>  '&#215;',
        '&divide;'   =>  '&#247;',
        '&forall;'   => '&#8704;',
        '&part;'     => '&#8706;',
        '&exist;'    => '&#8707;',
        '&empty;'    => '&#8709;',
        '&nabla;'    => '&#8711;',
        '&isin;'     => '&#8712;',
        '&notin;'    => '&#8713;',
        '&ni;'       => '&#8715;',
        '&prod;'     => '&#8719;',
        '&sum;'      => '&#8721;',
        '&minus;'    => '&#8722;',
        '&lowast;'   => '&#8727;',
        '&radic;'    => '&#8730;',
        '&prop;'     => '&#8733;',
        '&infin;'    => '&#8734;',
        '&ang;'      => '&#8736;',
        '&and;'      => '&#8743;',
        '&or;'       => '&#8744;',
        '&cap;'      => '&#8745;',
        '&cup;'      => '&#8746;',
        '&int;'      => '&#8747;',
        '&there4;'   => '&#8756;',
        '&sim;'      => '&#8764;',
        '&cong;'     => '&#8773;',
        '&asymp;'    => '&#8776;',
        '&ne;'       => '&#8800;',
        '&equiv;'    => '&#8801;',
        '&le;'       => '&#8804;',
        '&ge;'       => '&#8805;',
        '&sub;'      => '&#8834;',
        '&sup;'      => '&#8835;',
        '&nsub;'     => '&#8836;',
        '&sube;'     => '&#8838;',
        '&supe;'     => '&#8839;',
        '&oplus;'    => '&#8853;',
        '&otimes;'   => '&#8855;',
        '&perp;'     => '&#8869;',
        '&sdot;'     => '&#8901;',
        '&Alpha;'    =>  '&#913;',
        '&Beta;'     =>  '&#914;',
        '&Gamma;'    =>  '&#915;',
        '&Delta;'    =>  '&#916;',
        '&Epsilon;'  =>  '&#917;',
        '&Zeta;'     =>  '&#918;',
        '&Eta;'      =>  '&#919;',
        '&Theta;'    =>  '&#920;',
        '&Iota;'     =>  '&#921;',
        '&Kappa;'    =>  '&#922;',
        '&Lambda;'   =>  '&#923;',
        '&Mu;'       =>  '&#924;',
        '&Nu;'       =>  '&#925;',
        '&Xi;'       =>  '&#926;',
        '&Omicron;'  =>  '&#927;',
        '&Pi;'       =>  '&#928;',
        '&Rho;'      =>  '&#929;',
        '&Sigma;'    =>  '&#931;',
        '&Tau;'      =>  '&#932;',
        '&Upsilon;'  =>  '&#933;',
        '&Phi;'      =>  '&#934;',
        '&Chi;'      =>  '&#935;',
        '&Psi;'      =>  '&#936;',
        '&Omega;'    =>  '&#937;',
        '&alpha;'    =>  '&#945;',
        '&beta;'     =>  '&#946;',
        '&gamma;'    =>  '&#947;',
        '&delta;'    =>  '&#948;',
        '&epsilon;'  =>  '&#949;',
        '&zeta;'     =>  '&#950;',
        '&eta;'      =>  '&#951;',
        '&theta;'    =>  '&#952;',
        '&iota;'     =>  '&#953;',
        '&kappa;'    =>  '&#954;',
        '&lambda;'   =>  '&#955;',
        '&mu;'       =>  '&#956;',
        '&nu;'       =>  '&#957;',
        '&xi;'       =>  '&#958;',
        '&omicron;'  =>  '&#959;',
        '&pi;'       =>  '&#960;',
        '&rho;'      =>  '&#961;',
        '&sigmaf;'   =>  '&#962;',
        '&sigma;'    =>  '&#963;',
        '&tau;'      =>  '&#964;',
        '&upsilon;'  =>  '&#965;',
        '&phi;'      =>  '&#966;',
        '&chi;'      =>  '&#967;',
        '&psi;'      =>  '&#968;',
        '&omega;'    =>  '&#969;',
        '&thetasym;' =>  '&#977;',
        '&upsih;'    =>  '&#978;',
        '&piv;'      =>  '&#982;',
        '&OElig;'    =>  '&#338;',
        '&oelig;'    =>  '&#339;',
        '&Scaron;'   =>  '&#352;',
        '&scaron;'   =>  '&#353;',
        '&Yuml;'     =>  '&#376;',
        '&fnof;'     =>  '&#402;',
        '&circ;'     =>  '&#710;',
        '&tilde;'    =>  '&#732;',
        '&ensp;'     => '&#8194;',
        '&emsp;'     => '&#8195;',
        '&thinsp;'   => '&#8201;',
        '&zwnj;'     => '&#8204;',
        '&zwj;'      => '&#8205;',
        '&lrm;'      => '&#8206;',
        '&rlm;'      => '&#8207;',
        '&ndash;'    => '&#8211;',
        '&mdash;'    => '&#8212;',
        '&lsquo;'    => '&#8216;',
        '&rsquo;'    => '&#8217;',
        '&sbquo;'    => '&#8218;',
        '&ldquo;'    => '&#8220;',
        '&rdquo;'    => '&#8221;',
        '&bdquo;'    => '&#8222;',
        '&dagger;'   => '&#8224;',
        '&Dagger;'   => '&#8225;',
        '&bull;'     => '&#8226;',
        '&hellip;'   => '&#8230;',
        '&permil;'   => '&#8240;',
        '&prime;'    => '&#8242;',
        '&Prime;'    => '&#8243;',
        '&lsaquo;'   => '&#8249;',
        '&rsaquo;'   => '&#8250;',
        '&oline;'    => '&#8254;',
        '&euro;'     => '&#8364;',
        '&trade;'    => '&#8482;',
        '&larr;'     => '&#8592;',
        '&uarr;'     => '&#8593;',
        '&rarr;'     => '&#8594;',
        '&darr;'     => '&#8595;',
        '&harr;'     => '&#8596;',
        '&crarr;'    => '&#8629;',
        '&lceil;'    => '&#8968;',
        '&rceil;'    => '&#8969;',
        '&lfloor;'   => '&#8970;',
        '&rfloor;'   => '&#8971;',
        '&loz;'      => '&#9674;',
        '&spades;'   => '&#9824;',
        '&clubs;'    => '&#9827;',
        '&hearts;'   => '&#9829;',
        '&diams;'    => '&#9830;'
    );

    /**
     * Encode an ampersand in a string with numbered entities already.
     *
     * @param string $string The string to encode.
     *
     * @return string The encoded string.
     */
    protected static function encodeAnAmpersand(string $string): string
    {
        $string = preg_replace_callback(
            '/\&#[0-9]+;/',
            /**
             * Anonymous function for callback
             *
             * @param array Array of match.
             *
             * @return string The replacement.
             */
            function (array $m):string {
                $entity = rtrim($m[0], ';');
                $entity = substr($entity, 2);
                $return = '@@@@#@@@@' . $entity . ';';
                return $return;
            },
            $string
        );
        $string = preg_replace_callback(
            '/\&#x[A-F0-9]+;/',
            /**
             * Anonymous function for callback
             *
             * @param array Array of match.
             *
             * @return string The replacement.
             */
            function (array $m):string {
                $entity = rtrim($m[0], ';');
                $entity = substr($entity, 3);
                $return = '@@@@#@@@@x' . $entity . ';';
                return $return;
            },
            $string
        );
        $string = preg_replace('/\&/', '&amp;', $string);
        $string = preg_replace('/@@@@#@@@@/', '&#', $string);
        return $string;
    }//end encodeAnAmpersand()

    /**
     * Mostly intended for debugging but use it if you need it.
     *
     * @return array The HTML Named Entity to Numbered Entity array.
     */
    public static function getHtmlEntityMap()
    {
        return self::$HtmlEntityMap;
    }//end getHtmlEntityMap()

    /**
     * Converts named entities except for the five that are part of XML standard to their
     * numbered entity equivalents that always work in XML.
     *
     * @param string $string The string with entities to convert.
     * @param string $scheme Optional. The named entity scheme to use. Defaults to HTML.
     *                       Future entity schemes will be added later (e.g. MathML and
     *                       ISO and HTML5).
     *
     * @return string The string with all named entities (except the safe five) converted
     *                to numbered equivalents.
     */
    public static function namedToNumbered(string $string, string $scheme = 'html'): string
    {
        $scheme = strtolower(trim($scheme));
        $s = array();
        $r = array();
        switch ($scheme) {
            default:
                foreach (self::$HtmlEntityMap as $key => $value) {
                    $s[] = '/' . $key . '/';
                    $r[] = $value;
                }
        }
        return preg_replace($s, $r, $string);
    }//end namedToNumbered()

    /**
     * Normalize hex entities
     *
     * @param string $string The string to normalize entities in.
     *
     * @return string The string with normalized entities.
     */
    public static function normalizeNumbereredEntities(string $string): string
    {
        $string = preg_replace_callback(
            '/\&#[Xx][A-Fa-f0-9]+;/',
            /**
             * Anonymous function for callback
             *
             * @param array Array of match.
             *
             * @return string The normalized replacement.
             */
            function (array $m):string {
                $entity = rtrim($m[0], ';');
                $entity = substr($entity, 3);
                $entity = strtoupper($entity);
                $entity = ltrim($entity, '0');
                $return = '&#x' . $entity . ';';
                return $return;
            },
            $string
        );
        $string = preg_replace_callback(
            '/\&#[0-9]+;/',
            /**
             * Anonymous function for callback
             *
             * @param array Array of match.
             *
             * @return string The normalized replacement.
             */
            function (array $m):string {
                $entity = rtrim($m[0], ';');
                $entity = substr($entity, 2);
                $entity = ltrim($entity, '0');
                $return = '&#' . $entity . ';';
                return $return;
            },
            $string
        );
        // be nice - missing the ending ;
        $string = preg_replace_callback(
            '/\&#[Xx][A-Fa-f0-9]+[^A-Fa-f0-9;]/',
            /**
             * Anonymous function for callback
             *
             * @param array Array of match.
             *
             * @return string The normalized replacement.
             */
            function (array $m):string {
                $broken = $m[0];
                $n = strlen($broken);
                $last = substr($broken, ($n - 1), 1);
                $entity = substr($broken, 3, ($n-4));
                $entity = strtoupper($entity);
                $return = '&#x' . $entity . ';' . $last;
                return $return;
            },
            $string
        );
        $string = preg_replace_callback(
            '/\&#[0-9]+[^0-9;]/',
            /**
             * Anonymous function for callback
             *
             * @param array Array of match.
             *
             * @return string The normalized replacement.
             */
            function (array $m):string {
                $broken = $m[0];
                $n = strlen($broken);
                $last = substr($broken, ($n - 1), 1);
                $entity = substr($broken, 2, ($n-3));
                $return = '&#' . $entity . ';' . $last;
                return $return;
            },
            $string
        );
        return $string;
    }//end normalizeNumbereredEntities()

    /**
     * Display string sanitizer.
     *
     * @param string $string The string to sanitize.
     *
     * @return string The sanitized string.
     */
    public static function stringSanitizer(string $string): string
    {
        $string = self::normalizeNumbereredEntities($string);
        $s = array("/\</", "/\>/");
        $r = array('&lt;', '&gt;');
        $string = preg_replace($s, $r, $string);
        $string = self::namedToNumbered($string, 'html');
        // ampersands that are NOT part of a decimal or hex numbered entity
        $string = self::encodeAnAmpersand($string);
        // return lt and gt to named entities
        $s = array('/\&#60;/', '/\&#62;/');
        $r = array('&lt;', '&gt;');
        return preg_replace($s, $r, $string);
    }//end stringSanitizer()
}//end class

?>