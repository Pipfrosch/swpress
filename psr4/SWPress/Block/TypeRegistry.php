<?php
declare(strict_types=1);

/**
 * Blocks API: TypeRegistry class
 *
 * @package    SWPress
 * @subpackage Block
 * @author     WordPress Developers <wp-hackers@lists.automattic.com>
 * @license    https://opensource.org/licenses/GPL-2.0 GPLv2
 * @since      5.0.0
 * @link       https://wordpress.org/
 */

namespace SWPress\Block;

/**
 * Core class used for interacting with block types.
 *
 * @since 5.0.0
 */
final class TypeRegistry
{
    /**
     * Registered block types, as `$name => $instance` pairs.
     *
     * @since 5.0.0
     * @var   \SWPress\Block\Type[]
     */
    private $registered_block_types = array();

    /**
     * Container for the main instance of the class.
     *
     * @since 5.0.0
     * @var   \SWPress\Block\TypeRegistry|null
     */
    private static $instance = null;

    /**
     * Registers a block type.
     *
     * @since 5.0.0
     *
     * @param string|\SWPress\Block\Type $name Block type name including namespace, or alternatively a
     *                                   complete \SWPress\Block\Type instance. In case a \SWPress\Block\Type
     *                                   is provided, the $args parameter will be ignored.
     * @param array                $args {
     *     Optional. Array of block type arguments. Any arguments may be defined, however the
     *     ones described below are supported by default. Default empty array.
     *
     *     @type   callable $render_callback Callback used to render blocks of this block type.
     *     @type   array    $attributes      Block attributes mapping, property name to schema.
     * }
     * @return \SWPress\Block\Type|false The registered block type on success, or false on failure.
     */
    public function register($name, $args = array())
    {
        $block_type = null;
        if ($name instanceof \SWPress\Block\Type) {
            $block_type = $name;
            $name       = $block_type->name;
        }
        /**
         * @psalm-suppress DocblockTypeContradiction
         */
        if (! is_string($name)) {
            if (function_exists('__')) {
                $message = __('Block type names must be strings.');
            } else {
                $message = 'Block type names must be strings.';
            }
            \SWP::doing_it_wrong(__METHOD__, $message, '5.0.0');
            return false;
        }

        if (preg_match('/[A-Z]+/', $name)) {
            if (function_exists('__')) {
                $message = __('Block type names must not contain uppercase characters.');
            } else {
                $message = 'Block type names must not contain uppercase characters.';
            }
            \SWP::doing_it_wrong(__METHOD__, $message, '5.0.0');
            return false;
        }

        $name_matcher = '/^[a-z0-9-]+\/[a-z0-9-]+$/';
        if (! preg_match($name_matcher, $name)) {
            $message = 'Block type names must contain a namespace prefix. Example: my-plugin/my-custom-block-type';
            if (function_exists('__')) {
                $message = __($message);
            }
            \SWP::doing_it_wrong(__METHOD__, $message, '5.0.0');
            return false;
        }

        if ($this->is_registered($name)) {
            /* translators: %s: block name */
            if (function_exists('__')) {
                $message = sprintf(__('Block type "%s" is already registered.'), $name);
            } else {
                $message = sprintf('Block type "%s" is already registered.', $name);
            }
            \SWP::doing_it_wrong(__METHOD__, $message, '5.0.0');
            return false;
        }

        if (! $block_type) {
            $block_type = new Type($name, $args);
        }

        $this->registered_block_types[ $name ] = $block_type;

        return $block_type;
    }//end register()


    /**
     * Unregisters a block type.
     *
     * @since 5.0.0
     *
     * @param string|\SWPress\Block\Type $name Block type name including namespace, or alternatively a
     *                                   complete \SWPress\Block\Type instance.
     *
     * @return \SWPress\Block\Type|false The unregistered block type on success, or false on failure.
     */
    public function unregister($name)
    {
        if ($name instanceof \SWPress\Block\Type) {
            $name = $name->name;
        }

        if (! $this->is_registered($name)) {
            /* translators: %s: block name */
            if (function_exists('__')) {
                $message = sprintf(__('Block type "%s" is not registered.'), $name);
            } else {
                $message = sprintf('Block type "%s" is not registered.', $name);
            }
            \SWP::doing_it_wrong(__METHOD__, $message, '5.0.0');
            return false;
        }

        $unregistered_block_type = $this->registered_block_types[ $name ];
        unset($this->registered_block_types[ $name ]);

        return $unregistered_block_type;
    }//end unregister()


    /**
     * Retrieves a registered block type.
     *
     * @since 5.0.0
     *
     * @param string $name Block type name including namespace.
     *
     * @return \SWPress\Block\Type|null The registered block type, or null if it is not registered.
     */
    public function get_registered($name)
    {
        if (! $this->is_registered($name)) {
            return null;
        }

        return $this->registered_block_types[ $name ];
    }//end get_registered()


    /**
     * Retrieves all registered block types.
     *
     * @since 5.0.0
     *
     * @return \SWPress\Block\Type[] Associative array of `$block_type_name => $block_type` pairs.
     */
    public function get_all_registered()
    {
        return $this->registered_block_types;
    }//end get_all_registered()


    /**
     * Checks if a block type is registered.
     *
     * @since 5.0.0
     *
     * @param string $name Block type name including namespace.
     *
     * @return bool True if the block type is registered, false otherwise.
     */
    public function is_registered($name)
    {
        return isset($this->registered_block_types[ $name ]);
    }//end is_registered()


    /**
     * Utility method to retrieve the main instance of the class.
     *
     * The instance will be created if it does not exist yet.
     *
     * @since 5.0.0
     *
     * @return \SWPress\Block\TypeRegistry The main instance.
     */
    public static function get_instance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }//end get_instance()
}//end class

