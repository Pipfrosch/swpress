<?php
declare(strict_types=1);
/**
 * Block Serialization Parser
 *
 * @package    SWPress
 * @subpackage Block
 * @author     WordPress Developers <wp-hackers@lists.automattic.com>
 * @license    https://opensource.org/licenses/GPL-2.0 GPLv2
 * @link       https://wordpress.org/
 */

namespace SWPress\Block;

/**
 * Class ParserFrame
 *
 * Holds partial blocks in memory while parsing
 *
 * @internal
 * @since    3.8.0
 */

class ParserFrame
{
    /**
     * Full or partial block
     *
     * @since 3.8.0
     * @var   \SWPress\Block\ParserBlock
     */
    public $block;

    /**
     * Byte offset into document for start of parse token
     *
     * @since 3.8.0
     * @var   int
     */
    public $token_start;

    /**
     * Byte length of entire parse token string
     *
     * @since 3.8.0
     * @var   int
     */
    public $token_length;

    /**
     * Byte offset into document for after parse token ends
     * (used during reconstruction of stack into parse production)
     *
     * @since 3.8.0
     * @var   int
     */
    public $prev_offset;

    /**
     * Byte offset into document where leading HTML before token starts
     *
     * @since 3.8.0
     * @var   null|int
     */
    public $leading_html_start;

    /**
     * Constructor
     *
     * Will populate object properties from the provided arguments.
     *
     * @since 3.8.0
     *
     * @param \SWPress\Block\ParserBlock $block              Full or partial block.
     * @param int                   $token_start        Byte offset into document for start of parse token.
     * @param int                   $token_length       Byte length of entire parse token string.
     * @param int                   $prev_offset        Byte offset into document for after parse token ends.
     * @param int                   $leading_html_start Byte offset into document where leading HTML before token
     *                                                  starts.
     */
    public function __construct($block, $token_start, $token_length, $prev_offset = null, $leading_html_start = null)
    {
        $this->block              = $block;
        $this->token_start        = $token_start;
        $this->token_length       = $token_length;
        $this->prev_offset        = isset($prev_offset) ? $prev_offset : $token_start + $token_length;
        $this->leading_html_start = $leading_html_start;
    }//end __construct()
}//end class


