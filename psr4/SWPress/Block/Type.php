<?php
declare(strict_types=1);
/**
 * Blocks API: WP_Block_Type class
 *
 * @package    SWPress
 * @subpackage Block
 * @author     WordPress Developers <wp-hackers@lists.automattic.com>
 * @license    https://opensource.org/licenses/GPL-2.0 GPLv2
 * @since      5.0.0
 * @link       https://wordpress.org/
 */

namespace SWPress\Block;

/**
 * Core class representing a block type.
 *
 * @since 5.0.0
 *
 * @see register_block_type()
 */
class Type
{
    /**
     * Block type key.
     *
     * @since 5.0.0
     * @var   string
     */
    public $name;

    /**
     * Block type render callback.
     *
     * @since 5.0.0
     * @var   null|callable
     */
    public $render_callback = null;

    /**
     * Block type attributes property schemas.
     *
     * @since 5.0.0
     * @var   null|array
     */
    public $attributes = null;

    /**
     * Block type editor script handle.
     *
     * @since 5.0.0
     * @var   null|string
     */
    public $editor_script = null;

    /**
     * Block type front end script handle.
     *
     * @since 5.0.0
     * @var   null|string
     */
    public $script = null;

    /**
     * Block type editor style handle.
     *
     * @since 5.0.0
     * @var   null|string
     */
    public $editor_style = null;

    /**
     * Block type front end style handle.
     *
     * @since 5.0.0
     * @var   null|string
     */
    public $style = null;

    /**
     * Constructor.
     *
     * Will populate object properties from the provided arguments.
     *
     * @since 5.0.0
     *
     * @see register_block_type()
     *
     * @param string       $block_type Block type name including namespace.
     * @param array|string $args       Optional. Array or string of arguments for registering a block type.
     *                                 Default empty array.
     */
    public function __construct($block_type, $args = array())
    {
        $this->name = $block_type;

        $this->set_props($args);
    }//end __construct()


    /**
     * Renders the block type output for given attributes.
     *
     * @since 5.0.0
     *
     * @param array  $attributes Optional. Block attributes. Default empty array.
     * @param string $content    Optional. Block content. Default empty string.
     *
     * @return string Rendered block type output.
     */
    public function render(array $attributes = array(), string $content = ''): string
    {
        if (! $this->is_dynamic()) {
            return '';
        }

        $attributes = $this->prepare_attributes_for_render($attributes);
        if (! is_null($this->render_callback)) {
            return (string) call_user_func($this->render_callback, $attributes, $content);
        }
        //throw exception - might be better to use a Type error?
        throw \SWPress\Exceptions\InvalidArgumentException::invalidNullCallback();
    }//end render()


    /**
     * Returns true if the block type is dynamic, or false otherwise. A dynamic
     * block is one which defers its rendering to occur on-demand at runtime.
     *
     * @since 5.0.0
     *
     * @return boolean Whether block type is dynamic.
     */
    public function is_dynamic()
    {
        return is_callable($this->render_callback);
    }//end is_dynamic()


    /**
     * Validates attributes against the current block schema, populating
     * defaulted and missing values.
     *
     * @since 5.0.0
     *
     * @param array $attributes Original block attributes.
     *
     * @return array             Prepared block attributes.
     */
    public function prepare_attributes_for_render($attributes)
    {
        // If there are no attribute definitions for the block type, skip
        // processing and return vebatim.
        if (! isset($this->attributes)) {
            return $attributes;
        }

        foreach ($attributes as $attribute_name => $value) {
            // If the attribute is not defined by the block type, it cannot be
            // validated.
            if (! isset($this->attributes[ $attribute_name ])) {
                continue;
            }

            $schema = $this->attributes[ $attribute_name ];

            // Validate value by JSON schema. An invalid value should revert to
            // its default, if one exists. This occurs by virtue of the missing
            // attributes loop immediately following. If there is not a default
            // assigned, the attribute value should remain unset.
            
            /**
             * @psalm-suppress UndefinedFunction
             */
            $is_valid = rest_validate_value_from_schema($value, $schema);
            if (is_wp_error($is_valid)) {
                unset($attributes[ $attribute_name ]);
            }
        }

        // Populate values of any missing attributes for which the block type
        // defines a default.
        $missing_schema_attributes = array_diff_key($this->attributes, $attributes);
        foreach ($missing_schema_attributes as $attribute_name => $schema) {
            if (isset($schema['default'])) {
                $attributes[ $attribute_name ] = $schema['default'];
            }
        }

        return $attributes;
    }//end prepare_attributes_for_render()


    /**
     * Sets block type properties.
     *
     * @since 5.0.0
     *
     * @param array|string $args Array or string of arguments for registering a block type.
     *
     * @return void
     */
    public function set_props($args): void
    {
        $args = \SWP::swp_parse_args(
            $args,
            array(
                'render_callback' => null,
            )
        );

        $args['name'] = $this->name;

        foreach ($args as $property_name => $property_value) {
            $this->$property_name = $property_value;
        }
    }//end set_props()


    /**
     * Get all available block attributes including possible layout attribute from Columns block.
     *
     * @since 5.0.0
     *
     * @return array Array of attributes.
     */
    public function get_attributes()
    {
        return is_array($this->attributes) ?
            array_merge(
                $this->attributes,
                array(
                    'layout' => array(
                        'type' => 'string',
                    ),
                )
            ) :
            array(
                'layout' => array(
                    'type' => 'string',
                ),
            );
    }//end get_attributes()
}//end class

