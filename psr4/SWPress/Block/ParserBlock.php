<?php
declare(strict_types=1);
/**
 * Block Serialization Parser
 *
 * @package    SWPress
 * @subpackage Block
 * @author     WordPress Developers <wp-hackers@lists.automattic.com>
 * @license    https://opensource.org/licenses/GPL-2.0 GPLv2
 * @link       https://wordpress.org/
 */

namespace SWPress\Block;

/**
 * Class ParserBlock
 *
 * Holds the block structure in memory
 *
 * @since 3.8.0
 */
class ParserBlock
{
    /**
     * Name of block
     *
     * @example "core/paragraph"
     *
     * @since 3.8.0
     * @var   null|string
     */
    public $blockName;

    /**
     * Optional set of attributes from block comment delimiters
     *
     * @example null
     * @example array( 'columns' => 3 )
     *
     * @since 3.8.0
     * @var   array|null
     */
    public $attrs;

    /**
     * List of inner blocks (of this same class)
     *
     * @since 3.8.0
     * @var   \SWPress\Block\ParserBlock[]
     */
    public $innerBlocks;

    /**
     * Resultant HTML from inside block comment delimiters
     * after removing inner blocks
     *
     * @example "...Just <!-- wp:test /--> testing..." -> "Just testing..."
     *
     * @since 3.8.0
     * @var   string
     */
    public $innerHTML;

    /**
     * List of string fragments and null markers where inner blocks were found
     *
     * @example array(
     *   'innerHTML'    => 'BeforeInnerAfter',
     *   'innerBlocks'  => array( block, block ),
     *   'innerContent' => array( 'Before', null, 'Inner', null, 'After' ),
     * )
     *
     * @since 4.2.0
     * @var   array
     */
    public $innerContent;

    /**
     * Constructor.
     *
     * Will populate object properties from the provided arguments.
     *
     * @since 3.8.0
     *
     * @param null|string $name         Name of block.
     * @param array       $attrs        Optional set of attributes from block comment delimiters.
     * @param array       $innerBlocks  List of inner blocks (of this same class).
     * @param string      $innerHTML    Resultant HTML from inside block comment delimiters after removing inner blocks.
     * @param array       $innerContent List of string fragments and null markers where inner blocks were found.
     */
    public function __construct($name, $attrs, $innerBlocks, $innerHTML, $innerContent)
    {
        $this->blockName    = $name;
        $this->attrs        = $attrs;
        $this->innerBlocks  = $innerBlocks;
        $this->innerHTML    = $innerHTML;
        $this->innerContent = $innerContent;
    }//end __construct()
}//end class


