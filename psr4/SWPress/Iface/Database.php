<?php
/**
 * This is intended to provide a WordPress compatible database interface that
 * database classes MUST implement.
 *
 * Unfortunately some of the method names do not conform to the PSR-2 coding standard. This is because WordPress
 * expects existing public methods, and considerably predates both PHP-FIG and the PSR-2 coding standard.
 *
 * @package    SWPress
 * @subpackage Interfaces
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/GPL-2.0 GPLv2
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

namespace SWPress\Iface;

/**
 * The SWPress Database Interface.
 *
 * A work in progress. Required functions will be in alphabetical order.
 */
interface Database
{
    /**
     * Connect to and select database.
     *
     * ABSTRACT PUBLIC FUNCTION IN ABSTRACT CLASS
     *
     * @param bool $allow_bail Optional. Allows the function to bail. Default true.
     *
     * @return bool True with a successful connection, false on failure.
     */
    public function db_connect(bool $allow_bail = true): bool;

    /**
     * Delete a row in the table by where clauses.
     *
     * @param string       $table  The name of the database table.
     * @param array        $where  A Key => Value array where Key corresponds to a column in the database table.
     * @param array|string $format Optional. Array of formats to be mapped to each Value in the in the Key => Value
     *                             pair. Format is one of `%d` (integer), `%f` (float), or `%s` (string).
     *
     * @return int|bool The number of rows deleted, or false on error.
     */
    public function delete(string $table, array $where, $format);

    /**
     * Determines the best charset and collation to use given a charset and collation.
     *
     * ??? NOTE TO SELF - IMPLEMENT IN ABSTRACT CLASS
     *
     * @param string $charset The character set to check.
     * @param string $collate The collation to check.
     *
     * @return array The most appropriate character set and collation to use.
     */
    public function determine_charset(string $charset, string $collate);

    /**
     * Gets blog prefix.
     *
     * IMPLEMENTED IN ABSTRACT CLASS
     *
     * @param null|int $blog_id Optional.
     *
     * @return string Blog prefix.
     */
    public function get_blog_prefix($blog_id = null);

    /**
     * Retrieve a single variable from the database. For $columnNumber and $rowNumber, index starts at 0.
     *
     * @param string|null $query        Optional. When null, uses result from previous query. Defaults to null.
     * @param int         $columnNumber Optional. Which column number of result to select from. Defaults to 0.
     * @param int         $rowNumber    Optional. Which row number of result to select from. Defaults to 0.
     *
     * @return string|null The result string on success, Null on failure.
     */
    public function get_var($query = null, int $columnNumber = 0, int $rowNumber = 0);
    
    /** to do - has_cap */

    /**
     * Disables showing of database errors.
     *
     * By default database errors are not shown.
     *
     * @see   wpdb::show_errors()
     *
     * @return bool Whether showing of errors was active
     */
    public function hide_errors();

    /**
     * Set $this->charset and $this->collate
     *
     * IMPLEMENTED IN ABSTRACT CLASS
     *
     * @return void
     */
    public function init_charset();

    /**
     * Selects a database using the current database connection.
     *
     * The database name will be changed based on the current database
     * connection. On failure, the execution will bail and display an DB error.
     *
     * @param string $db  Database name.
     * @param mixed  $dbh Optional link identifier.
     *
     * @return void
     */
    public function select(string $db, $dbh = null);

    /**
     * Sets the blog ID.
     *
     * NOTE TO SELF - IMPLEMENT IN ABSTRACT CLASS - DONE
     *
     * @param int $blog_id              The blog ID
     * @param int $network_id Optional. The network id. Defaults to 0.
     *
     * @return int The previous blog id.
     */
    public function set_blog_id(int $blog_id, int $network_id = 0);

    /**
     * Sets the connection's character set.
     *
     * @param mixed    $dbh     The resource given by database connection.
     * @param string   $charset Optional. The character set. Default ''.
     * @param string   $collate Optional. The collation. Default ''.
     *
     * @return void
     */
    public function set_charset($dbh, string $charset = '', string $collate = '');

    /**
     * Change the current SQL mode, and ensure its WordPress compatibility.
     *
     * If no modes are passed, it will ensure the current server modes are compatible.
     *
     * @param array $modes Optional. A list of SQL modes to set.
     *
     * @return void
     */
    public function set_sql_mode($modes = array());

    /**
     * Sets the table prefix for the WordPress tables.
     *
     * IMPLEMENTED IN ABSTRACT CLASS
     *
     * @param string $prefix          Alphanumeric name for the new prefix.
     * @param bool   $set_table_names Optional. Whether the table names, e.g. wpdb::$posts, should be updated or not.
     *
     * @return string|\SWPress\Error\Error Old prefix or \SWPress\Error\Error on error
     */
    public function set_prefix(string $prefix, bool $set_table_names = true);

    /**
     * Enables showing of database errors.
     *
     * This function should be used only to enable showing of errors.
     * wpdb::hide_errors() should be used instead for hiding of errors. However,
     * this function can be used to enable and disable showing of database
     * errors.
     *
     * @since 0.71
     * @see   wpdb::hide_errors()
     *
     * @param bool $show Whether to show or hide errors.
     *
     * @return bool Old value for showing errors.
     */
    public function show_errors($show = true);

    /**
     * Strips any invalid characters from the string for a given table and column.
     *
     * NOTE TO SELF - IMPLEMENT IN ABSTRACT CLASS
     *
     * @param string $table  Table name.
     * @param string $column Column name.
     * @param string $value  The text to check.
     *
     * @return string|\SWPress\Error\Error The converted string, or a
     *                                     \SWPress\Error\Error object if the conversion fails.
     */
    public function strip_invalid_text_for_column(string $table, string $column, string $value);

    /**
     * Returns an array of SWPress tables.
     *
     * IMPLEMENTED IN ABSTRACT CLASS
     *
     * Also allows for the CUSTOM_USER_TABLE and CUSTOM_USER_META_TABLE to
     * override the WordPress users and usermeta tables that would otherwise
     * be determined by the prefix.
     *
     * The scope argument can take one of the following:
     *
     * 'all' - returns 'all' and 'global' tables. No old tables are returned.
     * 'blog' - returns the blog-level tables for the queried blog.
     * 'global' - returns the global tables for the installation, returning multisite tables only if running multisite.
     * 'ms_global' - returns the multisite global tables, regardless if current installation is multisite.
     * 'old' - returns tables which are deprecated.
     *
     * @param string $scope   Optional. Can be all, global, ms_global, blog, or old tables. Defaults to all.
     * @param bool   $prefix  Optional. Whether to include table prefixes. Default true. If blog
     *                        prefix is requested, then the custom users and usermeta tables will be mapped.
     * @param int    $blog_id Optional. The blog_id to prefix. Defaults to wpdb::$blogid. Used only when prefix is requested.
     *
     * @return array Table names. When a prefix is requested, the key is the unprefixed table name.
     */
    public function tables(string $scope = 'all', bool $prefix = true, int $blog_id = 0);

    /**
     * A bogus public function to test psalm.
     *
     * @param int $someInteger A bogus integer.
     *
     * @return void
     */
    //public function bogus(int $someInteger);
}//end interface

?>