<?php
if(php_sapi_name() === 'cli') {
  $string = "<?php\n/**\n *  Salts needed for SWPress\n *  These should be in the file salts.php\n *  These are 384-bit secure random salts.\n */\n";
  $encoded = array();
  for($i=0; $i<11; $i++) {
    $raw = random_bytes(48);
    $encoded[] = base64_encode($raw);
  }
  $string .= "define( 'AUTH_KEY',         '" . $encoded[0] . "' );\n";
  $string .= "define( 'SECURE_AUTH_KEY',  '" . $encoded[1] . "' );\n";
  $string .= "define( 'LOGGED_IN_KEY',    '" . $encoded[2] . "' );\n";
  $string .= "define( 'NONCE_KEY',        '" . $encoded[3] . "' );\n";
  $string .= "define( 'AUTH_SALT',        '" . $encoded[4] . "' );\n";
  $string .= "define( 'SECURE_AUTH_SALT', '" . $encoded[5] . "' );\n";
  $string .= "define( 'LOGGED_IN_SALT',   '" . $encoded[6] . "' );\n";
  $string .= "define( 'NONCE_SALT',       '" . $encoded[7] . "' );\n\n";
  
  $string .= "/**\n *  Salt to use for user activation and password reset.\n */\n";
  $string .= "define( 'ACTIVATION_SALT',  '" . $encoded[8] . "' );\n\n";
  
  $string .= "/**\n *  If you change this, everyone will need to do a password reset.\n *  This salt is used before password hash algorithm salt to create a pre-hash of the password\n *  so that if there is a database dump, even weak password are protected (unless this salt is\n *  also known to attacker). This is a 384-bit secure random salt.\n */\n";
  $string .= "define( 'PASSWORD_SALT',    '" . $encoded[9] . "' );\n\n\n";
  
  $string .= "/**\n *  The salt used when generating obfuscated keys for the Redis / APCu cache.\n */\n";
  $string .= "define ( 'SWP_CACHE_SALT',  '" . $encoded[10] . "' );\n\n\n";
  
  //32 byte crypto key
  $raw = random_bytes(32);
  $encoded = bin2hex($raw);
  $string .= "/**\n *  If you want the Redis / APCu cache encrypted, uncomment the line below. You can change it\n *  if you want but it must be a 32-byte hex encoded key.\n *\n *  Note that there is a performance penalty for encrypting the contents of the Redis / APCu\n *  cache but I have not measured what that is.\n */\n";
  $string .= "//define( 'SWP_CRYPTOKEY',  '" . $encoded . "' );\n\n";
  
  $string .= "\n?>\n";
  print $string;
}
?>