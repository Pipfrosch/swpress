<?php
declare(strict_types=1);

/**
 * Autoloading for SWPress.
 *
 * @package SWPress
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/swpress
 */

if (! defined('SWP_PSR0')) {
    define('SWP_PSR0', '/' . trim(ABSPATH, '/') . '/psr0');
}
if (! defined('SWP_PSR4')) {
    define('SWP_PSR4', '/' . trim(ABSPATH, '/') . '/psr4');
}
if (! defined('SWP_PEAR_PATH')) {
    $pearSearch = array('/usr/local/share/pear', '/usr/local/lib/pear', '/usr/share/pear', '/usr/lib/pear');
    foreach ($pearSearch as $dircheck) {
        if (file_exists($dircheck . '/PEAR.php')) {
            define('SWP_PEAR_PATH', $dircheck);
            break;
        }
    }
}

if (! class_exists('\SWP\Autoload')) {
    require(ABSPATH . 'Autoload.php');

    spl_autoload_register(function ($class) {
        \SWP\Autoload::loadClass($class);
    });
    spl_autoload_register(function ($class) {
        \SWP\Autoload::loadPSR0Class($class);
    });
    spl_autoload_register(function ($class) {
        \SWP\Autoload::loadPear($class);
    });
}

?>
