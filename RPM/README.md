RPM Spec Files
==============

My goal is for SWPress to be a project that is easy for Linux distributions to
package.

Components that are brought in from third party maintainers should be packaged
as separate packages so they can be individually updated as needed.

If a Linux (or other) distribution already has some of these third party
projects packaged, the distribution should patch the SWPress `swp-autoload.php`
autoloader so that it knows where to look for existing packaged versions.

Spec Files
----------

The spec files assume the dependencies are being packaged for use with SWPress
with SWPress installed in `/usr/share/swpress` which may not be where you want
it. Please treat them as templates.

Patches
-------

The `classic-editor-1.4-psr4.patch` patch splits the class from the code and
puts the class into a PSR-4 namespace.

The `Requests-1.7.0-cafile.patch` patch should be modified by a distribution
packager to change line 25 to point to a PEM encoded certificate bundle that is
maintained by the distribution. The current patch should be fine on RHEL/CentOS
or Fedora systems, but may not be the right location on, say, SuSE or whatever.


