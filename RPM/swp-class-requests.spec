Name:		swp-class-requests
Version:	1.7.0
Release:	0%{?dist}
Summary:	Requests for PHP class

Group:		SWPress/Core
License:	ISC
URL:		http://requests.ryanmccue.info/
Source0:	Requests-%{version}.zip
Patch0:		Requests-1.7.0-cafile.patch

#BuildRequires:	
#Requires:
BuildArch:	noarch

%description
Requests is a HTTP library written in PHP, for human beings. It is roughly based
on the API from the excellent Requests Python library. Requests is ISC Licensed
(similar to the new BSD license) and has no dependencies, except for PHP 5.2+.

%prep
%setup -q -n Requests-%{version}
%patch0 -p1


#%%build
#%%configure
#make %%{?_smp_mflags}


%install
for subdir in Auth Cookie Exception Proxy Response Transport Utility; do
  mkdir -p %{buildroot}%{_datadir}/swpress/psr0/Requests/${subdir}
done
mkdir -p %{buildroot}%{_datadir}/swpress/psr0/Requests/Exception/HTTP
mkdir -p %{buildroot}%{_datadir}/swpress/psr0/Requests/Exception/Transport

install -m644 library/Requests.php %{buildroot}%{_datadir}/swpress/psr0/
for file in Auth.php Cookie.php Exception.php Hooker.php Hooks.php IDNAEncoder.php IPv6.php IRI.php Proxy.php Response.php Session.php SSL.php Transport.php; do
  install -m644 library/Requests/${file} %{buildroot}%{_datadir}/swpress/psr0/Requests/
done
install -m644 library/Requests/Auth/Basic.php %{buildroot}%{_datadir}/swpress/psr0/Requests/Auth/
install -m644 library/Requests/Cookie/Jar.php %{buildroot}%{_datadir}/swpress/psr0/Requests/Cookie/
install -m644 library/Requests/Exception/HTTP.php %{buildroot}%{_datadir}/swpress/psr0/Requests/Exception/
install -m644 library/Requests/Exception/Transport.php %{buildroot}%{_datadir}/swpress/psr0/Requests/Exception/
for file in 304.php 305.php 306.php 400.php 401.php 402.php 403.php 404.php 405.php 406.php 407.php 408.php 409.php 410.php 411.php 412.php 413.php 414.php 415.php 416.php 417.php 418.php 428.php 429.php 431.php 500.php 501.php 502.php 503.php 504.php 505.php 511.php Unknown.php; do
  install -m644 library/Requests/Exception/HTTP/${file} %{buildroot}%{_datadir}/swpress/psr0/Requests/Exception/HTTP/
done
install -m644 library/Requests/Exception/Transport/cURL.php %{buildroot}%{_datadir}/swpress/psr0/Requests/Exception/Transport/
install -m644 library/Requests/Proxy/HTTP.php %{buildroot}%{_datadir}/swpress/psr0/Requests/Proxy/
install -m644 library/Requests/Response/Headers.php %{buildroot}%{_datadir}/swpress/psr0/Requests/Response/
install -m644 library/Requests/Transport/cURL.php %{buildroot}%{_datadir}/swpress/psr0/Requests/Transport/
install -m644 library/Requests/Transport/fsockopen.php %{buildroot}%{_datadir}/swpress/psr0/Requests/Transport/
install -m644 library/Requests/Utility/CaseInsensitiveDictionary.php %{buildroot}%{_datadir}/swpress/psr0/Requests/Utility/
install -m644 library/Requests/Utility/FilteredIterator.php %{buildroot}%{_datadir}/swpress/psr0/Requests/Utility/


%files
%defattr(-,root,root,-)
%doc CHANGELOG.md README.md
%license LICENSE
%{_datadir}/swpress/psr0/Requests.php
%{_datadir}/swpress/psr0/Requests



%changelog
* Fri Mar 22 2019 Alice Wonder <buildmaster@librelamp.com> 1.7.0-0
- Initial spec file.
