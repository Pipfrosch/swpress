Name:		swp-class-phpmailer
Version:	6.0.7
Release:	0%{?dist}
Summary:	A full-featured email creation and transfer class for PHP

Group:		SWPress/Core
License:	LGPL 2.1
URL:		https://github.com/PHPMailer/PHPMailer
Source0:	PHPMailer-%{version}.tar.gz

#BuildRequires:	
#Requires:
BuildArch:	noarch	

%description
A class for sending mail in PHP


%prep
%setup -q -n PHPMailer-%{version}


#%%build
#%%configure
#make %{?_smp_mflags}


%install
mkdir -p %{buildroot}%{_datadir}/swpress/psr4/PHPMailer/PHPMailer
install -m644 src/Exception.php %{buildroot}%{_datadir}/swpress/psr4/PHPMailer/PHPMailer/
#install -m644 src/OAuth.php %{buildroot}%{_datadir}/swpress/psr4/PHPMailer/PHPMailer/
install -m644 src/PHPMailer.php %{buildroot}%{_datadir}/swpress/psr4/PHPMailer/PHPMailer/
#install -m644 src/POP3.php %{buildroot}%{_datadir}/swpress/psr4/PHPMailer/PHPMailer/
install -m644 src/SMTP.php %{buildroot}%{_datadir}/swpress/psr4/PHPMailer/PHPMailer/


%files
%defattr(-,root,root,-)
%doc README.md SECURITY.md
%license COMMITMENT LICENSE
%{_datadir}/swpress/psr4/PHPMailer


%changelog
* Fri Mar 22 2019 Alice Wonder <buildmaster@librelamp.com> 6.0.7-0
- Initial spec file, not installing OAuth or POP3 classes

