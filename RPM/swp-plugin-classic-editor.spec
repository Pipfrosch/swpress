Name:		swp-plugin-classic-editor
Version:	1.4
Release:	0%{?dist}
Summary:	Enables the previous "classic" editor and the old-style Edit Post screen with TinyMCE

Group:		SWPress/Plugins
License:	GPLv2+
URL:		https://github.com/WordPress/classic-editor/
Source0:	classic-editor.%{version}.zip
# patch needs to be updated for every version
Patch0:		classic-editor-%{version}-psr4.patch

#BuildRequires:	
#Requires:	
BuildArch:	noarch

%description
Classic Editor is an official plugin maintained by the WordPress team that
restores the previous ("classic") WordPress editor and the "Edit Post" screen.
It makes it possible to use plugins that extend that screen, add old-style meta
boxes, or otherwise depend on the previous editor.

%prep
%setup -q -n classic-editor
%patch0 -p1


#%%build
#%%configure
#make %{?_smp_mflags}


%install
mkdir -p %{buildroot}%{_datadir}/swpress/psr4/SWPress/Plugins
mkdir -p %{buildroot}%{_datadir}/swpress/wp-content/plugins/classic-editor/js
install -m644 classic-editor.php %{buildroot}%{_datadir}/swpress/psr4/SWPress/Plugins/ClassicEditor.php
install -m644 js/block-editor-plugin.js %{buildroot}%{_datadir}/swpress/wp-content/plugins/classic-editor/js/
install -m644 plugin-classic-editor.php %{buildroot}%{_datadir}/swpress/wp-content/plugins/classic-editor/classic-editor.php


%files
%defattr(-,root,root,-)
%doc readme.txt
%license LICENSE.md
%{_datadir}/swpress/psr4/SWPress/Plugins/ClassicEditor.php
%{_datadir}/swpress/wp-content/plugins/classic-editor



%changelog
* Sun Mar 24 2019 Alice Wonder <buildmaster@librelamp.com> 1.4-0
- Initial spec file

