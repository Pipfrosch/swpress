#!/bin/bash

rm -f *.aux *.acn *.acr *.alg *.glg *.glo *.gls *.glsdefs *.chg *.cho *.chs *.mgg *.mgo *.mgs *.phg *.pho *.phs *.ist *.log *.out *.toc

pdflatex Documentation.tex && makeglossaries Documentation && pdflatex Documentation.tex && pdflatex Documentation.tex
