\longnewglossaryentry{ce:iso646}
{
  name={ISO/IEC 646 (7-bit ASCII)},
  type=charencodings,
  sort=ISO 0646
}
  {Often called 7-bit ASCII even though ASCII predates ISO 646 by several
  decades. ISO 646 is a 7-bit \gls{charenc} that includes the printable ASCII range
  (\texttt{x20}--\texttt{x7E}) supplemented by the \gls{ccode} codepoints in
  the range \texttt{x00}--\texttt{x1F} and at \texttt{x7F}.

  The control code codepoints \texttt{x00}--\texttt{x1F} are often referred to
  as the \texttt{C0} control code codepoints. The control code codepoint
  \texttt{x7F} is technically not a \texttt{C0} control code codepoint, but it
  is a 7-bit ASCII control code codepoint and is usually lumped together with
  the \texttt{C0} control code codepoints. Many of those control codes are no
  longer used in modern equipment but some are still used, such as the
  \texttt{x0A} Linefeed (typically used as a Newline on UNIX systems) and
  \texttt{x0D} Carriage Return (used as a Newline on DOS/Windows system).
  
  Using standardized codepoints for them is of great benefit to the
  interoperability of computer systems on a network.

  When transmitting text content over the Internet, it is \textbf{critical}
  that you only use \glspl{charenc} that have \gls{backcompat} with 7-bit
  ASCII. Encodings that do not may be susceptible to maliciously
  malformed data attacks and should simply not be allowed.

  As a 7-bit encoding, 7-bit ASCII supports 128 characters
  ($2^7 = 128$).

  \glspl{charenc} that are safe to use for transmission of data over the
  Internet will either extend 7-bit ASCII by adding characters after
  \texttt{x7F} while using a single byte for the \texttt{x00}-\texttt{x7F}
  range, or they will only assign codepoints to printable 7-bit ASCII range
  that are compatible with ISO-646 and will leave the codepoints used for
  \texttt{C0} control code codepoints unassigned.

  Reference for the attack vector is needed, I believe a description of the
  attack vector was given by the \gls{whatwg}.

  In MariaDB you can use this \gls{charenc} with the \texttt{ascii}
  \gls{dbcharset} but it would be better to use \texttt{utf8mb4} as UTF-8
  is fully compatible with 7-bit ASCII.}

  

\longnewglossaryentry{ce:iso8859}
{
  name={ISO/IEC 8859},
  type=charencodings,
  sort=ISO 8859-00
}
  {This is a series of 8-bit (1 byte) \glspl{charenc} that all use the
  same codepoints for printable characters as \gls{ce:iso646}
  (\texttt{x20}--\texttt{x7E}) and only add additional characters within the
  range \texttt{xA0}--\texttt{xFF}. By design, they do not assign any
  codepoints to \glspl{ccode}.

  ISO/IEC 8859 was a joint project between the \gls{ISO} and the \gls{IEC} but
  has since been disbanded, \gls{ce:utf8} has largely solved the problem for most
  use cases. The existing \glspl{charenc}, or closely related encodings
  based on them, are still sometimes used when either space or bandwidth is at a
  premium.

  By only using the printable 7-bit ASCII range and codepoints above
  \texttt{x9F}, the codepoints for the \texttt{C0} control codes and the
  codepoints for the \texttt{C1} control codes remain unassigned.

  When \glspl{charenc} from this family are served over the Internet, the
  \gls{IANA} assigned \gls{MIME} types for this family specify that codepoints
  within the \texttt{C0} and \texttt{C1} ranges be treated as the corresponding
  control codes even though the \glspl{charenc} do not specify them. This is
  important for the \texttt{C0} control codes, but I wish they had not done
  so for the \texttt{C1} control codes.

  Every control code in \texttt{C1} can be achieved using a sequence involving
  the Escape control code, which is part of \texttt{C0}. There really was not
  a need for IANA to specify those codepoints should be used with \texttt{C1}
  control codes in the ISO 8859 family of \glspl{charenc}.

  With only 128 codepoints avalaible in one byte encodings above the 7-bit
  ASCII range, it would have been better if IANA had specified the
  \texttt{x80}--\texttt{x9F} range be left available for future universally
  needed glyphs such as the Euro (\euro{}).}

\longnewglossaryentry{ce:iso8859part1}
{
  name={ISO/IEC 8859-1},
  type=charencodings,
  sort=ISO 8859-01
}
  {Generally referred to as ISO 8859-1 or Latin1, but please note that the
  MariaDB \gls{dbcharset} with the name \texttt{latin1} actually refers to
  the \gls{ce:win1252} \glspl{charenc}, which extends ISO 8859-1.

  ISO 8859-1 is part of the \gls{ce:iso8859} series of 8-bit (1
  byte) encodings. In addition to printable 7-bit ASCII, it covers other
  characters commonly used in the Americas and Western Europe.

  When sent over the Internet with the \texttt{ISO-8859-1} Character Encoding
  \gls{MIME} type, this encoding has full \gls{backcompat} with
  \gls{ce:iso646}. Many web servers will serve documents with this \gls{MIME}
  type automatically if another \gls{MIME} type is not specified, and many
  browsers will assume this \gls{MIME} type for documents sent over the
  Internet that do not specify a Character Encoding \gls{MIME} type.

  ISO 8859-1 is the basis for Unicode, every codepoint in ISO 8859-1 matches
  the \gls{ucodepoint} for the corresponding glyph.

  ISO 8859-1 is extended by \gls{ce:win1252}, which assigns the codepoints for the
  \texttt{C0} control codes but uses some codepoints in the
  \texttt{x80}--\texttt{x9F} range for printable characters, resulting in an
  incompatibility when the ISO-8859-1 \gls{MIME} type is used.
 
  In MariaDB you can use this Character Encoding with the \texttt{latin1}
  \gls{dbcharset} but it would probably be better to use Windows-1252 with
  that Database Charset, as Windows-1252 is a superset of this Character
  Encoding.

  It would be better however to use the \texttt{utf8mb4} Database Charset.}

\longnewglossaryentry{ce:iso8859part2}
{
  name={ISO/IEC 8859-2},
  type=charencodings,
  sort=ISO 8859-02
}
  {Generally referred to as ISO 8859-2 or Latin2, ISO 8859-1 is part of the
  \gls{ce:iso8859} series of 8-bit (1 byte) encodings. In addition to printable
  7-bit ASCII, it covers other characters commonly used in Central and Eastern
  European languages that use a Latin-based alphabet.

  Unfortunately ISO 8859-2 lacks a codepoint for the Euro (\euro{}) which is now commonly
  used in many of the countries that use the written languages this Character
  Encoding was intended for. \gls{ce:utf8} should probably be used instead.

  When sent over the Internet with the \texttt{ISO-8859-2} \gls{MIME} type,
  this Character Encoding has \gls{backcompat} with \gls{ce:iso646} and is a
  safe Character Encoding to use with the Internet, but UTF-8 is preferred so
  that the Euro can be directly encoded rather than requiring an
  \gls{xmlentity}.

  In MariaDB you can use this Character Encoding with the \texttt{latin2}
  \gls{dbcharset} but it is better to not use this Character Encoding at all
  and instead use UTF-8. For the written languages this Character Encoding
  services, most glyphs used are from the 7-bit ASCII range which only uses
  one byte to encode in UTF-8, so there is not much space to be saved by
  using this encoding over \texttt{utf8mb4} in the database.}

\longnewglossaryentry{ce:iso8859part5}
{
  name={ISO/IEC 8859-5},
  type=charencodings,
  sort=ISO 8859-05
}
  {Part of the \gls{ce:iso8859} series of 8-bit (1 byte) encodings. In addition
  to the printable 7-bit ASCII range, ISO 8859-5 covers characters from the
  Cyrillic alphabet and is sometimes referred to as Latin/Cyrillic.

  When served over the Internet with the charset \gls{MIME} type
  \texttt{ISO-8859-5} this \gls{charenc} does have \gls{backcompat} with
  \gls{ce:iso646} however this Character Encoding should not be used.

  Every glyph in this Character Encoding is covered by \gls{ce:win1251} though
  the codepoints differ above \texttt{x7F}, this encoding does not have a
  codepoint for the Euro (\euro{}), and this encoding is not supported by MariaDB.

  You should use \gls{ce:utf8} instead or if you \emph{must} use an 8-bit
  encoding, use \gls{ce:win1251}.}

\longnewglossaryentry{ce:iso8859part11}
{
  name={ISO/IEC 8859-11},
  type=charencodings,
  sort=ISO 8859-11
}
  {is generally reffered to as ISO 8859-11 and is part of the
  \gls{ce:iso8859} series of 8-bit (1 byte) encodings. In addition to printable
  7-bit ASCII, it covers characters from the Thai language.

  ISO 8859-11 is based on the earlier \gls{ce:tis620} and only differs in that
  ISO 8869-11 assigns the codepoint \texttt{xA0} (unused in TIS-620) to the
  Non-Breaking Space character (which happens to be \gls{ucodepoint}
  \texttt{u00A0}).

  All TIS-620 encoded text is valid ISO 8859-11 and all ISO 8859-11 text that
  does not contain a Non-Breaking Space is valid TIS-620.

  In MariaDB you can use this Character Encoding with the \texttt{tis620}
  \gls{dbcharset} but it would probably be better to use \gls{ce:win874} with
  that Database Charset, as Windows-874 is a superset of this Character
  Encoding.}

\longnewglossaryentry{ce:iso8859part12}
{
  name={ISO/IEC 8859-12},
  type=charencodings,
  sort=ISO 8859-12
}
  { was never finished. Originally it was to cover glyphs needed
  for Celtic languages, then that was change to Devanagari. In 1997 it was
  abandoned.}

\longnewglossaryentry{ce:iso8859part15}
{
  name={ISO/IEC 8859-15},
  type=charencodings,
  sort=ISO 8859-15
}
  {is generally referred to as ISO 8859-15 and is part of the
  \gls{ce:iso8859} series of 8-bit (1 byte) encodings. Basically it is an
  update to \gls{ce:iso8859part1} replacing some of the lesser used Western
  European glyphs with more commonly needed glyphs that were not part of
  ISO 8859-1.

  Even though every character it added is also part of \gls{ce:win1252} the
  characters are at different codepoints.

  This made ISO 8859-15 incompatible with both ISO 8859-11 and with
  Windows-1252, the two most common Western European \glspl{charenc}.

  For a very brief period, ISO 8859-15 was popular on some UNIX systems that
  had formerly used ISO \gls{ce:iso8859part1} as it did provide the Euro (\euro{}),
  but it was quickly replaced by \gls{ce:utf8} on those systems and is now
  rarely used on UNIX systems.

  ISO 8859-15 should not be used. It should be forgotten. Documents that use
  it should be changed to use UTF-8.

  There is not a MariaDB \gls{dbcharset} appropriate for this Character
  Encoding. The \texttt{latin1} charset may work, but the collation and
  display would be incorrect in some places.}

\longnewglossaryentry{ce:tis620}
{
  name={TIS-620},
  type=charencodings
}
  {\gls{TIS} 620-2533, usually referred to as just TIS-620, is an eight-bit
  (one byte) \gls{charenc} for the Thai Language, produced by the \gls{TISI}.

  It supports the printable 7-bit ASCII range (\texttt{x20}--\texttt{x7E})
  and adds Thai characters to the codepoints \texttt{xA1}--\texttt{xFB}
  within the one-byte range.

  The codepoints \texttt{x00}--\texttt{x1F}, \texttt{x7F}--\texttt{xA0},
  \texttt{xDB}--\texttt{xDE}, and \texttt{xFC}--\texttt{xFF} are not
  assigned in this Character Encoding.

  While this Character Encoding is not part of the \gls{ce:iso8859} family of
  Character Encodings, it does not assign any codepoints that interfere with
  \texttt{C0} or \texttt{C1} \glspl{ccode}, and it uses the same
  codepoints for printable characters as \gls{ce:iso646}.

  This Character Encoding is extended by \gls{ce:iso8859part11} and by
  \gls{ce:win874}.

  If serving content with this Character Encoding over the Internet, you
  probably want to specify the ISO-8859-11 \gls{MIME} type for the character
  encoding to make sure \texttt{C0} control codes are used. I do not know
  if browser software will support those control codes if TIS-620 is
  specified.

  In MariaDB you can use this Character Encoding with the \texttt{tis620}
  \gls{dbcharset} but it would probably be better to use Windows-874 with
  that Database Charset, as Windows-874 is a superset of this Character
  Encoding and supports additional characters commonly used in Thai content
  (such as curly quotes) that are not part of TIS-620.}

\longnewglossaryentry{ce:ucs2}
{
  name={UCS-2},
  type=charencodings
}
  {is a 16-bit (two byte) \gls{charenc} capable of encoding every glyph
  in the \gls{BLP} but it can not encode every \gls{ucodepoint}.

  UCS-2 is largely considered obsolete and should not be used. It was replaced
  by \gls{ce:utf16} but in most cases, \gls{ce:utf8} is the preferred choice.

  Prior to UTF-16 and UTF-8, UCS-2 was often used for languages that could not
  adequately be represented with 8 bit (1 byte) Character Encodings.

  UCS-2 does not have \gls{backcompat} with \gls{ce:iso646} and should not be
  used to transmit data over the Internet, there are exploitable security
  concerns as a result of the incompatibility if UCS-2 strings are not carefully
  validated.

  MariaDB does support UCS-2 but \gls{swp} does not by choice. There
  just simply is not a sane reason to use this encoding.}

\longnewglossaryentry{ce:utf8}
{
  name={UTF-8},
  type=charencodings,
  sort=UTF-08
}
  {is a variable byte \gls{charenc} that that can be used to represent
  every \gls{ucodepoint}.

  For the 7-bit ASCII range (\texttt{x00}--\texttt{x7F}) it uses one byte
  to represent the character, making UTF-8 very efficient when most of your
  content uses ASCII characters, as it is for Western European languages.

  Beyond the 7-bit ASCII range, UTF-8 uses one byte to specify how many bytes
  are used for the next character, followed by the Unicode Codepoint. Thus
  outside the 7-bit ASCII range, the number of bytes needed to describe the
  Unicode Codepoint plus one is how many bytes it takes to represent the
  character in UTF-8. This does makes UTF-8 inefficient when the majority of
  your content uses Unicode Codepoints beyong the 7-bit ASCII range.

  Many people confuse UTF-8 with Unicode. It is not Unicode, but rather it is
  a Character Encoding that supports all of Unicode.

  UTF-8 has \gls{backcompat} with \gls{ce:iso646} and as far as I am aware,
  it is the only Character Encoding to both maintain that backwards
  compatability \emph{and} have full support for every Unicode Codepoint.
  This is why UTF-8 has become the dominant Character Encoding on the Internet.}

\longnewglossaryentry{ce:utf16}
{
  name={UTF-16},
  type=charencodings,
  sort=UTF-16
}
  {is a variable byte \gls{charenc} that can be used to represent
  every \gls{ucodepoint}. It is an update to \gls{ce:ucs2} and has
  \gls{backcompat} with UCS-2.

  In addition to the enhanced ability to represent every single Unicode
  Codepoint, UTF-16 is also able to work with languages that read from right to
  left, UCS-2 was only able to work with scripts read from left to right.

  UTF-16 is frequently used in Java and the Microsoft Windows Operating System
  but it never really caught on in UNIX land, where UTF-8 dominates as the
  Unicode Character Encoding of choice.

  UTF-16 lacks Backwards Compatibility with \gls{ce:iso646} and should not be
  used to transmit data over the Internet.

  MariaDB does support UTF-16 but \gls{swp} does not by choice. There
  just simply is not a sane reason to use it.}

\longnewglossaryentry{ce:utf32}
{
  name={UTF-32},
  type=charencodings,
  sort=UTF-32
}
  {is a fixed four byte \gls{charenc} that can be used to represent every
  \gls{ucodepoint}. It is also referred to as UCS-4.

  UTF-32 uses 32-bits (4 bytes) for every codepoint. This makes it extremely
  inefficient with respect to data size.

  Historically UTF-32 and UCS-4 were functionally identical but theoretically
  could differ in the future, however restrictions on Unicode in
  \myhref{https://tools.ietf.org/html/rfc3629}{RFC 3629} remove those potential
  future differences, so UTF-32 and UCS-4 are now identical both functionally
  and in the future.

  UTF-32 lacks \gls{backcompat} with \gls{ce:iso646} and should not be used to
  transmit data over the Internet.

  MariaDB does support UTF-32 but \gls{swp} does not by choice. There
  just simply is not a sane reason to use it.}

\longnewglossaryentry{ce:win874}
{
  name={Windows-874},
  type=charencodings,
  sort=Windows-000874
}
  {(also known as Code Page 1162) is an 8-bit (1 byte) \gls{charenc}
  based on \gls{ce:iso8859part11}. It differs from ISO 8859-11 only by
  assigning the following additional codepoints that are not used by
  ISO 8859-11 (or by \gls{ce:tis620}):

  \begin{itemize}
    \item \texttt{x80} is used for the Euro [\euro{}] (\gls{ucodepoint} \texttt{u20AC})
    \item \texttt{x85} is used for the Horizontal Ellipsis [\textellipsis{}] (Unicode Codepoint \texttt{u2026})
    \item \texttt{x91} is used for the Left curly single quote [`] (Unicode Codepoint \texttt{u2018})
    \item \texttt{x92} is used for the Right curly single quote ['] (Unicode Codepoint \texttt{u2019})
    \item \texttt{x93} is used for the Left curly double quote [``] (Unicode Codepoint \texttt{u201C})
    \item \texttt{x94} is used for the Right curly double quote [''] (Unicode Codepoint \texttt{u201D})
    \item \texttt{x95} is used for the Bullet [\textbullet{}] (Unicode Codepoint \texttt{u2022})
    \item \texttt{x96} is used for the En-dash [\textendash{}] (Unicode Codepoint \texttt{u2013})
    \item \texttt{x97} is used for the Em-dash [\textemdash{}] (Unicode Codepoint \texttt{u2014})
  \end{itemize}

  All TIS-620 and ISO 8859-11 text is valid Windows-874. All Windows-874 text
  that lacks those added characters is valid ISO 8859-11. All Windows-874 text
  that lacks those added characters and also lacks a Non-Breaking Space is valid
  TIS-620.

  Windows-874 does not assign any \gls{ccode} codepoints. It does leave the
  \texttt{C0} codepoints unassigned so it is \emph{possible} that it has full
  \gls{backcompat} with \gls{ce:iso646} but I am not positive that is the case.

  If you use this Character Encoding for data to be served over the Internet,
  it would be better to convert it to \gls{ce:utf8} before transmitting just to
  make sure full backwards compatibility with ISO 646 is maintained.

  In MariaDB you can use this Character Encoding with the \texttt{tis620}
  \gls{dbcharset}.

  With \gls{swp}, content in a table that uses a \texttt{tis620}
  Database Charset is automatically re-encoded in UTF-8 for you before it is
  sent.}

\longnewglossaryentry{ce:win1250}
{
  name={Windows-1250},
  type=charencodings,
  sort=Windows-001250
}
  {Often referred to as CodePage 1250. An 8-bit (1 byte) \gls{charenc} that
  covers many of the Latin based written languages in Central and Eastern
  Europe.

  Windows-1250 covers every character glyph that is covered by
  \gls{ce:iso8859part2} plus it has a few additional character glyphs including
  but not limited to the Euro (\euro{}).
  However Windows-1250 uses different codepoints for the character glyphs it has
  in common with ISO 8859-2 above the \texttt{x20}-\texttt{x7E} 7-bit ASCII
  range.

  Windows-1250 has full \gls{backcompat} with \gls{ce:iso646} and is a safe
  character encoding to use for transmission of data over the Internet, but it
  is better to use \gls{ce:utf8} for transmission of data over the Internet.

  In MariaDB you can use this Character Encoding with the \texttt{cp1250}
  \gls{dbcharset} but it is better to not use this Character Encoding at all
  and instead use UTF-8. For the written languages this Character Encoding
  services, most glyphs used are from the 7-bit ASCII range which only uses
  one byte to encode in UTF-8, so there is not much space to be saved by using
  this encoding over \texttt{utf8mb4} in the database.}

\longnewglossaryentry{ce:win1251}
{
  name={Windows-1251},
  type=charencodings,
  sort=Windows-001251
}
  {Often referred to as CodePage 1251, An 8-bit (a byte) \gls{charenc} that
  covers 7-bit ASCII and the Cryllic alphabet.

  This encoding covers every glyph covered by \gls{ce:iso8859part5} though
  it uses different codepoints above the \texttt{x7F} range, and it has some
  additional printable glyphs, such as the Euro (\euro{}), which are very important in
  modern documents.

  Windows-1251 does have \gls{backcompat} with \gls{ce:iso646} and is a safe
  to use for transmission of data over the Internet, however it is recommended
  that you use \gls{ce:utf8} instead.

  In MariaDB you can use this Character Encoding with the \texttt{cp1251}
  \gls{dbcharset} though it is recommended to use UTF-8 with the
  \texttt{utf8mb4} Database Charset unless you \emph{really} need to limit your
  database content to an 8-bit encoding.

  If \texttt{cp1251} is used for the database in \gls{swp}, the
  content will automatically be re-encoded to UTF-8 for you for serving the
  content over the Internet.}

\longnewglossaryentry{ce:win1252}
{
  name={Windows-1252},
  type=charencodings,
  sort=Windows-001252
}
  {An 8-bit (1 byte) \gls{charenc}
  based on \gls{ce:iso8859part1}. It differs from ISO 8859-1 by adding the
  \texttt{C0} \gls{ccode} codepoints in the \texttt{x00}--\texttt{x1F}
  range and also adds the \texttt{x7F} control code codepoint, as well as adding
  several printable characters commonly used in business and typography within
  the \texttt{x80}--\texttt{x9F} codepoint range.

  Windows-1252 does have \gls{backcompat} with \gls{ce:iso646} and is a safe
  Character Encoding to use when transmitting data over the Internet, though
  \gls{ce:utf8} is preferable for transmitting data over the Internet.

  Windows-1252 is the Character Encoding used when MariaDB is set up to use a
  \texttt{latin1} charset which is the default. This results in many users
  using this Character Encoding in MariaDB and then being rather puzzled when
  Emoji glyphs that are entered into the database do not work.

  If your MariaDB (or MySQL) database uses the \texttt{latin1} charset for its
  tables, it is recommended to change them to use \texttt{utf8mb4} instead.

  With \texttt{latin1}, there is virtually no cost in database size to update
  to \texttt{utf8mb4}. A few existing glyphs use more than one byte in UTF-8
  but the most commonly used glyphs do not. For XML/HTML content you may even
  save space if you convert existing entities to their corresponding glyph.

  \gls{swp} uses \texttt{utf8mb4} by default and will update
  existing \texttt{latin1} tables to \texttt{utf8mb4} by default for you.}
