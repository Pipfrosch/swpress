% php terms
\longnewglossaryentry{php:function}
{
  name=function,
  type=phpterms
}
  {This definition differs from how some use the word function in the context
   of the PHP program language.

   A function as I use it is a PHP function outside the context of a class that
   is defined external to the PHP file that calls it.

   Generally speaking, what I refer to as a function is either built-in to PHP
   itself or a binary module that loads when the PHP processor starts. It can
   also be defined by the programmer in one file and then used in another, but
   I \emph{personally} frown upon that style of coding, it breaks easily.}

\longnewglossaryentry{php:procedure}
{
  name=procedure,
  type=phpterms
}
  {This definition differs from how some use the word procedure in the context
   of the PHP program language.

   A procedure as I use it is a user defined function outside the context of a
   class that is only intended to be used within the PHP file where it is
   defined.

   Some define as a procedure as a \texttt{function} that does not return a
   value (actually returns \texttt{void}). I do not like that definition, as
   some functions only return a value under certain conditions.

   A procedure is defined using the PHP \texttt{function} keyword.}

\longnewglossaryentry{php:method}
{
  name=method,
  type=phpterms
}
  {This definition may differ from how some use the word method in the context
   of the PHP program language.

   A method as I use it is a function defined within a class. With the
   exception of static methods, the class must be instantiated as an object in
   order to use a method from that class.

   A method is defined using the PHP
   \texttt{function} keyword preceded by a visibility keyword
   (e.g.\ \texttt{public}).}

\longnewglossaryentry{php:smethod}
{
  name={static method},
  type=phpterms
}
  {A static method is a \gls{php:method} that can be called without instantiating
   the class.

   A static method is defined by the PHP keyword combination
   \texttt{static function} preceded by a visibility keyword
   (e.g.\ \texttt{public}).

   It is my \emph{opinion} that static methods do not belong in a class that
   can be instantiated.}

\longnewglossaryentry{php:sclass}
{
  name={static class},
  type=phpterms,
  plural={static classes}
}
  {A static class is a class that only contains \glspl{php:smethod}
  and static properties and has no constructor so the class can not be
  instantiated as an object.

  A static class only differs from a instantiatable class in that it does not
  have a constructor and only contains static methods and properties.}

\longnewglossaryentry{php:emuns}
{
  name={Emulated Namespace},
  type=phpterms
}
  {For many years, the PHP language lacked proper
  \myhref{https://php.net/manual/en/language.namespaces.definition.php}{namespaces}.
  They were not added to PHP until PHP 5.3.0.

  To compensate, many programmers would emulate namespaces by using an
  underscore as part of the actual class name. This was (and is) quite common,
  for example, with \gls{pear} packages.

  The underscore as part of the class name to emulate a namespace also allowed
  the web application developer to use
  \myhref{https://www.php.net/manual/en/function.spl-autoload-register.php}{spl\_autoload\_register}
  (available since PHP 5.1.0 before actual namespaces existed) to locate third
  party class files in the search path and load the needed class.

  In modern PHP web applications, emulating namespaces should generally be avoided.}

%other

\longnewglossaryentry{charenc}
{
  name={Character Encoding},
  type=mymain
}
  {A Character Encoding is the mapping of binary bytes to characters used
   within a document. Each character (including non-printable characters)
   is assigned a numeric codepoint within the Character Encoding so that
   the character can be interpreted and correctly displayed.

   Virtually all Character Encodings use the same codepoints for the
   printable ASCII range (\texttt{u0020}--\texttt{u007E}) but they often differ
   for characters beyond the ASCII range.}

\longnewglossaryentry{charset}
{
  name={Document Charset},
  type=mymain
}
  {A Document Charset is meta information about a document (such as a web page
   or an e-mail message in the context of the Internet) that specifies the
   \gls{charenc} used for the document.

   If the Document Charset is missing or is incorrect, the content may not
   display correctly.}

\longnewglossaryentry{dbcharset}
{
  name={Database Charset},
  type=mymain
}
  {Most web applications store content in a database. The Database Charset is
   the \gls{charenc} used by the database, database table, or database column
   containing the data being stored.

   If the Database Charset differs from the Document Charset for the document
   being generated from the data in the database, it is important to properly
   convert the data coming out of the database when creating the document.

   Generally the Database Charsets you can select from match standardized
   Character Encodings but there are sometimes differences. For example, with
   MariaDB the \texttt{utf8} charset only supports one to three byte \gls{ce:utf8}
   characters (which means one or two byte \glspl{ucodepoint}), you have to use
   the \texttt{utf8mb4} Database Charset to get full UTF-8 support.

   At this time, SW Press supports several different Database Charsets but will
   always convert the content to UTF-8 when generating the web page.}

\longnewglossaryentry{emoji}
{
  name=Emoji,
  type=mymain
}
{A glyph that uses a picture to convey a meaning or concept rather than a
    combination of letter glyphs to convey meaning.
    
    The majority of Emoji glyphs are fairly well understood independent of the
    language (if any) they are being used with.
    
    As a communication tool, they often can express context that is normally
    conveyed with facial expressions or vocal inflection rather than with words,
    but that is not always the case.

    With mobile platforms where screen space is often at a premium, Emojis can
    be a very important communication tool.}

\longnewglossaryentry{ucodepoint}
{
  name={Unicode Codepoint},
  type=mymain
}
  {A numerical value, usually but not not always in hexadecimal, that uniquely
   identifies a character in the standardized
   \myhref{https://unicode.org/ucd/}{Unicode Character Database}.

   To differentiate between a Unicode Codepoint and a Charset Codepoint, Unicode
   Codepoint references are usually precededed by a \texttt{u} or \texttt{U+}. For
   example, the \euro{} character can be referred to as \texttt{u20AC} or as
   \texttt{U+20AC} even though the actual codepoint used for the Euro in your
   particular charset may differ (e.g.\ it is \texttt{xA4} in \texttt{ISO-8859-15}
   and it is \texttt{x80} in \texttt{Windows-1252}).

   Unicode Codepoints are often used with XML (and HTML) as entities to allow
   use of character glyphs that is not supported by the document character set or
   are difficult to directly input.}

\longnewglossaryentry{xmlentity}
{
  name={XML Entity},
  type=mymain,
  plural={XML Entities}
}
  {An XML Entity is a method of representing any Unicode character using only
   ASCII characters. It starts with an \texttt{\&\#} and ends with a \texttt{;}.
   Between those two is either the Demimal Unicode Codepoint or the Hexadecimal
   Unicode Codepoint preceded by a lower-case \text{x}.

   For example, the Euro \euro{} can be represented as either \texttt{\&\#8364;}
   or as \texttt{\&\#x20AC;}.

   All XML Entities created using Unicode Codepoints also work in HTML.}

\longnewglossaryentry{namedxmlentity}
{
  name={XML Named Entity},
  type=mymain,
  plural={XML Named Entities}
}
  {In XML you can also defined easy to remember case sensitive names to use in
   an XML Entity instead of using the Unicode Codepoint. There are only five
   predefined named entities in XML:

   \begin{itemize}
      \item \texttt{\&amp;} produces an \texttt{\&} Ampersand.
      \item \texttt{\&lt;} produces a \texttt{<} `less than' bracket.
      \item \texttt{\&gt;} produces a \texttt{>} `greater than' bracket.
      \item \texttt{\&quot;} produces a straight double quotation mark.
      \item \texttt{\&apos;} produces a straight single quotation mark (straight apostraphe).
   \end{itemize}

   Other named entities are possible but must be defined in the XML document
   itself or in the DTD. It is better to just used Unicode Codepoints, they are
   always understood by the XML parser.}

\longnewglossaryentry{namedhtmlentity}
{
  name={HTML Named Entity},
  type=mymain,
  plural={HTML Named Entities}
}
  {In HTML, the W3C decided a number of Unicode characters were common enough
   that named entities were created for them, allowing them to be used when the
   document charset did not support the glyph or when the glyph is difficult to
   directly input into the document.

   Unfortunately, which named entities are defined depends upon the version of
   HTML declared and when HTML is converted to XML, none of them work except
   for the five that are also defined as \glspl{namedxmlentity}.

   Generally speaking, it is better not to use named entities, even in HTML.}

\longnewglossaryentry{webfont}
{
  name={Webfont},
  type=mymain
}
  {A font file served over the Internet for use on the web page that references
   the remote font.

   Webfonts are usually OpenType fonts that are compressed using the
   \myhref{https://www.w3.org/TR/WOFF2/}{WOFF2} file format, but historically
   several other formats were used.}

\longnewglossaryentry{camelCase}
{
  name={camelCase},
  type=mymain
}
  {A single word formed using ASCII letters (and occasionally decimal digits
   when absolutely necessary for clarity) by combining two or more independent
   words delineated by using an upper case letter for the first letter of
   each word \emph{except} for the first word.

   Generally, camelCase words only contain ASCII alpha-numeric characters and
   \emph{never} include an underscore or white space. When decimal digits are
   used, they are never the first character in a camelCase word.

   The word `camelCase' itself is an example.}

\longnewglossaryentry{PascalCase}
{
  name={PascalCase},
  type=mymain
}
  {A single word formed using ASCII letters (and occassionally decimal digits
   when absolutely necessary for clarity) by combining two or more independent
   words delineated by using an upper case letter for the first letter of each
   \emph{including} the first word.

   Generally, PascalCase words only contain ASCII alpha-numeric characters and
   \emph{never} include an underscore or white space. When decimal digits are
   used, they are never the first character in a PascalCase word.

   The word `PascalCase' itself is an example.}

\longnewglossaryentry{backcompat}
{
  name={Backwards Compatibility},
  type=mymain
}
  {The ability to interoperate with legacy software or with data created by
  a legacy system.

  For example, the Super Drive in Old World PowerMacs that supported high
  density floppies were backwards compatible with the floppies on earlier Macs.
  Even though these floppy drives did not need to vary their rotation speed for
  the 1.44 MB high density floppy diskettes, they maintained the ability to do
  so in order to read and write the 400k single density and 400k double density
  floppy diskettes used by earlier Macintosh computers.

  The USB floppy drives that people purchased when Apple switched to New World
  Macs that did not have a floppy drive however could not vary their rotation
  speed, so they were unable to read or write the 400k or 800k floppies used by
  legacy Macintosh systems, they were not Backwards Compatible.

  Similarly, \gls{ce:utf8} is backwards compatible with \gls{ce:iso646} (7-bit
  ASCII) becausse any 7-bit ASCII content can be correctly interpreted as
  UTF-8 content and a UTF-8 document that only uses characters in the
  \texttt{u0000}--\texttt{u007F} range can correctly be read by software that
  is expecting 7-bit ASCII.

  However, even though all the same character glyphs are supported,
  \gls{ce:utf16} does not retain backwards compatibility with 7-bit ASCII so
  data exchange must use a conversion tool.}

\longnewglossaryentry{ccode}
{
  name={Control Code},
  type=mymain
}
  {A control code is a codepoint that is not a printable glyph and is not
  intended to be interpteted as part of the text being encoded, but rather is
  intended to give a control command to the software interpreting the text
  being encoded, such as the \texttt{LF} Line Feed control code that telss a
  printer the current line has ended, advance the paper and move to the
  beginning of a new line.

  They are not actual characters nor do they modify characters. They are
  instructions for the device.

  Most control codes are obsolete in modern context but some are still used.}
  







\longnewglossaryentry{easteregg}
{
  name={Easter Egg},
  type=mymain
}
  {In the ancient Pagan traditions, there was a Goddess of fertility named
   \Eostre{}. Having a high reproductive output, Rabbits are often associated
   with fertility, so a Rabbit was often used as an animal symbol of this
   Goddess.

   When Christians invaded the Pagan lands, they came to realize that their
   prudish anti-sex attitudes were rather boring, the Pagans were having a lot
   more fun. This upset the Christians greatly, so they decided to change their
   holiday that was already a bastardization of the Jewish Passover holiday to
   incorporate this \Eostre{} Goddess and the Easter Bunny was created as a
   cultural appropriation of the Rabbit symbolizing the Goddess \Eostre{}.

   Eggs are also often a symbol of fertility, so part of the cultural
   appropriation is the idea that the Easter Bunny creates beautifully colored
   eggs that are hidden for children to find.}


