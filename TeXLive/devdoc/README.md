Developer Documentation
=======================

Currently what is here is majorly incomplete and very well may contain some
factual errors. Certainly has speiling errors.

The TeX files may be freely redistributed. However compiled versions of the
documentation may not be distributed in digital or print form without my
explicit permission.

It is my *hope* that sales of this documentation will help fund the project.

As the project nears completion, I hope to start some kind of a fundraiser to
pay for a thorough security audit. It would be deceptive to use the name
Sanitized in the project name when only my eyes have looked for security
issues.

Anyway access to digital version of the Developer Documentation is one of the
ways I hope to say Thank You to those who can contribute.

A printed version of the Developer Documentation, which will be quite large,
will also be for sale.

But anyone who has access to TeXLive can build the PDF by running the shell
script `buildDoc.sh` so people like me with no money, they can have the nice
PDF too.

Currently I'm primarily working on documenting the class files, and that will
take time, but the Developer Documentation will have far more than just that
when all is said and done.

Cheers.

Oh - I have not started it yet, but user documentation will also be written
in LaTeX and that will be redistributable, I only am restricting redistribution
of compiled / printed versions of the Developer Documentation.
