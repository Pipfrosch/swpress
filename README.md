Sanitized W###Press
===================

This is a fork of https://wordpress.org/ and will not be functional for some
time. The intent is to provide a modernized version that does not leak tracking
information and fixes some of the horrible flaws in WordPress core that the
upstream developers refuse to fix.

To avoid any trademark issues, please simply refer to this fork, should it ever
reach a usable state, as SWPress or SWP for Sanitized W###Press.

Use of this fork will require a modern version of PHP with some PHP modules
that are not required by upstream WP.

Right now this project is not functional. Do not use.

You have to make changes to the rest of WordPress to implement what is here,
eventually this will be a full fork but it is not yet.


PHP CLASSES
-----------

All classes in WordPress are being ported to either
[PSR-4](https://www.php-fig.org/psr/psr-4/) or in a few cases to
[PSR-0](https://www.php-fig.org/psr/psr-0/).

When a class is generic in nature and also used without a namespace in other
projects, I am *probably* using non-namespaced PSR-0 without a vendor. This is
because such classes often are also installed in a PEAR structure, and the
autoloader looks for classes in PEAR directories, so if a plugin or theme wants
to use the class, I want it to find the version of the class used here which
means no namespace.

When a class is not generic in scope or available as a namespaced class already
(e.g. PHPMailer) then PSR-4 classes are used.


FUNCTIONS
---------

Most (perhaps all) functions in WordPress are being put into non-namespaced
PSR-0 static classes, with the exception of `wp-include/pluggable.php` where
at least some of them are put into a namespaced PSR-4 class.

For backwards compatibility with existing plugins and themes, they will still
exist as functions but the functions will simply call the static method from
the class.

Putting the functions in a class as static methods makes it easier to run
static analysis tools on the classes that are being ported, and also gives me
the ability to fix some of them in a way that is not necessarily API compatible
with the original function, but keep the original function as an API compatible
wrapper to the modified static method in the class so that plugins and themes
still work.


Database Interface
------------------

For the database class, the WordPress database class was modified to require
the `mysqlnd` driver, which is appropriate since PHP 7.2+ is assumed.

I am currently working on creating a PHP interface that is database generic and
fully compatible with the existing database class, so that alternative database
classes that implement the interface will then "just work" (famous last words).

The plan is to create an alternate database class that uses the PDO driver but
the `mysqli`/`mysqlnd` class will remain for those who do not want to use PDO.

With the interface, it should be possible to create database classes that use
other databases as well (such as PostgreSQL)


Translations / Internationalization
-----------------------------------

I have not even really looked at how WordPress handles translations but I do
not like a few things I have noticed.

Strings are translated using a function called `__` - e.g. if you have the
string "I like to eat Apples" and you want it translated into a different
language you would use the function call `__(I like to eat Apples)` and if
a translation exists for the target language, it will be replaced with the
translation.

The translations seem to only be used for error messages.

That is all fine and dandy, but a function named `__` while it may seem quite
clever is just absolute bullshit.

Another thing I noticed that I did not like, there is a function called
`wp_load_translations_early()` which is called when an error happens early in
the loading of WordPress. The very existence of that function tells me it is
being done in a bad way.

The `__` function will have to always be there, plugins and themes use it.
What I hope to do however is create a PHP interface for translations so that
just like with my plan for the database, alternative implementations can exist
but as autoloading classes that load everything they need when called so that
there is no need for the `wp_load_translations_early()` function anymore and it
can just return `void` when called.

The `__` function will then just be a wrapper to call whatever class for
translations the system administrator has configured to use. Maybe there will
never be more than one class that implements the interface, but I find it hard
to believe that translations is a one size fits all scenario.


Code Cleanup
------------

Currently I am using the following tools to clean up the WordPress code:

* [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)
* [Vimeo Psalm](https://github.com/vimeo/psalm)

In the (hopefully near) future I will also be using:

* [PHPUnit](https://phpunit.de/)

The configuration files I am using for PHP CodeSniffer can be found in the file
`psr2.phpcs.xml` for most things and in `psr2-for-psr0-classes.phpcs.xml` for
the PSR-0 classes.

I use *mostly* PSR-2 coding standard, with some of the PEAR standard for the
[PHPDoc](https://en.wikipedia.org/wiki/PHPDoc) documentation blocks.

The configuration file I use for `psalm` can be found in the `psalm.xml`. Note
that some classes have lines within them that explicitly silence some warnings
and errors. Part of this is to deal with false positives, but in some cases it
is because I suspect major rewrite is needed to fix the issues.



